
# ESP32 Environment Setup

For doing the following environment setup you would need a computer running **Ubuntu 16.04 LTS**.

## [ESP32 Tool-chain Setup](https://docs.espressif.com/projects/esp-idf/en/latest/get-started-legacy/linux-setup.html#setup-linux-toolchain-add-it-to-path-legacy "Permalink to this headline")

### Install Prerequisites
To compile with ESP-IDF you need to get the following packages. Please run the below command in a terminal.

```
sudo apt-get install gcc git wget make libncurses-dev flex bison gperf python python-pip python-setuptools python-serial python-cryptography python-future python-pyparsing python-pyelftools
```

#### Toolchain Setup

Please follow the below steps to download and setup your toolchain:

#### 1. Download and Setup Toolchain
To setup ESP32 Toolchain for **Ubuntu 16.04 LTS**, run the following commands in a **Terminal**:

```
mkdir $HOME/esp
cd $HOME/esp
```

then copy toolchain setup script from `(release-package-path)/platform-resources/setup/setup-esp-toolchain.sh` to `$HOME/esp`

```
cp -v -u -r (release-package-path)/emmate/res/platform/esp/setup-esp-toolchain.sh $HOME/esp
./setup-esp-toolchain.sh
```

It will take some time to download and setup the toolchain.

#### 2. Set PATH Environment Variable
To use the toolchain, you will need to update your `PATH` environment variable in `~/.profile` file. To make `xtensa-esp32-elf` available for all terminal sessions, add the following line to your `~/.profile` file:

```
export PATH="$HOME/esp/xtensa-esp32-elf/bin:$PATH"
```

Alternatively, you may create an alias for the above command. This way you can get the toolchain only when you need it. To do this, add different line to your `~/.profile` file:

```
alias get_esp32='export PATH="$HOME/esp/xtensa-esp32-elf/bin:$PATH"'
```

Then when you need the toolchain you can type `get_esp32` on the command line and the toolchain will be added to your `PATH`.

#### 3. Make Changes Effective and Verify
Log off and log in back to make the `.profile` changes effective. Run the following command to verify if `PATH` is correctly set:

```
printenv PATH
```

You are looking for similar result containing toolchain’s path at the beginning of displayed string:
    
```
$ printenv PATH
/home/user-name/esp/xtensa-esp32-elf/bin:/home/user-name/bin:/home/user-name/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
```

Instead of `/home/user-name` there should be a home path specific to your installation.

## ESP-IDF (SDK) Setup
Besides the toolchain, you also need **ESP-IDF (SDK)**. Please follow the below steps to download and setup the SDK.

#### 1. Download and Setup ESP-IDF (SDK)
First you need to copy the SDK setup script from `(release-package-path)/platform-resources/setup/setup-esp-sdk.sh` to `$HOME/esp`. To do so run the following commands:

```
cd $HOME/esp
cp -v -u -r (release-package-path)/emmate/res/platform/esp/setup-esp-sdk.sh $HOME/esp
./setup-esp-sdk.sh
```

ESP-IDF SDK will be downloaded into `$HOME/esp/esp-idf`. This will take several minutes to complete, depending upon your internet connection.

#### 2. Set PATH Environment Variable
Set up `IDF_PATH` by adding the following line to `~/.profile` file:

```
export IDF_PATH=$HOME/esp/esp-idf
```

#### 3. Make Changes Effective and Verify
Log off and log in back to make this change effective.

>Note:
If you have `/bin/bash` set as login shell, and both `.bash_profile` and `.profile` exist, then update `.bash_profile` instead.

Run the following command to check if **`IDF_PATH`** is set:

```
printenv IDF_PATH
```

The path previously entered in `~/.profile` file should be printed out.



## [Install the Required Python2 Packages](https://docs.espressif.com/projects/esp-idf/en/latest/get-started-legacy/index.html#step-4-install-the-required-python-packages "Permalink to this headline")

The python2 packages required by ESP-SDK are located in `IDF_PATH/requirements.txt`. You can install them by running:

```
python2 -m pip install --user -r $IDF_PATH/requirements.txt

```

## Install the Required Python3 Packages
```
Kconfiglib: pip3 install --user kconfiglib

Python3 standard GUI Library(tkinter): sudo apt install python3-tk
```


