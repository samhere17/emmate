
#include "your_module.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	CORE_LOGI(TAG, "Calling your_module_init() in your_module.c in your-module directory ...");
	your_module_init();
	CORE_LOGI(TAG, "Returned from your_module_init()");

	while(1){
		CORE_LOGI(TAG, "Calling your_module_loop() in your_module.c in your-module directory ...");
		your_module_loop();
		CORE_LOGI(TAG, "Sleeping for %d ms before looping ...", DELAY_10_SEC);
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
