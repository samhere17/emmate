'''
Created on 19-Jul-2019

@author: iqubuntu02
'''
import argparse
import sys
import os
from kconfiglib import Kconfig

# use GUI Screen menuconfig
from guiconfig import menuconfig

# use Terminal Screen menuconfig
#from menuconfig import menuconfig

__version__="0.0.0"



def write_confH(config, filename):
    CONFIG_HEADING = """/*
* Automatically generated file. DO NOT EDIT.
* EmMate IoT Development Framework Project Configuration
*/
"""
    config.write_autoconf(filename, header=CONFIG_HEADING)
    pass


OUTPUT_FORMATS = {#"config": write_config,
                  "autoconf": write_confH,  # only used with make in order to generate auto.conf
                  #"header": write_header,
                  #"cmake": write_cmake,
                  #"docs": write_docs,
                  #"json": write_json,
                  #"json_menus": write_json_menus,
                  }

def _main():
    parser = argparse.ArgumentParser(description='menuconf_gen.py v%s - Config Generation Tool' % __version__, prog=os.path.basename(sys.argv[0]))

    parser.add_argument('--config',
                        help='Project configuration settings',
                        nargs='?',
                        default=None)

#     parser.add_argument('--defaults',
#                         help='Optional project defaults file, used if --config file doesn\'t exist. '
#                              'Multiple files can be specified using multiple --defaults arguments.',
#                         nargs='?',
#                         default=[],
#                         action='append')

    parser.add_argument('--kconfig',
                        help='KConfig file with config item definitions',
                        required=True)
 
    parser.add_argument('--output', nargs=2, action='append',
                        help='Write output file (format and output filename)',
                        metavar=('FORMAT', 'FILENAME'),
                        default=[])

#     parser.add_argument('--env', action='append', default=[],
#                         help='Environment to set when evaluating the config file', metavar='NAME=VAL')

    args = parser.parse_args()
    #print(args)
    kconf= Kconfig(os.path.abspath(args.kconfig))
    menuconfig(kconf, args.config)
    for output_type, filename in args.output:
        output_function = OUTPUT_FORMATS[output_type]
        output_function(kconf, filename)
    
    
    pass

if __name__ == '__main__':
    _main()
    pass
