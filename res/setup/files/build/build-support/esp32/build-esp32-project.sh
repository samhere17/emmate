#!/bin/bash

#echo Start Application Build....
cd $BIN_DIRECTORY/".$PROJECT_NAME"
#echo "PWD = $PWD"


#setting current directory
export CURR_DIR=`pwd`

#get CPU core count
export CPU_CORE=$(nproc)
echo Number of CPU core: $CPU_CORE


export ESP32_MAKE_LOG_FILE_PATH=./make.log
export ESP32_FLASH_CMD_FILE_PATH=./.flashcmd

export ESP32_BIN_PATH=$BIN_DIRECTORY/".$PROJECT_NAME"/build

export ESP32_BOOTLOADER_BIN_PATH=$ESP32_BIN_PATH/bootloader/bootloader.bin
export ESP32_PROJECT_BIN_PATH=$ESP32_BIN_PATH/$PROJECT_NAME.bin
export ESP32_OTA_DATA_BIN_PATH=$ESP32_BIN_PATH/ota_data_initial.bin
export ESP32_PARTITIONS_BIN_PATH=$ESP32_BIN_PATH/partitions.bin


## Script Functions ###############################################################################
execute_make_cmd(){
	echo
	echo "## Execute EmMate Make ################################"
	make -j$CPU_CORE | tee $ESP32_MAKE_LOG_FILE_PATH
	
	# get return status of piped cmds, 
	# form PIPESTATUS array, using the Index of CMDs called through pipe 
	local make_cmd_return=${PIPESTATUS[0]}
	if [ $make_cmd_return -eq 0 ]
	then
		#echo `tail -n 1 $ESP32_MAKE_LOG_FILE_PATH`
		local flash_cmd=`tail -n 1 $ESP32_MAKE_LOG_FILE_PATH`
		echo 
		echo "$flash_cmd">$ESP32_FLASH_CMD_FILE_PATH
		
		## save flash cmd with copying bin files path 
		#flash_cmd=${flash_cmd//"$ESP32_BIN_PATH/bootloader/"/""}
		#flash_cmd=${flash_cmd//"$ESP32_BIN_PATH/"/""}
		#echo $flash_cmd
		
		mkdir -v -p $BIN_DIRECTORY/binary >> $LOG_FILE
		
		##echo "$flash_cmd">$BIN_DIRECTORY/binary/flashcmd
		
		cp -v -p $ESP32_BOOTLOADER_BIN_PATH $BIN_DIRECTORY/binary >> $LOG_FILE
		cp -v -p $ESP32_PROJECT_BIN_PATH $BIN_DIRECTORY/binary >> $LOG_FILE
		cp -v -p $ESP32_OTA_DATA_BIN_PATH $BIN_DIRECTORY/binary >> $LOG_FILE
		cp -v -p $ESP32_PARTITIONS_BIN_PATH $BIN_DIRECTORY/binary >> $LOG_FILE
		
		echo "## Execute EmMate Make Successful ################################"
		echo
		exit $make_cmd_return
	else
		#echo "MAKE_CMD_RETURN = $make_cmd_return"
		echo "## Execute EmMate Make Failed ################################"
		echo
		exit $make_cmd_return
	fi
}

execute_make_clean_cmd(){
	echo
	echo "## Execute EmMate Clean ################################"
	make -j$CPU_CORE clean
	local make_clean_cmd_return=$?
	if [ $make_clean_cmd_return -eq 0 ]
	then
		echo "## Execute EmMate Clean Successful ################################"
		echo
		exit $make_clean_cmd_return
	else
		echo "## Execute EmMate Clean Failed ################################"
		echo
		exit $make_clean_cmd_return
	fi
}


execute_flash_cmd(){
	echo
	echo "## Execute EmMate Flash ################################"
	
	local flash_cmd=`tail -n 1 $ESP32_MAKE_LOG_FILE_PATH`
	$flash_cmd
	flash_cmd_return=$?
	if [ $flash_cmd_return -eq 0 ]
	then
		echo "## Execute EmMate Flash Successful ################################"
		echo
		exit $flash_cmd_return
	else
		#echo "FLASH_CMD_RETURN = $flash_cmd_return"
		echo "## Execute EmMate Flash Failed ################################"
		echo
		exit $flash_cmd_return
	fi
}

execute_erase_flash_cmd(){
	echo
	echo "## Execute EmMate Erase-Flash ################################"
	
	make -j$CPU_CORE erase_flash
	local erase_flash_cmd_return=$?
	if [ $erase_flash_cmd_return -eq 0 ]
	then
		echo "## Execute EmMate Erase-Flash Successful ################################"
		echo
		exit $erase_flash_cmd_return
	else
		#echo "FLASH_CMD_RETURN = $flash_cmd_return"
		echo "## Execute EmMate Erase-Flash Failed ################################"
		echo
		exit $erase_flash_cmd_return
	fi
}


execute_make_monitor_cmd(){
	echo
	echo "## Execute EmMate Log View ################################"
	
	make monitor
	local make_monitor_stat=$?
	
	if [ $make_monitor_stat -eq 0 ]
	then
		echo "## Execute EmMate Log View Complete ################################"
		echo
		exit $make_monitor_stat
	else
		echo "## Execute EmMate Log View Failed ################################"
		echo
		exit $make_monitor_stat
	fi
}




## Script Logics ###############################################################################

if [[ $1 == "-make" ]]
then
	
	execute_make_cmd
	
fi

#if [[ $1 == "-clean" ]]
#then
#	execute_make_clean_cmd
#fi

if [[ $1 == "-flash" ]]
then
	
	if [[ -f $ESP32_FLASH_CMD_FILE_PATH ]]
	then 
		execute_flash_cmd
		exit $?
	else
		execute_make_cmd
		MAKE_CMD_RETURN=$?
		if [ $make_cmd_return -eq 0 ]
		then
			execute_flash_cmd
			exit $?
		else
			exit $?
		fi
	fi
	
fi

if [[ $1 == "-erase-flash" ]]
then
	execute_erase_flash_cmd
fi

if [[ $1 == "-log" ]]
then
	
	execute_make_monitor_cmd
	#gnome-terminal -e "make monitor"
	
fi


