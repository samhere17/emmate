#!/bin/bash



#
#------------------------------------------------------------------------------------------------------------------------------
# To run this script's function pass the following arguments from command line
# 1. Project Source-Code Directory where platform-specific project has been created.
# 2. Project Specific Name
# 3. Project Library / Framework Directory where platform specific libraries / Framework have been copied.

# Example: " gen-espfolderstruct.sh project_path project_name project_SDK_path  "

#echo "Destination Path $1"
DESTINATION_PATH=$1
PROJECT_NAME=$2
PROJECT_SDK=$3

echo
echo
echo "### Creating ESP Application Project Structure #############################################"
echo "Creating ESP Application Project Structure for Project:	$PROJECT_NAME"

CURR_SCRIPT_DIR=$PROJECT_DIR/build/build-support/esp32

echo .....................................................................
echo -e "Creating $1/components Directory"
mkdir -p $DESTINATION_PATH/components
echo -e "Creating $1/main Directory"
mkdir -p $DESTINATION_PATH/main
echo .....................................................................
echo

echo .....................................................................
echo -e "Copying component.mk to $DESTINATION_PATH/main Directory"
componentMkPath=$CURR_SCRIPT_DIR/component.mk
cp -v -u $componentMkPath $DESTINATION_PATH/main
echo .....................................................................
echo

echo .....................................................................
echo -e "Copying sdkconfig.defaults to $DESTINATION_PATH Directory"
#echo $PWD
#echo /platform/esp32/sdkconfig
cp -v -u $CURR_SCRIPT_DIR/sdkconfig  $DESTINATION_PATH
echo .....................................................................
echo

echo .....................................................................
echo -e "Coying partitions.csv to $DESTINATION_PATH Directory"
#echo $PWD
#echo /platform/esp32/partitions.csv
cp -v -u $CURR_SCRIPT_DIR/partitions.csv  $DESTINATION_PATH
echo .....................................................................
echo

#if [ $PROJ_BUILD_MODE == "coredev" ]
#then
#	echo .....................................................................
#	echo -e "Copying Makefile to $DESTINATION_PATH"
#	cp -v -u $CURR_SCRIPT_DIR/Makefile  $DESTINATION_PATH
#else
	echo .....................................................................
	echo -e "Creating Makefile to $DESTINATION_PATH Directory"
	#MakeFile Contents
	project_name_content="\n\nPROJECT_NAME := $PROJECT_NAME"
	#echo -en $project_name_content
	
	if [ ! -z $PROJECT_SDK ]
	then
	idf_path=$(realpath $PROJECT_SDK)
	#echo $idf_path
	export_idf_path_content="\n\nifndef IDF_PATH \n export IDF_PATH := $idf_path\nendif"
	#echo $export_idf_path_content
	fi
	
	MAKEFILE_PATH="$1/Makefile"
	TEMP_MAKEFILE_PATH="$1/.Makefile.tmp"
	
	makeFile_include_content='\n\ninclude $(IDF_PATH)/make/project.mk'
	#echo $makeFile_include_content
	
	makefile_content="$project_name_content\n$export_idf_path_content\n$makeFile_include_content"
	#echo -en $makefile_content>"$1/Makefile"
	echo -en $makefile_content>$TEMP_MAKEFILE_PATH
	
	if [ -f $MAKEFILE_PATH ]
	then
		makefile_path_diff=$(diff $MAKEFILE_PATH $TEMP_MAKEFILE_PATH)
		echo $makefile_path_diff
		if [ -z $makefile_path_diff ]
		then
			# do nothing
			# : used for pass from this (if/else) state
			: 
		else
			#echo execute_component.mk file_check
			cat $TEMP_MAKEFILE_PATH > $MAKEFILE_PATH
		fi
	else
		cat $TEMP_MAKEFILE_PATH > $MAKEFILE_PATH
	fi
	
	
	# remove all temp files from current directory
	rm -fv $(find "$1/" -maxdepth 1 -mindepth 1 -name '*.tmp')
	
	
#fi

echo .....................................................................
echo

echo "#####################################################################"
echo


