#!/bin/bash

## Script Functions ###############################################################################

make_executable(){
	local script=$1
	if [ ! -x $script ]
	then
		echo "The file is not executable"
		#make is executable
		chmod a+x $script
	fi
}



## Main Script ###############################################################################

echo
echo
echo "### Copying Application & Core's All Source files #############################################"
echo -e "Copying ESP Component & Source Files into ESP Project structure $1....."
echo

DESTINATION_PATH=$1
PROJECT_PATH=$2

COMPONENTS_PATH=$DESTINATION_PATH/components
MAIN_PATH=$DESTINATION_PATH/main


export APP_SRC_PATH=$PROJECT_PATH/src

export CORE_SRC_PATH=$PROJECT_PATH/platform/core

export ESP_SRC_PATH=$PROJECT_PATH/platform/esp


echo .....................................................................
#Copy Core Source file to Project/main...
echo -e "Copying Application Source files to $MAIN_PATH..."
cp -v -u -p $CORE_SRC_PATH/*.* $MAIN_PATH
echo .....................................................................
echo

echo
echo

CSV_FILE_PATH="$CURR_DIR/build-cpy.csv"
#echo $CSV_FILE_PATH.....................

VAR_COUNT=1

INPUT=$CSV_FILE_PATH
OLDIFS=$IFS
IFS=,
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
while read var sta recur core_path esp32_path status
do
	#echo "VAR_COUNT : $VAR_COUNT"
	#VAR_COUNT=$((VAR_COUNT+1))
	
	if [[ $sta = "y" ]]
	then
		
#		echo "i : $i"
#		echo "VAR : $var"
#		echo "stat : $sta"
#		echo "recur : $recur"
#		echo "core-path : $path1"
#		echo "esp32-path : $path2"
#		
#		echo .$sta.
	
#		echo copying files

		#echo $core_path
		REV_STR=$(echo $core_path | rev)
		DIR_NAME=$(echo $(echo "$REV_STR" | sed 's/\eroc\/.*//') | rev)
		#echo $DIR_NAME

		TARGET_PATH=$COMPONENTS_PATH/$DIR_NAME/
		#echo ..TARGET_PATH= $TARGET_PATH
		if [[ ! -d $TARGET_PATH ]]
		then
			mkdir -p -v $TARGET_PATH
		else
			:
		fi
		
		# copy directory recursively
		if [[ $recur = "-r" ]]
		then
			
			echo .....................................................................
			echo -e "############# Copying  core/$(basename $core_path) to  $TARGET_PATH "
			if [ -d $core_path/ ]
			then
				for entry in $core_path/*
				do
					if [ $(basename $entry) != "Kconfig" ]
					then
						cp -v -u  -r $entry $TARGET_PATH/
					fi
				done
			fi
			
			echo
		
			echo -e "############# Copying  esp/$(basename $esp32_path) to  $TARGET_PATH "
			if [ -d $esp32_path/ ]
			then
				for entry in $esp32_path/*
				do
					if [ $(basename $entry) != "Kconfig" ]
					then
						cp -v -u  -r $entry $TARGET_PATH/
					fi
				done
			fi
			echo .....................................................................
			
		else
			echo .....................................................................
			echo -e "############# Copying  core/$(basename $core_path) to  $TARGET_PATH "
			if [ -d $core_path/ ]
			then
				for entry in $core_path/*
				do
					if [ -f $entry ] && [ $(basename $entry) != "Kconfig" ]
					then
					cp -v -u  $entry $TARGET_PATH
					fi
				done
			fi
			echo
		
			echo -e "############# Copying  esp/$(basename $esp32_path) to  $TARGET_PATH "
			if [ -d $esp32_path/ ]
			then
				for entry in $esp32_path/*
				do
					echo $entry
					if [ -f $entry ] && [ $(basename $entry) != "Kconfig" ]
					then
						cp -v -u  $entry $TARGET_PATH
					fi
				done

			fi
			echo .....................................................................
		fi
		
			

	fi
	echo 
	
done < $INPUT
IFS=$OLDIFS


echo copy extra files
echo -e "############# Start Copying  ESP's extra files"
for esp_dir in $ESP_SRC_PATH/*
do
	if [ -d $esp_dir ]
	then
		present_stat=0
		for core_dir in "$CORE_SRC_PATH"/*
		do
  			if [ -d $core_dir ]
			then
				if [[ $(basename $esp_dir) = $(basename $core_dir) ]]
				then
					#echo $(basename $esp_dir) = $(basename $core_dir) "====> continue"
					#echo
					present_stat=1
					break
				fi
			fi
		done
		if [ $present_stat -eq 1 ]
		then
			:
			#echo "$esp_dir present in core_src\"
			#echo
		else
			echo -e "############# Copying  $esp_dir to  $COMPONENTS_PATH "
			#echo "$esp_dir not present in core_src/"
			cp -v -u -r $esp_dir/ 	$COMPONENTS_PATH/
			echo
		fi
		
	fi
	
done
echo -e "############# End Copying  ESP's extra files"






#echo .....................................................................
##Copy Core Component Source file to Project/components...
#echo -e "Copy Core Source files to $COMPONENTS_PATH..."
#
#for entry in $CORE_SRC_PATH/*
#do 
#	if [ -d $entry ]
#	then
#		echo -e "############# Copying  core/$(basename $entry) to  $COMPONENTS_PATH "
#		cp -v -u -p -r $entry $COMPONENTS_PATH
#		echo
#		echo
#	fi
#done
#
#echo .....................................................................
#echo
#
#
#echo
#echo
#
#
#echo .....................................................................
##Copy ESP Component Source file to Project/components...
#echo -e "Copying ESP (Platform) Specific Source files to $COMPONENTS_PATH..."
#
#for entry in $ESP_SRC_PATH/*
#do 
#	if [ -d $entry ]
#	then
#		echo -e "############# Copying  esp/$(basename $entry) to  $COMPONENTS_PATH "
#		cp -v -u -p -r $entry $COMPONENTS_PATH
#		echo
#		echo
#	fi
#done
#echo .....................................................................
echo
echo
echo
echo
echo
echo
echo
echo .....................................................................
#Copy Application Component Source file to Project/components...
echo -e "Copy Application Component Source file to $COMPONENTS_PATH..."

cp -v -u -p $APP_SRC_PATH/*.* $COMPONENTS_PATH
echo
echo

for entry in $APP_SRC_PATH/*
do 
	if [ -d $entry ]
	then
		if [ $(basename $entry) = "version" ]
		then
			echo -e "############# Copying  src/$(basename $entry) to  $COMPONENTS_PATH "
			line_count=$(sed -n '$=' $COMPONENTS_PATH/version/version.h)
			if [ $line_count -gt 2 ]
			then
				#cat $entry/version.h >> $COMPONENTS_PATH/version/version.h
				
				# read data from $entry/version.h and set the curson at line no.3
				# after then delete line from line no.3 to end and replace the data and update into same file
				# sed command documentation url: http://www.grymoire.com/Unix/Sed.html#uh-37
				# example: code https://superuser.com/questions/713568/replace-files-content-between-specific-line-numbers-with-another-file-using-bas
				sed -i -e "3r $entry/version.h" -e '3,$d' $COMPONENTS_PATH/version/version.h
			else
				cat $entry/version.h >> $COMPONENTS_PATH/version/version.h
			fi
			
			cat $COMPONENTS_PATH/version/version.h
			echo
			echo
			continue
		fi
		echo -e "############# Copying  src/$(basename $entry) to  $COMPONENTS_PATH "
		cp -v -u -p -r $entry $COMPONENTS_PATH
		echo
		echo
	fi
done
echo .....................................................................
echo

echo
echo

echo .....................................................................



echo "#####################################################################"
echo



		
