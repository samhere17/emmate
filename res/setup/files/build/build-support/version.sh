#!/bin/bash

echo
echo "### Application Version #############################################"

#echo Reading version conf file...
export VER_CONF_FILE=$APP_SRC_PATH/version/app_version.conf

#reading the existing version numbers from conf file
source $VER_CONF_FILE
export $(grep --regexp ^[A-Z] $VER_CONF_FILE | cut -d= -f1)

export APP_VERSION_NUMBER=$APP_VERSION_MAJOR"."$APP_VERSION_MINOR"."$APP_VERSION_PATCH"."$APP_VERSION_HOTFIX
export APP_DEV_VERSION_NUMBER=$APP_VERSION_MAJOR"."$APP_VERSION_MINOR"."$APP_VERSION_PATCH"."$APP_VERSION_HOTFIX"."$APP_VERSION_BUILD_NUMBER

echo .....................................................................
echo Existing versions: $APP_VERSION_NUMBER
echo Existing Dev versions: $APP_DEV_VERSION_NUMBER
echo .....................................................................


echo
echo Generating new version number

#incrementing the build number by 1(one)
export NEW_APP_VERSION_BUILD_NUMBER=$(($APP_VERSION_BUILD_NUMBER+1))

export BUILD_VERSION_NUMBER=$APP_VERSION_NUMBER
export BUILD_DEV_VERSION_NUMBER=$APP_VERSION_MAJOR"."$APP_VERSION_MINOR"."$APP_VERSION_PATCH"."$APP_VERSION_HOTFIX"."$NEW_APP_VERSION_BUILD_NUMBER

echo .....................................................................
echo Build Version: $BUILD_VERSION_NUMBER
echo Build Dev Version: $BUILD_DEV_VERSION_NUMBER
echo .....................................................................
echo

#updating the version conf file
echo Updating the version conf file

#the below redirection must be one > because we want to create a new header file everytime
echo "# Last Updated: "$CURRENT_DATE $CURRENT_TIME >  $VER_CONF_FILE
echo "APP_VERSION_MAJOR="$APP_VERSION_MAJOR >>  $VER_CONF_FILE
echo "APP_VERSION_MINOR="$APP_VERSION_MINOR >>  $VER_CONF_FILE
echo "APP_VERSION_PATCH="$APP_VERSION_PATCH >>  $VER_CONF_FILE
echo "APP_VERSION_HOTFIX="$APP_VERSION_HOTFIX >>  $VER_CONF_FILE
#the build number will have the NEW build number
echo "APP_VERSION_BUILD_NUMBER="$NEW_APP_VERSION_BUILD_NUMBER >>  $VER_CONF_FILE


#generating the version header file
echo Generating the version header file

export VER_HEADER_FILE=$APP_SRC_PATH/version/version.h

#the below redirection must be one > because we want to create a new header file everytime
echo "#define APP_VERSION_NUMBER " '"'$BUILD_VERSION_NUMBER'"'  >  $VER_HEADER_FILE
echo -e "\n"
echo "#define APP_DEV_VERSION_NUMBER " '"'$BUILD_DEV_VERSION_NUMBER'"' >>  $VER_HEADER_FILE

echo "#####################################################################"
echo
