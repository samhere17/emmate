# EmMate Framework

The EmMate Framework is a multi-architecture, platform-independant middleware that can be used for any micro to small scale embedded application development.

The prime objective of the EmMate Framework is to provide an abstraction between an application and the hardware. The EmMate Framework provides an unified interface for application development, code compilation, application deployment and debugging for multiple embedded platforms.

The diagram below provides an overview of the concept of EmMate Framework.

![](emmate-block-diagram.png)

### Plans for EmMate Framework Development
The EmMate Framework plans to support the following platforms. More platforms to be added in future:
1. PIC32 from Microchip
2. ESP32 from Espressif
3. nRF51 and nRF52 from Nordic Semiconductor
4. STM32 from STMicroelectronics
5. Raspberry Pi

The EmMate Framework plans to support the following RTOS. More OS to be added in future:
1. FreeRTOS

### Current Developement
Currently the EmMate Framework supports the following platforms:
1. ESP32

Currently the EmMate Framework supports the following RTOS:
1. FreeRTOS