/*
 * system_urls.h
 *
 *  Created on: 12-Aug-2019
 *      Author: Rohan Dey
 */

#ifndef SYSTEM_URLS_H_
#define SYSTEM_URLS_H_

#ifdef __cplusplus
extern "C" {
#endif

#define _DEV_SERVER		0
#define _STAGE_SERVER	0
#define _PROD_SERVER	1

#if _DEV_SERVER
#define IQ_HOST							"192.168.0.104:8080"
#define IQ_HOST_PORT 					8080
#define IQ_SOM_REGISTRATION_POST_URL 		"http://"IQ_HOST"/mig/rest/som/register"
#define IQ_SYS_HWIDENTIFY_POST_URL			"http://"IQ_HOST"/mig/rest/somthing/identify"
#define IQ_SYS_HEARTBEAT_POST_URL			"http://"IQ_HOST"/mig/rest/somthing/heartbeat"
#define IQ_SYS_APPCONFIG_POST_URL			"http://"IQ_HOST"/mig/rest/somthing/get/configuration"
#define IQ_SYS_APPCONFIG_STATUS_POST_URL	"http://"IQ_HOST"/mig/rest/somthing/configuration/update/status"
#define IQ_SYS_FOTASTATUS_POST_URL			"http://"IQ_HOST"/mig/rest/somthing/fota/update/status"
#define IQ_SYS_APPPOSTDATA_POST_URL			"http://"IQ_HOST"/mig/rest/somthing/post/data"
//#define IQ_SYS_BIGDATA_POST_URL			"http://"IQ_HOST"/mig/rest/somthing/bigdata"
#define IQ_HOST_ROOTCA				NULL

#elif _STAGE_SERVER
#define IQ_HOST 			"dev.iquesters.com"			/*!< Core_embedded's HTTP Host name*/
#define IQ_HOST_PORT 		443												/*!< Core_embedded's HTTPs Port */
#define IQ_MAIN_GET_URL 	"https://"IQ_HOST"/emb_api/test_apis/ota.json"	/*!< Core_embedded's OTA url */
#define IQ_MAIN_POST_URL	"https://"IQ_HOST"/emb_api/hitandget.php"		/*!< Core_embedded's HTTPs post url */

#define IQ_SOM_REG_HOST 	"192.168.1.2:8080"
#define IQ_SOM_REGISTRATION_POST_URL 	"http://"IQ_SOM_REG_HOST"/mig/rest/som/register" /*!< SOM registration URL */
#define IQ_SYS_HWIDENTIFY_POST_URL		IQ_MAIN_POST_URL
#define IQ_SYS_HEARTBEAT_POST_URL		"https://"IQ_HOST"/emb_api/test_apis/api/heartbeat.php"

#define IQ_HOST_ROOTCA  "-----BEGIN CERTIFICATE-----\n\
MIIEkjCCA3qgAwIBAgIQCgFBQgAAAVOFc2oLheynCDANBgkqhkiG9w0BAQsFADA/\n\
MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT\n\
DkRTVCBSb290IENBIFgzMB4XDTE2MDMxNzE2NDA0NloXDTIxMDMxNzE2NDA0Nlow\n\
SjELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUxldCdzIEVuY3J5cHQxIzAhBgNVBAMT\n\
GkxldCdzIEVuY3J5cHQgQXV0aG9yaXR5IFgzMIIBIjANBgkqhkiG9w0BAQEFAAOC\n\
AQ8AMIIBCgKCAQEAnNMM8FrlLke3cl03g7NoYzDq1zUmGSXhvb418XCSL7e4S0EF\n\
q6meNQhY7LEqxGiHC6PjdeTm86dicbp5gWAf15Gan/PQeGdxyGkOlZHP/uaZ6WA8\n\
SMx+yk13EiSdRxta67nsHjcAHJyse6cF6s5K671B5TaYucv9bTyWaN8jKkKQDIZ0\n\
Z8h/pZq4UmEUEz9l6YKHy9v6Dlb2honzhT+Xhq+w3Brvaw2VFn3EK6BlspkENnWA\n\
a6xK8xuQSXgvopZPKiAlKQTGdMDQMc2PMTiVFrqoM7hD8bEfwzB/onkxEz0tNvjj\n\
/PIzark5McWvxI0NHWQWM6r6hCm21AvA2H3DkwIDAQABo4IBfTCCAXkwEgYDVR0T\n\
AQH/BAgwBgEB/wIBADAOBgNVHQ8BAf8EBAMCAYYwfwYIKwYBBQUHAQEEczBxMDIG\n\
CCsGAQUFBzABhiZodHRwOi8vaXNyZy50cnVzdGlkLm9jc3AuaWRlbnRydXN0LmNv\n\
bTA7BggrBgEFBQcwAoYvaHR0cDovL2FwcHMuaWRlbnRydXN0LmNvbS9yb290cy9k\n\
c3Ryb290Y2F4My5wN2MwHwYDVR0jBBgwFoAUxKexpHsscfrb4UuQdf/EFWCFiRAw\n\
VAYDVR0gBE0wSzAIBgZngQwBAgEwPwYLKwYBBAGC3xMBAQEwMDAuBggrBgEFBQcC\n\
ARYiaHR0cDovL2Nwcy5yb290LXgxLmxldHNlbmNyeXB0Lm9yZzA8BgNVHR8ENTAz\n\
MDGgL6AthitodHRwOi8vY3JsLmlkZW50cnVzdC5jb20vRFNUUk9PVENBWDNDUkwu\n\
Y3JsMB0GA1UdDgQWBBSoSmpjBH3duubRObemRWXv86jsoTANBgkqhkiG9w0BAQsF\n\
AAOCAQEA3TPXEfNjWDjdGBX7CVW+dla5cEilaUcne8IkCJLxWh9KEik3JHRRHGJo\n\
uM2VcGfl96S8TihRzZvoroed6ti6WqEBmtzw3Wodatg+VyOeph4EYpr/1wXKtx8/\n\
wApIvJSwtmVi4MFU5aMqrSDE6ea73Mj2tcMyo5jMd6jmeWUHK8so/joWUoHOUgwu\n\
X4Po1QYz+3dszkDqMp4fklxBwXRsW10KXzPMTZ+sOPAveyxindmjkW8lGy+QsRlG\n\
PfZ+G6Z6h7mjem0Y+iWlkYcV4PIWL1iwBi8saCbGS5jN2p8M+X+Q7UNKEkROb3N6\n\
KOqkqm57TH2H3eDJAkSnh6/DNFu0Qg==\n\
-----END CERTIFICATE-----"														/*!< Hostgator's RootCA Certificate*/

#elif _PROD_SERVER

#define IQ_HOST						"13.234.183.14:8443"	// AWS
#define IQ_HOST_PORT 				8443					// AWS
#define IQ_SOM_REGISTRATION_POST_URL 		"https://"IQ_HOST"/mig/rest/som/register"
#define IQ_SYS_HWIDENTIFY_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/identify"
#define IQ_SYS_HEARTBEAT_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/heartbeat"
#define IQ_SYS_APPCONFIG_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/get/configuration"
#define IQ_SYS_APPCONFIG_STATUS_POST_URL	"https://"IQ_HOST"/mig/rest/somthing/configuration/update/status"
#define IQ_SYS_FOTASTATUS_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/fota/update/status"
#define IQ_SYS_APPPOSTDATA_POST_URL			"https://"IQ_HOST"/mig/rest/somthing/post/data"
//#define IQ_SYS_BIGDATA_POST_URL			"http://"IQ_HOST"/mig/rest/somthing/bigdata"

#define IQ_HOST_ROOTCA				"-----BEGIN CERTIFICATE-----\n\
MIIDyTCCArGgAwIBAgIEPLwBCTANBgkqhkiG9w0BAQsFADCBlDELMAkGA1UEBhMC\n\
SU4xFDASBgNVBAgTC1dlc3QgQmVuZ2FsMRAwDgYDVQQHEwdLb2xrYXRhMR8wHQYD\n\
VQQKExZJcXVlc3RlciBTb2x1dGlvbnMgTExQMSQwIgYDVQQLExtJcXVlc3RlciBN\n\
aWNybyBJb1QgUGxhdGZvcm0xFjAUBgNVBAMTDTEzLjIzNC4xODMuMTQwHhcNMTkw\n\
ODE0MTIwNjQ1WhcNMTkxMTEyMTIwNjQ1WjCBlDELMAkGA1UEBhMCSU4xFDASBgNV\n\
BAgTC1dlc3QgQmVuZ2FsMRAwDgYDVQQHEwdLb2xrYXRhMR8wHQYDVQQKExZJcXVl\n\
c3RlciBTb2x1dGlvbnMgTExQMSQwIgYDVQQLExtJcXVlc3RlciBNaWNybyBJb1Qg\n\
UGxhdGZvcm0xFjAUBgNVBAMTDTEzLjIzNC4xODMuMTQwggEiMA0GCSqGSIb3DQEB\n\
AQUAA4IBDwAwggEKAoIBAQCaSuOUl5Xtdem0LUd0mpcroA+MqCFfe/p6ZvstCJfW\n\
LndPC1vrXrhn/JZIjYlZG/LsSuQypMQR9Dy4FWGt1WzkGnjs9PFgfFk/abcIX/Ur\n\
9v4BuvM3MkUlj6pxhzq7BHmlC8oO3PICW2ac4yeb6fXkLAZWmjWXrnWNYzbHDab3\n\
/SOB2qXEayzdLUiz60pWnIEXdpU4Z5FbREs7qeJDFGk6eGA0S+uK4l9qpe2CQD6u\n\
pMFCgyNLhW1DtKHYDeg8+LGl9F8DEE5dJeoFYe4tZj3S2uYBAUVGij+QkMlnIfwj\n\
4r+/OPC1zKZyZov+6QczKWWHViCrieT40ijwep4Qk9+lAgMBAAGjITAfMB0GA1Ud\n\
DgQWBBTVbpigwPxPkVAfifbsPp3LUOfdLzANBgkqhkiG9w0BAQsFAAOCAQEAMSH/\n\
Rb+C5G9Ac942tg8TTUD8qi2/Qx/IiTU3okuv59jH5Dh/66SYet1B6fyxS8M5iEW0\n\
qCKrss2NePPveug4DYXr8tdOYIv2Kp1xjPWSx6msnTvgHCeYvYsRFqJJ9+6b8J3O\n\
YZf0FFNDigyxycVg8jokM0ISAaFcxxwbCN/gxTiq5f2gR3iVeCMajKGWhV2sPuKa\n\
ok9SJdQsAOiABWeCUM8OQa9HpP2tOTv0F/xf61dlS+w3FYHOnsvtYblNwGwNlLG5\n\
3K3gcGpu0sAz9UebgimYms4m9KmuVF8flUDGJABZgFwwSCr//QUaPe5ukHIL28LZ\n\
AyzNH13Mm8UrphCg+g==\n\
-----END CERTIFICATE-----"

#endif


#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_URLS_H_ */
