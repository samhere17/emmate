/*
 * system_init.h
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef SYSTEM_INIT_H_
#define SYSTEM_INIT_H_

#include "core_error.h"

/**
 * @brief  	This function initializes the entire system by doing the following
 * 			1. Check if Startup Mode requires HMI. If yes then initialize HMI's module
 * 			2. Initialize the persistent memory module
 * 			3. Read saved operating mode from persistent memory if any, else start with device config mode
 * 			4. Select Operation Mode depending upon the configurations read
 * 				a. If Operation Mode = Device Configuration Mode, then select & initialize the required configuration peripheral (BLE/USB/UART)
 * 				b. If Operation Mode = NETWORK_CONNECTION_MODE, then select & initialize network interface
 * 			5. After network has been acquired, the function sets system time and checks for OTA update
 * 			6. If OTA update is required, the OTA update process is started and the user application main task is started
 * 			7. If OTA update is not required, then the user application main task is started
 *
 *
 * @param[in]	sys_data Pointer to SystemData
 *
 * @return
 * 		- CORE_OK 		on success
 * 		- CORE_FAIL 	on failure
 */
core_err init_system(SystemData *sys_data);

/**
 * @brief  	Performs a Power On Self Test
 *
 * @return
 * 		- CORE_OK 		on success
 * 		- CORE_FAIL 	on failure
 */
core_err perform_post();

#endif /* SYSTEM_INIT_H_ */
