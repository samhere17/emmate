/*
 * File Name: sysconfig_helper.h
 * File Path: /emmate/src/system/sysconfig/sysconfig_helper.h
 * Description:
 *
 *  Created on: 12-May-2019
 *      Author: Rohan Dey
 */

#ifndef SYSCONFIG_HELPER_H_
#define SYSCONFIG_HELPER_H_

#include "core_config.h"
#include "core_common.h"
#include "core_error.h"
#include "gll.h"

typedef enum {
	SYSCONFIG_FREE = 0,	/**< When the System Configuration module is not in use */
	SYSCONFIG_BUSY		/**< When the System Configuration module is in use */
} SYSCONFIG_STATUS;

typedef struct {
	bool mem_stat;		/**< Status to check whether memory has been allocated or not */
	void* data;			/**< Data buffer */
} ActualData;

/**
 * @brief  	The SysconfigData structure contains only the metadata of a configuration.
 *
 * Steps taken to do a system configuration are:
 * 1. 	First the user sends the metadata (in JSON format terminated by \r\n) which consists an identifier, i.e. key
 * 		and the length of the actual data, i.e. value.
 * 2.	The purpose of using the metadata dataset before sending the actual data is to evaluate the total size of the
 * 		byte stream and each identifier's data length before hand. This will be used to allocate appropriate amount of memory
 * 3.	Finally the user sends the whole byte stream and the configurator parses it by each identifier's length and
 * 		copies the data in the appropriate location
 */
typedef struct {
	char *key;				/**< Data identifier */
	int value;				/**< Actual data's length */
	ActualData act_data;	/**< The structure to hold the actual data */
} SysconfigData;

/**
 * @brief 	This function returns the current status of the System Configuration module
 *
 * @return
 * 				- SYSCONFIG_STATUS	Refer SYSCONFIG_STATUS enum for the values
 * */
SYSCONFIG_STATUS get_sysconfig_stat();

/**
 * @brief 	This function initializes the System Configuration module by taking a list of 'keys' as function agruments.
 *
 * 			Each key will have a corresponding 'value' which has to be added later on.
 * 			Here, 'value' does not represent the actual value, but it represents the length of the data which the key
 * 			will finally hold.
 *
 * @note	Refer to the structure SysconfigData and its documentation for understanding this.
 * 			At a time only one instance of this module can run. Hence check the status before using this module by
 * 			calling get_sysconfig_stat()
 *
 * @param[in]	key_count	The number of keys the user is sending to be listed
 *
 * @return
 * 				- CORE_OK			If the listing of keys is successful
 * 				- CORE_ERR_NO_MEM	Not enough memory
 * 				- CORE_FAIL			If the module is already in use
 * */
core_err init_sysconfig(int key_count, ...);

/**
 * @brief 	This function is used to update any 'key' in an already existing SysconfigData list.
 *
 * @note	init_sysconfig() must be called before calling this function
 *
 * @param[in]	oldkey	Existing key in the list
 * @param[in]	newkey	New key to replace the old one
 *
 * @return
 * 				- CORE_OK	If key was successfully replaced
 * 				- CORE_FAIL	If the key could not be found
 * */
core_err update_sysconfig_metakey(char *oldkey, char *newkey);

/**
 * @brief 	This function is used to update any 'value' against a specific key. The values are initialized by 0.
 *
 * @note	init_sysconfig() must be called before calling this function
 *
 * @param[in]	oldkey	Existing key in the list
 * @param[in]	newkey	New value to replace the old one
 *
 * @return
 * 				- CORE_OK	If value was successfully replaced
 * 				- CORE_FAIL	If the key could not be found
 * */
core_err update_sysconfig_metavalue(char *oldkey, int newvalue);

/**
 * @brief 	This function prints out the entire SysconfigData list
 *
 * @note	init_sysconfig() must be called before calling this function
 * */
void print_sysconfig_list();

/**
 * @brief 	This function deinitializes the System Configuration module and deletes the SysconfigData list
 *
 * */
void deinit_sysconfig();


#endif /* SYSCONFIG_HELPER_H_ */
