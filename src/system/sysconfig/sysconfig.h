/*
 * File Name: sysconfig.h
 * File Path: /emmate/src/system/sysconfig/sysconfig.h
 * Description:
 *
 *  Created on: 12-May-2019
 *      Author: Rohan Dey
 */

#ifndef SYSCONFIG_H_
#define SYSCONFIG_H_

#include "core_config.h"
#include "core_common.h"
#include "core_error.h"
#include "core_constant.h"
#include "wifi.h"
#include "sysconfig_helper.h"


typedef struct {
	char devid[DEV_ID_LEN];			/**< Unique Device ID. Could be MAC ID*/
	uint16_t devid_len;				/**< Length of Device ID*/
#if CONFIG_USE_WIFI
	WifiConfig wifi_cfg;			/**< Structure to contain Wi-Fi configurations */
#endif
} SystemConfig;

#endif /* SYSCONFIG_H_ */
