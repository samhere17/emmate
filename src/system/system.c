/*
 * system.c
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */

#include <string.h>
#include "core_config.h"
#include "system.h"
#include "core_utils.h"
#if CONFIG_USE_OTA
#include "fota_core.h"
#endif
#include "core_system_info.h"
#include "system_init.h"
#include "system_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "board_ids.h"
#include "module_thread_priorities.h"

#define TAG LTAG_SYSTEM

/* Globals */
SystemData m_sys_data;

static void set_sysdata_to_default(SystemData *sysdata) {
	memset(sysdata, 0, sizeof(SystemData));
	sysdata->oper_mode = STARTUP;
#if CONFIG_USE_DEVCONFIG
	set_devcfg_to_default(&sysdata->dev_cfg);
#endif
}

void start_system() {
	core_err ret;

	/* Initialize the systems logger */
	init_core_logger((CORE_LOG_OUTPUT_CONSOLE | CORE_LOG_OUTPUT_FILE | CORE_LOG_OUTPUT_NETWORK), CORE_LOG_INFO);

	/* Set system log levels */
	set_all_log_levels_at_init();

	/* Read and save the hardware specific information */
	read_save_system_info();

	/* Print the acquired system info */
	print_system_info();

	/* Initialize the underlying hardware */
	set_sysdata_to_default(&m_sys_data);

	/* Initialize the system level event group */
	m_sys_data.sys_evt_hdl = event_group_create();
	if (m_sys_data.sys_evt_hdl == NULL) {
		CORE_LOGE(TAG, "Error: System Initialization failed! Rebooting!");
		/* Reboot the system and try again */
		core_system_restart();
	}

	/* Initialize the system */
	ret = init_system(&m_sys_data);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Error: System Initialization failed! Rebooting!");
		/* Reboot the system and try again */
		core_system_restart();
	}

	/* Register the factory reset button */
#if (CONFIG_HAVE_SYSTEM_HMI & CONFIG_USE_BUTTON)
	register_factory_reset_button();
#endif

	/* Perform a Power On Self Test */
	perform_post();
}

/**
 * @brief Start the User Application
 *
 * Create a thread to start the User Application.
 *
 */
void start_application() {
	/* Create a thread to perform the notifications */
	BaseType thread_stat;
	thread_stat = TaskCreate(core_app_main, "emmate-app", TASK_STACK_SIZE_8K, NULL, THREAD_PRIORITY_EMMATE_APP, NULL);
	if (thread_stat == false) {
		CORE_LOGE(TAG, "Failed to <EmMate App> create thread!");
		printf("Rebooting\r\n");
		/* Reboot the system and try again */
		core_system_restart();
	}
	/* System Started Successfully, show LED notification */
//	show_system_running_notification();
}
