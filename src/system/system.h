/*
 * system.h
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_logger.h"
#include "core_config.h"
#include "core_common.h"
#if CONFIG_USE_EVENT_GROUP
#include "event_group_core.h"
#endif
#if CONFIG_USE_DEVCONFIG
#include "device_config.h"
#endif
#include "threading.h"

// System Event Bits
#define	DEVCFG_DATA_VALIDATION_BIT		EVT_BIT0		/*!< Device Config Data Validation wait bit */
#define CONN_CONNECTING_BIT				EVT_BIT1		/*!< Network Connecting wait bit */
#define CONN_GOT_IP_BIT					EVT_BIT2		/*!< Network Connection success wait bit */
#define CONN_GOT_IP_FAILED				EVT_BIT3		/*!< Network Connection Failed wait bit */
#define CONN_CONFIG_FAILED_BIT			EVT_BIT4		/*!< Network Connection Configuration Failed wait bit */
#define	DEVCFG_COMPLETE_BIT				EVT_BIT5		/*!< Device Config Completion wait bit */
#define HTTP_RESPONSE_RECV_COMPLETE		EVT_BIT6		/*!< HTTP Response receive wait bit */
#define START_SYSHEARTBEAT_BIT			EVT_BIT7		/*!< System Heartbeat module should wait on this bit and then start */
// Add more when needed

typedef enum {
	STARTUP = 200,					/*!< System StartUp Operation mode */
	SYSTEM_CONFIG_MODE_FACTORY,			/*!< System Lv1 Configuration Operation mode */
	NETWORK_CONNECTION_MODE,		/*!< System Network Connection Operation mode */
	VERIFY_RUNNING_FIRMWARE,
	SETUP_SYSTIME_SNTP,				/*!< Mode to setup system time via SNTP */
	SOM_REGISTRATION_MODE,			/*!< SOM Registration Mode */
	HARDWARE_IDENTIFICATION,		/*!< Hardware Identification Mode */
	SYSTEM_HEARTBEAT,				/*!< Mode to start the System Heartbeat process */
	START_APPLICATION,					/*!< System Working Operation mode */
	MAINTENANCE_MODE,				/*!< System Maintenance Operation mode */
	OTA_UPDATE_MODE,				/*!< System OTA Update Operation mode */
	SYS_DO_NOTHING					/*!< Mode where all error handling has been tried and nothing more can be done */
} SYSTEM_OPERATION_MODES;


typedef struct {
	SYSTEM_OPERATION_MODES oper_mode;		/*!< System Data variable for Operation Control */
#if CONFIG_USE_DEVCONFIG
	DeviceConfig dev_cfg;					/*!< System Data variable for Device Config */
#endif
	EventGrpHandle sys_evt_hdl;				/*!< System Data variable for Group Event Control */
} SystemData;

/*
 * @brief This function is used to set the log level for the entire system
 * */
void set_system_log_levels();

/**
 * @brief This is the starting point of the EmMate Framework.
 * Here, the following tasks are done:
 * 	- Verify the booted image
 * 	- Initialize the logger
 * 	- Read hardware specific information from the chip
 * 	- Read the framework and application information, i.e. version numbers
 * 	- Print the acquired system info, i.e. hardware and software
 * 	- Initialize the system level event group
 * 	- Initialize the system peripherals. See the function init_system() for further details
 * 	- Register the system level Reset Button
 *	- Perform a Power On Self Test
 */
void start_system();

/**
 * @brief Start the User Application
 *
 * Create a thread to start the User Application.
 *
 */
void start_application();

/**
 * @brief This is the user application's entry point. It is called from the core framework as a thread
 *
 * @return
 *
 **/
void core_app_main(void * param);

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_H_ */
