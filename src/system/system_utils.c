/*
 * File Name: system_utils.c
 * File Path: /emmate/src/system/system_utils.c
 * Description:
 *
 *  Created on: 28-Apr-2019
 *      Author: Rohan Dey
 */
#include <string.h>
#include <stdlib.h>

#include "system_utils.h"
#include "threading.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "thing.h"
#if CONFIG_USE_HMI
#include "hmi.h"
#include "system_hmi_led_notification.h"
#include "system_hmi_button.h"
#endif
#include "system_platform.h"
#if CONFIG_USE_PERSISTENT
#include "persistent_mem.h"
#endif

#define TAG LTAG_SYSTEM

extern SystemData m_sys_data;
static factoryreset_function m_factoryreset_func = NULL;
static systemreboot_function m_systemreboot_func = NULL;

EventGrpHandle get_system_evtgrp_hdl() {
	return m_sys_data.sys_evt_hdl;
}

/****************************************** SYSTEM REBOOT RELATED ******************************************/
void notify_system_reboot_event() {
	if (m_systemreboot_func != NULL)
		m_systemreboot_func();
}

core_err register_system_reboot_event_function(systemreboot_function systemreboot_func) {
	if (systemreboot_func != NULL) {
		m_systemreboot_func = systemreboot_func;
		return CORE_OK;
	} else {
		return CORE_ERR_INVALID_ARG;
	}
}

void core_system_restart() {
	printf("Rebooting in ...\r\n");
	for (int i = 3; i > 0; i--) {
		printf("%d\r\n", i);
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
	}
	core_platform_restart();
}

/****************************************** FACTORY RESET RELATED ******************************************/
#if (CONFIG_HAVE_SYSTEM_HMI & CONFIG_USE_BUTTON)
static void reset_button_handler(uint32_t press_mode) {
	if (press_mode == LONG_PRESSED) {
		notify_factory_reset_event();
		show_system_resetting_notification();
		TaskDelay(DELAY_3_SEC / TICK_RATE_TO_MS);

		notify_system_reboot_event();
#if CONFIG_USE_PERSISTENT
		CORE_LOGW(TAG, "Factory Reset Button Pressed! Erasing the persistent memory ...");
		core_err ret = erase_configs_from_persistent_mem();
//		core_err ret = reset_persistent_mem_for_reconfig();
		if (CORE_OK != ret) {
			CORE_LOGE(TAG, "Failed to erase persistent memory, factory reset is not successful");
			return;
		}
#endif
		CORE_LOGW(TAG, "Dispatching reboot event ...");
		core_system_restart();
	}
}

void register_factory_reset_button() {
	/* Register the Reset Button Callback Handler */
	register_hmi_reset_button_cb(reset_button_handler);
}
#endif	/* (CONFIG_HAVE_SYSTEM_HMI & CONFIG_USE_BUTTON) */

void notify_factory_reset_event() {
	if (m_factoryreset_func != NULL)
		m_factoryreset_func();
}

core_err register_factory_reset_event_function(factoryreset_function factoryreset_func) {
	if (factoryreset_func != NULL) {
		m_factoryreset_func = factoryreset_func;
		return CORE_OK;
	} else {
		return CORE_ERR_INVALID_ARG;
	}
}

/**************************************************** TASK DEBUG RELATED ****************************************************/
#if CONFIG_SHOW_THREAD_DEBUG
// Testing Block used for Get current Running Task list
void getTaskList(void* pvParameters) {
	char ptrTaskList[1024 * 2] = { 0 };
	while (1) {
		bzero(ptrTaskList, 1024 * 2);
		vTaskList(ptrTaskList);
		printf("********************************************************************\r\n");
		printf("Task\t  State\t   Prio\t    Stack\t    Num\r\n");
		printf("********************************************************************\r\n");
		printf("%s\r\n", ptrTaskList);
		printf("********************************************************************\r\n");
		printf("Stack remaining for task '%s' is %d bytes, ptrTaskList length=%d\r\n", TaskGetTaskName(NULL),
				TaskGetStackHighWaterMark(NULL), strlen(ptrTaskList));

		printf("Free Heap Memory: %d\r\n", heap_caps_get_free_size(MALLOC_CAP_8BIT));
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);

	}
}
void start_sysTaskList_task() {
	TaskCreate(getTaskList, "getTaskList", TASK_STACK_SIZE_4K, NULL, THREAD_PRIORITY_10, NULL);
}
#endif	/* CONFIG_SHOW_THREAD_DEBUG */
