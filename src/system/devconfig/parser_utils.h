/*
 * parser_utils.h
 *
 *	File Path: /emmate/src/system/devconfig/parser_utils.h
 *
 *	Project Name: EmMate
 *	
 *  Created on: 17-Apr-2019
 *
 *      Author: Noyel Seth
 */

#ifndef SRC_SYSTEM_DEVCONFIG_PARSER_UTILS_H_
#define SRC_SYSTEM_DEVCONFIG_PARSER_UTILS_H_

#include "device_config.h"
#include "threading.h"

core_err parser_cnf_data(QueueHandle *recv_queue, DeviceConfig *devcfg, char* ble_metabuff,uint16_t sizeof_ble_metabuff,char* ble_authId);

#endif /* SRC_SYSTEM_DEVCONFIG_PARSER_UTILS_H_ */
