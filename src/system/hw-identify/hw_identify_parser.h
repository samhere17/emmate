/*
 * hw_identify_parser.h
 *
 *  Created on: 29-Jul-2019
 *      Author: Rohan Dey
 */

#ifndef HW_IDENTIFY_PARSER_H_
#define HW_IDENTIFY_PARSER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

#include "hw_identify.h"



/**
 *
 */
core_err make_hwid_json(char **ppbuf, int *plen, HWIdentificationRequest *hwid_req);

/**
 *
 */
core_err parse_hwid_response_json(char *json_buff, HWIdentificationResponse *hwid_resp);

/**
 *
 */
void print_hwid_response(HWIdentificationResponse *hwid_resp);

#ifdef __cplusplus
}
#endif

#endif /* HW_IDENTIFY_PARSER_H_ */
