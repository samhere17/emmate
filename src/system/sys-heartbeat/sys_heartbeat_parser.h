/*
 * File Name: sys_heartbeat_parser.h
 * File Path: /emmate/src/system/sys-heartbeat/sys_heartbeat_parser.h
 * Description:
 *
 *  Created on: 29-May-2019
 *      Author: Rohan Dey
 */

#ifndef SYS_HEARTBEAT_PARSER_H_
#define SYS_HEARTBEAT_PARSER_H_

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "sys_heartbeat.h"

/**
 * */
core_err make_sys_hb_request_json(char **ppbuf, int *plen, SysHeartbeatRequest *hb_req);

/**
 * */
core_err parse_sys_hb_response_json(char *json_buff, SysHeartbeatResponse *hb_resp);

/**
 * */
void print_sys_hb_response(SysHeartbeatResponse *regn_resp);

#endif /* SYS_HEARTBEAT_PARSER_H_ */
