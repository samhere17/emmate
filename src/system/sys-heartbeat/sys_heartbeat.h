/*
 * File Name: sys_heartbeat.h
 * File Path: /emmate/src/system/sys-heartbeat/sys_heartbeat.h
 * Description:
 *
 *  Created on: 29-May-2019
 *      Author: Rohan Dey
 */

#ifndef SYS_HEARTBEAT_H_
#define SYS_HEARTBEAT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#if CONFIG_USE_OTA
#include "fota_core.h"
#endif
#include "version.h"

#define SYS_HB_INTERVAL_SEC		(CONFIG_SYSTEM_HEARTBEAT_INTERVAL)
#define SYS_HB_MIN_INTERVAL		(10)

typedef enum {
	SYSHB_STATE_NO_SOMID = 0,
	SYSHB_STATE_HAVE_SOMID,
} SYSHB_STATES;

/**
 * @brief	Data structure to contain a SYS Heartbeat's POST request data to be sent to the server
 *
 * @note	SYS Heartbeat request JSON structure:
 *	{
 *		"somthing_id": "",
 *		"core_version": "",
 *		"app_version": ""
 *	}
 *
 * */
typedef struct {
	char somthing_id[SOMTHING_ID_LEN+1];			/**< A System On Module (SOM) unique identifier. To be fetched from a hardwired silicon UID */
	char core_version[CORE_VERSION_NUMBER_LEN+1];	/**< Core Framework's version number */
	char app_version[APP_VERSION_NUMBER_LEN+1];		/**< Core Application's version number */
} SysHeartbeatRequest;

/**
 * @brief	Data structure to contain a SYS Heartbeat's response from the server
 *
 * @note	SYS Heartbeat response JSON structure:
 *
 *	{
 *		"status": true,
 * 		"error": {
 * 			"err_code": 123,
 * 			"err_msg": "This is an error!"
 * 		},
 * 		"sysconfig": {
 *			"freq": 123456,						// integer - heartbeat frequency (in secs)
 *			"standby": false,					// bool - put system in standby or running mode
 *			"standby_freq": 12345678,			// integer - heartbeat frequency when standby (in secs)
 * 		},
 * 		"fota": {
 * 			"id": "",						// Fota ID: all fota status activities to be made using this id
 *			"stat": true,
 *			"url_len": 62,
 *			"url": "https://dev.iquesters.com/emb_api/test_pis/upload/firmware.hex",
 *			"cert_len": 0,
 *			"cert": null,
 *			"ver": "0.0.0.1",
 *			"fname": "firmware.hex",
 *			"fsize": 18410,
 *			"sch": "1559735400"
 *		}
 *		"conf": true
 *	}
 *
 * */
typedef struct {
	double freq;					/**< heartbeat frequency  (in secs) */
	int standby;					/**< put system in standby or running mode */
	double standby_freq;			/**< heartbeat frequency when in standby  (in secs) */
} RemoteSystemConfig;

typedef struct {
	int status;						/**< Sys Heartbeat Status. Success or Failure */
	CoreError error;				/**< Error object */
	RemoteSystemConfig sysconfig;	/**< System configuration object  */
#if CONFIG_USE_OTA
	FotaInfo fota;					/**< FOTA Info */
#endif
	int conf;						/**< check if application has app specific configurations */
} SysHeartbeatResponse;

/**
 * @brief	This function starts the system's heartbeat process.
 *
 * 			A heartbeat is a process where a device hits the server at a fixed time interval to let the server know
 * 			that it is alive.
 *
 * 			This function internally starts a low priority thread/task which loops at a fixed interval which can be
 * 			configured using the macro CONFIG_SYSTEM_HEARTBEAT_INTERVAL.
 *
 * 			In this heartbeat process the system informs the following informations to the server:
 * 			1. The device's SOM ID
 * 			2. The running core and application version numbers
 * 			3. Basic system diagnostics report
 *
 * 			As a response from the server, the system receives the following informations:
 * 			1. The status of this post with any error
 * 			2. The FOTA information
 *
 * 	@return
 * 			- CORE_OK	If the heartbeat was started successfully
 * 			- CORE_FAIL	If failure
 * */
core_err start_system_heartbeat();

/**
 * @brief		This function sets the system's heartbeat interval. The input parameter must be in seconds and greater than 10
 *
 * @param[in]	interval_sec The interval in seconds. Valid values: interval_sec must be greater than 10
 *
 * @return
 * 			- CORE_OK					If the value was set successfully
 * 			- CORE_ERR_INVALID_ARG		If interval_sec is less than 10
 * */
//core_err set_sysheartbeat_interval(uint32_t interval_sec);

#ifdef __cplusplus
}
#endif

#endif /* SYS_HEARTBEAT_H_ */
