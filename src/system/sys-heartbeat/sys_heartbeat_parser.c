/*
 * File Name: sys_heartbeat_parser.c
 * File Path: /emmate/src/system/sys-heartbeat/sys_heartbeat_parser.c
 * Description:
 *
 *  Created on: 29-May-2019
 *      Author: Rohan Dey
 */

#include "sys_heartbeat_parser.h"
#include "input_processor.h"
#include "inpproc_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "systime.h"
#include "core_utils.h"
#include <string.h>

#define TAG	LTAG_SYSHB

core_err make_sys_hb_request_json(char **ppbuf, int *plen, SysHeartbeatRequest *hb_req) {
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);
	char *serialized_string = NULL;

	CORE_LOGD(TAG, "Going to make SYS Heartbeat JSON with the following values:");

	/* Set JSON key value */
	CORE_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(hb_req->somthing_id, "->"), hb_req->somthing_id);
	json_object_set_string(root_object, GET_VAR_NAME(hb_req->somthing_id, "->"), hb_req->somthing_id);

	CORE_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(hb_req->core_version, "->"), hb_req->core_version);
	json_object_set_string(root_object, GET_VAR_NAME(hb_req->core_version, "->"), hb_req->core_version);

	CORE_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(hb_req->app_version, "->"), hb_req->app_version);
	json_object_set_string(root_object, GET_VAR_NAME(hb_req->app_version, "->"), hb_req->app_version);

	serialized_string = json_serialize_to_string(root_value);

	size_t len = json_serialization_size(root_value);
	len = len - 1;  // since json_serialization_size returns size + 1
	CORE_LOGD(TAG, "SYS Heartbeat Request JSON Len = %d\r\n", len);

	char *ptemp = (char*) malloc(len);
	if (ptemp == NULL) {
		CORE_LOGE(TAG, "make_sys_hb_request_json malloc failed!");
		return CORE_FAIL;
	}
	memset(ptemp, 0x00, len);
	memcpy(ptemp, serialized_string, len);
	*plen = len;
	*ppbuf = ptemp;

	json_value_free(root_value);
	json_free_serialized_string(serialized_string);
	return CORE_OK;

}

#if CONFIG_USE_OTA
static void cleanup_fota_data(FotaInfo *fota) {
//	fota->stat = false;
	memset(fota->id, 0, sizeof(fota->id));
	fota->url_len = 0;
	if (fota->url != NULL)
		free(fota->url);
	fota->cert_len = 0;
	if (fota->cert != NULL)
		free(fota->cert);
	memset(fota->ver, 0, CORE_VERSION_NUMBER_LEN + 1);
	memset(fota->fname, 0, CONFIG_FIRMWARE_FILENAME_MAX_LEN);
	fota->fsize = 0;
#if CONFIG_PLATFORM_ESP_IDF
	memset(&fota->sch, 0, sizeof(struct tm));
#endif
}

static core_err parse_fota_json(JSON_Object *rootObj, FotaInfo *fota) {
	core_err ret = CORE_FAIL;

	/* Get the fota object */
	JSON_Value* fota_value = json_object_get_value(rootObj, GET_VAR_NAME(fota, NULL));
	if (fota_value != NULL) {
		if (json_value_get_type(fota_value) != JSONObject) {
			CORE_LOGD(TAG, "[ %s ] is not a valid JSON object!", GET_VAR_NAME(fota, NULL));
//			fota->stat = -1;
//			ret = CORE_FAIL;
		} else {
			JSON_Object *fota_obj = json_value_get_object(fota_value);
			if (fota_obj != NULL) {
				CORE_LOGD(TAG, "fota_obj is not NULL!");

				ret = cpy_json_str_obj(fota_obj, GET_VAR_NAME(fota->id, "->"), fota->id);
				if (ret == CORE_FAIL) {
					CORE_LOGE(TAG, "Could not parse JSON key %s, not proceeding further!",
							GET_VAR_NAME(fota->id, "->"));
					return CORE_FAIL;
				}

				/* Get OTA Status */
//				fota->stat = json_object_get_boolean(fota_obj, GET_VAR_NAME(fota->stat, "->"));
//				if (fota->stat == true) {
//					CORE_LOGI(TAG, "FOTA is needed, processing further...");
				/* OTA is required, so get the permalink len first */
				fota->url_len = json_object_get_number(fota_obj, GET_VAR_NAME(fota->url_len, "->"));
				if (fota->url_len == 0) {
					CORE_LOGE(TAG, "Could not parse JSON key %s, not proceeding further!",
							GET_VAR_NAME(fota->url_len, "->"));
					return CORE_FAIL;
				}

				/* allocating memory for OTA update permalink */
				fota->url = (char*) calloc(fota->url_len + 1, sizeof(char));
				if (fota->url != NULL) {
					/* get fota permalink */
					ret = cpy_json_str_obj(fota_obj, GET_VAR_NAME(fota->url, "->"), fota->url);
					if (ret == CORE_FAIL) {
						CORE_LOGE(TAG, "Could not parse JSON key %s, not proceeding further!",
								GET_VAR_NAME(fota->url, "->"));
						return CORE_FAIL;
					}
					/* Add a NULL character at the end */
					int len = (int) fota->url_len;
					fota->url[len] = '\0';
				} else {
					CORE_LOGE(TAG, "Failed to allocate memory for %s json object data", GET_VAR_NAME(fota->url, "->"));
					return CORE_FAIL;
				}
#if 0
				/* Get the server certificate len */
				fota->cert_len = json_object_get_number(fota_obj, GET_VAR_NAME(fota->cert_len, "->"));
				if (fota->cert_len != 0) {
//						CORE_LOGE(TAG, "Could not parse JSON key %s, not proceeding further!", GET_VAR_NAME(fota->url_len, "->"));
//						return CORE_FAIL;

					/* allocating memory for server certificate */
					fota->cert = (char*) calloc(fota->cert_len, sizeof(char));
					if (fota->cert != NULL) {
						/* get the server certificate */
						ret = cpy_json_str_obj(fota_obj, GET_VAR_NAME(fota->cert, "->"), fota->cert);
						if (ret == CORE_FAIL) {
							CORE_LOGE(TAG, "Could not parse JSON key %s, not proceeding further!",
									GET_VAR_NAME(fota->cert, "->"));
							return CORE_FAIL;
						}
					} else {
						CORE_LOGE(TAG, "Failed to allocate memory for %s json object data",
								GET_VAR_NAME(fota->cert, "->"));
						return CORE_FAIL;
					}
				}
#endif
				/* get the firmware version */
				ret = cpy_json_str_obj(fota_obj, GET_VAR_NAME(fota->ver, "->"), fota->ver);
				if (ret == CORE_FAIL) {
					CORE_LOGE(TAG, "Could not parse JSON key %s, not proceeding further!",
							GET_VAR_NAME(fota->ver, "->"));
					return CORE_FAIL;
				}

				/* get the firmware file name */
				ret = cpy_json_str_obj(fota_obj, GET_VAR_NAME(fota->fname, "->"), fota->fname);
				if (ret == CORE_FAIL) {
					CORE_LOGE(TAG, "Could not parse JSON key %s, not proceeding further!",
							GET_VAR_NAME(fota->fname, "->"));
					return CORE_FAIL;
				}

				/* get firmware bin size */
				fota->fsize = json_object_get_number(fota_obj, GET_VAR_NAME(fota->fsize, "->"));
				if (fota->fsize == 0) {
					CORE_LOGE(TAG, "Could not parse JSON key %s, not proceeding further!",
							GET_VAR_NAME(fota->fsize, "->"));
					return CORE_FAIL;
				}
				/* get and store scheduled ota date-time */
				char time_ms[20] = { 0 };
				ret = cpy_json_str_obj(fota_obj, GET_VAR_NAME(fota->sch, "->"), time_ms);
				if (ret == CORE_FAIL) {
					CORE_LOGE(TAG, "Could not parse JSON key %s, not proceeding further!",
							GET_VAR_NAME(fota->sch, "->"));
					return CORE_FAIL;
				}
				CORE_LOGD(TAG, "Scheduled FOTA Milli Seconds: %s", time_ms);
				/* we got the timestamp, now store it in struct tm */
				convert_str_millis_to_tm(time_ms, &fota->sch);
				char strftime_buf[64];
				strftime(strftime_buf, sizeof(strftime_buf), "%c", &fota->sch);
				CORE_LOGD(TAG, "Scheduled FOTA: date/time is: %s\n", strftime_buf);

				ret = CORE_OK;
//				} else if (fota->stat == false) {
//					CORE_LOGI(TAG, "No FOTA necessary...");
//					ret = CORE_OK;
//				} else if (fota->stat == -1) {
//					CORE_LOGE(TAG, "Could not parse key %s, not proceeding further!", GET_VAR_NAME(fota->stat, "->"));
//					ret = CORE_FAIL;
//					goto end_of_func;
//				}
			} else {	//if (fota_obj != NULL)
				CORE_LOGE(TAG, "fota_obj is null");
				ret = CORE_FAIL;
			}
		}
	} else {
		CORE_LOGE(TAG, "fota_value is null");
//		fota->stat = -1;
		ret = CORE_OK;
	}

	return ret;
}
#endif /* CONFIG_USE_OTA */

static core_err parse_remotesysconfig_json(JSON_Object *rootObj, RemoteSystemConfig *sysconfig) {
	core_err ret = CORE_FAIL;

	/* Get the sysconfig object */
	JSON_Value* scval = json_object_get_value(rootObj, GET_VAR_NAME(sysconfig, NULL));
	if (scval != NULL) {
		if (json_value_get_type(scval) != JSONObject) {
			CORE_LOGE(TAG, "[ %s ] is not a valid JSON object!", GET_VAR_NAME(sysconfig, NULL));
			ret = CORE_FAIL;
		} else {
			JSON_Object *scobj = json_value_get_object(scval);
			if (scobj != NULL) {
				/* Get the heartbeat frequency during normal operation */
				sysconfig->freq = json_object_get_number(scobj, GET_VAR_NAME(sysconfig->freq, "->"));
				/* Get the system standby status */
				sysconfig->standby = json_object_get_boolean(scobj, GET_VAR_NAME(sysconfig->standby, "->"));
				/* Get the heartbeat frequency during standby operation */
				sysconfig->standby_freq = json_object_get_number(scobj, GET_VAR_NAME(sysconfig->standby_freq, "->"));

				ret = CORE_OK;
			} else {
				ret = CORE_FAIL;
			}
		}
	} else {
		CORE_LOGE(TAG, "scval is null");
		ret = CORE_FAIL;
	}

	return ret;
}

core_err parse_sys_hb_response_json(char *json_buff, SysHeartbeatResponse *hb_resp) {
	core_err ret = CORE_FAIL;

	JSON_Value* root_value = NULL;
	JSON_Object * rootObj = NULL;

	root_value = json_parse_string(json_buff);

	if (root_value != NULL) {
		if (json_value_get_type(root_value) != JSONObject) {
			CORE_LOGE(TAG, "JSON Value type not matched");
			ret = CORE_FAIL;
//			goto free_memory;
		} else {
			rootObj = json_value_get_object(root_value);

			/* Parse the common info: stat and error */
			ret = inproc_parse_json_common_info(json_buff, &hb_resp->status, &hb_resp->error);
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "inproc_parse_json_common_info failed! not proceeding further!");
				ret = CORE_FAIL;
				goto free_memory;
			}

			/* Check if heartbeat response as successful */
			if (hb_resp->status) {

				ret = parse_remotesysconfig_json(rootObj, &hb_resp->sysconfig);
				if (ret != CORE_OK) {
					CORE_LOGE(TAG, "Failed to parse remote sysconfig JSON response");
					ret = CORE_OK;	// Even if this parsing fails, the heartbeat was successful
				}

#if CONFIG_USE_OTA
				/* Parse the FOTA response */
				ret = parse_fota_json(rootObj, &hb_resp->fota);
				if (ret != CORE_OK) {
					cleanup_fota_data(&hb_resp->fota);
					CORE_LOGD(TAG, "Failed to parse FOTA JSON response");
					ret = CORE_OK;	// Even if this parsing fails, the heartbeat was successful
				}
#endif /* CONFIG_USE_OTA */

				/* Get the application configuration status */
				hb_resp->conf = json_object_get_boolean(rootObj, GET_VAR_NAME(hb_resp->conf, "->"));
			} else {
				CORE_LOGE(TAG, "The server returned status as not successful! stat = %d", hb_resp->status);
				ret = CORE_FAIL;
			}
		}
		free_memory:
		/* clear root_value */
		json_value_free(root_value);
	} else {
		CORE_LOGE(TAG, "Could not create JSON root object");
		ret = CORE_FAIL;
	}
	return ret;
}

void print_sys_hb_response(SysHeartbeatResponse *hb_resp) {
	CORE_LOGD(TAG, "Parsing SYS heartbeat response completed... The following data was received:");

	CORE_LOGD(TAG, "%s : %s", GET_VAR_NAME(hb_resp->status, "->"), hb_resp->status ? "true" : "false");
	CORE_LOGD(TAG, "%s : %d", GET_VAR_NAME(hb_resp->error.err_code, "->"), hb_resp->error.err_code);
	CORE_LOGD(TAG, "%s : %d", GET_VAR_NAME(hb_resp->sysconfig.freq, "->"), (int )hb_resp->sysconfig.freq);
	CORE_LOGD(TAG, "%s : %s", GET_VAR_NAME(hb_resp->sysconfig.standby, "->"),
			hb_resp->sysconfig.standby ? "true" : "false");
	CORE_LOGD(TAG, "%s : %d", GET_VAR_NAME(hb_resp->sysconfig.standby_freq, "->"),
			(int )hb_resp->sysconfig.standby_freq);
#if CONFIG_USE_OTA
	if (strlen(hb_resp->fota.id) > 0) {
		CORE_LOGD(TAG, "%s : %s", GET_VAR_NAME(hb_resp->fota.id, "->"), hb_resp->fota.id ? hb_resp->fota.id : "no id");
		CORE_LOGD(TAG, "%s : %d", GET_VAR_NAME(hb_resp->fota.url_len, "->"), (int )hb_resp->fota.url_len);
		CORE_LOGD(TAG, "%s :", GET_VAR_NAME(hb_resp->fota.url, "->"));
		CORE_LOGD(TAG, "%s : %s", GET_VAR_NAME(hb_resp->fota.url, "->"),
				hb_resp->fota.url ? hb_resp->fota.url : "no url");
#if 0
		CORE_LOGD(TAG, "%s : %d", GET_VAR_NAME(hb_resp->fota.cert_len, "->"), (int )hb_resp->fota.cert_len);
		CORE_LOGD(TAG, "%s : %s", GET_VAR_NAME(hb_resp->fota.cert, "->"),
				hb_resp->fota.cert ? "cert is present" : "no cert");
#endif
		CORE_LOGD(TAG, "%s : %s", GET_VAR_NAME(hb_resp->fota.ver, "->"), hb_resp->fota.ver);
		CORE_LOGD(TAG, "%s : %s", GET_VAR_NAME(hb_resp->fota.fname, "->"), hb_resp->fota.fname);
		CORE_LOGD(TAG, "%s : %d", GET_VAR_NAME(hb_resp->fota.fsize, "->"), (int )hb_resp->fota.fsize);
		char strftime_buf[64];
		strftime(strftime_buf, sizeof(strftime_buf), "%c", &hb_resp->fota.sch);
		CORE_LOGD(TAG, "%s : %s", GET_VAR_NAME(hb_resp->fota.sch, "->"), strftime_buf);
	}
#endif /* CONFIG_USE_OTA */
	CORE_LOGD(TAG, "%s : %s", GET_VAR_NAME(hb_resp->conf, "->"), hb_resp->conf ? "true" : "false");
	CORE_LOGD(TAG, "");
}
