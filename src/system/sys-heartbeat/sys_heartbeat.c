/*
 * File Name: sys_heartbeat.c
 * File Path: /emmate/src/system/sys-heartbeat/sys_heartbeat.c
 * Description:
 *
 *  Created on: 29-May-2019
 *      Author: Rohan Dey
 */

#include "sys_heartbeat.h"
#include "sys_heartbeat_parser.h"
#include "http_client_api.h"
#include "http_client_core.h"
#include "http_constant.h"
#include "threading.h"
#include "module_thread_priorities.h"
#include "system.h"
#include "system_urls.h"
#include "system_utils.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "board_ids.h"
#include "event_group_core.h"
#include <string.h>

#define TAG	LTAG_SYSHB

core_err _get_configurations_for_application(char *req_data, int req_len);

typedef struct {
	ThreadHandle hb_thread;
	SYSHB_STATES hb_state;
	uint32_t hb_interval_sec;
	bool standby;
	int hb_standby_interval_sec;
} SysHeartbeatData;

static SysHeartbeatData syshb;

//{"status":true,"error":null,"sysconfig":{"freq":0,"standby":false,"standby_freq":0},"fota":{"id":"77","url":"https://13.234.183.14:8443/web-file-dir/secure/23ee34b7/test-app.bin","url_len":68,"ver":"0","fname":"test-app.bin","fsize":1441312,"sch":"1567613162555"},"conf":false}
#define SYS_HB_HTTP_RESPONSE_SIZE	(1*512)
#define SYSHB_MAX_RETRY				5

static core_err make_sys_heartbeat_data(SysHeartbeatRequest *hb_req, char **json_buf, int *json_len) {
	core_err ret = CORE_FAIL;

	get_somthing_id(hb_req->somthing_id);
	strcpy(hb_req->core_version, CORE_DEV_VERSION_NUMBER);
#ifdef APP_DEV_VERSION_NUMBER
	strcpy(hb_req->app_version, APP_DEV_VERSION_NUMBER);
#else
	strcpy(hb_req->app_version, "NO APP VERSION FOUND");
#endif
	ret = make_sys_hb_request_json(json_buf, json_len, hb_req);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "make_sys_hb_request_json failed!");
	}
	return ret;
}

static core_err validate_sys_heartbeat_response(SysHeartbeatResponse *hb_resp) {
	core_err ret = CORE_FAIL;

	if (hb_resp->status) {
		ret = CORE_OK;
	}

	return ret;
}

#if CONFIG_USE_OTA
static void notify_fota_module(SysHeartbeatResponse *hb_resp) {

	CORE_LOGD(TAG, "Calling the FOTA module to begin the FOTA process");
	start_fota_task(&hb_resp->fota);
}
#endif

static core_err set_sysheartbeat_interval(uint32_t interval_sec) {
	if (interval_sec < SYS_HB_MIN_INTERVAL)
		return CORE_ERR_INVALID_ARG;

	syshb.hb_interval_sec = interval_sec;
	return CORE_OK;
}

static core_err set_standby_sysheartbeat_interval(uint32_t interval_sec) {
	if (interval_sec < SYS_HB_MIN_INTERVAL)
		return CORE_ERR_INVALID_ARG;

	syshb.hb_standby_interval_sec = interval_sec;
	return CORE_OK;
}

void perform_system_heartbeat(void * params) {
	core_err ret;
	int hb_retry = 0;
	bool retry_http_oper = false;
	char *http_response = NULL;
	size_t http_response_len = 0;

	CORE_LOGD(TAG, "Waiting for START_SYSHEARTBEAT_BIT event ...");
	/* Wait until we get a confirmation to start the heartbeat */
	event_group_wait_bits(get_system_evtgrp_hdl(), START_SYSHEARTBEAT_BIT, true, false, EventMaxDelay);

	CORE_LOGD(TAG, "Waiting for network IP ...");
	/* Wait until we have a network IP */
	event_group_wait_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT, false, true, EventMaxDelay);

	while (1) {
		/* Make heartbeat JSON */
		CORE_LOGD(TAG, "Starting Sys Heartbeat ...");

		/* Make the SYS Heartbeat request data to be posted to server */
		char *json_buf;
		int json_len;
		SysHeartbeatRequest hb_req;
		memset(&hb_req, 0, sizeof(SysHeartbeatRequest));
		ret = make_sys_heartbeat_data(&hb_req, &json_buf, &json_len);
		CORE_LOGI(TAG, "Request: %.*s", json_len, json_buf);

		/* Initialize a SYS Heartbeat response data to be populated after receiving the response from server */
		SysHeartbeatResponse hb_resp;
		memset(&hb_resp, 0, sizeof(SysHeartbeatResponse));

		/* Allocate memory for the http response */
		http_response = (char*) calloc(SYS_HB_HTTP_RESPONSE_SIZE, sizeof(char));
		if (http_response == NULL) {
			CORE_LOGE(TAG, "memory allocation for http response failed");
			retry_http_oper = true;
			goto free_memory;
		}
		memset(http_response, 0, SYS_HB_HTTP_RESPONSE_SIZE);
		http_response_len = 0;

		/* Do http operation */
		uint16_t http_stat = 0;
		ret = do_http_operation(IQ_SYS_HEARTBEAT_POST_URL, IQ_HOST_PORT, IQ_HOST_ROOTCA, HTTP_CLIENT_METHOD_POST,
		IQ_HOST, HTTP_USER_AGENT,
		CONTENT_TYPE_APPLICATION_JSON, json_buf, json_len, http_response, &http_response_len, SYS_HB_HTTP_RESPONSE_SIZE,
				&http_stat);

		if (ret == CORE_OK) {
			CORE_LOGI(TAG, "Response: %.*s", http_response_len, http_response);

			/* Parse the http response data */
			CORE_LOGD(TAG, "Going to parse SYS Heartbeat response");
			ret = parse_sys_hb_response_json(http_response, &hb_resp);
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "Sys Heartbeat response parsing failed! Free memory and try again!");
				retry_http_oper = true;
				goto free_memory;
			} else {
				/* Print the sys heartbeat response values received from the server */
				print_sys_hb_response(&hb_resp);

				/* Check and validate the response data */
				CORE_LOGD(TAG, "Going to validate the parse sys heartbeat response");
				ret = validate_sys_heartbeat_response(&hb_resp);
				if (ret == CORE_OK) {
					if (hb_resp.sysconfig.freq != 0)
						set_sysheartbeat_interval((uint32_t) hb_resp.sysconfig.freq);
					if (hb_resp.sysconfig.standby != -1)
						syshb.standby = hb_resp.sysconfig.standby;
					if (hb_resp.sysconfig.standby_freq != 0)
						set_standby_sysheartbeat_interval((uint32_t) hb_resp.sysconfig.standby_freq);
#if CONFIG_USE_OTA
					/* Check if FOTA is required */
					if (strlen(hb_resp.fota.id) > 0) {
//					if (hb_resp.fota.stat) {
						CORE_LOGI(TAG, "FOTA is needed, processing further...");
						notify_fota_module(&hb_resp);
					}
#endif
					if (hb_resp.conf == true) {
						// TODO: Get application configurations from server and send them to the app
						_get_configurations_for_application(json_buf, json_len);
					}
					hb_retry = 0;
				} else {
					retry_http_oper = true;
					goto free_memory;
				}
			}
		} else {
			retry_http_oper = true;
			CORE_LOGE(TAG, "HTTP failed with status code = [ %d ]!", http_stat);
//			if (hb_retry != SYSHB_MAX_RETRY) {
//				CORE_LOGI(TAG, "Retrying...");
//				TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
//			} else
//				CORE_LOGE(TAG, "Retry max count reached!");
		}

		free_memory:
		/* Free the allocated http respose memory */
		CORE_LOGD(TAG, "Freeing allocated response memory");
		free(json_buf);
		free(http_response);

		if (retry_http_oper) {
			retry_http_oper = false;
			if (++hb_retry < SYSHB_MAX_RETRY) {
				CORE_LOGW(TAG, "Retrying in %d secs... Retry Count = %d", (DELAY_5_SEC/1000), hb_retry);
				TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
			} else
				CORE_LOGE(TAG, "Retry max count reached!");
		}

		/* Go to sleep */
		if ((hb_retry == 0) || (hb_retry == SYSHB_MAX_RETRY)) {
			hb_retry = 0;
			CORE_LOGD(TAG, "sys-heartbeat task going to sleep for %d ms", SYS_HB_INTERVAL_SEC);
			TaskDelay((syshb.hb_interval_sec * 1000) / TICK_RATE_TO_MS);
		}
	}
}

core_err start_system_heartbeat() {
	core_err ret = CORE_FAIL;

	/* Initialize sys heartbeat data to default values */
	syshb.hb_state = SYSHB_STATE_HAVE_SOMID;
	set_sysheartbeat_interval(SYS_HB_INTERVAL_SEC);
	set_standby_sysheartbeat_interval(SYS_HB_INTERVAL_SEC);
	syshb.standby = false;
#if 1
	/* Create a thread to perform system heartbeat */
	BaseType thread_stat;
	thread_stat = TaskCreate(perform_system_heartbeat, "sys-heartbeat", TASK_STACK_SIZE_8K, NULL, THREAD_PRIORITY_SYSHB,
			&syshb.hb_thread);
	if (thread_stat == false) {
		CORE_LOGE(TAG, "Failed to create thread: %s, %d", (char*) __FILE__, __LINE__);
		ret = CORE_FAIL;
	} else {
		ret = CORE_OK;
	}
#endif
	return ret;
}
