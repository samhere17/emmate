/*
 * File Name: systime.c
 * File Path: /emmate/src/system/systime/systime.c
 * Description:
 *
 *  Created on: 27-Apr-2019
 *      Author: Rohan Dey
 */

#include <string.h>
#include "systime.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "threading.h"
#include "system.h"
#include "system_utils.h"
#include "event_group_core.h"
#if CONFIG_USE_SNTP
#include "sntp_client_core.h"
#endif

#define TAG	LTAG_SYSTEM_SYSTIME

#if CONFIG_PLATFORM_ESP_IDF
/* Set the system time */
int core_settimeofday(const struct timeval *tv, const struct timezone *tz) {
	return settimeofday(tv, tz);
}

/* Returns 0 if success and -1 if failed and curr_time as an out param */
int get_systime(struct tm *curr_time) {
	time_t now;
	struct tm timeinfo;
	time(&now);
	localtime_r(&now, &timeinfo);

	// Is time set? If not, tm_year will be (1970 - 1900).
	if (timeinfo.tm_year < (2016 - 1900)) {
		memcpy(curr_time, &timeinfo, sizeof(struct tm));
		CORE_LOGI(TAG, "Time is not set yet.\n");
		return -1;
	} else {
		memcpy(curr_time, &timeinfo, sizeof(struct tm));
		char strftime_buf[64];
		strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
		CORE_LOGI(TAG, "The current UTC date/time is: %s", strftime_buf);
		return 0;
	}
}

int get_systime_seconds(time_t *curr_time_secs) {
	struct tm timeinfo;
	int val = get_systime(&timeinfo);
	if (val == 0) {
		*curr_time_secs = convert_tm_to_seconds(&timeinfo);
		return 0;
	} else {
		return -1;
	}
}

int get_systime_millis(uint64_t *curr_time_ms) {
	struct tm timeinfo;
	int val = get_systime(&timeinfo);
	if (val == 0) {
		*curr_time_ms = convert_tm_to_millis(&timeinfo);
		return 0;
	} else {
		return -1;
	}
}

bool is_systime_set() {
	bool ret = false;

#if CONFIG_PLATFORM_ESP_IDF
	struct tm timeinfo;
	int val = get_systime(&timeinfo);
	if (val == 0) {
		ret = true;
	} else {
		ret = false;
	}
#endif

	return ret;
}

time_t convert_tm_to_seconds(struct tm* time) {
	return mktime(time);
}

uint64_t convert_tm_to_millis(struct tm* time) {
	time_t time_secs = mktime(time);
	return (time_secs * 1000);
}

struct tm * convert_seconds_to_tm(const time_t seconds, struct tm *time) {
	return gmtime_r(&seconds, time);
}

struct tm * convert_millis_to_tm(uint64_t millis, struct tm *time) {
	const time_t seconds = millis / 1000;
	return gmtime_r(&seconds, time);
}

struct tm * convert_str_seconds_to_tm(char *secs, struct tm *time) {
	if (secs != NULL) {
		time_t seconds = atol(secs);
		return gmtime_r(&seconds, time);
	}
	return NULL;
}

struct tm * convert_str_millis_to_tm(char *millis, struct tm *time) {
	if (millis != NULL) {
		//CORE_LOGI(TAG, "Input millis: %s", millis);
		uint64_t msec = atoll(millis);
		//CORE_LOGI(TAG, "Converted millis: %lld", msec);
		time_t seconds = msec / 1000;
		//CORE_LOGI(TAG, "Converted seconds: %ld", seconds);
		return gmtime_r(&seconds, time);
	}
	return NULL;
}

double diff_time_with_now(struct tm *time_to_cmp) {
	struct tm now_time;
	int ret = get_systime(&now_time);
	if (ret == -1) {
		CORE_LOGE(TAG, "System Time is not set");
		return -DBL_MAX;
	}

	time_t t1_cmp = convert_tm_to_seconds(time_to_cmp);
	time_t t2_now = convert_tm_to_seconds(&now_time);
	double diff_secs = difftime(t1_cmp, t2_now); // If positive, then tm1 > tm2
	CORE_LOGD(TAG, "Diff time in seconds = %lf", diff_secs);

	return diff_secs;
}

double diff_between_time(struct tm *end_time, struct tm *begin_time) {
	time_t t1_cmp = convert_tm_to_seconds(end_time);
	time_t t2_now = convert_tm_to_seconds(begin_time);
	double diff_secs = difftime(t1_cmp, t2_now); // If positive, then tm1 > tm2
	CORE_LOGD(TAG, "Diff time in seconds = %lf", diff_secs);

	return diff_secs;
}
#endif	/* CONFIG_PLATFORM_ESP_IDF */

#if CONFIG_HAVE_SNTP_SYSTIME
static void systime_setup_thread(void * params) {
	/* Wait until we have a network IP */
	event_group_wait_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT, false, true, EventMaxDelay);

	// TODO: remove the below delay, done temporarily
	TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);

	if (is_systime_set() == false) {
		initialize_sntp_core();
//		uint8_t count = 0;
		while ((is_systime_set() == false) /*&& (++count <= 100)*/) {
			CORE_LOGI(TAG, "Waiting for SNTP to set System Time ...");
			TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);
		}
		if (is_systime_set() == false)
			CORE_LOGW(TAG, "Could not set System Time through SNTP, proceeding anyway ...");
	}

	/* System time is set, now kill the task and exit. Time updation will be taken care by internal RTC and SNTP */
	TaskDelete(NULL);
}

core_err start_system_time_module() {
	core_err ret = CORE_FAIL;

	/* Create a thread to perform hardware identification */
	BaseType thread_stat;
	thread_stat = TaskCreate(systime_setup_thread, "sys-time-setup", TASK_STACK_SIZE_4K, NULL, THREAD_PRIORITY_3, NULL);
	if (thread_stat == false) {
		CORE_LOGE(TAG, "Failed to create thread!");
		ret = CORE_FAIL;
	} else {
		ret = CORE_OK;
	}

	return ret;
}
#endif	/* CONFIG_HAVE_SNTP_SYSTIME */
