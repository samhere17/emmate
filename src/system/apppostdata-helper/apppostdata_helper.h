/*
 * apppostdata_helper.h
 *
 *  Created on: 12-Aug-2019
 *      Author: Rohan Dey
 */

#ifndef APPPOSTDATA_HELPER_H_
#define APPPOSTDATA_HELPER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

/**
 * @brief		This function is used to post data to server via http(s)
 *
 * @param[in]	post_data 	The data to be posted. Must be NULL terminated JSON string.
 * 							The length of this string must be less than or equal to CONFIG_APPPOSTDATA_MAX_SIZE
 * 							CONFIG_APPPOSTDATA_MAX_SIZE can be set in EmMate menuconfig
 *
 * @return
 * 			- CORE_OK					If the data was posted successfully
 * 			- CORE_ERR_INVALID_ARG		If the input param is NULL
 * 			- CORE_ERR_NO_MEM			If the length of the input parameter is greater than CONFIG_APPPOSTDATA_MAX_SIZE
 * 										Either pass the data size within CONFIG_APPPOSTDATA_MAX_SIZE, or increase the
 * 										CONFIG_APPPOSTDATA_MAX_SIZE from menuconfig
 * 			- CORE_ERR_INVALID_STATE	If the system doesn't have a somthing_id yet. Wait till the server identifies this hardware
 * 			- CORE_FAIL					Any other error
 * */
core_err post_application_json_via_http(char *post_data);

#ifdef __cplusplus
}
#endif

#endif /* APPPOSTDATA_HELPER_H_ */
