/*
 * appconfig_helper_status.c
 *
 *  Created on: 16-Aug-2019
 *      Author: Rohan Dey
 */

#include "appconfig_helper_status.h"
#include "http_client_api.h"
#include "input_processor.h"
#include "inpproc_utils.h"
#include "system_urls.h"
#include "core_utils.h"
#include "board_ids.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include <string.h>

#define APPCONFIG_STATUS_RESPONSE_SIZE	(sizeof(int) + sizeof(CoreError) + 1)
#define TAG	LTAG_APPCFG
/*
 * {
 * 		"somthing_id": "123",
 * 		"config_stat":20
 * }
 * */
typedef struct {
	char somthing_id[SOMTHING_ID_LEN + 1];
	int config_stat;
} AppConfigStatusRequest;

typedef struct {
	int status; /**< Sys Heartbeat Status. Success or Failure */
	CoreError error; /**< Error object */
} AppConfigStatusResponse;

static core_err make_appconfig_status_request(char **ppbuf, int *plen, AppConfigStatusRequest *cfgstat_req) {
//	core_err ret = CORE_FAIL;
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);
	char *serialized_string = NULL;

	CORE_LOGD(TAG, "Going to make App Config Status JSON with the following values:");

	/* Set JSON key value */
	CORE_LOGD(TAG, "[%s]: %s", GET_VAR_NAME(cfgstat_req->somthing_id, "->"), cfgstat_req->somthing_id);
	json_object_set_string(root_object, GET_VAR_NAME(cfgstat_req->somthing_id, "->"), cfgstat_req->somthing_id);

	CORE_LOGD(TAG, "[%s]: %d", GET_VAR_NAME(cfgstat_req->config_stat, "->"), cfgstat_req->config_stat);
	json_object_set_number(root_object, GET_VAR_NAME(cfgstat_req->config_stat, "->"), (int) cfgstat_req->config_stat);

	serialized_string = json_serialize_to_string(root_value);

	size_t len = json_serialization_size(root_value);
	len = len - 1;  // since json_serialization_size returns size + 1
	CORE_LOGD(TAG, "AppConfig Status Request JSON Len = %d\r\n", len);

	char *ptemp = (char*) malloc(len);
	if (ptemp == NULL) {
		CORE_LOGE(TAG, "make_appconfig_status_request malloc failed!");
		return CORE_FAIL;
	}
	memset(ptemp, 0x00, len);
	memcpy(ptemp, serialized_string, len);
	*plen = len;
	*ppbuf = ptemp;

	json_value_free(root_value);
	json_free_serialized_string(serialized_string);
	return CORE_OK;
}

static core_err parse_appconfig_status_response(char *json_buff, AppConfigStatusResponse *cfgstat_resp) {
	core_err ret = CORE_FAIL;

	/* Parse the common info: stat and error */
	ret = inproc_parse_json_common_info(json_buff, &cfgstat_resp->status, &cfgstat_resp->error);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "inproc_parse_json_common_info failed! not proceeding further!");
		ret = CORE_FAIL;
	}

	return ret;
}

core_err _send_appconfig_status_to_server(int config_stat) {
	core_err ret = CORE_FAIL;

	char *http_resp = NULL;
	size_t http_resp_len = 0;
	char *json_buf = NULL;
	int json_len = 0;

	AppConfigStatusRequest cfgstat_req;
	memset(&cfgstat_req, 0, sizeof(AppConfigStatusRequest));

	get_somthing_id(cfgstat_req.somthing_id);
	cfgstat_req.config_stat = config_stat;

	ret = make_appconfig_status_request(&json_buf, &json_len, &cfgstat_req);
	CORE_LOGI(TAG, "Request: %.*s", json_len, json_buf);

	/* Allocate memory for the http response */
	http_resp = (char*) calloc(APPCONFIG_STATUS_RESPONSE_SIZE, sizeof(char));
	if (http_resp == NULL) {
		CORE_LOGE(TAG, "memory allocation for http response failed");
		goto free_memory;
	}
	memset(http_resp, 0, APPCONFIG_STATUS_RESPONSE_SIZE);
	http_resp_len = 0;

	/* Do http operation */
	uint16_t http_stat = 0;
	ret = do_http_operation(IQ_SYS_APPCONFIG_STATUS_POST_URL, IQ_HOST_PORT, IQ_HOST_ROOTCA, HTTP_CLIENT_METHOD_POST,
	IQ_HOST, HTTP_USER_AGENT,
	CONTENT_TYPE_APPLICATION_JSON, json_buf, json_len, http_resp, &http_resp_len, APPCONFIG_STATUS_RESPONSE_SIZE,
			&http_stat);

	if (ret == CORE_OK) {
		CORE_LOGI(TAG, "Response: %.*s", http_resp_len, http_resp);

		AppConfigStatusResponse cfgstat_resp;
		memset(&cfgstat_resp, 0, sizeof(AppConfigStatusResponse));

		ret = parse_appconfig_status_response(http_resp, &cfgstat_resp);
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "AppConfig Status Response parsing failed, don't know what to do yet!!");
			// TODO: Handle error
		}
	} else {
		CORE_LOGE(TAG, "HTTP failed with status code = [ %d ]", http_stat);
		ret = CORE_FAIL;
	}

	free_memory:
	/* Free the allocated http respose memory */
	CORE_LOGD(TAG, "Freeing allocated response memory");
	free(json_buf);
	free(http_resp);

	return ret;
}
