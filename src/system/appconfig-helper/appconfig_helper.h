/*
 * appconfig_helper.h
 *
 *  Created on: 07-Aug-2019
 *      Author: Rohan Dey
 */

#ifndef APPCONFIG_HELPER_H_
#define APPCONFIG_HELPER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

/** OTA status to be sent to server */
typedef enum {
//	APPCFG_STATUS_NEW = 1,
//	APPCFG_STATUS_AVAILABLE = 2,
//	APPCFG_STATUS_NEEDED = 3,
//	APPCFG_STATUS_DOWNLOADING = 30,
//	APPCFG_STATUS_DOWNLOADED = 31,
//	APPCFG_STATUS_DOWNLOAD_FAILED = 32,
//	APPCFG_STATUS_UPDATING = 20,
	APPCFG_STATUS_UPDATED = 21,
	APPCFG_STATUS_UPDATE_FAILED = 22,
	APPCFG_STATUS_UNKNOWN = 0,
} APPCFG_STATUSES;

/**
 * @brief	Function pointer type to point to application's configuration callback function
 *
 * @note	The function signature should be as follows:
 *
 * 			core_err myappconfig(char * myapp_config, size_t myapp_config_len)
 */

typedef core_err (*appconfig_function)(char * app_config, size_t app_config_len);

/**
 * @brief		This function registers a function of type appconfig_function, which will be called when an application specific
 * 				configurations are received from the server. This should be registered by the application to get the configurations
 * 				in the form of JSON
 *
 * @param[in]	appcfg_func This is the pointer to the function which will be registered
 *
 * @return
 * 			- CORE_OK					If the function was registered successfully
 * 			- CORE_ERR_INVALID_ARG		If the input param is NULL
 * */
core_err register_app_configuration_function(appconfig_function appcfg_func);

#ifdef __cplusplus
}
#endif

#endif /* APPCONFIG_HELPER_H_ */
