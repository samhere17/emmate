/*
 * File Name: threading.h
 * File Path: /emmate/src/threading/threading.h
 * Description:
 *
 *  Created on: 14-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef THREADING_H_
#define THREADING_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_common.h"
#include "threading_platform.h"

/* Thread/Task related datatypes and macros */
typedef void (*ThreadFunc)(void *);	// The thread function must be of this type. e.g. void mytask(void *p);

/**
 * TaskCreate() can only be used to create a task
 *
 * @param func Pointer to the task entry function.  Tasks
 * must be implemented to never return (i.e. continuous loop).
 *
 * @param name A descriptive name for the task.  This is mainly used to
 * facilitate debugging.  Max length is 16.
 *
 * @param stack_depth The size of the task stack specified as the number of
 * bytes. Note that this differs from vanilla RTOS.
 *
 * @param params Pointer that will be used as the parameter for the task
 * being created.
 *
 * @param priority The priority at which the task should run.
 *
 * @param thread_handle Used to pass back a handle by which the created task
 * can be referenced.
 *
 * @return pdPASS if the task was successfully created and added to a ready
 * list
 *
 * @note If program uses thread local variables (ones specified with "__thread" keyword)
 * then storage for them will be allocated on the task's stack.
 *
 */
#define TaskCreate(func /*ThreadFunc*/, name /*const char * const*/, stack_depth /*const uint32_t*/, params /*void * const*/, priority /*int*/, thread_id /*ThreadHandle*/) \
		TaskPfCreate(func,name,stack_depth,params,priority,thread_id)

/**
 * Create a new task with a specified affinity.
 *
 * This function is similar to TaskPfCreate, but allows setting task affinity
 * in SMP system.
 *
 * @param func Pointer to the task entry function.  Tasks
 * must be implemented to never return (i.e. continuous loop).
 *
 * @param name A descriptive name for the task.  This is mainly used to
 * facilitate debugging.  Max length is 16.
 *
 * @param stack_depth The size of the task stack specified as the number of
 * bytes. Note that this differs from vanilla RTOS.
 *
 * @param params Pointer that will be used as the parameter for the task
 * being created.
 *
 * @param priority The priority at which the task should run.
 *
 * @param thread_handle Used to pass back a handle by which the created task
 * can be referenced.

 * @param core_id If the value is tskNO_AFFINITY, the created task is not
 * pinned to any CPU, and the scheduler can run it on any core available.
 * Other values indicate the index number of the CPU which the task should
 * be pinned to. Specifying values larger than (portNUM_PROCESSORS - 1) will
 * cause the function to fail.
 *
 * @return pdPASS if the task was successfully created and added to a ready
 * list, otherwise an error code defined in the file projdefs.h
 *
 */
#define TaskCreatePinnedToCore(func, name, stack_depth, params, priority, thread_handle, core_id) \
		TaskPfCreatePinnedToCore(func, name, stack_depth, params, priority, thread_handle, core_id) \


/**
 * Obtain the state of any task.
 *
 * @param thread_handle Handle of the task to be queried.
 *
 * @return The state of thread_handle at the time the function was called.  Note the
 * state of the task might change between the function being called, and the
 * functions return value being tested by the calling task.
 */
#define TaskGetState(thread_handle)	TaskPfGetState(thread_handle) /* Returns a THREAD_STATE */

/**
 * Remove a task from the RTOS real time kernel's management.
 *
 * @note The idle task is responsible for freeing the kernel allocated
 * memory from tasks that have been deleted.  It is therefore important that
 * the idle task is not starved of microcontroller processing time if your
 * application makes any calls to TaskDelete ().  Memory allocated by the
 * task code is not automatically freed, and should be freed before the task
 * is deleted.
 *
 * @param thread_handle The handle of the task to be deleted.  Passing NULL will
 * cause the calling task to be deleted.
 */
#define TaskDelete(thread_handle)		TaskPfDelete(thread_handle)

/**
 * Get tick count
 *
 * @return The count of ticks.
 */
#define TaskGetTickCount				TaskPfGetTickCount			/* Returns a TickType */

/**
 * Delay a task for a given number of ticks.
 *
 * The actual time that the task remains blocked depends on the tick rate.
 * The constant portTICK_RATE_MS can be used to calculate real time from
 * the tick rate - with the resolution of one tick period.
 *
 * INCLUDE_vTaskDelay must be defined as 1 for this function to be available.
 * See the configuration section for more information.
 *
 * vTaskDelay() specifies a time at which the task wishes to unblock relative to
 * the time at which vTaskDelay() is called.  For example, specifying a block
 * period of 100 ticks will cause the task to unblock 100 ticks after
 * vTaskDelay() is called.  vTaskDelay() does not therefore provide a good method
 * of controlling the frequency of a periodic task as the path taken through the
 * code, as well as other task and interrupt activity, will effect the frequency
 * at which vTaskDelay() gets called and therefore the time at which the task
 * next executes.  See vTaskDelayUntil() for an alternative API function designed
 * to facilitate fixed frequency execution.  It does this by specifying an
 * absolute time (rather than a relative time) at which the calling task should
 * unblock.
 *
 * @param ticks_to_delay The amount of time, in tick periods, that
 * the calling task should block.
 *
 */
#define TaskDelay(ticks_to_delay)		TaskPfDelay(ticks_to_delay)

/**
 * Get task name
 *
 * @return The text (human readable) name of the task referenced by the handle
 * xTaskToQuery.  A task can query its own name by either passing in its own
 * handle, or by setting xTaskToQuery to NULL.
 */
#define TaskGetTaskName(thread_handle)		TaskPfGetTaskName(thread_handle)	/* Returns (char *) */

/**
 * Returns the high water mark of the stack associated with xTask.
 *
 * High water mark is the minimum free stack space there has been (in bytes
 * rather than words as found in vanilla RTOS) since the task started.
 * The smaller the returned number the closer the task has come to overflowing its stack.
 *
 * @param thread_handle Handle of the task associated with the stack to be checked.
 * Set thread_handle to NULL to check the stack of the calling task.
 *
 * @return The smallest amount of free stack space there has been (in bytes
 * rather than words as found in vanilla RTOS) since the task referenced by
 * thread_handle was created.
 */
#define TaskGetStackHighWaterMark(thread_handle)	TaskPfGetStackHighWaterMark(thread_handle)/* Returns int */

/**
 * Suspend a task.
 *
 * When suspended, a task will never get any microcontroller processing time,
 * no matter what its priority.
 *
 * Calls to TaskSuspend are not accumulative -
 * i.e. calling TaskSuspend () twice on the same task still only requires one
 * call to TaskResume () to ready the suspended task.
 *
 * @param thread_handle Handle to the task being suspended.  Passing a NULL
 * handle will cause the calling task to be suspended.
 *
 */
#define TaskSuspend(thread_handle)		TaskPfSuspend(thread_handle)

/**
 * Resumes a suspended task.
 *
 * A task that has been suspended by one or more calls to TaskSuspend ()
 * will be made available for running again by a single call to
 * TaskResume ().
 *
 * @param thread_handle Handle to the task being readied.
 *
 */
#define TaskResume(thread_handle)		TaskPfResume(thread_handle)

/* Queue related datatypes and macros */
typedef void * QueueHandle;

/**
 * Creates a new queue instance.  This allocates the storage required by the
 * new queue and returns a handle for the queue.
 *
 * @param queue_len The maximum number of items that the queue can contain.
 *
 * @param item_size The number of bytes each item in the queue will require.
 * Items are queued by copy, not by reference, so this is the number of bytes
 * that will be copied for each posted item.  Each item on the queue must be
 * the same size.
 *
 * @return If the queue is successfully create then a handle to the newly
 * created queue is returned.  If the queue cannot be created then 0 is
 * returned.
 */
#define QueueCreate(queue_len /*int*/, item_size /*int*/) \
		QueuePfCreate(queue_len, item_size)

/**
 * Receive an item from a queue.  The item is received by copy so a buffer of
 * adequate size must be provided.  The number of bytes copied into the buffer
 * was defined when the queue was created.
 *
 * Successfully received items are removed from the queue.
 *
 * @param queue The handle to the queue from which the item is to be
 * received.
 *
 * @param buffer Pointer to the buffer into which the received item will
 * be copied.
 *
 * @param ticks_to_wait The maximum amount of time the task should block
 * waiting for an item to receive should the queue be empty at the time
 * of the call.
 *
 * @return pdTRUE if an item was successfully received from the queue,
 * otherwise pdFALSE.
 */
#define QueueReceive(queue /*QueueHandle*/, buffer /*(void*)*/, ticks_to_wait /*uint32_t*/) \
		QueuePfReceive(queue, buffer, ticks_to_wait)

/**
 * Post an item on a queue.  The item is queued by copy, not by reference.
 * This function must not be called from an interrupt service routine.
 *
 * @param queue The handle to the queue on which the item is to be posted.
 *
 * @param item_to_queue A pointer to the item that is to be placed on the
 * queue.  The size of the items the queue will hold was defined when the
 * queue was created, so this many bytes will be copied from pvItemToQueue
 * into the queue storage area.
 *
 * @param ticks_to_wait The maximum amount of time the task should block
 * waiting for space to become available on the queue, should it already
 * be full.  The call will return immediately if this is set to 0 and the
 * queue is full.
 *
 * @return pdTRUE if the item was successfully posted, otherwise errQUEUE_FULL.
 */
#define QueueSend(queue /*QueueHandle*/, item_to_queue /*const void * const*/, ticks_to_wait /*uint32_t*/) \
		QueuePfSend(queue, item_to_queue, ticks_to_wait)

/**
 * Post an item to the back of a queue.  It is safe to use this function from
 * within an interrupt service routine.
 *
 * Items are queued by copy not reference so it is preferable to only
 * queue small items, especially when called from an ISR.  In most cases
 * it would be preferable to store a pointer to the item being queued.
 *
 * @param queue The handle to the queue on which the item is to be posted.
 *
 * @param item_to_queue A pointer to the item that is to be placed on the
 * queue.  The size of the items the queue will hold was defined when the
 * queue was created, so this many bytes will be copied from pvItemToQueue
 * into the queue storage area.
 *
 * @param[out] pxHigherPriorityTaskWoken QueueSendFromISR() will set
 * *pxHigherPriorityTaskWoken to pdTRUE if sending to the queue caused a task
 * to unblock, and the unblocked task has a priority higher than the currently
 * running task.  If QueueSendFromISR() sets this value to pdTRUE then
 * a context switch should be requested before the interrupt is exited.
 *
 * @return pdTRUE if the data was successfully sent to the queue, otherwise
 * errQUEUE_FULL.
 *
 */
#define QueueSendFromISR(queue,item_to_queue,pxHigherPriorityTaskWoken)	QueuePfSendFromISR(queue,item_to_queue,pxHigherPriorityTaskWoken)

/**
 * Reset a queue back to its original empty state.  pdPASS is returned if the
 * queue is successfully reset.  pdFAIL is returned if the queue could not be
 * reset because there are tasks blocked on the queue waiting to either
 * receive from the queue or send to the queue.
 *
 * @param queue The queue to reset
 * @return always returns pdPASS
 */
#define QueueReset(queue /*QueueHandle*/)		QueuePfReset(queue)

/**
 * Delete a queue - freeing all the memory allocated for storing of items
 * placed on the queue.
 *
 * @param queue A handle to the queue to be deleted.

 */
#define QueueDelete(queue /*QueueHandle*/)	QueuePfDelete(queue)

/**
 * Return the number of messages stored in a queue.
 *
 * @param queue A handle to the queue being queried.
 *
 * @return The number of messages available in the queue.
 */
#define QueueMessagesWaiting(queue)				QueuePfMessagesWaiting(queue)


 /**
  * That implements a mutex semaphore by using the existing queue
  * mechanism.
  *
  * Internally, mutex semaphores use a block of memory,
  * in which the mutex structure is stored.  If a mutex is created
  * using SemaphoreCreateMutex() then the required memory is automatically
  * dynamically allocated inside the SemaphoreCreateMutex() function.
  *
  * Mutexes created using this function can be accessed using the SemaphoreTake()
  * and SemaphoreGive() macros.
  *
  * This type of semaphore uses a priority inheritance mechanism so a task
  * 'taking' a semaphore MUST ALWAYS 'give' the semaphore back once the
  * semaphore it is no longer required.
  *
  * Mutex type semaphores cannot be used from within interrupt service routines.
  *
  * @return If the mutex was successfully created then a handle to the created
  * semaphore is returned.  If there was not enough heap to allocate the mutex
  * data structures then NULL is returned.
  *
  */
#define	SemaphoreCreateMutex()								SemaphorePfCreateMutex()


 /**
  * Delete a semaphore.  This function must be used with care.  For example,
  * do not delete a mutex type semaphore if the mutex is held by a task.
  *
  * @param xSemaphore A handle to the semaphore to be deleted.
  *
  */
#define	SemaphoreDelete( xSemaphore )							SemaphorePfDelete( xSemaphore )

 /**
  * To release a semaphore.  The semaphore must have previously been
  * created with a call to SemaphoreCreateMutex() and obtained using SemaphoreTake().
  *
  * This macro must not be used from an ISR.  See SemaphoreGiveFromISR () for
  * an alternative which can be used from an ISR.
  *
  * @param xSemaphore A handle to the semaphore being released.  This is the
  * handle returned when the semaphore was created.
  *
  * @return pdTRUE if the semaphore was released.  pdFALSE if an error occurred.
  * Semaphores are implemented using queues.  An error can occur if there is
  * no space on the queue to post a message - indicating that the
  * semaphore was not first obtained correctly.
  */
#define SemaphoreGive( xSemaphore )								SemaphorePfGive( xSemaphore )


 /**
  * To obtain a semaphore.  The semaphore must have previously been
  * created with a call to SemaphoreCreateMutex().
  *
  * @param xSemaphore A handle to the semaphore being taken - obtained when
  * the semaphore was created.
  *
  * @param xTicksToWait The time in ticks(in ms) to wait for the semaphore to become
  * available.
  *
  * @return pdTRUE if the semaphore was obtained.  pdFALSE
  * if xTicksToWait expired without the semaphore becoming available.
  *
  */
#define SemaphoreTake( xSemaphore, xTicksToWait )				SemaphorePfTake( xSemaphore, xTicksToWait )


 /**********************************************************************************************/

 /*
  * Wrapper function and structures for the portmacro.h
  */

 /* "mux" data structure (spinlock) */
 typedef pf_portMUX_TYPE		core_portMUX_TYPE;


  /* Calling core*_CRITICAL from ISR context would cause an assert failure.
   * If the parent function is called from both ISR and Non-ISR context then call core*_CRITICAL_SAFE
   */
 #define core_portENTER_CRITICAL(mux)    		pf_portENTER_CRITICAL(mux)
 #define core_portEXIT_CRITICAL(mux)			pf_portEXIT_CRITICAL(mux)


 // Keep this in sync with the portMUX_TYPE struct definition please.

#define core_portMUX_INITIALIZER_UNLOCKED		pf_portMUX_INITIALIZER_UNLOCKED


#ifdef __cplusplus
}
#endif
#endif /* THREADING_H_ */
