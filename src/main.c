/*
 * main.c
 *
 *  Created on: 10-Apr-2019
 *      Author: Rohan Dey
 */
#include "core_config.h"
#include "core_constant.h"
#include "system.h"

static void print_startup_message() {
	printf("\r\n\r\n");
	printf("\r\n");
	printf("  ______           __  __       _         ______                                           _    ");printf("\r\n");
	printf(" |  ____|         |  \\/  |     | |       |  ____|                                         | |   ");printf("\r\n");
	printf(" | |__   _ __ ___ | \\  / | __ _| |_ ___  | |__ _ __ __ _ _ __ ___   _____      _____  _ __| | __");printf("\r\n");
	printf(" |  __| | '_ ` _ \\| |\\/| |/ _` | __/ _ \\ |  __| '__/ _` | '_ ` _ \\ / _ \\ \\ /\\ / / _ \\| '__| |/ /");printf("\r\n");
	printf(" | |____| | | | | | |  | | (_| | ||  __/ | |  | | | (_| | | | | | |  __/\\ V  V / (_) | |  |   < ");printf("\r\n");
	printf(" |______|_| |_| |_|_|  |_|\\__,_|\\__\\___| |_|  |_|  \\__,_|_| |_| |_|\\___| \\_/\\_/ \\___/|_|  |_|\\_\\");printf("\r\n");
	printf("     _____                      _               _____       _       _   _                          ");printf("\r\n");
	printf("    |_   _|                    | |             / ____|     | |     | | (_)                         ");printf("\r\n");
	printf("      | |  __ _ _   _  ___  ___| |_ ___ _ __  | (___   ___ | |_   _| |_ _  ___  _ __  ___          ");printf("\r\n");
	printf("      | | / _` | | | |/ _ \\/ __| __/ _ \\ '__|  \\___ \\ / _ \\| | | | | __| |/ _ \\| '_ \\/ __|         ");printf("\r\n");
	printf("     _| || (_| | |_| |  __/\\__ \\ ||  __/ |     ____) | (_) | | |_| | |_| | (_) | | | \\__ \\         ");printf("\r\n");
	printf("    |_____\\__, |\\__,_|\\___||___/\\__\\___|_|    |_____/ \\___/|_|\\__,_|\\__|_|\\___/|_| |_|___/         ");printf("\r\n");
	printf("             | |                                                                                   ");printf("\r\n");
	printf("             |_| ");printf("\r\n");
	printf("\r\n");
}

void
#if defined (CONFIG_PLATFORM_ESP_IDF)
/* ESP IDF version: v4.1-dev-256-g9f145ff */
app_main
#elif defined (PLATFORM_HARMONY)
main
#else
main
#endif
() {
	/* Print the startup message so that we know our framework's code is starting from here */
	print_startup_message();

	/* core initialize */
	start_system();

	/* start the application */
	start_application();

	while (1) {
		TaskDelay(DELAY_1_MIN / TICK_RATE_TO_MS);
	}
}
