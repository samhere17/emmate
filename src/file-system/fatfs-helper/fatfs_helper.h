/*
 * fatfs_helper.h
 *
 *  Created on: 03-Jul-2019
 *      Author: Rohan Dey
 */

#ifndef FATFS_HELPER_H_
#define FATFS_HELPER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "sdmmc_host_core.h"
#include "fatfs_helper_platform.h"

typedef fat_sdmmc_mount_config_pf_t	fat_sdmmc_mount_config_core_t;

typedef struct {
	sdmmc_core_card_t* card;
} FatFSHelper;

/**
 * @brief Convenience function to get FAT filesystem on SD card registered
 *
 * This is an all-in-one function which does the following:
 * - initializes SDMMC driver with default configurations
 * - initializes SD card with default configurations
 * - mounts FAT partition on SD card using FATFS library
 *
 * @note 	Before calling this function make sure that internal pullup of SDMMC pins are set
 * 			SDMMC lines CMD, D0, D1, D2, D3 should have external 10k pull-ups. Internal pull-ups are not sufficient.
 * 			However, enabling internal pull-ups does make a difference some boards.
 *
 * @param partition_name     		Path where partition should be registered (e.g. "/mypartition")
 * @param format_if_mount_failed	If this is set to true, SD card will be partitioned and formatted in case when mounting fails.
 * @param max_open_files			Max number of open files
 *
 * @return
 *      - CORE_OK on success
 *      - CORE_ERR_INVALID_STATE if init_and_mount_fatfs_on_sdmmc was already called
 *      - CORE_ERR_NO_MEM if memory can not be allocated
 *      - CORE_ERR_INVALID_ARG if input params are incorrect, i.e. partition_name is NULL or max_open_files is 0
 *      - CORE_FAIL if partition can not be mounted
 */
core_err init_and_mount_fatfs_on_sdmmc(char *partition_name, bool format_if_mount_failed, int max_open_files);

/**
 * @brief Unmount FAT filesystem and release resources acquired using init_and_mount_fatfs_on_sdmmc()
 *
 * @return
 *      - CORE_OK on success
 *      - CORE_ERR_INVALID_STATE if init_and_mount_fatfs_on_sdmmc() hasn't been called
 */
core_err unmount_fatfs_sdmmc();

#ifdef __cplusplus
}
#endif

#endif /* FATFS_HELPER_H_ */
