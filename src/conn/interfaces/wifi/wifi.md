# Wi-Fi Module
## Overview
The Wi-Fi module exposes APIs to connect to any Wi-Fi Access Point.

## Features
1. Configure the Wi-Fi SSID & Password
2. Connect to any Wi-Fi Access Point
3. Get the Wi-Fi hardware's MAC address

## How to use the Wi-Fi Module from an Application

**Note: It is not recommended to use the Wi-Fi module's APIs directly from the user's application. The application should always use the [Connectivity Module APIs](../../README.md) for basic network related operations**

However, for advanced use cases these APIs may be called directly from the application. Please see [Wi-Fi examples](../../../../examples/conn/interfaces/wifi) for details

##### Header(s) to include

```
wifi.h
wifi_constants.h
```
##### Sample APIs