/*
 * File Name: ble_constant.h
 * File Path: /emmate/src/conn/ble/ble_constant.h
 * Description:
 *
 *  Created on: 27-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef BLE_CONSTANT_H_
#define BLE_CONSTANT_H_

#include "core_constant.h"

#define BLE_MAC_LEN			6								/**< Length in bytes of BLE MAC address */
#define BLE_PASSKEY_LEN		SOM_BT_PASSKEY_LEN

#define BLE_CONFIG_SUCCESS_RESPONSE		"SUCCESS\r\n"
#define BLE_CONFIG_FAILED_RESPONSE		"FAILED\r\n"

#define BLE_METADATA_RECV_SUCCESS		"OK\r\n"
#define BLE_METADATA_RECV_FAILED		"NOT OK\r\n"

#define BLE_TIMEOUT_MSG					"TIMEOUT\r\n"
#define BLE_AUTHENTICATION_FAILED		"INVALID ID\r\n"
#define BLE_DATA_RECV_FAILED			"INVALID_LENGTH\r\n"
#define BLE_DATA_RECV_SUCCESS			"DATA RECEIVED\r\n"

// Messages of connection step of Network
#define NET_CONNECTING_MSG 			"CONNECTING TO NETWORK\r\n"
#define NET_CONNECTION_FAIL_MSG		"NETWORK CONNECTION FAILED\r\n"
#define NET_CONNECTED_MSG 			"CONNECTED TO NETWORK\r\n"

#if 0

//Messages of connection step of AWS server
#define AWS_CONNICTING_MSG 				"AWS CONNECTING\r\n"
#define AWS_WRONG_CERTIFICATE_MSG		"AWS INVALID CERTIFICATE\r\n"
#define AWS_WRONG_ROOTCA_MSG			"AWS INVALID ROOTCA\r\n"
#define AWS_WRONG_PRIVATE_KEY_MSG		"AWS INVALID PRIVATE_KEY\r\n"
#define AWS_WRONG_URL_MSG				"AWS INVALID URL\r\n"
#define AWS_NETWORK_ERROR_MSG			"NETWORK ERROR\r\n"
#define AWS_CONNECTION_FAILED_MSG		"AWS CONNECTION FAILED\r\n"
#define AWS_CONNECTED_MSG 				"AWS CONNECTED\r\n"

//Messages of AWS shadow update steps
#define SHADOW_UPDATING_MSG 			"SHADOW UPDATING\r\n"
#define SHADOW_REJECTED_MSG 			"SHADOW REJECTED\r\n"
#define SHADOW_ACCEPTED_MSG 			"SHADOW ACCEPTED\r\n"
#define SHADOW_UPDATE_TIMEOUT_MSG 		"SHADOW UPDATE TIMEOUT\r\n"

#endif


#endif /* BLE_CONSTANT_H_ */
