/*
 * ble.c
 *
 *	File Path: /emmate/src/conn/ble/ble.c
 *
 *	Project Name: EmMate
 *	
 *  Created on: 16-Apr-2019
 *
 *      Author: Noyel Seth
 */

#include "ble_platform.h"
#include "ble.h"
#include "hmi.h"
#include "system_hmi_led_notification.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include <string.h>

#define TAG LTAG_CONN_BLE

static BleInfo ble_info;

static void ble_connect_event_handler(BLE_CONNECTION_STATUS status) {
	if (status == BLE_CONNECTED) {
		CORE_LOGI(TAG, "BLE is connected");
		show_ble_connected_notification();
	} else {
		CORE_LOGI(TAG, "BLE is disconnected");
		show_ble_advertising_notification();
	}
}

/*******************************************************************************************/

/**
 * @brief
 *
 * @return
 *
 **/
core_err set_ble_adv_name(char* pble_adv) {
	core_err err = CORE_FAIL;
	if (NULL != pble_adv) {
		if (strlen(pble_adv) > ble_platform_get_ble_max_name_size()) {
			return CORE_ERR_INVALID_SIZE;
		} else {
			err = ble_platform_set_ble_adv_name(pble_adv);
		}
	}
	return err;
}

/**
 * @brief	Initialize the  BLE Peripheral
 *
 * @return
 *
 **/
core_err init_ble() {
	core_err err = CORE_FAIL;
	err = ble_platform_config_ble();
	ble_platform_set_connect_event_handler(ble_connect_event_handler);
	show_ble_advertising_notification();
	return err;
}

/**
 * @brief	De-initialize BLE Peripheral
 *
 * @return
 *
 **/
core_err deinit_ble() {
	core_err err = CORE_FAIL;
	err = ble_platform_deconfig_ble();
	return err;
}

/**
 * @brief	Enabling BLE Peripheral
 *
 * @return
 *
 **/
core_err enable_ble() {
	core_err err = CORE_FAIL;
	err = ble_platform_enable_ble(ble_info.ble_mac);
	return err;
}

/**
 * @brief	Disabling BLE Peripheral
 *
 * @return
 *
 **/
core_err disable_ble() {
	core_err err = CORE_FAIL;
	err = ble_platform_disable_ble();
	return err;
}

/**
 * @brief	Get Connection Status
 *
 * @return
 *
 **/
BLE_CONNECTION_STATUS isbleconnected() {
	return ble_platform_isble_connected();
}

void get_ble_mac(uint8_t *ble_mac) {
	if (ble_info.ble_mac != NULL) {
		memcpy(ble_mac, ble_info.ble_mac, BLE_MAC_LEN);
	} else {
		CORE_LOGE(TAG, "Unable to get BLE MAC");
		memset(ble_mac, 0, BLE_MAC_LEN);
	}
}

