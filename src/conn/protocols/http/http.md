# HTTP Client Module
## Overview
The HTTP Client module provides API for making HTTP/S requests from an application. To use the APIs the following steps have to be followed:
1. First we must initialize the HTTP client module by calling the API `init_http_client()` and pass an object of `HttpClientData`
2. Next the API `perform_http_client_process()` must be called with the same `HttpClientData` object which was passed to `init_http_client()`. This function performs all operations, from opening the connection, sending data, downloading data and closing the connection if necessary. This function performs its job and blocks the current task until it is done. While this function is performing the HTTP tasks all events generated in between are invoked into the callback of type `http_res_recv_handle_cb`, which was set before the `init_http_client()` was called.
3. Generally the applicatino should wait in a while loop after the `perform_http_client_process()` API is called unless the event `HTTP_CLIENT_EVENT_ON_FINISH` is received in the callback.
4. To get the HTTP status code `get_http_status_code()` must be called.
5. Finally `deinit_http_client()` API must be called to cleanup the resources. This is the last function to be called. It will close the connection (if any) and free up all the memory allocated by the HTTP client. 

## Features
1. Supports HTTP and HTTPS
2. Both GET and POST requests can be made
3. Multiple connections can be opened. Each connection must have its own handle of type `HttpClientData`

## How to use the HTTP Module from an Application

For a HTTP request consider the following code snippet. Please see the [HTTP examples](../../../../examples/conn/protocols/http) to get a better idea

```
static uint8_t event_stat = false;
void my_http_response_handler(uint8_t http_event_stat, char* data, size_t data_len) {
	event_stat = http_event_stat;
	switch (http_event_stat) {
	case HTTP_CLIENT_EVENT_ON_DATA: {
		// Copy your response data to your buffer here	
		break;
	}
	case HTTP_CLIENT_EVENT_ON_FINISH: {
		break;
	}
	case HTTP_CLIENT_EVENT_DISCONNECTED: {
		break;
	}
	}
}

/* Initialize a http client data to be used for doing a http request */
HttpClientData http_cli_data;
bzero(&http_cli_data, sizeof(HttpClientData));

/* Set necessary configurations */
http_cli_data.http_cnf_data.url = MY_POST_URL;
http_cli_data.http_cnf_data.cert_pem = NULL;	// For a HTTPS request the certificate must be passed here
http_cli_data.http_cnf_data.host_server = MY_POST_HOST;
http_cli_data.http_cnf_data.port = MY_HOST_PORT;
http_cli_data.http_cnf_data.method = HTTP_CLIENT_METHOD_POST;
http_cli_data.http_cnf_data.user_agent = "core-embed/1.0";
http_cli_data.http_cnf_data.content_type = CONTENT_TYPE_APPLICATION_JSON;
http_cli_data.http_cnf_data.post_data = data_buf;
http_cli_data.http_cnf_data.post_data_len = data_len;
http_cli_data.http_ctrl.http_recv_handler = my_http_response_handler;

/* Initialize a http client instance */
ret = init_http_client(&http_cli_data);
if (ret != CORE_OK) {
	CORE_LOGE(TAG, "HTTP client init failed!");
	// Handle error
}

/* Perform the http process */
ret = perform_http_client_process(&http_cli_data);
if (ret != CORE_OK) {
	CORE_LOGE(TAG, "HTTP perform failed");
	// Handle error
}

/* Wait for the http event to finish */
while (event_stat != HTTP_CLIENT_EVENT_ON_FINISH) {
	CORE_LOGD(TAG, "waiting for http action to be finished");
	TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
}

/* Check the http status code */
uint16_t http_stat = get_http_status_code(&http_cli_data);
CORE_LOGI(TAG, "http status: %d", http_stat);
if (http_stat == HTTP_OK_RES_CODE) {
	CORE_LOGI(TAG, "The HTTP POST was successful");
}

/* Our http tasks are done, now deinitialize the http client instance */
deinit_http_client(&http_cli_data);
```

##### Header(s) to include
```
http_client_core.h
http_constant.h
