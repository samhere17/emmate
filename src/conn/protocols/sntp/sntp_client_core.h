/*
 * File Name: sntp_client_platform.h
 * File Path: /emmate/src/conn-proto/sntp-client/sntp_client_core.h
 * Description:
 *
 *  Created on: 27-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef SNTP_CLIENT_CORE_H_
#define SNTP_CLIENT_CORE_H_

#include "core_config.h"
#include "core_common.h"
#include "core_error.h"

#include "sntp_client_platform.h"

/**
 * @brief initialize the SNTP Client
 *
 **/
void initialize_sntp_core(void);

#endif /* SNTP_CLIENT_CORE_H_ */
