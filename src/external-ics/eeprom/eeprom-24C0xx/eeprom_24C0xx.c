/*
 * File Name: eeprom_24C0xx.c
 * File Path: /emmate/src/external-ics/eeprom/eeprom-24C0xx/eeprom_24C0xx.c
 * Description:
 *
 *  Created on: 20-Jun-2019
 *      Author: Noyel Seth
 */

#include "eeprom_24C0xx.h"

/**
 * The EmMate Framework has integrated external ICs support Library
 * Application developer can use this Library directly by using EmMate Framework.
 * For using this Library we need to defined 'CONFIG_USE_EEPROM_24C0XX' before include the eeprom_24C0xx.h
 */
#ifdef CONFIG_USE_EEPROM_24C0XX

/**
 * The integrated external ICs support Library eeprom_24C0xx use two GPIO for complete the eeprom_24C0xx process
 * For eeprom_24C0xx library, I2C control external EEPROM ICs
 * SDA GPIO need to define into Application's thing.h as:
 * #define THING_EEPROM_24C0XX_SDA_GPIO 	SOM_PIN_x
 */
#if THING_EEPROM_24C0XX_SDA_GPIO
#define EEPROM_24C0XX_SDA_GPIO		THING_EEPROM_24C0XX_SDA_GPIO
#else
#error "THING_EEPROM_24C0XX_SDA_GPIO is not defined. Please set this MACRO in thing.h"

#endif

/**
 * The integrated external ICs support Library eeprom_24C0xx use two GPIO for complete the eeprom_24C0xx process
 * For eeprom_24C0xx library, I2C control external EEPROM ICs
 * SCL GPIO need to define into Application's thing.h as:
 * #define THING_EEPROM_24C0XX_SCL_GPIO 	SOM_PIN_x
 */
#if THING_EEPROM_24C0XX_SCL_GPIO
#define EEPROM_24C0XX_SCL_GPIO		THING_EEPROM_24C0XX_SCL_GPIO
#else
#error "THING_EEPROM_24C0XX_SCL_GPIO is not defined. Please set this MACRO in thing.h"

#endif

#define TAG "eeprom_24C0xx_lib"

#define CLK_SPEED      100000         // clock speed 100kHz

I2C_CORE_DRV_HANDLE eeprom_24C0xx_drv_handle = NULL;

/**
 *
 */
core_err eeprom_24C0xx_init() {
	core_err ret = CORE_FAIL;
	eeprom_24C0xx_drv_handle = init_i2c_core_master(I2C_CORE_NUM_1, EEPROM_24C0XX_SDA_GPIO, EEPROM_24C0XX_SCL_GPIO,
	CLK_SPEED, 1); // initialize I2C eeprom_24C0xx peripheral
	if (eeprom_24C0xx_drv_handle == NULL) {
		CORE_LOGE(TAG, "Failed to initialize eeprom_24C0xx");
		ret = CORE_FAIL;
	} else {
		ret = CORE_OK;
	}
	return ret;
}

/**
 *
 */
core_err eeprom_24C0xx_write(uint8_t eeprom_slave_addr, uint8_t eeprom_wr_addr, char* data_wr, uint8_t sizeof_data_wr) {
	core_err ret = CORE_FAIL;
	if (data_wr != NULL) {
		if (eeprom_24C0xx_drv_handle != NULL) {
			for (int i = 0; i < sizeof_data_wr; i++) {
				ret = i2c_core_master_write_slave(eeprom_24C0xx_drv_handle, eeprom_slave_addr, eeprom_wr_addr + i,
						(uint8_t*) &data_wr[i], 1); // Data write
				if (ret != CORE_OK) {
					CORE_LOGE(TAG, "Failed to write into eeprom_24C0xx address: 0x%x", eeprom_slave_addr);
					return ret;
				}
				TaskDelay(DELAY_10_MSEC / TICK_RATE_TO_MS);
			}
		} else {
			return CORE_PERIPHERAL_NOT_INITIALIZE;
		}
	} else {
		return CORE_ERR_INVALID_ARG;
	}

	return ret;
}

/**
 *
 */
core_err eeprom_24C0xx_read(uint8_t eeprom_slave_addr, uint8_t eeprom_rd_addr, char* data_rd, uint8_t sizeof_data_rd) {
	core_err ret = CORE_FAIL;
	if (data_rd != NULL) {
		if (eeprom_24C0xx_drv_handle != NULL) {
			for (int i = 0; i < sizeof_data_rd; i++) {
				ret = i2c_core_master_read_slave(eeprom_24C0xx_drv_handle, eeprom_slave_addr, eeprom_rd_addr + i,
						(uint8_t*) &data_rd[i], 1, 0); // Data read
				if (ret != CORE_OK) {
					CORE_LOGE(TAG, "Failed to read from eeprom_24C0xx address: 0x%x", eeprom_slave_addr);
					return ret;
				}
				TaskDelay(DELAY_10_MSEC / TICK_RATE_TO_MS);
			}
		} else {
			return CORE_PERIPHERAL_NOT_INITIALIZE;
		}
	} else {
		return CORE_ERR_INVALID_ARG;
	}

	return ret;
}

/**
 *
 */
core_err eeprom_24C0xx_deinit() {
	core_err ret = CORE_FAIL;
	if (eeprom_24C0xx_drv_handle != NULL) {
		ret = deinit_i2c_core_master(eeprom_24C0xx_drv_handle); // de-initialize I2C eeprom_24C0xx peripheral
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to de-initialize eeprom_24C0xx");
			ret = CORE_FAIL;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

#endif /* CONFIG_USE_EEPROM_24C0XX */
