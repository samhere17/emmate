/*
 * File Name: eeprom_24C0xx.h
 * File Path: /emmate/src/external-ics/eeprom/eeprom-24C0xx/eeprom_24C0xx.h
 * Description:
 *
 *  Created on: 20-Jun-2019
 *      Author: Noyel Seth
 */

#ifndef EEPROM_24C0XX_H_
#define EEPROM_24C0XX_H_


#include "external_ics.h"

#ifdef CONFIG_USE_EEPROM_24C0XX

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"
#include "i2c_core.h"

/**
 * @brief	Initialize the eeprom_24C0xx process
 *
 * @return
 *     - CORE_OK 						Success
 *     - CORE_FAIL 						Driver install error
 */
core_err eeprom_24C0xx_init();


/**
 * @brief write buffer to eeprom_24C0xx peripheral
 *
 * @param eeprom_slave_addr		slave address of eeprom_24C0xx peripheral
 * @param eeprom_wr_addr		eeprom_24C0xx data store address
 * @param data_wr[in]			write data buffer
 * @param sizeof_data_wr		write data buffer size
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			eeprom_24C0xx not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		eeprom_24C0xx driver not initialize
 */
core_err eeprom_24C0xx_write(uint8_t eeprom_slave_addr, uint8_t eeprom_wr_addr, char* data_wr, uint8_t sizeof_data_wr);


/**
 * @brief read buffer from eeprom_24C0xx peripheral
 *
 * @param eeprom_slave_addr		slave address of eeprom_24C0xx peripheral
 * @param eeprom_rd_addr		eeprom_24C0xx data fetch address
 * @param data_rd[out]			read data buffer
 * @param sizeof_data_rd		read data buffer size
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			eeprom_24C0xx not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		eeprom_24C0xx driver not initialize
 */
core_err eeprom_24C0xx_read(uint8_t eeprom_slave_addr, uint8_t eeprom_rd_addr, char* data_rd, uint8_t sizeof_data_rd);


/**
 * @brief de-initialize the eeprom_24C0xx process
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_FAIL 							Driver delete error
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		eeprom_24C0xx driver not initialize
 */
core_err eeprom_24C0xx_deinit();


#endif /* CONFIG_USE_EEPROM_24C0XX */


#endif /* EEPROM_24C0XX_H_ */
