/*
 * File Name: eeprom_24AA02UID.h
 * File Path: /emmate/src/external-ics/eeprom/eeprom_24AA02UID/eeprom_24AA02UID.h
 * Description:
 *
 *  Created on: 21-Jun-2019
 *      Author: Noyel Seth
 */

#ifndef EEPROM_24AA02UID_H_
#define EEPROM_24AA02UID_H_

#include "external_ics.h"

/**
 * The EmMate Framework has integrated external ICs support Library
 * Application developer can use this Library directly by using EmMate Framework.
 * For using this Library we need to defined 'CONFIG_USE_EEPROM_24AA02UID' before include the eeprom_24AA02UID.h
 */
#if CONFIG_USE_EEPROM_24AA02UID

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"
#include "i2c_core.h"

/******************************************** Init & Deinit 24AA02UID ******************************************************************/
/**
 * @brief	Initialize the eeprom_24AA02UID process
 *
 * @return
 *     - CORE_OK 						Success
 *     - CORE_FAIL 						Driver install error
 */
core_err eeprom_24AA02UID_init();

/**
 * @brief de-initialize the eeprom_24AA02UID process
 *
 * @return
 *     - CORE_OK 						Success
 *     - CORE_FAIL 						Driver delete error
 */
core_err eeprom_24AA02UID_deinit();

/******************************************* Write 24AA02UID EEPROM *******************************************************************/
/**
 * @brief write buffer to eeprom_24AA02UID peripheral
 *
 * @param eeprom_wr_addr		eeprom_24AA02UID data store address
 * @param data_wr[in]				write data buffer
 * @param sizeof_data_wr		write data buffer size
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			eeprom_24AA02UID not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		eeprom_24AA02UID driver not initialize
 */
core_err eeprom_24AA02UID_write(uint8_t eeprom_wr_addr, char* data_wr, uint8_t sizeof_data_wr);

/*************************************** Read 24AA02UID EEPROM ***********************************************************************/
/**
 * @brief read buffer from eeprom_24AA02UID peripheral
 *
 * @param eeprom_rd_addr		eeprom_24AA02UID data fetch address
 * @param data_rd[out]			read data buffer
 * @param sizeof_data_rd		read data buffer size
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			eeprom_24AA02UID not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		eeprom_24AA02UID driver not initialize
 */
core_err eeprom_24AA02UID_read(uint8_t eeprom_rd_addr, char* data_rd, uint8_t sizeof_data_rd);

/************************************* Read 24AA02UID Manufacturer & Device Code *************************************************************************/
/**
 * @brief read 24AA02UID Manufacturer code from eeprom_24AA02UID peripheral
 *
 * @param mfg_code[out]				mfg_code read data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			eeprom_24AA02UID not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		eeprom_24AA02UID driver not initialize
 */
core_err eeprom_24AA02UID_read_mfg_code(uint8_t* mfg_code);

/**
 * @brief read 24AA02UID Device code from eeprom_24AA02UID peripheral
 *
 * @param dev_code[out]				dev_code read data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			eeprom_24AA02UID not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		eeprom_24AA02UID driver not initialize
 */
core_err eeprom_24AA02UID_read_device_code(uint8_t* dev_code);

/*********************************************** Read UID Bit Wise ***************************************************************/
/**
 * @brief read 32bit UID code from eeprom_24AA02UID peripheral
 *
 * @param uid_code[out]				uid_code read data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			eeprom_24AA02UID not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		eeprom_24AA02UID driver not initialize
 */
core_err eeprom_24AA02UID_read_32bit_uid_code(uint8_t* uid_code);

/**
 * @brief read 48bit UID code from eeprom_24AA02UID peripheral
 *
 * @param uid_code[out]				uid_code read data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			eeprom_24AA02UID not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		eeprom_24AA02UID driver not initialize
 */
core_err eeprom_24AA02UID_read_48bit_uid_code(uint8_t* uid_code);

/**
 * @brief read 64bit UID code from eeprom_24AA02UID peripheral
 *
 * @param uid_code[out]				uid_code read data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			eeprom_24AA02UID not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		eeprom_24AA02UID driver not initialize
 */
core_err eeprom_24AA02UID_read_64bit_uid_code(uint8_t* uid_code);

/**
 * @brief read 128bit UID code from eeprom_24AA02UID peripheral
 *
 * @param uid_code[out]				uid_code read data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			eeprom_24AA02UID not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		eeprom_24AA02UID driver not initialize
 */
core_err eeprom_24AA02UID_read_128bit_uid_code(uint8_t* uid_code);

/**
 * @brief read 256bit UID code from eeprom_24AA02UID peripheral
 *
 * @param uid_code[out]				uid_code read data
 *
 * @return
 *     - CORE_OK 							Success
 *     - CORE_ERR_INVALID_ARG 				Parameter error
 *     - CORE_FAIL 							Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 			eeprom_24AA02UID not installed
 *     - CORE_ERR_TIMEOUT 					Operation timeout because the bus is busy.
 *     - CORE_PERIPHERAL_NOT_INITIALIZE		eeprom_24AA02UID driver not initialize
 */
core_err eeprom_24AA02UID_read_256bit_uid_code(uint8_t* uid_code);

#endif /* CONFIG_USE_EEPROM_24AA02UID */

#endif /* EEPROM_24AA02UID_H_ */
