/*
 * File Name: eeprom_24AA02UID.c
 * File Path: /emmate/src/external-ics/eeprom/eeprom_24AA02UID/eeprom_24AA02UID.c
 * Description:
 *
 *  Created on: 21-Jun-2019
 *      Author: Noyel Seth
 */

#include "eeprom_24AA02UID.h"

/**
 * The EmMate Framework has integrated external ICs support Library
 * Application developer can use this Library directly by using EmMate Framework.
 * For using this Library we need to define 'CONFIG_USE_EEPROM_24AA02UID' before including the eeprom_24AA02UID.h
 */
#if CONFIG_USE_EEPROM_24AA02UID

/*
 * 24AA02UID IC Doc:
 * https://ww1.microchip.com/downloads/en/DeviceDoc/20005202A.pdf
 *
 */

/**
 * The integrated external ICs support Library eeprom_24AA02UID use two GPIO for complete the eeprom_24AA02UID process
 * For eeprom_24AA02UID library, I2C control external EEPROM  ICs
 * SDA GPIO need to define into Application's thing.h as:
 * #define THING_EEPROM_24AA02UID_SDA_GPIO 	SOM_PIN_x
 */
#if THING_EEPROM_24AA02UID_SDA_GPIO
#define EEPROM_24AA02UID_SDA_GPIO		THING_EEPROM_24AA02UID_SDA_GPIO
#else
#error "THING_EEPROM_24AA02UID_SDA_GPIO is not defined. Please set this MACRO in thing.h"

#endif

/**
 * The integrated external ICs support Library eeprom_24AA02UID use two GPIO for complete the eeprom_24AA02UID process
 * For eeprom_24AA02UID library, I2C control external EEPROM ICs
 * SCL GPIO need to define into Application's thing.h as:
 * #define THING_EEPROM_24AA02UID_SCL_GPIO 	SOM_PIN_x
 */
#if THING_EEPROM_24AA02UID_SCL_GPIO
#define EEPROM_24AA02UID_SCL_GPIO		THING_EEPROM_24AA02UID_SCL_GPIO
#else
#error "THING_EEPROM_24AA02UID_SCL_GPIO is not defined. Please set this MACRO in thing.h"

#endif

#define TAG "eeprom_24AA02UID_lib"

#define CLK_SPEED      100000         // clock speed 100kHz

#define EEPROM_24AA02UID_SLAVE_ADDRESS	0xA0 // 0b10100000

#define EEPROM_24AA02UID_ROM_START_ARRDESS	0x00
#define EEPROM_24AA02UID_ROM_END_ARRDESS	0x80

#define EEPROM_24AA02UID_MANUFACTURER_CODE_ADDRESS	0XFA
#define EEPROM_24AA02UID_DEVICE_CODE_ADDRESS		0XFB

#define EEPROM_24AA02UID_32BIT_UID_CODE_ADDRESS		0xFC
#define EEPROM_24AA02UID_48BIT_UID_CODE_ADDRESS 	0xFA
#define EEPROM_24AA02UID_64BIT_UID_CODE_ADDRESS 	0xF8
#define EEPROM_24AA02UID_128BIT_UID_CODE_ADDRESS 	0xF0
#define EEPROM_24AA02UID_256BIT_UID_CODE_ADDRESS 	0xE0

I2C_CORE_DRV_HANDLE eeprom_24aa02uid_drv_handle = NULL;

/******************************************** Init & Deinit 24AA02UID ******************************************************************/

core_err eeprom_24AA02UID_init() {
	core_err ret = CORE_FAIL;
	eeprom_24aa02uid_drv_handle = init_i2c_core_master(I2C_CORE_NUM_1, EEPROM_24AA02UID_SDA_GPIO,
			EEPROM_24AA02UID_SCL_GPIO, CLK_SPEED, 1); // initialize I2C eeprom_24AA02UID peripheral
	if (eeprom_24aa02uid_drv_handle == NULL) {
		CORE_LOGE(TAG, "Failed to initialize eeprom_24AA02UID");
		ret = CORE_FAIL;
	} else {
		ret = CORE_OK;
	}
	return ret;
}

core_err eeprom_24AA02UID_deinit() {
	core_err ret = CORE_FAIL;
	if (eeprom_24aa02uid_drv_handle != NULL) {
		ret = deinit_i2c_core_master(eeprom_24aa02uid_drv_handle); // de-initialize I2C eeprom_24AA02UID peripheral
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to de-initialize eeprom_24AA02UID");
			ret = CORE_FAIL;
		}
	} else {
		ret = CORE_FAIL;
	}
	return ret;
}

/******************************************* Write 24AA02UID EEPROM *******************************************************************/

core_err eeprom_24AA02UID_write(uint8_t eeprom_wr_addr, char* data_wr, uint8_t sizeof_data_wr) {
	core_err ret = CORE_FAIL;

	uint8_t expected_write_byte_length = EEPROM_24AA02UID_ROM_END_ARRDESS - eeprom_wr_addr;

	if (data_wr != NULL) {

		if (eeprom_wr_addr >= EEPROM_24AA02UID_ROM_END_ARRDESS) {
			CORE_LOGE(TAG,
					"Failed to write into eeprom_24AA02UID: Invalid ROM Address, Valid ROM Address range [0x%x - 0x%x]",
					EEPROM_24AA02UID_ROM_START_ARRDESS, EEPROM_24AA02UID_ROM_END_ARRDESS);
			return CORE_ERR_INVALID_ARG;
		}

		if (sizeof_data_wr > expected_write_byte_length) {
			CORE_LOGE(TAG, "Failed to write into eeprom_24AA02UID: Invalid Write-Data length");
			return CORE_ERR_INVALID_ARG;
		}

		if (eeprom_24aa02uid_drv_handle != NULL) {
			for (int i = 0; i < sizeof_data_wr; i++) {
				ret = i2c_core_master_write_slave(eeprom_24aa02uid_drv_handle, EEPROM_24AA02UID_SLAVE_ADDRESS,
						eeprom_wr_addr + i, (uint8_t*) &data_wr[i], 1); // Data write
				if (ret != CORE_OK) {
					CORE_LOGE(TAG, "Failed to write into eeprom_24AA02UID");
					return ret;
				}
				TaskDelay(DELAY_10_MSEC / TICK_RATE_TO_MS);
			}
		} else {
			return CORE_PERIPHERAL_NOT_INITIALIZE;
		}
	} else {
		return CORE_ERR_INVALID_ARG;
	}

	return ret;
}

/*************************************** Read 24AA02UID EEPROM ***********************************************************************/

core_err eeprom_24AA02UID_read(uint8_t eeprom_rd_addr, char* data_rd, uint8_t sizeof_data_rd) {
	core_err ret = CORE_FAIL;

	uint8_t expected_read_byte_length = EEPROM_24AA02UID_ROM_END_ARRDESS - eeprom_rd_addr;

	if (data_rd != NULL) {

		if (eeprom_rd_addr >= EEPROM_24AA02UID_ROM_END_ARRDESS) {
			CORE_LOGE(TAG,
					"Failed to read from eeprom_24AA02UID: Invalid ROM Address, Valid ROM Address range [0x%x - 0x%x]",
					EEPROM_24AA02UID_ROM_START_ARRDESS, EEPROM_24AA02UID_ROM_END_ARRDESS);
			return CORE_ERR_INVALID_ARG;
		}

		if (sizeof_data_rd > expected_read_byte_length) {
			CORE_LOGE(TAG, "Failed to write into eeprom_24AA02UID: Invalid Write-Data length");
			return CORE_ERR_INVALID_ARG;
		}

		if (eeprom_24aa02uid_drv_handle != NULL) {
			for (int i = 0; i < sizeof_data_rd; i++) {
				ret = i2c_core_master_read_slave(eeprom_24aa02uid_drv_handle, EEPROM_24AA02UID_SLAVE_ADDRESS,
						eeprom_rd_addr + i, (uint8_t*) &data_rd[i], 1, 0); // Data write
				if (ret != CORE_OK) {
					CORE_LOGE(TAG, "Failed to write into eeprom_24AA02UID");
					return ret;
				}
				TaskDelay(DELAY_10_MSEC / TICK_RATE_TO_MS);
			}
		} else {
			return CORE_PERIPHERAL_NOT_INITIALIZE;
		}
	} else {
		return CORE_ERR_INVALID_ARG;
	}

	return ret;
}

/************************************* Read 24AA02UID Manufacturer & Device Code *************************************************************************/

core_err eeprom_24AA02UID_read_mfg_code(uint8_t* mfg_code) {
	core_err ret = CORE_FAIL;
	if (mfg_code != NULL) {
		if (eeprom_24aa02uid_drv_handle != NULL) {
			ret = i2c_core_master_read_slave(eeprom_24aa02uid_drv_handle, EEPROM_24AA02UID_SLAVE_ADDRESS,
			EEPROM_24AA02UID_MANUFACTURER_CODE_ADDRESS, mfg_code, 1, 0); // MANUFACTURER_CODE READ
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "Failed to read MANUFACTURER_CODE from eeprom_24AA02UID");
				return ret;
			}
		} else {
			return CORE_PERIPHERAL_NOT_INITIALIZE;
		}
	} else {
		return CORE_ERR_INVALID_ARG;
	}
	return ret;
}

/**
 *
 */
core_err eeprom_24AA02UID_read_device_code(uint8_t* dev_code) {
	core_err ret = CORE_FAIL;
	if (dev_code != NULL) {
		if (eeprom_24aa02uid_drv_handle != NULL) {
			ret = i2c_core_master_read_slave(eeprom_24aa02uid_drv_handle, EEPROM_24AA02UID_SLAVE_ADDRESS,
			EEPROM_24AA02UID_DEVICE_CODE_ADDRESS, dev_code, 1, 0); // DEVICE_CODE READ
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "Failed to read DEVICE_CODE from eeprom_24AA02UID");
				return ret;
			}
		} else {
			return CORE_PERIPHERAL_NOT_INITIALIZE;
		}
	} else {
		return CORE_ERR_INVALID_ARG;
	}
	return ret;
}

/*********************************************** Read UID Bit Wise ***************************************************************/

core_err eeprom_24AA02UID_read_32bit_uid_code(uint8_t* uid_code) {
	core_err ret = CORE_FAIL;
	if (uid_code != NULL) {
		if (eeprom_24aa02uid_drv_handle != NULL) {
			ret = i2c_core_master_read_slave(eeprom_24aa02uid_drv_handle, EEPROM_24AA02UID_SLAVE_ADDRESS,
			EEPROM_24AA02UID_32BIT_UID_CODE_ADDRESS, uid_code, 4, 0); // 32BIT_UID_CODE READ
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "Failed to read 32BIT_UID_CODE from eeprom_24AA02UID");
				return ret;
			}
		} else {
			return CORE_PERIPHERAL_NOT_INITIALIZE;
		}
	} else {
		return CORE_ERR_INVALID_ARG;
	}
	return ret;
}

/**
 *
 */
core_err eeprom_24AA02UID_read_48bit_uid_code(uint8_t* uid_code) {
	core_err ret = CORE_FAIL;
	if (uid_code != NULL) {
		if (eeprom_24aa02uid_drv_handle != NULL) {
			ret = i2c_core_master_read_slave(eeprom_24aa02uid_drv_handle, EEPROM_24AA02UID_SLAVE_ADDRESS,
			EEPROM_24AA02UID_48BIT_UID_CODE_ADDRESS, uid_code, 6, 0); // 48BIT_UID_CODE READ
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "Failed to read 48BIT_UID_CODE from eeprom_24AA02UID");
				return ret;
			}
		} else {
			return CORE_PERIPHERAL_NOT_INITIALIZE;
		}
	} else {
		return CORE_ERR_INVALID_ARG;
	}
	return ret;
}

/**
 *
 */
core_err eeprom_24AA02UID_read_64bit_uid_code(uint8_t* uid_code) {
	core_err ret = CORE_FAIL;
	if (uid_code != NULL) {
		if (eeprom_24aa02uid_drv_handle != NULL) {
			ret = i2c_core_master_read_slave(eeprom_24aa02uid_drv_handle, EEPROM_24AA02UID_SLAVE_ADDRESS,
			EEPROM_24AA02UID_64BIT_UID_CODE_ADDRESS, uid_code, 8, 0); // 64BIT_UID_CODE READ
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "Failed to read 64BIT_UID_CODE from eeprom_24AA02UID");
				return ret;
			}
		} else {
			return CORE_PERIPHERAL_NOT_INITIALIZE;
		}
	} else {
		return CORE_ERR_INVALID_ARG;
	}
	return ret;
}

/**
 *
 */
core_err eeprom_24AA02UID_read_128bit_uid_code(uint8_t* uid_code) {
	core_err ret = CORE_FAIL;
	if (uid_code != NULL) {
		if (eeprom_24aa02uid_drv_handle != NULL) {
			ret = i2c_core_master_read_slave(eeprom_24aa02uid_drv_handle, EEPROM_24AA02UID_SLAVE_ADDRESS,
			EEPROM_24AA02UID_128BIT_UID_CODE_ADDRESS, uid_code, 16, 0); // 128BIT_UID_CODE READ
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "Failed to read 128BIT_UID_CODE from eeprom_24AA02UID");
				return ret;
			}
		} else {
			return CORE_PERIPHERAL_NOT_INITIALIZE;
		}
	} else {
		return CORE_ERR_INVALID_ARG;
	}
	return ret;
}

/**
 *
 */
core_err eeprom_24AA02UID_read_256bit_uid_code(uint8_t* uid_code) {
	core_err ret = CORE_FAIL;
	if (uid_code != NULL) {
		if (eeprom_24aa02uid_drv_handle != NULL) {
			ret = i2c_core_master_read_slave(eeprom_24aa02uid_drv_handle, EEPROM_24AA02UID_SLAVE_ADDRESS,
			EEPROM_24AA02UID_256BIT_UID_CODE_ADDRESS, uid_code, 32, 0); // 256BIT_UID_CODE READ
			if (ret != CORE_OK) {
				CORE_LOGE(TAG, "Failed to read 256BIT_UID_CODE from eeprom_24AA02UID");
				return ret;
			}
		} else {
			return CORE_PERIPHERAL_NOT_INITIALIZE;
		}
	} else {
		return CORE_ERR_INVALID_ARG;
	}
	return ret;
}

#endif /* CONFIG_USE_EEPROM_24AA02UID */
