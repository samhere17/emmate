/*
 * File Name: ds1307.c
 * File Path: /emmate/src/external-ics/rtc/ds1307/ds1307.c
 * Description:
 *
 *  Created on: 20-Jun-2019
 *      Author: Noyel Seth
 */

#include "ds1307.h"

/**
 * The EmMate Framework has integrated external ICs support Library
 * Application developer can use this Library directly by using EmMate Framework.
 * For using this Library we need to defined 'CONFIG_USE_DS1307' before include the ds1307.h
 */
#ifdef CONFIG_USE_DS1307

/**
 * The integrated external ICs support Library ds1307 use two GPIO for complete the ds1307 process
 * For ds1307 library, I2C control external RTC IC
 * SDA GPIO need to define into Application's thing.h as:
 * #define THING_DS1307_SDA_GPIO 	SOM_PIN_x
 */
#if THING_DS1307_SDA_GPIO
#define DS1307_SDA_GPIO		THING_DS1307_SDA_GPIO
#else
#error "THING_DS1307_SDA_GPIO is not defined. Please set this MACRO in thing.h as: #define THING_DS1307_SDA_GPIO 	SOM_PIN_x"

#endif

/**
 * The integrated external ICs support Library ds1307 use two GPIO for complete the ds1307 process
 * For ds1307 library, I2C control external RTC IC
 * SCL GPIO need to define into Application's thing.h as:
 * #define THING_DS1307_SCL_GPIO 	SOM_PIN_x
 */
#if THING_DS1307_SCL_GPIO
#define DS1307_SCL_GPIO		THING_DS1307_SCL_GPIO
#else
#error "THING_DS1307_SCL_GPIO is not defined. Please set this MACRO in thing.h"

#endif

#define TAG "ds1307_lib"

#define CLK_SPEED      100000         // clock speed 100kHz

#define DS1307_SLAVE_ADDRESS		0xD0     // Slave address of RTC-DS1307 0b11010000

#define DS1307_SECOND_ADDRESS    	0x00     // SECOND command address
#define DS1307_MINUTE_ADDRESS    	0x01     // MINUTE command address
#define DS1307_HOUR_ADDRESS    		0x02     // HOUR command address
#define DS1307_DAY_ADDRESS    		0x03     // DAY command address
#define DS1307_DATE_ADDRESS    		0x04     // DATE command address
#define DS1307_MONTH_ADDRESS    	0x05     // MONTH command address
#define DS1307_YEAR_ADDRESS    		0x06     // YEAR command address
#define DS1307_SQWE_ADDRESS    		0x07     // SQEW command address

I2C_CORE_DRV_HANDLE rtc_ds1307_drv_handle = NULL;

/**
 *
 */
core_err ds1307_init() {
	core_err ret = CORE_FAIL;
	rtc_ds1307_drv_handle = init_i2c_core_master(I2C_CORE_NUM_1, DS1307_SDA_GPIO, DS1307_SCL_GPIO, CLK_SPEED, 1); // initialize I2C ds1307 peripheral
	if (rtc_ds1307_drv_handle == NULL) {
		CORE_LOGE(TAG, "Failed to initialize ds1307");
		ret = CORE_FAIL;
	} else {
		ret = CORE_OK;
	}
	return ret;
}

/**
 *
 */
core_err ds1307_deinit() {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = deinit_i2c_core_master(rtc_ds1307_drv_handle); // de-initialize I2C ds1307 peripheral
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to de-initialize ds1307");
			ret = CORE_FAIL;
		}
	} else {
		ret = CORE_FAIL;
	}
	return ret;
}

/******************************** Write & Read SECOND ********************************/
/**
 *
 */
core_err ds1307_write_sec(uint8_t* data_wr) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_write_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_SECOND_ADDRESS, data_wr,
				1); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to write SECONDS into DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/**
 *
 */
core_err ds1307_read_sec(uint8_t* data_rd) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_read_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_SECOND_ADDRESS, data_rd,
				1, 0); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to read SECONDS from DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/******************************** Write & Read MINUTE ********************************/
/**
 *
 */
core_err ds1307_write_min(uint8_t* data_wr) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_write_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_MINUTE_ADDRESS, data_wr,
				1); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to write MINUTE into DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/**
 *
 */
core_err ds1307_read_min(uint8_t* data_rd) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_read_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_MINUTE_ADDRESS, data_rd,
				1, 0); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to read MINUTE from DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/******************************** Write & Read HOUR ********************************/
/**
 *
 */
core_err ds1307_write_hour(uint8_t* data_wr) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_write_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_HOUR_ADDRESS, data_wr, 1); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to write HOUR into DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/**
 *
 */
core_err ds1307_read_hour(uint8_t* data_rd) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_read_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_HOUR_ADDRESS, data_rd, 1, 0); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to read HOUR from DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/******************************** Write & Read DAY ********************************/
/**
 *
 */
core_err ds1307_write_day(uint8_t* data_wr) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_write_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_DAY_ADDRESS, data_wr, 1); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to write DAY into DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/**
 *
 */
core_err ds1307_read_day(uint8_t* data_rd) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_read_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_DAY_ADDRESS, data_rd, 1, 0); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to read DAY from DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/******************************** Write & Read DATE ********************************/
/**
 *
 */
core_err ds1307_write_date(uint8_t* data_wr) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_write_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_DATE_ADDRESS, data_wr, 1); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to write DATE into DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/**
 *
 */
core_err ds1307_read_date(uint8_t* data_rd) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_read_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_DATE_ADDRESS, data_rd, 1, 0); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to read DATE from DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/******************************** Write & Read MONTH ********************************/
/**
 *
 */
core_err ds1307_write_month(uint8_t* data_wr) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_write_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_MONTH_ADDRESS, data_wr,
				1); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to write MONTH into DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/**
 *
 */
core_err ds1307_read_month(uint8_t* data_rd) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_read_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_MONTH_ADDRESS, data_rd, 1, 0); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to read MONTH from DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/******************************** Write & Read YEAR ********************************/
/**
 *
 */
core_err ds1307_write_year(uint8_t* data_wr) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_write_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_YEAR_ADDRESS, data_wr, 1); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to write YEAR into DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/**
 *
 */
core_err ds1307_read_year(uint8_t* data_rd) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_read_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_YEAR_ADDRESS, data_rd, 1, 0); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to read YEAR from DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/******************************** Write & Read SQWE ********************************/
/**
 *
 */
core_err ds1307_write_sqwe(uint8_t* data_wr) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_write_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_SQWE_ADDRESS, data_wr, 1); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to write SQWE into DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/**
 *
 */
core_err ds1307_read_sqwe(uint8_t* data_rd) {
	core_err ret = CORE_FAIL;
	if (rtc_ds1307_drv_handle != NULL) {
		ret = i2c_core_master_read_slave(rtc_ds1307_drv_handle, DS1307_SLAVE_ADDRESS, DS1307_SQWE_ADDRESS, data_rd, 1, 0); // Data write
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to read SQWE from DS1307");
			return ret;
		}
	} else {
		return CORE_PERIPHERAL_NOT_INITIALIZE;
	}
	return ret;
}

/******************************** Write & Read Time ********************************/
/**
 *
 */
core_err ds1307_write_time(uint8_t* data_sec, uint8_t* data_min, uint8_t* data_hr) {
	core_err ret = CORE_FAIL;
	ret = ds1307_write_sec(data_sec);
	if (ret != CORE_OK) {
		return ret;
	}

	ret = ds1307_write_min(data_min);
	if (ret != CORE_OK) {
		return ret;
	}

	ret = ds1307_write_hour(data_hr);
	if (ret != CORE_OK) {
		return ret;
	}
	return ret;
}

/**
 *
 */
core_err ds1307_read_time(uint8_t* data_sec, uint8_t* data_min, uint8_t* data_hr) {
	core_err ret = CORE_FAIL;
	ret = ds1307_read_sec(data_sec);
	if (ret != CORE_OK) {
		return ret;
	}

	ret = ds1307_read_min(data_min);
	if (ret != CORE_OK) {
		return ret;
	}

	ret = ds1307_read_hour(data_hr);
	if (ret != CORE_OK) {
		return ret;
	}
	return ret;
}

/******************************** Write & Read Date ********************************/
/**
 *
 */
core_err ds1307_write_date_and_year(uint8_t* data_day, uint8_t* data_date, uint8_t* data_month, uint8_t* date_yr) {
	core_err ret = CORE_FAIL;
	ret = ds1307_write_day(data_day);
	if (ret != CORE_OK) {
		return ret;
	}

	ret = ds1307_write_date(data_date);
	if (ret != CORE_OK) {
		return ret;
	}

	ret = ds1307_write_month(data_month);
	if (ret != CORE_OK) {
		return ret;
	}

	ret = ds1307_write_year(date_yr);
	if (ret != CORE_OK) {
		return ret;
	}
	return ret;
}

/**
 *
 */
core_err ds1307_read_date_and_year(uint8_t* data_day, uint8_t* data_date, uint8_t* data_month, uint8_t* date_yr) {
	core_err ret = CORE_FAIL;
	ret = ds1307_read_day(data_day);
	if (ret != CORE_OK) {
		return ret;
	}

	ret = ds1307_read_date(data_date);
	if (ret != CORE_OK) {
		return ret;
	}

	ret = ds1307_read_month(data_month);
	if (ret != CORE_OK) {
		return ret;
	}

	ret = ds1307_read_year(date_yr);
	if (ret != CORE_OK) {
		return ret;
	}
	return ret;
}

#endif /* CONFIG_USE_DS1307 */
