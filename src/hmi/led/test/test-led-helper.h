/*
 * test-led-helper.h
 *
 *  Created on: 24-Aug-2019
 *      Author: Rohan Dey
 */

#ifndef TEST_LED_HELPER_H_
#define TEST_LED_HELPER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_constant.h"
#include "core_error.h"
#include "core_logger.h"

typedef enum {
	TEST_CASE_1,
	TEST_CASE_2,
	TEST_CASE_3,
	TEST_CASE_4,
	TEST_CASE_5,
	TEST_CASE_6,
	TEST_CASE_7,
	TEST_CASE_8,
	TEST_CASE_9,
	TEST_CASE_10
} TEST_CASES;

void test_led_helper_start(void);

#ifdef __cplusplus
}
#endif

#endif /* TEST_LED_HELPER_H_ */
