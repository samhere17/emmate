/*
 * File Name: led_helper.h
 * File Path: /emmate/src/hmi/led/led_helper.h
 * Description:
 *
 *  Created on: 14-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef LED_HELPER_H_
#define LED_HELPER_H_

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "thing.h"

#define NOTIFY_TIME_FOREVER	-1

/* Color Codes */
#define COLOR_RED			0xff0000
#define COLOR_GREEN			0x00ff00
#define COLOR_BLUE			0x0000ff
#define COLOR_WHITE			0xffffff
#define COLOR_MAGENTA		0xff00ff
#define COLOR_YELLOW		0xffff00
#define COLOR_CYAN			0x00ffff

#define COLOR_MONO COLOR_RED

#define IS_COLOR_RED(col)		(col == COLOR_RED)		? true	: false
#define IS_COLOR_GREEN(col)		(col == COLOR_GREEN)	? true	: false
#define IS_COLOR_BLUE(col)		(col == COLOR_BLUE)		? true	: false
#define IS_COLOR_WHITE(col)		(col == COLOR_WHITE)	? true	: false
#define IS_COLOR_MAGENTA(col)	(col == COLOR_MAGENTA)	? true	: false
#define IS_COLOR_YELLOW(col)	(col == COLOR_YELLOW)	? true	: false
#define IS_COLOR_CYAN(col)		(col == COLOR_CYAN)		? true	: false

#define GET_COLOR(COL)

typedef enum {
	MONO_LED = 0,
	RGB_LED
} LED_TYPE;

//typedef struct {
//	uint32_t curr_sys_tick;
//	uint32_t on_tick;
//	uint32_t off_tick;
//} LedClock;

typedef struct {
	uint16_t repeat_count;		/**< Number of times a pattern should repeat */
	uint16_t on_ms;				/**< ON time in ms */
	uint16_t off_ms;			/**< OFF time in ms */
	uint32_t color_code;		/**< Only applicable for RGB LED */
//	LedClock led_clock;
} LedPattern;

typedef struct {
	int idx;				/**< Index number of the LED */
	LED_TYPE led_type;			/**< Type of LED. RGB or Monochrome */
	uint8_t m_rled_pin;			/**< Mono / Red LED index */
	uint8_t gled_pin;			/**< Green LED index */
	uint8_t bled_pin;			/**< Blue LED index */
	uint8_t pattern_count;		/**< Number of patterns */
	LedPattern *led_pattern;	/**< Each LED can support n no. of patterns which will be repeated */
} LedInfo;

typedef struct {
	int notify_period;			/**< Max time (in secs) for which a notification will run. If -1 then time period = infinity
									 When notify_period = -1 the notification will stop only when a new
									 notification is enqueued */
	uint8_t led_count;			/**< No. of LEDs to participate in a notification */
	LedInfo *led_info;
} LedNotifyData;

/* Function Declarations */
/**
 * @brief  Initialize a LED Notification
 *
 * @param[in]	led_count Number of LEDs to be used in the current notification
 * @param[in]	period Time period (in seconds) for which the notification should run
 *
 * @return
 *         - CORE_OK
 *         - CORE_ERR_INVALID_ARG		Invalid input parameter
 *         - CORE_ERR_NO_MEM
 *         - CORE_ERR_NOT_SUPPORTED		One notification is already initialized, start it by calling make_and_show_led_notification() before initializing the next one
 *         - CORE_ERR_INVALID_STATE		If the led helper module is not initialized
 */
core_err init_led_notification(uint8_t led_count, int period);

/**
 * @brief  Set a LED's type and pin number
 *
 * @param[in]	led_idx LED index
 * @param[in]	type LED Type (mono/rgb)
 * @param[in]	m_r_idx Mono/RED pin number
 * @param[in]	g_idx GREEN pin number
 * @param[in]	b_idx BLUE pin number
 *
 * @return
 *         - CORE_OK
 *         - CORE_ERR_INVALID_ARG		Invalid input parameter
 *         - CORE_ERR_INVALID_STATE		If the led notification is not initialized. Initialize it by calling init_led_notification()
 */
core_err set_led_type_idx(uint8_t led_idx, LED_TYPE type, uint8_t m_r_idx, uint8_t g_idx, uint8_t b_idx);
/**
 * @brief  Map 'n' number of patterns to an LED
 *
 * @param[in]	led_idx LED index
 * @param[in]	argc Number of patterns to follow
 * @param[in]	... 'n' number of patterns of type LedPattern*
 *
 * @note		The parameter 'argc' must be equal to the number of 'LedPattern*' to follow, else unexpected behavior is
 * 				expected.
 *
 * @return
 *         - CORE_OK
 *         - CORE_ERR_INVALID_ARG		Invalid input parameter
 *         - CORE_ERR_NO_MEM
 *         - CORE_ERR_INVALID_STATE		If the led notification is not initialized
 */
core_err map_patterns_to_led(uint8_t led_idx, int argc, ...);
/**
 * @brief  Start the LED notification task
 *
 * @return
 *         - CORE_OK
 *         - CORE_FAIL
 *         - CORE_ERR_INVALID_STATE		If led helper module or led notification is not initialized
 */
core_err start_led_notification();

/**
 * @brief  Stop any running led notification
 */
void stop_led_notification();

/**
 * @brief  Initializes all LEDs present in the board (BSP)
 *
 * @return
 *         - CORE_OK
 *         - CORE_FAIL
 */
core_err init_leds();


#endif /* LED_HELPER_H_ */
