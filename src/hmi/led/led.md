# LED Module
## Overview
The LED Module provides APIs to create and show LED notifications. Here, the application can initialize a LED notification, define the number of LEDs that will be used in the notification, provide characteristics of each LED, make one or more patterns to be used in the notification and finally start the LED notification.

## Features
- Define a LED notification which will run for a particular duration
- Define a LED notification which will run forever
- Use monochrome or RGB leds
- Use 1 to 'n' number of LEDs in a notification
- Use 1 to 'n' number of patterns in a notification
- Support various LED connections:
  - LED connected directly to a microcontroller pin
  - LED connected through a shift register *(not yet implemented)*
- PWM for generating any color *(not yet implemented)*
	

## How to use the LED Module from an Application
The below code snippet shows an LED notification with the following configurations:
- Number of LED = 1
- LED Type = `MONO_LED`
- LED pin = `SOM_PIN_51` (see thing.h in any LED example)
- LED pattern = `on_time=250ms`, `off_time=250ms` and `repeat=1`

```
#include "hmi.h"
#include "led_helper.h"
#include "led_patterns.h"
#include "core_logger.h"
#include "thing.h"

void show_my_led_notification() {
	core_err ret;
	int num_leds = 1;

	/* Initialize an LED notification */
	ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "LED notification initialization failed");
		// Handle error
	}
	
	/* Set the LED type and pin number */
	set_led_type_idx(num_leds, MONO_LED, SOM_PIN_51, 0, 0);
	
	/* Map the predefined patterns LED_MONO_250_250_1 with this notification */
	map_patterns(num_leds, 1, LED_MONO_250_250_1);

	/* Start the led notification */
	start_led_notification();
}

void core_app_main(void * param) {
	init_hmi_led();
	show_my_led_notification();
	
	while(1){
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
```

Please see the [LED Examples](../../../examples/hmi/led/) for more info

##### Header(s) to include

```
hmi.h
led_helper.h
led_patterns.h
```
