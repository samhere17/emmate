/*
 * File Name: led_patterns.c
 * File Path: /emmate/src/hmi/led/led_patterns.c
 * Description:
 *
 *  Created on: 17-Apr-2019
 *      Author: Rohan Dey
 */

#include "led_patterns.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#if CONFIG_USE_LED

#define TAG LTAG_HMI_LED

LedPattern* make_led_pattern(uint32_t color_code, uint16_t on_ms, uint16_t off_ms, uint16_t repeat) {

	LedPattern *pattern = (LedPattern*) calloc(1, sizeof(LedPattern));
	if (pattern == NULL) {
		CORE_LOGE(TAG, "Failed to allocate memory: %s, %d", (char*) __FILE__, __LINE__);
		return 0;
	}

	pattern->color_code = color_code;
	pattern->on_ms = on_ms;
	pattern->off_ms = off_ms;
	pattern->repeat_count = repeat;

	return pattern;
}
#endif /* CONFIG_USE_LED */
