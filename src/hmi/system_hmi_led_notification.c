/*
 * system_hmi_led_notification.c
 *
 *  Created on: 20-Jul-2019
 *      Author: Rohan Dey
 */

#include "system_hmi_led_notification.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_LED
#include "led_helper.h"
#include "led_patterns.h"
#endif

#define TAG	LTAG_HMI

#if (CONFIG_HAVE_SYSTEM_HMI && CONFIG_USE_LED)

///////////////////////////////////////// LEDS /////////////////////////////////////////
void show_booting_notification() {
	int num_leds = 1;

	core_err ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_led_notification failed!");
		return;
	}
#if CONFIG_HAVE_HMI_RGBLED_IN_STARTUP

	set_led_type_idx(num_leds, RGB_LED, SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE);
	map_patterns_to_led(num_leds, 1, LED_WHITE_100_250_1);

#elif CONFIG_HAVE_HMI_BICOLORLED_IN_STARTUP

#else /* CONFIG_HAVE_HMI_MONOLED_IN_STARTUP */

	set_led_type_idx(num_leds, MONO_LED, SYSTEM_HMI_LED_MONO_RED, 0, 0);
	map_patterns_to_led(num_leds, 1, LED_MONO_100_250_1);

#endif
	start_led_notification();
}

void show_ble_connected_notification() {
	int num_leds = 1;
	core_err ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_led_notification failed!");
		return;
	}
#if CONFIG_HAVE_HMI_RGBLED_IN_STARTUP

	set_led_type_idx(num_leds, RGB_LED, SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE);
	map_patterns_to_led(num_leds, 1, LED_BLUE_1000_1_1);

#elif CONFIG_HAVE_HMI_BICOLORLED_IN_STARTUP

#else /* CONFIG_HAVE_HMI_MONOLED_IN_STARTUP */

	set_led_type_idx(num_leds, MONO_LED, SYSTEM_HMI_LED_MONO_RED, 0, 0);
	map_patterns_to_led(num_leds, 1, LED_BLUE_1000_1_1);

#endif
	start_led_notification();
}

void show_ble_advertising_notification() {
	int num_leds = 1;

	core_err ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_led_notification failed!");
		return;
	}
#if CONFIG_HAVE_HMI_RGBLED_IN_STARTUP

	set_led_type_idx(num_leds, RGB_LED, SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE);
	map_patterns_to_led(num_leds, 1, LED_BLUE_100_100_1);

#elif CONFIG_HAVE_HMI_BICOLORLED_IN_STARTUP

#else /* CONFIG_HAVE_HMI_MONOLED_IN_STARTUP */

	set_led_type_idx(num_leds, MONO_LED, SYSTEM_HMI_LED_MONO_RED, 0, 0);
	map_patterns_to_led(num_leds, 1, LED_BLUE_100_100_1);

#endif
	start_led_notification();
}


void show_network_connected_notification() {
	int num_leds = 1;
	core_err ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_led_notification failed!");
		return;
	}
#if CONFIG_HAVE_HMI_RGBLED_IN_STARTUP

	set_led_type_idx(num_leds, RGB_LED, SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE);
	map_patterns_to_led(num_leds, 1, LED_GREEN_1000_1_1);

#elif CONFIG_HAVE_HMI_BICOLORLED_IN_STARTUP

#else /* CONFIG_HAVE_HMI_MONOLED_IN_STARTUP */

	set_led_type_idx(num_leds, MONO_LED, SYSTEM_HMI_LED_MONO_RED, 0, 0);
	map_patterns_to_led(num_leds, 1, LED_MONO_1_1000_1);

#endif
	start_led_notification();
}

void show_network_connecting_notification() {
	int num_leds = 1;
	core_err ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_led_notification failed!");
		return;
	}
#if CONFIG_HAVE_HMI_RGBLED_IN_STARTUP

	set_led_type_idx(num_leds, RGB_LED, SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE);
	map_patterns_to_led(num_leds, 1, LED_RED_500_500_1);

#elif CONFIG_HAVE_HMI_BICOLORLED_IN_STARTUP

#else /* CONFIG_HAVE_HMI_MONOLED_IN_STARTUP */

	set_led_type_idx(num_leds, MONO_LED, SYSTEM_HMI_LED_MONO_RED, 0, 0);
	map_patterns_to_led(num_leds, 1, LED_MONO_500_500_1);

#endif
	start_led_notification();
}

void show_system_running_notification() {
	int num_leds = 1;
	core_err ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_led_notification failed!");
		return;
	}
#if CONFIG_HAVE_HMI_RGBLED_IN_STARTUP

	set_led_type_idx(num_leds, RGB_LED, SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE);
	map_patterns_to_led(num_leds, 1, LED_GREEN_100_100_2);

#elif CONFIG_HAVE_HMI_BICOLORLED_IN_STARTUP

#else /* CONFIG_HAVE_HMI_MONOLED_IN_STARTUP */

	set_led_type_idx(num_leds, MONO_LED, SYSTEM_HMI_LED_MONO_RED, 0, 0);
	map_patterns_to_led(num_leds, 1, LED_MONO_100_100_2);

#endif
	start_led_notification();
}

void show_system_fault_notification() {
	int num_leds = 1;
	core_err ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_led_notification failed!");
		return;
	}
#if CONFIG_HAVE_HMI_RGBLED_IN_STARTUP

	set_led_type_idx(num_leds, RGB_LED, SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE);
	map_patterns_to_led(num_leds, 1, LED_RED_1000_1_1);

#elif CONFIG_HAVE_HMI_BICOLORLED_IN_STARTUP

#else /* CONFIG_HAVE_HMI_MONOLED_IN_STARTUP */

	set_led_type_idx(num_leds, MONO_LED, SYSTEM_HMI_LED_MONO_RED, 0, 0);
	map_patterns_to_led(num_leds, 1, LED_MONO_1000_1_1);

#endif
	start_led_notification();
}

void show_system_resetting_notification() {
	int num_leds = 1;
	core_err ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "init_led_notification failed!");
		return;
	}
#if CONFIG_HAVE_HMI_RGBLED_IN_STARTUP

	set_led_type_idx(num_leds, RGB_LED, SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE);
	map_patterns_to_led(num_leds, 1, LED_WHITE_1000_1_1);

#elif CONFIG_HAVE_HMI_BICOLORLED_IN_STARTUP

#else /* CONFIG_HAVE_HMI_MONOLED_IN_STARTUP */

	set_led_type_idx(num_leds, MONO_LED, SYSTEM_HMI_LED_MONO_RED, 0, 0);
	map_patterns_to_led(num_leds, 1, LED_MONO_1000_1_1);

#endif
	start_led_notification();
}

#else

void show_booting_notification(){}
void show_ble_connected_notification(){}
void show_ble_advertising_notification(){}
void show_network_connected_notification(){}
void show_network_connecting_notification(){}
void show_system_running_notification(){}
void show_system_fault_notification(){}
void show_system_resetting_notification() {}
#endif	/* (CONFIG_HAVE_SYSTEM_HMI && CONFIG_USE_LED) */

