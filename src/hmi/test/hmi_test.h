/*
 * File Name: hmi_test.h
 * File Path: /emmate/src/hmi/test/hmi_test.h
 * Description:
 *
 *  Created on: 11-May-2019
 *      Author: Noyel Seth
 */

#ifndef SRC_HMI_TEST_HMI_TEST_H_
#define SRC_HMI_TEST_HMI_TEST_H_

#include "core_config.h"

#if TESTING

void init_touch_btn();

void init_unknown_btn();

#endif

#endif /* SRC_HMI_TEST_HMI_TEST_H_ */
