# Human-Machine Interface (HMI)
## Overview
The Human-Machine Interface module provides easy to use features that can be used by an application interact with the external world. 

This module provides generic features of HMI that can be used in any application. Some examples would be:
	- Blinking LEDs in any pattern
	- Taking input from 1 or more push buttons
	- Displaying text on a LCD and similar HMI devices

Most of the application code is already written in this module thus making it easier for the app developer. Only specific APIs calls of this module needs to be made to generate the required output.

## Submodules
The HMI module has the following submodules:
1. LED - For more details see [Details of LED Module](led/led.md)
2. Button - For more details see [Details of Button Module](button/button.md)
3. Character LCD *(not yet implemented)*
4. Graphic LCD *(not yet implemented)*
5. OLED Display *(not yet implemented)*
6. Touchscreen Display *(not yet implemented)*

## How to use the HMI Module from an Application

##### Header(s) to include

```
hmi.h
```
#### Sample APIs
1. For initializing the LED submodule

```
init_hmi_led();
```

2. For initializing the Button submodule

```
init_hmi_button();
```