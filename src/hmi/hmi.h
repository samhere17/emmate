/*
 * File Name: hmi.h
 * File Path: /emmate/src/hmi/hmi.h
 * Description:
 *
 *  Created on: 14-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef HMI_H_
#define HMI_H_

#include "core_config.h"
#include "core_common.h"
#if CONFIG_USE_LED
#include "led_helper.h"
#include "led_patterns.h"
#endif
#if CONFIG_USE_BUTTON
#include "button_helper.h"
#endif

typedef void (*reset_interaction_cb)(uint32_t rst_intact_type);

#if (CONFIG_HAVE_SYSTEM_HMI)
/**
 * @brief  Initialize the HMI module for doing system starup activities only
 * @return
 */
void init_startup_hmi_module();
/**
 * @brief  Register the reset callback function
 *
 * @param[in]	reset_interaction_cb function pointer
 *
 * @return
 */
void register_hmi_reset_button_cb(reset_interaction_cb cb_handler);
#endif

#if CONFIG_USE_LED
/**
 * @brief 	Initializes the LED module of the Core Framework
 * 			Any LED related functionality must be done only after calling this function
 * */
void init_hmi_led();
#endif

#if CONFIG_USE_BUTTON
/**
 * @brief 	Initializes the Button module of the Core Framework
 * 			Any Button related functionality must be done only after calling this function
 * */
void init_hmi_button();
#endif

#endif /* HMI_H_ */
