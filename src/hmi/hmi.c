/*
 * File Name: hmi.c
 * File Path: /emmate/src/hmi/hmi.c
 * Description:
 *
 *  Created on: 14-Apr-2019
 *      Author: Rohan Dey
 */

#include "hmi.h"
#include "system_hmi_led_notification.h"
#include "system_hmi_button.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "threading.h"
#include "thing.h"
#if TESTING
#include "hmi_test.h"
#endif

#define TAG	LTAG_HMI

#if (CONFIG_HAVE_SYSTEM_HMI)

void init_startup_hmi_module() {
#if CONFIG_USE_LED
#if (CONFIG_HAVE_HMI_MONOLED_IN_STARTUP || CONFIG_HAVE_HMI_RGBLED_IN_STARTUP)
	init_hmi_led();
	CORE_LOGD(LTAG_HMI, "show startup notification");
	show_booting_notification();
#endif
#endif	/* CONFIG_USE_LED */

#if CONFIG_USE_BUTTON
	init_hmi_button();
	CORE_LOGD(LTAG_HMI, "startup button interaction");
	init_reset_btn();
#endif	/* CONFIG_USE_BUTTON */

#if CONFIG_HAVE_HMI_LCD_IN_STARTUP
#endif
}

#endif	/* CONFIG_HAVE_SYSTEM_HMI */

#if CONFIG_USE_LED
void init_hmi_led() {
	init_leds();
}
#endif

#if CONFIG_USE_BUTTON
void init_hmi_button() {
	init_buttons();
}
#endif
