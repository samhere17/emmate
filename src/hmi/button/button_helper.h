/*
 * File Name: button_helper.h
 * File Path: /emmate/src/hmi/button/button_helper.h
 * Description:
 *
 *  Created on: 10-May-2019
 *      Author: Noyel Seth
 */

#ifndef BUTTON_HELPER_H_
#define BUTTON_HELPER_H_

#include "thing.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_config.h"
#include "core_error.h"
#include "gpio_core.h"

typedef enum {
	INTERACTION_NONE = 0,
	PRESSED = 100, /**< Button state Pressed (action define the combine condition of Button Pressed & Released).*/
	SHORT_PRESSED, /**< Button state Short Pressed.*/
	LONG_PRESSED, /**< Button state Long Pressed.*/
	SINGLE_PRESSED, /**< Button state Single Pressed.*/
	DOUBLE_PRESSED, /**< Button state Double Pressed.*/
} BTN_INTERACTION_TYPE;


#define DEFAULT_LONG_PRESS_DURATION					CONFIG_DEFAULT_BUTTON_LONG_PRESS_DURATION	/**< Default Long Press Duration*/
#define DEFAULT_SHORT_PRESS_DURATION				CONFIG_DEFAULT_BUTTON_SHORT_PRESS_DURATION		/**< Default Short Press Duration*/
#define DEFAULT_DOUBLE_PRESS_INTERVAL_DURATION		CONFIG_DEFAULT_BUTTON_DOUBLE_PRESS_INTERVAL_DURATION		/**< Default Duration between Double press*/
#define DEFAULT_PRESS_DURATION						CONFIG_DEFAULT_BUTTON_PRESS_DURATION		/**< Default Press Duration for Double press*/

typedef void (*notify_btn_state)(uint8_t io_pin, BTN_INTERACTION_TYPE intact_type);

typedef struct {
	uint32_t press_tick; /**< Hold system tick-time at the time button is been pressed*/
	uint32_t release_tick; /**< Hold system tick-time at the time button is been released*/
	bool press_stat;bool release_stat;
	uint8_t pressed_count;
} ButtonHelperData;

typedef struct {
	notify_btn_state notify_btn_stateCb; /**< Callback function to notify the Button interaction type */
	gpio_io_num io_pin; /**< GPIO number of the connected Button */
	gpio_interrupt_type intr_type; /**< Interrupt type (Positive/Negative edge, High/Low level, Any edge[Interrupt-on-Change])*/
	//gpio_io_mode io_mode;
//	gpio_io_pullup	io_pullup_mode;				/**< button GPIO's internal PullUp mode*/
//	gpio_io_pulldown	io_pulldown_mode;		/**< button GPIO's internal PullDown mode*/

	gpio_io_pull_mode io_pull_mode; /**< button GPIO's internal Pullmodes */

	uint32_t press_duration; /**< Duration between button pressed & released */
	uint16_t btn_interaction_type; /**< button interaction type  */

	ButtonHelperData bnt_hlp_dt; /**< Button helper data*/
} ButtonData;

/**
 * @brief  Initialize the Button based on input "ButtonData" parameters
 *
 * @param[in]	btn_dt passed the configuration of the Button
 *
 * @return
 * 		- CORE_OK 		on success
 * 		- CORE_FAIL		on failed
 *
 */
core_err init_btn_interface(ButtonData *btn_dt);

/**
 * @brief  Initialize the Button with Long Press Interaction type
 * @note	Default Long Press duration "DEFAULT_LONG_PRESS_DURATION"
 *
 * @param[in]	btn_gpio	passed the Button IO number
 *
 * @param[in]	pull_mode	passed the Pull-mode (Only-PullUP, Only-PullDown, PullUP-PullDown, Floating)
 *
 * @param[in]	cb_handler	Callback function for receive Button Interaction
 *
 * @return
 * 		- CORE_OK 		on success
 * 		- CORE_FAIL		on failed
 */
core_err init_long_press_btn_interface(gpio_io_num btn_gpio, gpio_io_pull_mode pull_mode, notify_btn_state cb_handler);

/**
 * @brief  Initialize the Button with Short Press Interaction type
 * @note	Default Long Press duration "DEFAULT_SHORT_PRESS_DURATION"
 *
 * @param[in]	btn_gpio	passed the Button IO number
 *
 * @param[in]	pull_mode	passed the Pull-mode (Only-PullUP, Only-PullDown, PullUP-PullDown, Floating)
 *
 * @param[in]	cb_handler	Callback function for receive Button Interaction
 *
 * @return
 * 		- CORE_OK 		on success
 * 		- CORE_FAIL		on failed
 */
core_err init_short_press_btn_interface(gpio_io_num btn_gpio, gpio_io_pull_mode pull_mode, notify_btn_state cb_handler);

/**
 * @brief  Initialize the Button with Long Press Interaction type
 * @note	Default Press duration "DEFAULT_PRESS_DURATION" and "DEFAULT_DOUBLE_PRESS_INTERVAL_DURATION" default duration between 2presses
 *
 *
 * @param[in]	btn_gpio	passed the Button IO number
 *
 * @param[in]	pull_mode	passed the Pull-mode (Only-PullUP, Only-PullDown, PullUP-PullDown, Floating)
 *
 * @param[in]	cb_handler	Callback function for receive Button Interaction
 *
 * @return
 * 		- CORE_OK 		on success
 * 		- CORE_FAIL		on failed
 */
core_err init_double_press_btn_interface(gpio_io_num btn_gpio, gpio_io_pull_mode pull_mode,
		notify_btn_state cb_handler);

/**
 * @brief  Initialize the Button to Long, Short & Double Press Interaction
 *
 * @param[in]	btn_gpio	passed the Button IO number
 *
 * @param[in]	pull_mode	passed the Pull-mode (Only-PullUP, Only-PullDown, PullUP-PullDown, Floating)
 *
 * @param[in]	cb_handler	Callback function for receive Button Interaction
 *
 * @return
 * 		- CORE_OK 		on success
 * 		- CORE_FAIL		on failed
 */
core_err init_all_press_btn_interface(gpio_io_num btn_gpio, gpio_io_pull_mode pull_mode, notify_btn_state cb_handler);

/**
 * @brief  Initialize the Button with Custom Single Press Interaction type
 *
 * @param[in]	btn_gpio		passed the Button IO number
 *
 * @param[in]	pull_mode		passed the Pull-mode (Only-PullUP, Only-PullDown, PullUP-PullDown, Floating)
 *
 * @param[in]	press_duration	passed the duration for hold the Button pressed
 *
 * @param[in]	cb_handler		Callback function for receive Button Interaction
 *
 * @return
 * 		- CORE_OK 		on success
 * 		- CORE_FAIL		on failed
 */
core_err init_custom_press_btn_interface(gpio_io_num btn_gpio, gpio_io_pull_mode pull_mode, uint32_t press_duration,
		notify_btn_state cb_handler);


/**
 * @brief  Initialize the Button with Custom Long Press Interaction type
 *
 * @param[in]	btn_gpio		passed the Button IO number
 *
 * @param[in]	pull_mode		passed the Pull-mode (Only-PullUP, Only-PullDown, PullUP-PullDown, Floating)
 *
 * @param[in]	press_duration	duration for hold the Button pressed
 *
 * @param[in]	cb_handler		Callback function for receive Button Interaction
 *
 * @return
 * 		- CORE_OK 		on success
 * 		- CORE_FAIL		on failed
 */
core_err init_custom_long_press_btn_interface(gpio_io_num btn_gpio, gpio_io_pull_mode pull_mode, uint32_t press_duration, notify_btn_state cb_handler);

/**
 * @brief  Initializes all Buttons present in the board (BSP)
 *
 * @return
 * 		- CORE_OK 		on success
 * 		- CORE_FAIL		on failed
 *
 */
core_err init_buttons();

#endif /* BUTTON_HELPER_H_ */
