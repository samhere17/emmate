# Button Module
## Overview
The Button Module provides APIs to initialize and use push-buttons. Here, the application can configure the button's pin number, enable/disable pullup or pulldown registers, set an interrupt type, configure how long the button has to be held down for identifying a valid push and set a callback function which will be called when a push is detected.

## Features
- This module is able to detect the following events for a single button:
  - Single press
  - Double press *(not yet implemented)*
  - Long press *(not yet implemented)*
  - Press and hold for minimum duration *(not yet implemented)*
- This module is able to detect the following events for a multiple buttons:
  - By holding one button a second button is pressed *(not yet implemented)*
  - By holding two buttons a third button is pressed *(not yet implemented)*
	

## How to use the Button Module from an Application

The below code snippet shows a initializing and using 1 button with the following configurations:
- Number of Button = 1
- Pin connection = `SOM_PIN_51` (see thing.h in any Button example)
- Pulldown Mode = `GPIO_IO_PULLDOWN_ENABLE`
- Pullup Mode = `GPIO_IO_PULLUP_DISABLE`
- Type of interrupt = `GPIO_INTERRUPT_ANYEDGE`
- Duration to press = `500 ms`
- Callback = `my_button_callback`

```
#include "hmi.h"
#include "button_helper.h"
#include "core_logger.h"
#include "thing.h"

void my_button_callback(uint8_t io_pin, uint32_t rst_intact_type) {
	CORE_LOGI(TAG, "Button is pressed");
}

void init_my_button() {
	ButtonData btn_dt;
	
	btn_dt.io_pin = BSP_BTN_2;
	btn_dt.io_pulldown_mode = GPIO_IO_PULLDOWN_ENABLE;
	btn_dt.io_pullup_mode = GPIO_IO_PULLUP_DISABLE;
	btn_dt.int_type = GPIO_INTERRUPT_ANYEDGE;
	btn_dt.press_duration = 500;
	btn_dt.notify_btn_stateCb = hmi_btn_event_handler;

	core_err ret = init_btn_interface(&btn_dt);
	if (ret != CORE_OK) {
		CORE_LOGE("Failed to initialize the button");
	}
}

void core_app_main(void * param) {
	init_my_button();
	
	while(1){
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}

```
Please see the [Button Examples](../../../examples/hmi/button/) for more info

##### Header(s) to include

```
hmi.h
button_helper.h
```
