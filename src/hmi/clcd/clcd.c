/*
 * File Name: clcd.c
 *
 * Description:
 *
 *  Created on: 30-Aug-2019
 *      Author: Noyel Seth
 */

#include "clcd.h"
#include "clcd_core.h"
#include "gpio_helper_api.h"
#include "delay_utils.h"
#include "gll.h"
#include "threading.h"

#include <string.h>
#include <math.h>

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG "clcd"

#define DEV_LOG 1

/****************************************************************************************************/

typedef struct {
	bool init_done;
	uint8_t clcd_max_row;
	uint8_t clcd_max_col;
	uint8_t uid_range;
	uint16_t clcd_max_display_string;
	ThreadHandle clcd_task_handle;
} CLCD_CONFIG_DATA;

typedef struct {
	int uid;
	uint8_t string[MAX_DISPLAY_DATA_LEN];
	int strlen;
	uint8_t start_row;
	uint8_t start_col;
	uint8_t start_point;
	uint8_t end_row;
	uint8_t end_col;
	uint8_t end_point;
	uint8_t direction;
	uint16_t blink_interval;
	uint16_t scroll_interval;
	uint16_t display_char_count;
	//unsigned char display_loop_count;
	uint8_t length_count_for_right_scroll;
	uint8_t length_decount_for_right_scroll;
	uint8_t length_count_for_left_scroll;
	uint8_t length_decount_for_left_scroll;
	uint16_t blink_interval_count;
	uint16_t scroll_interval_count;
	bool blink;
//unsigned char eol;
} CLCD_STRING_DISPLAY_DETAILS;

typedef struct {
	CLCD_STRING_DISPLAY_DETAILS display_details;
	CLCD_STRING_OVERWRITE_STAT over_write;
} CLCD_SET_STRING_CONFIG_DATA;

/****************************************************************************************************/

static uint16_t data_count = 0;
//static char uid_count = START_UID;
static uint16_t conflict_data_count = 0;
//int display_struct_count = 0;
CLCD_CONFIG_DATA clcd_config_data;
unsigned char **lcd_display_array = NULL;
uint16_t lcd_display_array_size = 0;
CLCD_STRING_DISPLAY_DETAILS *clcd_display_string_details;
int *conflict_arr = NULL;
bool print_data_ready = false;
uint8_t CLCD_ROWS_HOME_ADRR[MAX_ROW] = { LINE1_HOME, LINE2_HOME, LINE3_HOME, LINE4_HOME };
CLCD_SET_STRING_CONFIG_DATA cmd_data = { 0 };

/**************************************** CLCD Static Functions ************************************************************/

static void free_lcd_display_array() {
	int i = 0;
	for (i = 0; i < clcd_config_data.clcd_max_row; i++) {
		free(lcd_display_array[i]); // free allocated memory
	}
	free(lcd_display_array);
}

static void clear_lcd_display_array() {
	int i = 0;
	for (i = 0; i < clcd_config_data.clcd_max_row; i++) {
		memset(lcd_display_array[i], ' ', clcd_config_data.clcd_max_col);
		////CORE_LOGD(TAG,"%d\n", i);
	}
}

static void clear_display() {
	write_lcd_cmd(CLEAR_DISPLAY_CMD);
	delay_ms(100);
	//is_busy();
}

static void set_cursor(unsigned char row_number, unsigned char col_number) {
	if (col_number > MAX_COL) {
		col_number = 0;
	}
	write_lcd_cmd(col_number + row_number);
	//Wait,if LCD is busy
	delay_us(100);
}

static void set_cursor_at_home() {
	write_lcd_cmd(RETURN_HOME_CMD);
	delay_us(100);
}

static int get_new_uid() {
	int i = 1;
	srand(i);
	int uid = (rand() % 100) + 1; // in the range 1 to 100
	for (i = 0; i < clcd_config_data.clcd_max_display_string;) {
		if (clcd_display_string_details[i].uid != 0 && clcd_display_string_details[i].uid != uid) {
			i++;
			continue;
		} else if (clcd_display_string_details[i].uid == 0) {
			i++;
			continue;
		} else {
			uid = (rand() % 100) + 1; // in the range 1 to 100
			i = 0;
			continue;
		}
	}
	return uid;
}

static void copy_display_struct(CLCD_STRING_DISPLAY_DETAILS *clcd_str_data, CLCD_SET_STRING_CONFIG_DATA *data, char uid) {
	//CORE_LOGD(TAG,"###################################################### %s\n", __func__);
	clcd_str_data->uid = uid;
	strcpy((char*) clcd_str_data->string, (char*) data->display_details.string);

	//CORE_LOGD(TAG,"display_data = %s\r\n", clcd_str_data->string);

	clcd_str_data->strlen = strlen((char*) clcd_str_data->string);
	clcd_str_data->start_row = data->display_details.start_row;
	clcd_str_data->start_col = data->display_details.start_col;
	clcd_str_data->end_row = data->display_details.end_row;
	clcd_str_data->end_col = data->display_details.end_col;
	clcd_str_data->start_point = (clcd_str_data->start_row * clcd_config_data.clcd_max_col) + clcd_str_data->start_col;
	clcd_str_data->end_point = (clcd_str_data->end_row * clcd_config_data.clcd_max_col) + clcd_str_data->end_col;
	clcd_str_data->direction = data->display_details.direction;
	clcd_str_data->display_char_count = (clcd_str_data->end_point - clcd_str_data->start_point) + 1;
	clcd_str_data->scroll_interval = data->display_details.scroll_interval;
	clcd_str_data->blink_interval = data->display_details.blink_interval;

	//CORE_LOGD(TAG,"blink_interval = %d\r\n", clcd_str_data->blink_interval);
	//CORE_LOGD(TAG,"display_char_count = %d\r\n", clcd_str_data->display_char_count);

}

static void clear_display_structure() {
	memset(clcd_display_string_details, 0,
			sizeof(CLCD_STRING_DISPLAY_DETAILS) * clcd_config_data.clcd_max_display_string);
}

static void write_display_str(const unsigned char* str) {
	unsigned char i = 0;
	while (i < clcd_config_data.clcd_max_col) {
		delay_us(10);
		if (*str != '\0') {
			////CORE_LOGD(TAG,"%c", *str);
			write_lcd_data(*str++);
		} else {
			write_lcd_data(' ');
			str++;
		}
		is_busy();
		i++;
	}
}

static void clcd_display_string_config() {

	clear_lcd_display_array();

	memcpy(lcd_display_array[0], "Init done", sizeof("Init done") - 1);
	set_cursor(LINE1_HOME, 1);
	delay_ms(1000);

	write_display_str(lcd_display_array[0]);
	memcpy(lcd_display_array[0], "CLCD 16x2", sizeof("CLCD 16x2") - 1);
	set_cursor(LINE2_HOME, 0);
	write_display_str(lcd_display_array[0]);

}

static bool conflict(char new_start_point, char new_end_point) {
	//CORE_LOGD(TAG,"###################################################### %s\n", __func__);
	int i = 0;
	bool conflict_bool = false;

	for (i = 0; i < clcd_config_data.clcd_max_display_string;) {
		if (clcd_display_string_details[i].uid != 0) {
			if (new_start_point >= clcd_display_string_details[i].start_point
					&& new_start_point <= clcd_display_string_details[i].end_point) {
				conflict_arr[conflict_data_count++] = clcd_display_string_details[i].uid;
				i++;

				//CORE_LOGD(TAG,"ex_sp <= new_sp <=ex_ep\r\n");

				conflict_bool = true;
				continue;
			}
			if (new_end_point >= clcd_display_string_details[i].start_point
					&& new_end_point <= clcd_display_string_details[i].end_point) {
				conflict_arr[conflict_data_count++] = clcd_display_string_details[i].uid;

				//CORE_LOGD(TAG,"ex_sp <= new_ep <=ex_ep\r\n");

				i++;
				conflict_bool = true;
				continue;
			}
			if (new_start_point <= clcd_display_string_details[i].start_point
					&& new_end_point >= clcd_display_string_details[i].end_point) {
				conflict_arr[conflict_data_count++] = clcd_display_string_details[i].uid;
				i++;
				conflict_bool = true;
				continue;
			}
		}
		i++;
	}

	//CORE_LOGD(TAG,"conflict: uid\r\n");
	for (i = 0; i < clcd_config_data.clcd_max_display_string; i++) {
		if (conflict_arr[i] != -1) {
			//CORE_LOGD(TAG,"%d, ", conflict_arr[i]);
		}
	}
	//CORE_LOGD(TAG,"\r\n");
	//CORE_LOGD(TAG,"conflict_bool = %d\r\n", conflict_bool);
	return conflict_bool;
}

///*
// *
// */
//char update_string_data(CLCD_SET_STRING_CONFIG_DATA *data) {
//	int i = 0;
//	char exist_uid_idx = -1;
//	char start_point, end_point;
//	memset(conflict_arr, -1, sizeof(conflict_arr));
//	conflict_data_count = 0;
//	start_point = (data->display_details.start_row * clcd_config_data.clcd_max_col) + data->display_details.start_col;
//	end_point = (data->display_details.end_row * clcd_config_data.clcd_max_col) + data->display_details.end_col;
//
//	//CORE_LOGD(TAG,"UID = %c, start_pt = %d, end_pt = %d\r\n", data->display_details.uid, start_point, end_point);
//
//	if (data_count < clcd_config_data.clcd_max_display_string) {
//		for (i = 0; i < clcd_config_data.clcd_max_display_string; i++) {
//			if (clcd_display_string_details[i].uid == data->display_details.uid) {
//				exist_uid_idx = i;
//				conflict_arr[conflict_data_count++] = clcd_display_string_details[i].uid;
//				conflict_check(exist_uid_idx, start_point, end_point);
//				return CONFLICT_STRING;
//			} else {
//				if (conflict_check(exist_uid_idx, start_point, end_point) == true) {
//					//CORE_LOGD(TAG,"conflict: uid\r\n");
//					for (i = 0; i < clcd_config_data.clcd_max_display_string; i++) {
//						if (conflict_arr[i] != -1)
//							//CORE_LOGD(TAG,"%c, ", conflict_arr[i]);
//					}
//					//CORE_LOGD(TAG,"\r\n");
//					return CONFLICT_STRING;
//				}
//			}
//		}
//		memset(&clcd_display_string_details[data_count], 0, sizeof(CLCD_STRING_DISPLAY_DETAILS));
//		copy_data(&clcd_display_string_details[data_count], data);
//		data_count++;
//		make_display();
//	} else {
//
//	}
//	return 0;
//}

static unsigned char set_string_data(CLCD_SET_STRING_CONFIG_DATA *data) {
	//CORE_LOGD(TAG,"###################################################### %s\n", __func__);
	uint16_t i = 0;
	char uid = 0;
	char start_point, end_point;
	//    memset(conflict_arr, -1, sizeof (conflict_arr));
	//    conflict_data_count = 0;
	start_point = (data->display_details.start_row * clcd_config_data.clcd_max_col) + data->display_details.start_col;
	end_point = (data->display_details.end_row * clcd_config_data.clcd_max_col) + data->display_details.end_col;
	uid = get_new_uid();

	//CORE_LOGD(TAG,"UID = %d, start_pt = %d, end_pt = %d\r\n", uid, start_point, end_point);

	if (data->over_write != OVERWRITE) {
		memset(conflict_arr, -1, clcd_config_data.clcd_max_display_string);
		conflict_data_count = 0;
		if (conflict(start_point, end_point) != true) {
			if (data_count < clcd_config_data.clcd_max_display_string) {
				for (i = 0; i < clcd_config_data.clcd_max_display_string; i++) {
					if (clcd_display_string_details[i].uid == 0) {
						memset(&clcd_display_string_details[i], 0, sizeof(CLCD_STRING_DISPLAY_DETAILS));
						copy_display_struct(&clcd_display_string_details[i], data, uid);
						data_count++;
						return uid;
					}
				}
			} else {
				// TODO:
			}
		} else {
			memset(conflict_arr, -1, clcd_config_data.clcd_max_display_string);
			conflict_data_count = 0;
			return CONFLICT_STRING;
		}
	} else {
		if (conflict_data_count != 0) {

			//CORE_LOGD(TAG,"conflict...2\r\n");

			int conflict_count = 0, j;
			for (conflict_count = 0; conflict_count < conflict_data_count; conflict_count++) {
				for (j = 0; j < clcd_config_data.clcd_max_display_string; j++) {
					if (clcd_display_string_details[j].uid == conflict_arr[conflict_count]) {

						//CORE_LOGD(TAG,"%d..j=%d,conflict_count=%d \r\n", clcd_display_string_details[j].uid, j,conflict_count);

						memset(&clcd_display_string_details[j], 0, sizeof(CLCD_STRING_DISPLAY_DETAILS));
						data_count--;
						data->over_write = NOT_OVERWRITE;
						return set_string_data(data);
						break;
					}
				}
			}
			memset(conflict_arr, -1, clcd_config_data.clcd_max_display_string);
			conflict_data_count = 0;
			return CONFLICT_STRING_OVERWRITE;
		} else {
			if (conflict(start_point, end_point) == true) {
				return set_string_data(data);      // recursive call for over write new string with old conflicted string
				//return CONFLICT_STRING_CHECK_AND_OVERWRITE;
			} else {
				data->over_write = NOT_OVERWRITE;
				return set_string_data(data);
				//return CONFLICT_STRING_NOT_CONFLICT_WITH_OVERWRITE_ARGUMENT;
			}
		}
	}
	return 0;
}

static void char_enter_mode_left_to_right() {
	//CORE_LOGD(TAG,"###################################################### %s\n", __func__);
	//    //CORE_LOGD(TAG,"0x%x\r\n", ((0x00 | INCREMET_CURSOR_CMD))& ~INCREMET_CURSOR_CMD);
	//    write_lcd_cmd(((0x00 | INCREMET_CURSOR_CMD))& ~INCREMET_CURSOR_CMD);
	//    delay_ms(250);
	write_lcd_cmd(INCREMET_CURSOR_CMD);
	is_busy();
}

static void display_text() {
	if (print_data_ready == true) {
		//CORE_LOGD(TAG,"###################################################### %s\n", __func__);
		//write_lcd_cmd(CLEAR_DISPLAY_CMD); //delay_ms(250);
		//is_busy();
		char_enter_mode_left_to_right(); //delay_ms(250);
		int i = 0;
		for (i = 0; i < clcd_config_data.clcd_max_row; i++) {
			set_cursor(CLCD_ROWS_HOME_ADRR[i], 0);
			write_display_str(lcd_display_array[i]);
		}
//		set_cursor(LINE1_HOME, 0);
//		write_display_str(lcd_display_array[0]);
//		set_cursor(LINE2_HOME, 0);
//		write_display_str(lcd_display_array[1]);
//		set_cursor(LINE3_HOME, 0);
//		write_display_str(lcd_display_array[2]);
//		set_cursor(LINE4_HOME, 0);
//		write_display_str(lcd_display_array[3]);
		print_data_ready = false;
	} else {
		print_data_ready = false;
	}
}

static void scroll_off(CLCD_STRING_DISPLAY_DETAILS *clcd_display_string_details) {
	//CORE_LOGD(TAG,"###################################################### %s\n", __func__);
	int line = -1;

	int i = 0;
	int j = 0;
	char display_char_count = clcd_display_string_details->display_char_count; //clcd_display_string_details->end_point - clcd_display_string_details->start_point+1;
	char str_len = strlen((char*) clcd_display_string_details->string);
	line = clcd_display_string_details->start_row;
	j = clcd_display_string_details->start_col;
	for (i = 0; i < str_len;) {
		for (j = clcd_display_string_details->start_col; j < clcd_config_data.clcd_max_col; j++) {
			lcd_display_array[line][j] = clcd_display_string_details->string[i++];
			//CORE_LOGD(TAG,"%c", lcd_display_array[line][j]);
			if (i >= str_len)
				break;
			if (i >= display_char_count)
				break;
		}
		////CORE_LOGD(TAG,"i=%d,\r\n", i);
		j = 0;
		line++;
		if (i >= display_char_count)
			break;
	}
}

static void scroll_left_to_right(CLCD_STRING_DISPLAY_DETAILS *clcd_display_string_details) {
	//CORE_LOGD(TAG,"###################################################### %s\n", __func__);
	int line = -1;
	if (clcd_display_string_details->start_row == FIRST_ROW)
		line = FIRST_ROW;
	else if (clcd_display_string_details->start_row == SECOND_ROW)
		line = SECOND_ROW;
	if (clcd_display_string_details->start_row == THIRD_ROW)
		line = THIRD_ROW;
	if (clcd_display_string_details->start_row == FOURTH_ROW)
		line = FOURTH_ROW;

	memset(lcd_display_array[line] + clcd_display_string_details->start_col, ' ',
			clcd_display_string_details->display_char_count);
	if (clcd_display_string_details->start_row == clcd_display_string_details->end_row) {
		if (clcd_display_string_details->length_count_for_right_scroll
				> clcd_display_string_details->display_char_count) {
			if (clcd_display_string_details->string[FIRST_ROW] != '\0') {
				strncpy((char*) lcd_display_array[line] + clcd_display_string_details->start_col,
						(char*) clcd_display_string_details->string
								+ ((clcd_display_string_details->strlen - 1)
										- clcd_display_string_details->length_decount_for_right_scroll),
						clcd_display_string_details->display_char_count);
			}
		} else {
			if (clcd_display_string_details->string[FIRST_ROW] != '\0') {
				strncpy(
						(char*) lcd_display_array[line] + clcd_display_string_details->start_col
								+ clcd_display_string_details->length_count_for_right_scroll,
						(char*) clcd_display_string_details->string,
						clcd_display_string_details->display_char_count
								- clcd_display_string_details->length_count_for_right_scroll);
			}
		}
	} else { // for multiline
		int i = 0, j = 0;
		char str_len = strlen((char*) clcd_display_string_details->string);
		line = clcd_display_string_details->start_row;
		j = clcd_display_string_details->start_col + clcd_display_string_details->length_count_for_right_scroll;
		for (i = 0; i <= clcd_display_string_details->end_row - clcd_display_string_details->start_row; i++) {
			if (j > clcd_config_data.clcd_max_col * (i + 1)) {
				line++;
			} else {

			}
		}
		//        if (j > clcd_config_data.clcd_max_col) {
		//            line++;
		//        } else if (j > clcd_config_data.clcd_max_col) {
		//            line++;
		//        } else if (j > clcd_config_data.clcd_max_col) {
		//            line++;
		//        } else if (j > clcd_config_data.clcd_max_col) {
		//            line++;
		//        }

		for (i = 0; i < str_len;) {
			for (j = j; j < clcd_config_data.clcd_max_col; j++) {
				lcd_display_array[line][j] = clcd_display_string_details->string[i++];
				if (i >= str_len)
					break;
				if (i >= clcd_display_string_details->display_char_count)
					break;
			}
			////CORE_LOGD(TAG,"i=%d,\r\n", i);
			j = 0;
			line++;
			if (i >= clcd_display_string_details->display_char_count)
				break;
		}
	}
	if (clcd_display_string_details->scroll_interval_count == clcd_display_string_details->scroll_interval - 1) {
		if (clcd_display_string_details->length_count_for_right_scroll
				<= clcd_display_string_details->display_char_count) {
			clcd_display_string_details->length_count_for_right_scroll++;
		} else if (clcd_display_string_details->length_decount_for_right_scroll < clcd_display_string_details->strlen) {
			clcd_display_string_details->length_decount_for_right_scroll++;
			if (clcd_display_string_details->length_decount_for_right_scroll >= clcd_display_string_details->strlen) {
				clcd_display_string_details->length_count_for_right_scroll = 0;
				clcd_display_string_details->length_decount_for_right_scroll = 0;
			}
		}
		clcd_display_string_details->scroll_interval_count = 0;
	} else {
		clcd_display_string_details->scroll_interval_count++;
	}
}

static void scroll_right_to_left(CLCD_STRING_DISPLAY_DETAILS *clcd_display_string_details) {
	//CORE_LOGD(TAG,"###################################################### %s\n", __func__);
	////CORE_LOGD(TAG,"%d.\n", max_char_len);
	int line = -1;
	if (clcd_display_string_details->start_row == FIRST_ROW)
		line = 0;
	else if (clcd_display_string_details->start_row == SECOND_ROW)
		line = 1;
	if (clcd_display_string_details->start_row == THIRD_ROW)
		line = 2;
	if (clcd_display_string_details->start_row == FOURTH_ROW)
		line = 3;
	////CORE_LOGD(TAG,"%d\n",clcd_display_string_details->start_col);
	memset(lcd_display_array[line] + clcd_display_string_details->start_col, ' ',
			clcd_display_string_details->display_char_count);
	if (clcd_display_string_details->start_row == clcd_display_string_details->end_row) {
		if (clcd_display_string_details->length_count_for_left_scroll > clcd_display_string_details->strlen) {
			////CORE_LOGD(TAG,"%d", (clcd_display_string_details->end_col - clcd_display_string_details->length_decount_for_left_scroll - 1));
			strncpy(
					(char*) lcd_display_array[line]
							+ (clcd_display_string_details->end_col
									- clcd_display_string_details->length_decount_for_left_scroll),
					(char*) clcd_display_string_details->string,
					(clcd_display_string_details->display_char_count
							- (clcd_display_string_details->display_char_count
									- (clcd_display_string_details->length_decount_for_left_scroll + 1))));
		} else {
			if (clcd_display_string_details->string[clcd_display_string_details->length_count_for_left_scroll]
					!= '\0') {
				strncpy((char*) lcd_display_array[line] + clcd_display_string_details->start_col,
						(char*) clcd_display_string_details->string
								+ clcd_display_string_details->length_count_for_left_scroll,
						clcd_display_string_details->display_char_count);
			} else {
				memset(lcd_display_array[line] + clcd_display_string_details->start_col, ' ',
						clcd_display_string_details->display_char_count);
			}
		}
	} else {        // for multiline

	}
	if (clcd_display_string_details->scroll_interval_count == clcd_display_string_details->scroll_interval - 1) {
		if (clcd_display_string_details->length_count_for_left_scroll <= clcd_display_string_details->strlen) //max_char_len)
				{
			clcd_display_string_details->length_count_for_left_scroll++;
		} else if (clcd_display_string_details->length_count_for_left_scroll > clcd_display_string_details->strlen) //max_char_len)
				{
			clcd_display_string_details->length_decount_for_left_scroll++;
			if (clcd_display_string_details->length_decount_for_left_scroll
					== clcd_display_string_details->display_char_count) {
				clcd_display_string_details->length_decount_for_left_scroll = 0;
				clcd_display_string_details->length_count_for_left_scroll = 0;
				//return;
			}

		}
		clcd_display_string_details->scroll_interval_count = 0;
	} else {
		clcd_display_string_details->scroll_interval_count++;
	}
}

void make_display() {
	int count = 0;
	//CORE_LOGD(TAG,"###################################################### %s\n", __func__);
	clear_lcd_display_array();

	for (count = 0; count < clcd_config_data.clcd_max_display_string; count++) {
		if (strlen((char*) clcd_display_string_details[count].string) != 0
				&& clcd_display_string_details[count].direction == SCROLL_LEFT_TO_RIGHT) {
			if (clcd_display_string_details[count].blink_interval != 0) {
				if (clcd_display_string_details[count].blink_interval_count
						== clcd_display_string_details[count].blink_interval - 1) {
					clcd_display_string_details[count].blink_interval_count = 0;
					clcd_display_string_details[count].blink = !clcd_display_string_details[count].blink;
				} else {
					clcd_display_string_details[count].blink_interval_count++;
				}
				if (clcd_display_string_details[count].blink != true)
					scroll_left_to_right(&clcd_display_string_details[count]);
			} else {
				scroll_left_to_right(&clcd_display_string_details[count]);
			}
		} else if (strlen((char*) clcd_display_string_details[count].string) != 0
				&& clcd_display_string_details[count].direction == SCROLL_RIGHT_TO_LEFT) {
			if (clcd_display_string_details[count].blink_interval != 0) {
				if (clcd_display_string_details[count].blink_interval_count
						== clcd_display_string_details[count].blink_interval - 1) {
					clcd_display_string_details[count].blink_interval_count = 0;
					clcd_display_string_details[count].blink = !clcd_display_string_details[count].blink;
				} else {
					clcd_display_string_details[count].blink_interval_count++;
				}
				if (clcd_display_string_details[count].blink != true)
					scroll_right_to_left(&clcd_display_string_details[count]);
			} else {
				scroll_right_to_left(&clcd_display_string_details[count]);
			}
		} else if (strlen((char*) clcd_display_string_details[count].string) != 0
				&& clcd_display_string_details[count].direction == SCROLL_OFF) {
			if (clcd_display_string_details[count].blink_interval != 0) {
				if (clcd_display_string_details[count].blink_interval_count
						== clcd_display_string_details[count].blink_interval - 1) {
					clcd_display_string_details[count].blink_interval_count = 0;
					clcd_display_string_details[count].blink = !clcd_display_string_details[count].blink;
					//                if (clcd_display_string_details[count].blink == true)
					//                    //CORE_LOGD(TAG,"blink = true, %d\r\n",clcd_display_string_details[count].blink);
					//                else if (clcd_display_string_details[count].blink == false)
					//                     //CORE_LOGD(TAG,"blink = false, %d\r\n",clcd_display_string_details[count].blink);
				} else {
					clcd_display_string_details[count].blink_interval_count++;
				}
				if (clcd_display_string_details[count].blink != true)
					scroll_off(&clcd_display_string_details[count]);
			} else {
				scroll_off(&clcd_display_string_details[count]);
			}
		}
	}
	print_data_ready = true;
	display_text();
}

/**************************************** CLCD Display Process Task ************************************************************/

static void clcd_display_process(void* param) {
	//CORE_LOGD(TAG,"###################################################### %s\n", __func__);
	while (1) {
		make_display();
		TaskDelay(DELAY_20_MSEC / TICK_RATE_TO_MS);
	}
}

/**************************************** CLCD User's Functions ************************************************************/

/*
 *
 */
core_err init_clcd_lcd(uint8_t clcd_init_row, uint8_t clcd_init_col) {
	core_err ret = CORE_FAIL;
	if (clcd_config_data.init_done != true) {

		ret = init_clcd_core();
		if (ret != CORE_OK) {
			return CORE_FAIL;
		}

		//CORE_LOGD(TAG,"row = %d\n", clcd_init_row);

		clcd_config_data.clcd_max_row = clcd_init_row;

		//CORE_LOGD(TAG,"col = %d\n", clcd_init_col);

		clcd_config_data.clcd_max_col = clcd_init_col;

		//CORE_LOGD(TAG,"string_count = %d\n", (clcd_init_col / DEFAULT_STRING_DIVIDER) * clcd_init_row);

		clcd_config_data.clcd_max_display_string = (clcd_init_col / DEFAULT_STRING_DIVIDER) * clcd_init_row;
		clcd_config_data.uid_range = START_UID + clcd_config_data.clcd_max_display_string;

		//CORE_LOGD(TAG,"uid_range = %d\r\n", clcd_config_data.uid_range);

		if (clcd_init_row > MAX_ROW || clcd_init_col > MAX_COL || clcd_init_row < 1 || clcd_init_col < 1) {
			return CORE_ERR_INVALID_SIZE;
		} else {
			lcd_display_array_size = clcd_config_data.clcd_max_col * clcd_config_data.clcd_max_row;
			int i = 0;
			if (lcd_display_array == NULL) {
				lcd_display_array = (unsigned char**) malloc(clcd_init_row * sizeof(unsigned char*));
				for (i = 0; i < clcd_init_row; i++) {
					lcd_display_array[i] = (unsigned char*) malloc(clcd_init_col * sizeof(unsigned char));
				}
				if (lcd_display_array == NULL) {
					CORE_LOGE(TAG, "Failed Init %dx%d CLCD..", clcd_init_row, clcd_init_col);
					lcd_display_array_size = 0;
					return ret;
				}
			} else {

			}
			CORE_LOGI(TAG, "lcd_display_array init done");
		}

		if (clcd_display_string_details == NULL) {
			clcd_display_string_details = (CLCD_STRING_DISPLAY_DETAILS *) malloc(
					clcd_config_data.clcd_max_display_string * sizeof(CLCD_STRING_DISPLAY_DETAILS));
			if (clcd_display_string_details == NULL) {
				CORE_LOGE(TAG, "Failed Init %dx%d CLCD..", clcd_init_row, clcd_init_col);

				free_lcd_display_array();
				lcd_display_array = NULL;

				lcd_display_array_size = 0;
				return CORE_FAIL;
			}
		} else {
			free(clcd_display_string_details);
			clcd_display_string_details = NULL;
			clcd_display_string_details = (CLCD_STRING_DISPLAY_DETAILS *) malloc(
					clcd_config_data.clcd_max_display_string * sizeof(CLCD_STRING_DISPLAY_DETAILS));
		}

		if (conflict_arr == NULL) {
			conflict_arr = (int*) calloc(clcd_config_data.clcd_max_display_string, sizeof(int));
			if (conflict_arr == NULL) {
				CORE_LOGE(TAG, "Failed Init %dx%d CLCD..", clcd_init_row, clcd_init_col);
				free_lcd_display_array();
				lcd_display_array = NULL;

				free(clcd_display_string_details);
				clcd_display_string_details = NULL;

				lcd_display_array_size = 0;
				return CORE_FAIL;
			}
			memset(conflict_arr, -1, clcd_config_data.clcd_max_display_string);
			CORE_LOGI(TAG, "conflict_arr init done");
		} else {
			free(conflict_arr);
			conflict_arr = NULL;
			conflict_arr = (int*) calloc(clcd_config_data.clcd_max_display_string, sizeof(int));
			if (conflict_arr == NULL) {
				CORE_LOGE(TAG, "Failed Init %dx%d CLCD..", clcd_init_row, clcd_init_col);
				free_lcd_display_array();
				lcd_display_array = NULL;
				free(clcd_display_string_details);
				clcd_display_string_details = NULL;
				lcd_display_array_size = 0;
				return CORE_FAIL;
			}
			memset(conflict_arr, -1, clcd_config_data.clcd_max_display_string);
			CORE_LOGI(TAG, "conflict_arr init done");
		}

		data_count = 0;
		clcd_config_data.init_done = true;
		clcd_display_string_config();
		clear_display_structure();

		bool stat = TaskCreate(clcd_display_process, "clcd_task", TASK_STACK_SIZE_3K, NULL, THREAD_PRIORITY_5,
				&clcd_config_data.clcd_task_handle);
		if (stat != true) {
			CORE_LOGE(TAG, "Failed Init %dx%d CLCD..", clcd_init_row, clcd_init_col);
			free_lcd_display_array();
			lcd_display_array = NULL;

			free(clcd_display_string_details);
			clcd_display_string_details = NULL;

			free(conflict_arr);
			conflict_arr = NULL;

			lcd_display_array_size = 0;

			return CORE_FAIL;
		}
		ret = CORE_OK;

	}
	return ret;
}

/*
 *
 */
core_err deinit_clcd() {
	core_err ret = CORE_FAIL;

	//clear_display_structure();
	//delay_ms(100);
	if (clcd_config_data.clcd_task_handle != NULL) {
		TaskDelete(clcd_config_data.clcd_task_handle);
		clcd_config_data.clcd_task_handle = NULL;
	}

	clear_display();

	free_lcd_display_array();
	lcd_display_array = NULL;

	free(clcd_display_string_details);
	clcd_display_string_details = NULL;

	free(conflict_arr);
	conflict_arr = NULL;

	lcd_display_array_size = 0;
	return ret = CORE_OK;
}

/*
 *
 */
core_err set_string(char *str, uint8_t start_row, uint8_t end_row, uint8_t start_col, uint8_t end_col,
		CLCD_STRING_SCROLL_OPTION scroll_opt, uint16_t scroll_frq, uint16_t blink_frq,
		CLCD_STRING_OVERWRITE_STAT over_write) {
	memset(&cmd_data, 0x00, sizeof(cmd_data));

	if (str == NULL) {
		return CORE_ERR_INVALID_ARG;
	}

	if (start_row > clcd_config_data.clcd_max_row || end_row > clcd_config_data.clcd_max_row) {
		return CORE_ERR_INVALID_ARG;
	}

	if (start_col > clcd_config_data.clcd_max_col || end_col > clcd_config_data.clcd_max_col) {
		return CORE_ERR_INVALID_ARG;
	}

	strcpy((char*) cmd_data.display_details.string, str);
	cmd_data.display_details.start_row = start_row;
	cmd_data.display_details.start_col = start_col;
	cmd_data.display_details.end_row = end_row;
	cmd_data.display_details.end_col = end_col;
	cmd_data.display_details.direction = scroll_opt;
	if (scroll_frq == 0) {
		cmd_data.display_details.scroll_interval = 0;
	} else {
		if (scroll_frq < DELAY_250_MSEC) {
			cmd_data.display_details.scroll_interval = DELAY_250_MSEC / DELAY_20_MSEC;
		} else {
			cmd_data.display_details.scroll_interval = scroll_frq / DELAY_20_MSEC;
		}
	}

	if (blink_frq == 0) {
		cmd_data.display_details.blink_interval = 0;
	} else {
		if (blink_frq < DELAY_250_MSEC) {
			cmd_data.display_details.blink_interval = DELAY_250_MSEC / DELAY_20_MSEC;
		} else {
			cmd_data.display_details.blink_interval = blink_frq / DELAY_20_MSEC;
		}
	}

	cmd_data.over_write = over_write;
	int val = set_string_data(&cmd_data);
	if (cmd_data.over_write == OVERWRITE) {
		//CORE_LOGD(TAG,"_______________________________________________%d\r\n", val);
		if (val == CONFLICT_STRING_CHECK_AND_OVERWRITE) {
			val = set_string_data(&cmd_data);
			//CORE_LOGD(TAG,"-----------------------------------------------------------------%d\r\n", val);
		} else if (CONFLICT_STRING_NOT_CONFLICT_WITH_OVERWRITE_ARGUMENT == val) {
			cmd_data.over_write = 0;
			val = set_string_data(&cmd_data);
			//CORE_LOGD(TAG,"-----------------------------------------------------------------%d\r\n", val);
		}
	}else{
		//CORE_LOGW(TAG,"_______________________________________________%d\r\n", val);
	}
	return val;
}

/*
 *
 */
char max_display_str() {
	if (clcd_config_data.init_done == false)
		return 0;
	else
		return clcd_config_data.clcd_max_display_string;
}

/*
 *
 */
char min_uid_range() {
	if (clcd_config_data.init_done == false)
		return 0;
	else
		return START_UID;
}

/*
 *
 */
char max_uid_range() {
	if (clcd_config_data.init_done == false)
		return 0;
	else
		return clcd_config_data.uid_range - 1;
}

//char* max_display_support() {
//	return MAX_DISPLAY_SUPPORT;
//}

/*
 *
 */
char clcd_max_row() {
	if (clcd_config_data.init_done == false)
		return 0;
	else
		return clcd_config_data.clcd_max_row;
}

/*
 *
 */
char clcd_max_col() {
	if (clcd_config_data.init_done == false)
		return 0;
	else
		return clcd_config_data.clcd_max_col;
}

