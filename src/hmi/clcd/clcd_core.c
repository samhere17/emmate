/*
 * File Name: clcd_core.c
 * 
 * Description:
 *
 *  Created on: 02-Sep-2019
 *      Author: Noyel Seth
 */

#include "clcd_core.h"
#include "gpio_helper_api.h"
#include "delay_utils.h"
#include <string.h>

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG "clcd_core"

#define CLCD_INIT_DONE_MSG		"Init_Done.."

#define CMD_MODE 0
#define DATA_MODE 1

#define LCD_WRITE_MODE  0
#define LCD_READ_MODE  1

#define EIGHT_BIT_TWO_LINE_DISPLAY		write_lcd_cmd(TWO_LINE_EIGHT_BIT_DISPLAY_CMD);
#define ELEVEN_BIT_TWO_LINE_DISPLAY		write_lcd_cmd(TWO_LINE_ELEVEN_BIT_DISPLAY_CMD);
#define DISPLAY_ON_CURSOR_ON			write_lcd_cmd(DISPLAY_ON_CURSOR_ON_CMD);
#define CLEAR_DISPLAY					write_lcd_cmd(CLEAR_DISPLAY_CMD);
#define ENTRY_MODE						write_lcd_cmd(INCREMET_CURSOR_CMD);

#define LCD_STROBE() {set_gpio_value(LCD_EN, HIGH); \
					delay_us(100);\
					set_gpio_value(LCD_EN, LOW);}

void is_busy(void) {
	//LCD_D7_RC5_SetDigitalInput();
	set_gpio_value(LCD_RS, CMD_MODE);
	set_gpio_value(LCD_RW, LCD_READ_MODE);

	delay_us(10);
	do {
		LCD_STROBE()
		;
	} while (get_gpio_value(LCD_BUSY) == HIGH);

	set_gpio_value(LCD_RW, LCD_WRITE_MODE);
	delay_us(10);
	//printf("%s\n", __func__);
	//LCD_D7_RC5_SetDigitalOutput();
}

void write_lcd_cmd(unsigned char cmd) {

	set_gpio_value(LCD_D7, (cmd & 0x80));
	set_gpio_value(LCD_D6, (cmd & 0x40));
	set_gpio_value(LCD_D5, (cmd & 0x20));
	set_gpio_value(LCD_D4, (cmd & 0x10));

	set_gpio_value(LCD_D3, (cmd & 0x08));
	set_gpio_value(LCD_D2, (cmd & 0x04));
	set_gpio_value(LCD_D1, (cmd & 0x02));
	set_gpio_value(LCD_D0, (cmd & 0x01));

	set_gpio_value(LCD_RS, CMD_MODE);
	set_gpio_value(LCD_RW, LCD_WRITE_MODE);

	LCD_STROBE()
	;
}

void write_lcd_data(unsigned char data) {

	set_gpio_value(LCD_D7, (data & 0x80));
	set_gpio_value(LCD_D6, (data & 0x40));
	set_gpio_value(LCD_D5, (data & 0x20));
	set_gpio_value(LCD_D4, (data & 0x10));

	set_gpio_value(LCD_D3, (data & 0x08));
	set_gpio_value(LCD_D2, (data & 0x04));
	set_gpio_value(LCD_D1, (data & 0x02));
	set_gpio_value(LCD_D0, (data & 0x01));

	set_gpio_value(LCD_RS, DATA_MODE);
	set_gpio_value(LCD_RW, LCD_WRITE_MODE);

	LCD_STROBE()
	;
}

void write_str(char* str) {
	char i = 0;
	char len = strlen(str);
	while (i < len) {
		write_lcd_data(*str++);
		is_busy();
		i++;
	}
}

core_err init_clcd_core(void) {
	core_err ret = CORE_FAIL;

	// Configure DataBus GPIO
	ret = configure_gpio(LCD_D0, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}
	ret = configure_gpio(LCD_D1, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}
	ret = configure_gpio(LCD_D2, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}
	ret = configure_gpio(LCD_D3, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

	ret = configure_gpio(LCD_D4, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}
	ret = configure_gpio(LCD_D5, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}
	ret = configure_gpio(LCD_D6, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}
	ret = configure_gpio(LCD_D7, GPIO_IO_MODE_INPUT_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

	// Configure controlBus GPIO
	ret = configure_gpio(LCD_RS, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}
	ret = configure_gpio(LCD_RW, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}
	ret = configure_gpio(LCD_EN, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (ret != CORE_OK) {
		return CORE_FAIL;
	}

	set_gpio_value(LCD_EN, LOW);
	delay_ms(250);

	//EIGHT_BIT_TWO_LINE_DISPLAY;
	write_lcd_cmd(TWO_LINE_EIGHT_BIT_DISPLAY_CMD);
	delay_ms(250);

	write_lcd_cmd(DISPLAY_ON_CURSOR_ON_CMD);
	//DISPLAY_ON_CURSOR_ON
	;
	delay_ms(250);

	write_lcd_cmd(CLEAR_DISPLAY_CMD);
	//CLEAR_DISPLAY
	;
	delay_ms(250);

	write_lcd_cmd(INCREMET_CURSOR_CMD);
	//ENTRY_MODE
	;
	delay_ms(250);

	write_lcd_cmd(DISPLAY_ON_CURSOR_OFF_CMD);
	//write_lcd_cmd(DISPLAY_ON_CURSOR_BLINK_CMD);
	delay_ms(250);

	//printf("init complete\n");
	write_str(CLCD_INIT_DONE_MSG);
	delay_ms(250);
	//delay_ms(2000);
	write_lcd_cmd(CLEAR_DISPLAY_CMD);
	is_busy();
	write_lcd_cmd(RETURN_HOME_CMD);
	is_busy();
	//write_lcd_cmd(DISPLAY_ON_CURSOR_OFF_CMD);
	//is_busy();
	CORE_LOGI(TAG,"CLCD Core init done\n");
	return ret;

}
