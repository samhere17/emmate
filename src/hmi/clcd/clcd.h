/*
 * File Name: clcd.h
 * 
 * Description:
 *
 *  Created on: 30-Aug-2019
 *      Author: Noyel Seth
 */

#ifndef CLCD_CLCD_H_
#define CLCD_CLCD_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include "core_error.h"

#define MAX_ROW     CONFIG_CLCD_MAX_ROW
#define MAX_COL     CONFIG_CLCD_MAX_COLUMN

#define FIRST_ROW       0
#define SECOND_ROW      1
#define THIRD_ROW       2
#define FOURTH_ROW      3

#define DEFAULT_STRING_DIVIDER          4

#define START_UID       'A'

#define MAX_DISPLAY_STRUCT     20

#define MAX_DISPLAY_DATA_LEN    40

#define MAX_DATA_LEN    40

typedef enum {
	NOT_OVERWRITE = 0, OVERWRITE,
} CLCD_STRING_OVERWRITE_STAT;

typedef enum {
	SCROLL_OFF = 0, SCROLL_RIGHT_TO_LEFT = 1, SCROLL_LEFT_TO_RIGHT = 2,
} CLCD_STRING_SCROLL_OPTION;

typedef enum {
	CONFLICT_STRING = 200,
	CONFLICT_STRING_CHECK_AND_OVERWRITE = 201,
	CONFLICT_STRING_OVERWRITE = 202,
	CONFLICT_STRING_NOT_CONFLICT_WITH_OVERWRITE_ARGUMENT = 203,
}CLCD_DISPLAY_STRING_CONFLICT_STATUS;


/**
 *	@brief Intilize the Character LCD
 *
 *	@param	clcd_init_row		CLCD Row range
 *
 *	@param	clcd_init_col		CLCD Column range
 *
 *	@return
 *     - CORE_OK on success
 *     - CORE_FAIL on fail
 *     - CORE_ERR_INVALID_SIZE	If CLCD ROW and Column size greater then the CLCD MAX Row & Column size
 *
 */
core_err init_clcd_lcd(uint8_t clcd_init_row, uint8_t clcd_init_col);

/**
 *	@brief De-Intilize the Character LCD
 *
 *	@return
 *     - CORE_OK on success
 *     - CORE_FAIL on fail
 *
 */
core_err deinit_clcd();

/**
 *	@brief Set string to Character LCD
 *
 *	@param	str				Display String
 *
 *	@param	start_row		String's Start Row number
 *
 *	@param	end_row			String's End Row number
 *
 *	@param	start_col		String's Start Column number
 *
 *	@param	end_col			String's End Column number
 *
 *	@param	scroll_opt		String's Scroll direction
 *
 *	@param	scroll_frq		String's Scroll time(ms)
 *
 *	@param	blink_frq		String's Blink time(ms)
 *
 *	@param	over_write		Overwrite the conflicted strings
 *
 *
 *	@return
 *		- UID of the Current set String
 *		- CONFLICT_STRING	If set string getting conflict with other configured string.
 *		- CORE_ERR_INVALID_ARG if invalid str, column and row number.
 *
 */
core_err set_string(char *str, uint8_t start_row, uint8_t end_row, uint8_t start_col, uint8_t end_col,
		CLCD_STRING_SCROLL_OPTION scroll_opt, uint16_t scroll_frq, uint16_t blink_frq,
		CLCD_STRING_OVERWRITE_STAT over_write);


#ifdef __cplusplus
}
#endif

#endif /* CLCD_CLCD_H_ */
