/*
 * File Name: clcd_core.h
 * 
 * Description:
 *
 *  Created on: 02-Sep-2019
 *      Author: Noyel Seth
 */

#ifndef CLCD_CORE_H_
#define CLCD_CORE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_error.h"
#include "thing.h"


// LCD_Config_CMD
#define TWO_LINE_EIGHT_BIT_DISPLAY_CMD      0X38            /*  0x001110XX      (RB7-RB0)
                                                                (0x001 DL N F X X)
                                                                DL = 1 (8 BIT_MODE DATA_BUS)
                                                                N = 1   (2-LINE DISPLAY)
                                                                F = 0 (5x8 DOT FORMAT CHAR DISPLAY)
                                                                Note: F = 1 (5x11 DOT FORMAT Char Display)
                                                            */
#define TWO_LINE_ELEVEN_BIT_DISPLAY_CMD      0X3C           // 0x001111XX

#define CLEAR_DISPLAY_CMD       0x01
#define RETURN_HOME_CMD     0x02

#define DECREMENT_CURSOR_CMD    0x04    //(shift to left)
#define INCREMET_CURSOR_CMD     0x06    //(shift to right)

#define SHIFT_DISPLAY_RIGHT_CMD 0x05
#define SHIFT_DISPLAY_LEFT_CMD  0x07

#define DISPLAY_OFF_CURSOR_OFF_CMD  	0x08
#define DISPLAY_OFF_CURSOR_ON_CMD   	0x0A
#define DISPLAY_ON_CURSOR_OFF_CMD   	0x0C
#define DISPLAY_ON_CURSOR_ON_CMD    	0x0E
#define DISPLAY_ON_CURSOR_BLINK_CMD 	0x0F

#define SHIFT_CURSOR_POSITION_TO_LEFT_CMD   0X10
#define SHIFT_CURSOR_POSITION_TO_RIGHT_CMD   0X14

#define SHIFT_ENTIRE_DISPLAY_TO_LEFT_CMD    0x18
#define SHIFT_ENTIRE_DISPLAY_TO_RIGHT_CMD    0x1C

#define MAX_CORDINATE_OF_ROW_1      0x93
#define MAX_CORDINATE_OF_ROW_2      0xD3
#define MAX_CORDINATE_OF_ROW_3      0xA7
#define MAX_CORDINATE_OF_ROW_4      0xE7

#define LINE1_HOME						0x80
#define LINE2_HOME						0xC0
#define LINE3_HOME						0x94
#define LINE4_HOME						0xD4

/**
 *	@brief CLCD configuration
 *
 *	@return
 *     - CORE_OK on success
 *     - CORE_FAIL on fail
 *
 */
core_err init_clcd_core(void);

void write_lcd_data(unsigned char data);
void write_lcd_cmd(unsigned char cmd);
void is_busy(void);


#ifdef __cplusplus
}
#endif

#endif /* CLCD_CORE_H_ */
