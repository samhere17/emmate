/*
 * system_hmi_button.h
 *
 *  Created on: 20-Jul-2019
 *      Author: Rohan Dey
 */

#ifndef SRC_HMI_SYSTEM_HMI_BUTTON_H_
#define SRC_HMI_SYSTEM_HMI_BUTTON_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_error.h"
#include "core_constant.h"
#if CONFIG_USE_BUTTON
#include "button_helper.h"
#endif
#include "hmi.h"
///////////////////////////////////////// BUTTONS /////////////////////////////////////////

void init_reset_btn();
void register_hmi_reset_button_cb(reset_interaction_cb cb_handler);

#ifdef __cplusplus
}
#endif

#endif /* SRC_HMI_SYSTEM_HMI_BUTTON_H_ */
