/*
 * File Name: core_intr_alloc.h
 * File Path: /emmate/src/peripherals/include/core_intr_alloc.h
 * Description:
 *
 *  Created on: 30-May-2019
 *      Author: Noyel Seth
 */

#ifndef CORE_INTR_ALLOC_H_
#define CORE_INTR_ALLOC_H_

#include "pf_peripheral_common.h"

//Keep the LEVELx values as they are here; they match up with (1<<level)
#define CORE_INTR_FLAG_LEVEL1 			PF_INTR_FLAG_LEVEL1	///< Accept a Level 1 interrupt vector (lowest priority)
#define CORE_INTR_FLAG_LEVEL2 			PF_INTR_FLAG_LEVEL2	///< Accept a Level 2 interrupt vector
#define CORE_INTR_FLAG_LEVEL3 			PF_INTR_FLAG_LEVEL3	///< Accept a Level 3 interrupt vector
#define CORE_INTR_FLAG_LEVEL4 			PF_INTR_FLAG_LEVEL4	///< Accept a Level 4 interrupt vector
#define CORE_INTR_FLAG_LEVEL5 			PF_INTR_FLAG_LEVEL5	///< Accept a Level 5 interrupt vector
#define CORE_INTR_FLAG_LEVEL6 			PF_INTR_FLAG_LEVEL6	///< Accept a Level 6 interrupt vector
#define CORE_INTR_FLAG_NMI 				PF_INTR_FLAG_NMI			///< Accept a Level 7 interrupt vector (highest priority)
#define CORE_INTR_FLAG_SHARED 			PF_INTR_FLAG_SHARED	///< Interrupt can be shared between ISRs
#define CORE_INTR_FLAG_EDGE 			PF_INTR_FLAG_EDGE		///< Edge-triggered interrupt
#define CORE_INTR_FLAG_IRAM 			PF_INTR_FLAG_IRAM			///< ISR can be called if cache is disabled
#define CORE_INTR_FLAG_INTRDISABLED 	PF_INTR_FLAG_INTRDISABLED		///< Return with this interrupt disabled

#define CORE_INTR_FLAG_LOWMED 			PF_INTR_FLAG_LOWMED ///< Low and medium prio interrupts. These can be handled in C.
#define CORE_INTR_FLAG_HIGH 			PF_INTR_FLAG_HIGH ///< High level interrupts. Need to be handled in assembly.

#define CORE_INTR_FLAG_LEVELMASK 		PF_INTR_FLAG_LEVELMASK	///< Mask for all level flags

#endif /* CORE_INTR_ALLOC_H_ */
