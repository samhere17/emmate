/*
 * File Name: i2c_core_local.h
 * File Path: /EmMate-fork/src/peripherals/i2c/i2c_core_local.h
 * Description:
 *
 *  Created on: 27-Jun-2019
 *      Author: Noyel Seth
 */

#ifndef I2C_CORE_LOCAL_H_
#define I2C_CORE_LOCAL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <string.h>

#include "gll.h"
#include "threading.h"
#include "core_config.h"

/**************************************************************************************************************
 **************************************************************************************************************
 **************************************************************************************************************/

#define CORE_I2C_CLIENTS_NUMBER			5 //CONFIG_I2C_CLIENTS_NUMBER

typedef enum {
	I2C_CORE_NOT_INTITIALIZE = 0, I2C_CORE_READY, I2C_CORE_IDLE, I2C_CORE_BUSY,
} I2C_CORE_DRV_STATUS;

/**
 * @brief I2C connection initialization parameters
 */
typedef struct {
	uint32_t i2c_port; /*!< I2C platform port  */
	uint32_t i2c_mode; /*!< I2C mode */

	uint32_t sda_io_num; /*!< GPIO number for I2C sda signal */
	uint32_t scl_io_num; /*!< GPIO number for I2C scl signal */

	bool is_internal_pullup_enable; /*!< GPIO's Internal pull-up mode enable variable for I2C sda/scl signal*/

	union {
		struct {
			uint32_t clk_speed; /*!< I2C clock frequency for master mode, (no higher than 1MHz for now) */
		} master;
		struct {
			uint8_t addr_10bit_en; /*!< I2C 10bit address mode enable for slave mode */
			uint16_t slave_addr; /*!< I2C address for slave mode */
		} slave;
	};
} I2C_CONN_DATA;

typedef void* I2C_CORE_DRV_HANDLE;
typedef void* I2C_CORE_CLIENT_HANDLE;


typedef struct _I2C_CORE_DRV_OBJ {
	I2C_CORE_DRV_STATUS status;
	I2C_CONN_DATA i2c_conn_data;
	SemaphoreHandle mutex_handle;
	gll_t *i2c_client_list;
	/* Number of clients possible with the hardware instance */
	uint8_t numClients;
} I2C_CORE_DRV_OBJ;

typedef struct _I2C_CORE_CLIENT_OBJ {
	I2C_CORE_DRV_OBJ *i2c_drv_obj;
	char node_name[15];
} I2C_CORE_CLIENT_OBJ;

#define DRV_INVALID_HANDLE	NULL

#define I2C_CLIENT_NODE_PREFIX_NAME		"node-"

/**************************************************************************************************************
 **************************************************************************************************************
 **************************************************************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* I2C_CORE_LOCAL_H_ */
