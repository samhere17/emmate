/*
 * File Name: adc_core_channel.h
 * File Path: /emmate/src/peripherals/adc/adc_core_channel.h
 * Description:
 *
 *  Created on: 29-May-2019
 *      Author: Noyel Seth
 */

#ifndef ADC_CORE_CHANNEL_H_
#define ADC_CORE_CHANNEL_H_

#include "core_config.h"
#include "adc_platform_channel.h"

/**************************** ADC Channel 1 ******************************************/
#define ADC_1_CORE_CHANNEL_0_GPIO	ADC_1_PF_CHANNEL_0_GPIO
#if !CONFIG_PLATFORM_ESP_IDF
#define ADC_1_CORE_CHANNEL_1_GPIO	ADC_1_PF_CHANNEL_1_GPIO
#define ADC_1_CORE_CHANNEL_2_GPIO	ADC_1_PF_CHANNEL_2_GPIO
#endif
#define ADC_1_CORE_CHANNEL_3_GPIO	ADC_1_PF_CHANNEL_3_GPIO
#define ADC_1_CORE_CHANNEL_4_GPIO	ADC_1_PF_CHANNEL_4_GPIO
#define ADC_1_CORE_CHANNEL_5_GPIO	ADC_1_PF_CHANNEL_5_GPIO
#define ADC_1_CORE_CHANNEL_6_GPIO	ADC_1_PF_CHANNEL_6_GPIO
#define ADC_1_CORE_CHANNEL_7_GPIO	ADC_1_PF_CHANNEL_7_GPIO

/**************************** ADC Channel 2 **************************************/
#define ADC_2_CORE_CHANNEL_0_GPIO	ADC_2_PF_CHANNEL_0_GPIO
#define ADC_2_CORE_CHANNEL_1_GPIO	ADC_2_PF_CHANNEL_1_GPIO
#define ADC_2_CORE_CHANNEL_2_GPIO	ADC_2_PF_CHANNEL_2_GPIO
#define ADC_2_CORE_CHANNEL_3_GPIO	ADC_2_PF_CHANNEL_3_GPIO
#define ADC_2_CORE_CHANNEL_4_GPIO	ADC_2_PF_CHANNEL_4_GPIO
#define ADC_2_CORE_CHANNEL_5_GPIO	ADC_2_PF_CHANNEL_5_GPIO
#define ADC_2_CORE_CHANNEL_6_GPIO	ADC_2_PF_CHANNEL_6_GPIO
#if !CONFIG_PLATFORM_ESP_IDF
#define ADC_2_CORE_CHANNEL_7_GPIO	ADC_2_PF_CHANNEL_7_GPIO
#define ADC_2_CORE_CHANNEL_8_GPIO	ADC_2_PF_CHANNEL_8_GPIO
#define ADC_2_CORE_CHANNEL_9_GPIO	ADC_2_PF_CHANNEL_9_GPIO
#endif

#endif /* ADC_CORE_CHANNEL_H_ */
