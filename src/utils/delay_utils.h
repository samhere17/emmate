/*
 * delay_utils.h
 *
 *  Created on: 29-Aug-2019
 *      Author: Rohan Dey
 */

#ifndef DELAY_UTILS_H_
#define DELAY_UTILS_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 *	@brief	Create Delay for Milliseconds
 *
 *	@param	ms_delay	Delay amount
 *
 */
void delay_ms(uint16_t ms_delay);

/**
 *	@brief	Create Delay for Microseconds
 *
 *	@param	us_delay	Delay amount
 *
 */
void delay_us(uint16_t us_delay);

#ifdef __cplusplus
}
#endif

#endif /* DELAY_UTILS_H_ */
