/*
 * OWTemp.h
 *
 *  Created on: 23-Jun-2019
 *      Author: Rohan Dey
 */

#ifndef OWTEMP_H_
#define OWTEMP_H_

//#include "Slaves/Sensors/Sensors.h"
//#include "Slaves/Switches/Switches.h"
#include <iostream>
#include <list>
#include <iterator>
#include "Sensors.h"
#include "Switches.h"

using namespace std;
using namespace OneWire;

#define OWTEMP	"owtemp"

class OWTemp {
private:
	DS2413 m_switch;
	DS18B20 m_temp_sensor;

public:
	/**
    * @brief OWTemp command results
    */
    enum CmdResult
    {
        Success,
        OpFailure
    };

    static const uint8_t DS18B20_FAMILY_CODE = 0x28;
    static const uint8_t DS2413_FAMILY_CODE = 0x3A;

    /**
    * @brief OWTemp Constructor
    *
    * @details setOWSwitchRomId() and setI2CBridgeRomId() must be
    * called before any other member fxs.
    *
    * @param[in] selector - MultidropRomIterator object that
    * encapsulates ROM fxs of 1-wire protocol
    */
	OWTemp(RandomAccessRomIterator & selector);

	/**
    * @brief setOWSwitchRomId
    *
    * @details sets the RomId of the DS2413
    *
    * On Entry:
    * @param[in] romId - RomId of the DS2413 for this module
    *
    * @return none
    */
    void setOWSwitchRomId(const RomId & romId)
    {
        m_switch.setRomId(romId);
    };


    /**
    * @brief getOWSwitchRomId
    *
    * @details Gets the RomId of the DS2413 for this sensor.
    * The romId must have been set first.
    *
    * @return RomId of the DS2413 for this sensor
    */
    RomId getOWSwitchRomId(void)
    {
        return m_switch.romId();
    };

    /**
    * @brief setOWTempSensorRomId
    *
    * @details sets the RomId of the DS18B20
    *
    * On Entry:
    * @param[in] romId - RomId of the DS18B20 for this module
    *
    * @return none
    */
    void setOWTempSensorRomId(const RomId & romId)
    {
        m_temp_sensor.setRomId(romId);
    };


    /**
    * @brief getOWTempSensorRomId
    *
    * @details Gets the RomId of the DS18B20 for this sensor.
    * The romId must have been set first.
    *
    * @return RomId of the DS18B20 for this sensor
    */
    RomId getOWTempSensorRomId(void)
    {
        return m_temp_sensor.romId();
    };

    /**
    * @brief disconnectTempSensor
    *
    * @details Disconnects the DS18B20 sensor
    *
    * @return CmdResult - result of operation
    */
    CmdResult disconnectTempSensor(void);


    /**
    * @brief connectTempSensor
    *
    * @details Connects the DS18B20 to 1-wire bus
    *
    * @return CmdResult - result of operation
    */
    CmdResult connectTempSensor(void);

    /**
    * @brief connectOWbus
    *
    * @details Connects down stream devices on 1-wire bus
    *
    * @return CmdResult - result of operation
    */
    CmdResult connectOWbus(void);

    /**
    * @brief disconnectOWbus
    *
    * @details Disconnects down stream devices on 1-wire bus
    *
    * @return CmdResult - result of operation
    */
    CmdResult disconnectOWbus(void);

    /**
    * @brief getTemperature
    *
    * @details Reads the temperature from a DS18B20 sensor
    *
    * @param[out]	temp The read temperature in Celcius
    *
    * @return CmdResult - result of operation
    */
    CmdResult getTemperature(float & temp);

    /**
    * @brief OWTemp Destructor
    */
	virtual ~OWTemp();
};

#endif /* OWTEMP_H_ */
