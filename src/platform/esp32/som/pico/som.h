
#if 1
/*
 * This Header file is to represent the SOM pin connection with the Microcontroller gpio pins.
 * This will is also reflect the functions in comment section.
 *
 * SOM pin header for ESP32 chip
 *
 * Created By:- Sourav Gupta (Hardware desk)
 * Created For:-Embeded Team
 * -------------------------------------------------------------------------------------------
 * Date:- 7.09.2019
 * Revision:- 1 . Initial pin paths located in the header files
 *
 *
 */

/*Analog pins*/

/*This pins can only be used as input*/
#define SOM_PIN_2 		39 //ADC1 Channel 3
#define SOM_PIN_8 		//36
//------------------------------------------------------------------------------------------------------------------------------------------------

/*GPIO*/
#define SOM_PIN_79		16	/* Note: Cannot be used when PSRAM is connected */
#define SOM_PIN_85		17	/* Note: Cannot be used when PSRAM is connected */
#define SOM_PIN_97		21
#define SOM_PIN_98		22
#define SOM_PIN_192		2
#define SOM_PIN_101		36 //ADC1 Channel 0    //25
#define SOM_PIN_25		26
#define SOM_PIN_27		18
#define SOM_PIN_47		14
#define SOM_PIN_49		4
#define SOM_PIN_51		12      /*Firmware Reset - Soft Reset*/
#define SOM_PIN_53		13
#define SOM_PIN_190		15


/*UART Pins*/
//UART 1/2
#define SOM_PIN_36 		34 // RXD
#define SOM_PIN_38 		 0 // TXD

//UART 0

#define SOM_PIN_33 		 3 // RXD
#define SOM_PIN_35		 1 // TXD
//------------------------------------------------------------------------------------------------------------------------------------------------

/*SPI Pins*/
#define SOM_PIN_86 		33 // CS
#define SOM_PIN_88 		32 // CLK
#define SOM_PIN_90 		35 // MISO / RXD
#define SOM_PIN_92 		19 // MOSI / TXD
//------------------------------------------------------------------------------------------------------------------------------------------------


/*I2C*/
#define SOM_PIN_194 	23	//SDA
#define SOM_PIN_196 	 5	//SCL
//------------------------------------------------------------------------------------------------------------------------------------------------


/*Power Pins*/
/*This pins are used for power delivery across the SOM, hence not reflecting across the SOM SODIMM module as gpios*/
#define SOM_PIN_42  //3.3V
#define SOM_PIN_84  //3.3V
#define SOM_PIN_108 //3.3V
#define SOM_PIN_148 //3.3V
#define SOM_PIN_198 //3.3V
#define SOM_PIN_200 //3.3V
#define SOM_PIN_39  //GND
#define SOM_PIN_41  //GND
#define SOM_PIN_83  //GND
#define SOM_PIN_109 //GND
#define SOM_PIN_147 //GND
#define SOM_PIN_181 //GND
#define SOM_PIN_197 //GND
#define SOM_PIN_199 //GND

/*Special Purpose Pins*/
#define SOM_PIN_26  //EN //This pin is used for reset the ESP module.
//------------------------------------------------------------------------------------------------------------------------------------------------

/*Unused Pins*/
#define SOM_PIN_3		25
#define SOM_PIN_4		27
#define SOM_PIN_5
#define SOM_PIN_6
#define SOM_PIN_1
#define SOM_PIN_7
#define SOM_PIN_9
#define SOM_PIN_10
#define SOM_PIN_11
#define SOM_PIN_12
#define SOM_PIN_13
#define SOM_PIN_14
#define SOM_PIN_15
#define SOM_PIN_16
#define SOM_PIN_17
#define SOM_PIN_18
#define SOM_PIN_19
#define SOM_PIN_20
#define SOM_PIN_21
#define SOM_PIN_22
#define SOM_PIN_23
#define SOM_PIN_24
#define SOM_PIN_28
#define SOM_PIN_30
#define SOM_PIN_31
#define SOM_PIN_32
#define SOM_PIN_34
#define SOM_PIN_37
#define SOM_PIN_40
#define SOM_PIN_43
#define SOM_PIN_44
#define SOM_PIN_45
#define SOM_PIN_46
#define SOM_PIN_48
#define SOM_PIN_50
#define SOM_PIN_52
#define SOM_PIN_54
#define SOM_PIN_55
#define SOM_PIN_56
#define SOM_PIN_57
#define SOM_PIN_58
#define SOM_PIN_59
#define SOM_PIN_60
#define SOM_PIN_61
#define SOM_PIN_62
#define SOM_PIN_63
#define SOM_PIN_64
#define SOM_PIN_65
#define SOM_PIN_66
#define SOM_PIN_67
#define SOM_PIN_68
#define SOM_PIN_69
#define SOM_PIN_70
#define SOM_PIN_71
#define SOM_PIN_72
#define SOM_PIN_73
#define SOM_PIN_74
#define SOM_PIN_75
#define SOM_PIN_76
#define SOM_PIN_77
#define SOM_PIN_78
#define SOM_PIN_80
#define SOM_PIN_81
#define SOM_PIN_82
#define SOM_PIN_87
#define SOM_PIN_89
#define SOM_PIN_91
#define SOM_PIN_93
#define SOM_PIN_94
#define SOM_PIN_95
#define SOM_PIN_96
#define SOM_PIN_99
#define SOM_PIN_100
#define SOM_PIN_102
#define SOM_PIN_103
#define SOM_PIN_104
#define SOM_PIN_105
#define SOM_PIN_106
#define SOM_PIN_107
#define SOM_PIN_110
#define SOM_PIN_112
#define SOM_PIN_111
#define SOM_PIN_113
#define SOM_PIN_114
#define SOM_PIN_115
#define SOM_PIN_116
#define SOM_PIN_117
#define SOM_PIN_118
#define SOM_PIN_119
#define SOM_PIN_120
#define SOM_PIN_121
#define SOM_PIN_122
#define SOM_PIN_123
#define SOM_PIN_124
#define SOM_PIN_125
#define SOM_PIN_126
#define SOM_PIN_127
#define SOM_PIN_128
#define SOM_PIN_129
#define SOM_PIN_130
#define SOM_PIN_131
#define SOM_PIN_132
#define SOM_PIN_133
#define SOM_PIN_134
#define SOM_PIN_135
#define SOM_PIN_136
#define SOM_PIN_137
#define SOM_PIN_138
#define SOM_PIN_139
#define SOM_PIN_140
#define SOM_PIN_141
#define SOM_PIN_142
#define SOM_PIN_143
#define SOM_PIN_144
#define SOM_PIN_145
#define SOM_PIN_146
#define SOM_PIN_149
#define SOM_PIN_150
#define SOM_PIN_151
#define SOM_PIN_152
#define SOM_PIN_153
#define SOM_PIN_154
#define SOM_PIN_155
#define SOM_PIN_156
#define SOM_PIN_157
#define SOM_PIN_158
#define SOM_PIN_159
#define SOM_PIN_160
#define SOM_PIN_161
#define SOM_PIN_162
#define SOM_PIN_163
#define SOM_PIN_164
#define SOM_PIN_165
#define SOM_PIN_166
#define SOM_PIN_167
#define SOM_PIN_168
#define SOM_PIN_169
#define SOM_PIN_170
#define SOM_PIN_171
#define SOM_PIN_172
#define SOM_PIN_173
#define SOM_PIN_175
#define SOM_PIN_177
#define SOM_PIN_179
#define SOM_PIN_182
#define SOM_PIN_184
#define SOM_PIN_186
#define SOM_PIN_188
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//The END.

#else
/*
 * This Header file is to represent the SOM pin connection with the Microcontroller gpio pins.
 * This will is also reflect the functions in comment section.
 *
 * SOM pin header for ESP32 chip
 *
 * Created By:- Sourav Gupta (Hardware desk)
 * Created For:-Embeded Team
 * -------------------------------------------------------------------------------------------
 * Date:- 3.06.2019
 * Revision:- 1 . Initial pin paths located in the header files
 *
 *
 */

/*Analog pins*/

/*This pins can only be used as input*/
#define SOM_PIN_2 		39 //ADC1 Channel 3
//------------------------------------------------------------------------------------------------------------------------------------------------

/*GPIO*/
#define SOM_PIN_79		16	// GPIO /* Used in the framework as System's GREEN LED */ /* Note: Cannot be used when PSRAM is connected */
#define SOM_PIN_85		17	// GPIO /* Used in the framework as System's BLUE LED */ /* Note: Cannot be used when PSRAM is connected */
#define SOM_PIN_97		2 	// GPIO /* Used in the framework as System's RED/MONO LED */
#define SOM_PIN_101		36 	// GPIO /* Used in the framework as System's Factory Reset Button */

/*UART Pins*/
//UART 1/2
#define SOM_PIN_36 		34 // RXD
#define SOM_PIN_38 		14 // TXD

//UART 0
#define SOM_PIN_27		18 // RTS
#define SOM_PIN_29		 5 // DSR /*This pin is also used in I2C*/
#define SOM_PIN_33 		 3 // RXD
#define SOM_PIN_35		 1 // TXD
//------------------------------------------------------------------------------------------------------------------------------------------------

/*SPI Pins*/
#define SOM_PIN_86 		33 // CS  /*This pin is also used in I2S*/
#define SOM_PIN_88 		32 // CLK /*This pin is also used in I2S*/
#define SOM_PIN_90 		35 // MISO
#define SOM_PIN_92 		12 // MOSI
//------------------------------------------------------------------------------------------------------------------------------------------------

/*I2S Pins*/
#define SOM_PIN_174		32 // LRCLK /*This pin is also used in SPI*/

//***This pin (GPIO33) is dupilcated for IN/OUT operation in I2S. However, colibri T20 module employs DIN and DOUT as separate pins, but in ESP32 this are a single pin which have both functions***//
#define SOM_PIN_176		33 // DIN   /*This pin is also used in SPI*/
#define SOM_PIN_178		33 // DOUT  /*This pin is also used in SPI*/
#define SOM_PIN_180		23 //BCLK   /*This pin is also used in I2C*/
//------------------------------------------------------------------------------------------------------------------------------------------------

/*SD CARD*/
/* NOT SUPPORTED. Use SPI Mode to interface SDMCC Cards */
//------------------------------------------------------------------------------------------------------------------------------------------------

/*I2C*/
#define SOM_PIN_194 	23	//SDA  /* This pin is also used in I2S */
#define SOM_PIN_196 	 5	//SCL  /* This pin is also used in UART 0 DSR */
//------------------------------------------------------------------------------------------------------------------------------------------------

/*Ethernet*/
/*This pins are internally connected with the Ethernet hardware in the SOM, hence not reflecting across the SOM SODIMM module as a gpios*/
#define SOM_PIN_98		0	// 50 MHz clock to be provided as input to this pin
#define SOM_PIN_53		13	// PHY PWR Control 	/* Cannot be used as internally used by Ethernet */
#define SOM_PIN_190		15	// MDC				/* Cannot be used as internally used by Ethernet */
#define SOM_PIN_49		 4	// MDIO				/* Cannot be used as internally used by Ethernet */
#define SOM_PIN_183 // Ethernet Link Status LED
#define SOM_PIN_185 // Ethernet Speed Status LED
#define SOM_PIN_187 // Ethernet TXO-
#define SOM_PIN_189 // Ethernet TXO+
#define SOM_PIN_191 // Ethernet GND
#define SOM_PIN_193 // Ethernet RXI-
#define SOM_PIN_195 // Ethernet RXI+
//------------------------------------------------------------------------------------------------------------------------------------------------

/*Power Pins*/
/*This pins are used for power delivery across the SOM, hence not reflecting across the SOM SODIMM module as gpios*/
#define SOM_PIN_42  //3.3V
#define SOM_PIN_84  //3.3V
#define SOM_PIN_108 //3.3V
#define SOM_PIN_148 //3.3V
#define SOM_PIN_198 //3.3V
#define SOM_PIN_200 //3.3V
#define SOM_PIN_39  //GND
#define SOM_PIN_41  //GND
#define SOM_PIN_83  //GND
#define SOM_PIN_109 //GND
#define SOM_PIN_147 //GND
#define SOM_PIN_181 //GND
#define SOM_PIN_197 //GND
#define SOM_PIN_199 //GND

/*Special Purpose Pins*/
#define SOM_PIN_26  //EN //This pin is used for reset the ESP module.
//------------------------------------------------------------------------------------------------------------------------------------------------

/*Unused Pins*/
#define SOM_PIN_3
#define SOM_PIN_4
#define SOM_PIN_5
#define SOM_PIN_6
#define SOM_PIN_1
#define SOM_PIN_7
#define SOM_PIN_8
#define SOM_PIN_9
#define SOM_PIN_10
#define SOM_PIN_11
#define SOM_PIN_12
#define SOM_PIN_13
#define SOM_PIN_14
#define SOM_PIN_15
#define SOM_PIN_16
#define SOM_PIN_17
#define SOM_PIN_18
#define SOM_PIN_19
#define SOM_PIN_20
#define SOM_PIN_21
#define SOM_PIN_22
#define SOM_PIN_23
#define SOM_PIN_24
#define SOM_PIN_25
#define SOM_PIN_28
#define SOM_PIN_30
#define SOM_PIN_31
#define SOM_PIN_32
#define SOM_PIN_34
#define SOM_PIN_37
#define SOM_PIN_40
#define SOM_PIN_43
#define SOM_PIN_44
#define SOM_PIN_45
#define SOM_PIN_46
#define SOM_PIN_47
#define SOM_PIN_48
#define SOM_PIN_50
#define SOM_PIN_51
#define SOM_PIN_52
#define SOM_PIN_54
#define SOM_PIN_55
#define SOM_PIN_56
#define SOM_PIN_57
#define SOM_PIN_58
#define SOM_PIN_59
#define SOM_PIN_60
#define SOM_PIN_61
#define SOM_PIN_62
#define SOM_PIN_63
#define SOM_PIN_64
#define SOM_PIN_65
#define SOM_PIN_66
#define SOM_PIN_67
#define SOM_PIN_68
#define SOM_PIN_69
#define SOM_PIN_70
#define SOM_PIN_71
#define SOM_PIN_72
#define SOM_PIN_73
#define SOM_PIN_74
#define SOM_PIN_75
#define SOM_PIN_76
#define SOM_PIN_77
#define SOM_PIN_78
#define SOM_PIN_80
#define SOM_PIN_81
#define SOM_PIN_82
#define SOM_PIN_87
#define SOM_PIN_89
#define SOM_PIN_91
#define SOM_PIN_93
#define SOM_PIN_94
#define SOM_PIN_95
#define SOM_PIN_96
#define SOM_PIN_99
#define SOM_PIN_100
#define SOM_PIN_102
#define SOM_PIN_103
#define SOM_PIN_104
#define SOM_PIN_105
#define SOM_PIN_106
#define SOM_PIN_107
#define SOM_PIN_110
#define SOM_PIN_112
#define SOM_PIN_111
#define SOM_PIN_113
#define SOM_PIN_114
#define SOM_PIN_115
#define SOM_PIN_116
#define SOM_PIN_117
#define SOM_PIN_118
#define SOM_PIN_119
#define SOM_PIN_120
#define SOM_PIN_121
#define SOM_PIN_122
#define SOM_PIN_123
#define SOM_PIN_124
#define SOM_PIN_125
#define SOM_PIN_126
#define SOM_PIN_127
#define SOM_PIN_128
#define SOM_PIN_129
#define SOM_PIN_130
#define SOM_PIN_131
#define SOM_PIN_132
#define SOM_PIN_133
#define SOM_PIN_134
#define SOM_PIN_135
#define SOM_PIN_136
#define SOM_PIN_137
#define SOM_PIN_138
#define SOM_PIN_139
#define SOM_PIN_140
#define SOM_PIN_141
#define SOM_PIN_142
#define SOM_PIN_143
#define SOM_PIN_144
#define SOM_PIN_145
#define SOM_PIN_146
#define SOM_PIN_149
#define SOM_PIN_150
#define SOM_PIN_151
#define SOM_PIN_152
#define SOM_PIN_153
#define SOM_PIN_154
#define SOM_PIN_155
#define SOM_PIN_156
#define SOM_PIN_157
#define SOM_PIN_158
#define SOM_PIN_159
#define SOM_PIN_160
#define SOM_PIN_161
#define SOM_PIN_162
#define SOM_PIN_163
#define SOM_PIN_164
#define SOM_PIN_165
#define SOM_PIN_166
#define SOM_PIN_167
#define SOM_PIN_168
#define SOM_PIN_169
#define SOM_PIN_170
#define SOM_PIN_171
#define SOM_PIN_172
#define SOM_PIN_173
#define SOM_PIN_175
#define SOM_PIN_177
#define SOM_PIN_179
#define SOM_PIN_182
#define SOM_PIN_184
#define SOM_PIN_186
#define SOM_PIN_188
#define SOM_PIN_192
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//The END.
#endif
