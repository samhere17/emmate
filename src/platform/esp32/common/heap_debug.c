/*
 * File Name: heap_debug.c
 * File Path: /emmate/src/common/esp-idf/heap_debug.c
 * Description:
 *
 *  Created on: 19-Apr-2019
 *      Author: Rohan Dey
 */

#include "core_common.h"
#include "heap_debug.h"

void check_heap_integrity_all() {
	bool heap_state = heap_caps_check_integrity_all(true);
	if (heap_state == false) {
		printf("Heap corruption detected: %s, %d\n", (char*) __FILE__, __LINE__);
	} else 	printf("Heap Ok: %s, %d\n", (char*) __FILE__, __LINE__);
}
