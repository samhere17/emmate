/*
 * File Name: http_platform.c
 * File Path: /emmate/src/platform/esp/conn-proto/http-client/http_client_platform.c
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */

#include "http_client_platform.h"
#include "threading.h"
#include "core_constant.h"

#include <string.h>
#include <stdlib.h>
#include "core_logger.h"

#define TAG "http_client_platform"


/*
static HTTPClientPlatformHelper http_cli_pf_helper;

static esp_err_t _http_event_handler(esp_http_client_event_t *evt) {
	switch (evt->event_id) {
	case HTTP_EVENT_ERROR:
		CORE_LOGD(TAG, "HTTP_EVENT_ERROR");
		break;
	case HTTP_EVENT_ON_CONNECTED:
		CORE_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
		break;
	case HTTP_EVENT_HEADER_SENT:
		CORE_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
		break;
	case HTTP_EVENT_ON_HEADER:
		CORE_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
		break;
	case HTTP_EVENT_ON_DATA:
		CORE_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
		http_cli_pf_helper.response_len += evt->data_len;
		break;
	case HTTP_EVENT_ON_FINISH:
		CORE_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
		break;
	case HTTP_EVENT_DISCONNECTED:
		CORE_LOGD(TAG, "HTTP_EVENT_DISCONNECTED");
		break;
	}
	http_cli_pf_helper.http_cli_pf_event_handler(evt->event_id);
	return ESP_OK;
}
*/

//static void http_client_task(void *pvParameters) {
//	while (1) {
//
//		THREAD_DELAY_CORE(DELAY_1_SEC/TICK_RATE_TO_MS);
//	}
//}

/***************************************************************************************************************/

/**
 * @brief
 *
 * @return
 *
 **/

/*
core_err init_http_cli_platform(char* url, char* cert_pem, HTTPClientEventHandler handler) {
	core_err err = CORE_FAIL;

	http_cli_pf_helper.http_cli_pf_event_handler = handler;

	http_cli_pf_helper.http_cli_pf_cfg.url = url;
	http_cli_pf_helper.http_cli_pf_cfg.cert_pem = cert_pem;
	http_cli_pf_helper.http_cli_pf_cfg.event_handler = _http_event_handler;
	http_cli_pf_helper.response_len = 0;

	http_cli_pf_helper.http_cli_pf_client = http_client_pf_init(&http_cli_pf_helper.http_cli_pf_cfg);

	if (http_cli_pf_helper.http_cli_pf_client != NULL) {
		err = CORE_OK;
	}

	return err;
}
*/

/**
 * @brief
 *
 * @return
 *
 **/
/*
core_err config_http_cli_platform_header(HTTP_CLIENT_PF_METHOD method, char* host_server, char* user_agent,
		char* content_type, char* post_data, size_t post_data_len) {
	core_err err = CORE_FAIL;

	// set the Client WEB Host URL
	if (http_cli_pf_helper.http_cli_pf_cfg.url != NULL) {
		err = http_client_pf_set_url(http_cli_pf_helper.http_cli_pf_client, http_cli_pf_helper.http_cli_pf_cfg.url);
		if (err != CORE_OK) {
			return err;
		}
	} else {
		return CORE_FAIL;
	}

	// set the HTTP Method
	http_cli_pf_helper.http_cli_pf_method = method;
	err = http_client_pf_set_method(http_cli_pf_helper.http_cli_pf_client, http_cli_pf_helper.http_cli_pf_method);
	if (err != CORE_OK) {
		return err;
	}

	// set the HTTP Client HTTP Server
	if (host_server != NULL) {
		err = http_client_pf_set_header(http_cli_pf_helper.http_cli_pf_client, "Host", host_server);
		if (err != CORE_OK) {
			return err;
		}
	} else {
		return CORE_FAIL;
	}

	// set the HTTP Client User agent
	if (user_agent != NULL) {
		err = http_client_pf_set_header(http_cli_pf_helper.http_cli_pf_client, "User-Agent", user_agent);
		if (err != CORE_OK) {
			return err;
		}
	} else {
		return CORE_FAIL;
	}

	// set the HTTP Client Content Type
	if (content_type != NULL) {
		err = http_client_pf_set_header(http_cli_pf_helper.http_cli_pf_client, "Content-Type", content_type);
		if (err != CORE_OK) {
			return err;
		}
	} else {
		return CORE_FAIL;
	}

	// set the HTTP Client Post Data
	if (http_cli_pf_helper.http_cli_pf_method == HTTP_CLIENT_PF_METHOD_POST) {
		err = http_client_pf_set_post_field(http_cli_pf_helper.http_cli_pf_client, post_data, post_data_len);
		if (err != CORE_OK) {
			return err;
		}
	}
	return err;

}
*/

/**
 * @brief
 *
 * @return
 *
 **/
/*
core_err perform_http_cli_platform_http() {
	core_err err = CORE_FAIL;
	err = http_client_pf_perform(http_cli_pf_helper.http_cli_pf_client);
	if (err == CORE_OK) {
		CORE_LOGI(TAG, "Status = %d, content_length = %d",
				http_client_pf_get_status_code(http_cli_pf_helper.http_cli_pf_client),
				http_client_pf_get_content_length(http_cli_pf_helper.http_cli_pf_client));
	}
	return err;
}
*/

/**
 * @brief
 *
 * @return
 *
 **/
/*
uint16_t get_http_cli_platform_status_code() {
	return http_client_pf_get_status_code(http_cli_pf_helper.http_cli_pf_client);
}
*/

/**
 * @brief
 *
 * @return
 *
 **/
/*
core_err get_http_cli_platform_response(char** buff, size_t* len) {
	core_err err = CORE_FAIL;

	char* res_buff = (char*) calloc(http_cli_pf_helper.response_len + 1, sizeof(char));
	if (res_buff != NULL) {
		do {
			char temp_buff[100] = { 0 };
			size_t temp_len = sizeof(temp_buff) - 1;
			bzero(temp_buff, sizeof(temp_buff));
			size_t ret = esp_http_client_read(http_cli_pf_helper.http_cli_pf_client, (char *) temp_buff, temp_len);

			if (ret < 0) {
				CORE_LOGE(TAG, "esp_tls_conn_read  returned -0x%x", -ret);
				break;
			}

			if (ret == 0) {
				CORE_LOGE(TAG, "connection closed");
				break;
			}

			temp_len = ret;
			CORE_LOGD(TAG, "%d bytes read", temp_len);
			// Print response directly to stdout as it is read
			for (int i = 0; i < temp_len; i++) {
				putchar(temp_buff[i]);
			}
			strncat(res_buff, temp_buff, ret);
		} while (1);
	} else {
		CORE_LOGI(TAG, "Failed to allocate memory for HTTP Response");
	}
	*len = http_cli_pf_helper.response_len;
	*buff = res_buff;
	CORE_LOGI(TAG, "data = %s\r\n", *buff);
	//err = http_client_pf_cleanup(http_cli_pf_helper.http_cli_pf_client);

	return err;
}
*/


/**
 * @brief
 *
 * @return
 *
 **/
/*
core_err get_http_cli_platform_clear_client() {
	core_err err = http_client_pf_cleanup(http_cli_pf_helper.http_cli_pf_client);
	return err;
}
*/
