/*
 * File Name: http_platform.h
 * File Path: /emmate/src/platform/esp/conn-proto/http-client/http_client_platform.h
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef HTTP_PLATFORM_H_
#define HTTP_PLATFORM_H_

#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "esp_http_client.h"

//typedef void (*HTTPClientEventHandler) (uint8_t event_id);

#define HTTP_CLIENT_PF_ERR_HTTP_BASE				ESP_ERR_HTTP_BASE              /*!< Starting number of HTTP error codes */
#define HTTP_CLIENT_PF_ERR_HTTP_MAX_REDIRECT		ESP_ERR_HTTP_MAX_REDIRECT      /*!< The error exceeds the number of HTTP redirects */
#define HTTP_CLIENT_PF_ERR_HTTP_CONNECT				ESP_ERR_HTTP_CONNECT           /*!< Error open the HTTP connection */
#define HTTP_CLIENT_PF_ERR_HTTP_WRITE_DATA			ESP_ERR_HTTP_WRITE_DATA        /*!< Error write HTTP data */
#define HTTP_CLIENT_PF_ERR_HTTP_FETCH_HEADER		ESP_ERR_HTTP_FETCH_HEADER      /*!< Error read HTTP header from server */
#define HTTP_CLIENT_PF_ERR_HTTP_INVALID_TRANSPORT	ESP_ERR_HTTP_INVALID_TRANSPORT /*!< There are no transport support for the input scheme */
#define HTTP_CLIENT_PF_ERR_HTTP_CONNECTING			ESP_ERR_HTTP_CONNECTING        /*!< HTTP connection hasn't been established yet */
#define HTTP_CLIENT_PF_ERR_HTTP_EAGAIN				ESP_ERR_HTTP_EAGAIN            /*!< Mapping of errno EAGAIN to esp_err_t */

/**
 * @brief   HTTP Client events id
 */
typedef enum {
	HTTP_CLIENT_PF_EVENT_ERROR = HTTP_EVENT_ERROR, /*!< This event occurs when there are any errors during execution */
	HTTP_CLIENT_PF_EVENT_ON_CONNECTED = HTTP_EVENT_ON_CONNECTED, /*!< Once the HTTP has been connected to the server, no data exchange has been performed */
	HTTP_CLIENT_PF_EVENT_HEADER_SENT = HTTP_EVENT_HEADER_SENT, /*!< After sending all the headers to the server */
	HTTP_CLIENT_PF_EVENT_ON_HEADER = HTTP_EVENT_ON_HEADER, /*!< Occurs when receiving each header sent from the server */
	HTTP_CLIENT_PF_EVENT_ON_DATA = HTTP_EVENT_ON_DATA, /*!< Occurs when receiving data from the server, possibly multiple portions of the packet */
	HTTP_CLIENT_PF_EVENT_ON_FINISH = HTTP_EVENT_ON_FINISH, /*!< Occurs when finish a HTTP session */
	HTTP_CLIENT_PF_EVENT_DISCONNECTED = HTTP_EVENT_DISCONNECTED, /*!< The connection has been disconnected */
} HTTP_CLIENT_PF_EVENT_ID;

/**
 * @brief HTTP method
 */
typedef enum {
	HTTP_CLIENT_PF_METHOD_GET = HTTP_METHOD_GET, /*!< HTTP GET Method */
	HTTP_CLIENT_PF_METHOD_POST = HTTP_METHOD_POST, /*!< HTTP POST Method */
	HTTP_CLIENT_PF_METHOD_PUT = HTTP_METHOD_PUT, /*!< HTTP PUT Method */
	HTTP_CLIENT_PF_METHOD_PATCH = HTTP_METHOD_PATCH, /*!< HTTP PATCH Method */
	HTTP_CLIENT_PF_METHOD_DELETE = HTTP_METHOD_DELETE, /*!< HTTP DELETE Method */
	HTTP_CLIENT_PF_METHOD_HEAD = HTTP_METHOD_HEAD, /*!< HTTP HEAD Method */
	HTTP_CLIENT_PF_METHOD_NOTIFY = HTTP_METHOD_NOTIFY, /*!< HTTP NOTIFY Method */
	HTTP_CLIENT_PF_METHOD_SUBSCRIBE = HTTP_METHOD_SUBSCRIBE, /*!< HTTP SUBSCRIBE Method */
	HTTP_CLIENT_PF_METHOD_UNSUBSCRIBE = HTTP_METHOD_UNSUBSCRIBE,/*!< HTTP UNSUBSCRIBE Method */
	HTTP_CLIENT_PF_METHOD_OPTIONS = HTTP_METHOD_OPTIONS, /*!< HTTP OPTIONS Method */
	HTTP_CLIENT_PF_METHOD_MAX = HTTP_METHOD_MAX,
} HTTP_CLIENT_PF_METHOD;




/* Wrapper typedefs for abstracting the platform specific names */
/**
 * @brief Http Client Platform Config
 **/
typedef esp_http_client_config_t HttpClientPfConfig;

/**
 * @brief HTTP Client Platform Handle
 **/
typedef esp_http_client_handle_t HttpClientPfHandle;

/**
 * @brief HTTP Client Platform Method
 **/
typedef esp_http_client_method_t HttpClientPfMethod;

/**
 * @brief HTTP Client Platform Eventhandle
 **/
typedef esp_http_client_event_t HttpClientPfEvent;





/* Wrapper functions for abstracting the platform specific names */
/**
 * @brief      Start a HTTP session
 *             This function must be the first function to call,
 *             and it returns a esp_http_client_handle_t that you must use as input to other functions in the interface.
 *             This call MUST have a corresponding call to esp_http_client_cleanup when the operation is complete.
 *
 * @param[in]  config   The configurations, see `http_client_config_t`
 *
 * @return
 *     - `esp_http_client_handle_t`
 *     - NULL if any errors
 */
#define http_client_pf_init(config)						esp_http_client_init(/*const esp_http_client_config_t* */config) /* Returns esp_http_client_handle_t*/

/**
 * @brief      Set URL for client, when performing this behavior, the options in the URL will replace the old ones
 *
 * @param[in]  client  The esp_http_client handle
 * @param[in]  url     The url
 *
 * @return
 *  - ESP_OK
 *  - ESP_FAIL
 */
#define http_client_pf_set_url(client, url) 			esp_http_client_set_url(client, url)

/**
 * @brief      Set http request method
 *
 * @param[in]  client  The esp_http_client handle
 * @param[in]  method  The method
 *
 * @return     ESP_OK
 */
#define http_client_pf_set_method(client, method)		esp_http_client_set_method(client, method)

/**
 * @brief      Set http request header, this function must be called after esp_http_client_init and before any
 *             perform function
 *
 * @param[in]  client  The esp_http_client handle
 * @param[in]  key     The header key
 * @param[in]  value   The header value
 *
 * @return
 *  - ESP_OK
 *  - ESP_FAIL
 */
#define http_client_pf_set_header(client, key, value)	esp_http_client_set_header(client, /*const char* */key, /*const char* */value)


/**
 * @brief      Set post data, this function must be called before `esp_http_client_perform`.
 *             Note: The data parameter passed to this function is a pointer and this function will not copy the data
 *
 * @param[in]  client  The esp_http_client handle
 * @param[in]  data    post data pointer
 * @param[in]  len     post length
 *
 * @return
 *  - ESP_OK
 *  - ESP_FAIL
 */
#define http_client_pf_set_post_field(client, data, len) esp_http_client_set_post_field(client, /*const char* */data, /*int*/len)

/**
 * @brief      Invoke this function after `esp_http_client_init` and all the options calls are made, and will perform the
 *             transfer as described in the options. It must be called with the same esp_http_client_handle_t as input as the esp_http_client_init call returned.
 *             esp_http_client_perform performs the entire request in either blocking or non-blocking manner. By default, the API performs request in a blocking manner and returns when done,
 *             or if it failed, and in non-blocking manner, it returns if EAGAIN/EWOULDBLOCK or EINPROGRESS is encountered, or if it failed. And in case of non-blocking request,
 *             the user may call this API multiple times unless request & response is complete or there is a failure. To enable non-blocking esp_http_client_perform(), `is_async` member of esp_http_client_config_t
 *             must be set while making a call to esp_http_client_init() API.
 *             You can do any amount of calls to esp_http_client_perform while using the same esp_http_client_handle_t. The underlying connection may be kept open if the server allows it.
 *             If you intend to transfer more than one file, you are even encouraged to do so.
 *             esp_http_client will then attempt to re-use the same connection for the following transfers, thus making the operations faster, less CPU intense and using less network resources.
 *             Just note that you will have to use `esp_http_client_set_**` between the invokes to set options for the following esp_http_client_perform.
 *
 * @note       You must never call this function simultaneously from two places using the same client handle.
 *             Let the function return first before invoking it another time.
 *             If you want parallel transfers, you must use several esp_http_client_handle_t.
 *             This function include `esp_http_client_open` -> `esp_http_client_write` -> `esp_http_client_fetch_headers` -> `esp_http_client_read` (and option) `esp_http_client_close`.
 *
 * @param      client  The esp_http_client handle
 *
 * @return
 *  - ESP_OK on successful
 *  - ESP_FAIL on error
 */
#define http_client_pf_perform(client)					esp_http_client_perform(client)

/**
 * @brief      Get http response status code, the valid value if this function invoke after `esp_http_client_perform`
 *
 * @param[in]  client  The esp_http_client handle
 *
 * @return     Status code
 */
#define http_client_pf_get_status_code(client)			esp_http_client_get_status_code(client) /* Returns int */

/**
 * @brief      Get http response content length (from header Content-Length)
 *             the valid value if this function invoke after `esp_http_client_perform`
 *
 * @param[in]  client  The esp_http_client handle
 *
 * @return
 *     - (-1) Chunked transfer
 *     - Content-Length value as bytes
 */
#define http_client_pf_get_content_length(client)		esp_http_client_get_content_length(client) /* Returns int */

/**
 * @brief      Read data from http stream
 *
 * @param[in]  client  The esp_http_client handle
 * @param      buffer  The buffer
 * @param[in]  len     The length
 *
 * @return
 *     - (-1) if any errors
 *     - Length of data was read
 */
#define http_client_pf_read(client, buffer, len)		esp_http_client_read(client, /*char* */buffer, /*int*/len) /* Returns int */


/**
 * @brief      This function must be the last function to call for an session.
 *             It is the opposite of the esp_http_client_init function and must be called with the same handle as input that a esp_http_client_init call returned.
 *             This might close all connections this handle has used and possibly has kept open until now.
 *             Don't call this function if you intend to transfer more files, re-using handles is a key to good performance with esp_http_client.
 *
 * @param[in]  client  The esp_http_client handle
 *
 * @return
 *     - ESP_OK
 *     - ESP_FAIL
 */
#define http_client_pf_cleanup(client)					esp_http_client_cleanup(client)

//typedef struct {
//	HttpClientPfHandle http_cli_pf_client;
//	HTTP_CLIENT_PF_METHOD http_cli_pf_method;
//	HttpClientPfConfig http_cli_pf_cfg;
//	HTTPClientEventHandler http_cli_pf_event_handler;
//	char* http_response;
//	size_t response_len;
//} HTTPClientPlatformHelper;

/**
 * @brief
 *
 * @return
 *
 **/
//core_err init_http_cli_platform(char* url, char* cert_pem, HTTPClientEventHandler handler);

/**
 * @brief
 *
 * @return
 *
 **/
//core_err config_http_cli_platform_header(HTTP_CLIENT_PF_METHOD method, char* host_server, char* user_agent,
//		char* content_type, char* post_data, size_t post_data_len);

/**
 * @brief
 *
 * @return
 *
 **/
//core_err perform_http_cli_platform_http();

/**
 * @brief
 *
 * @return
 *
 **/
//uint16_t get_http_cli_platform_status_code();

/**
 * @brief
 *
 * @return
 *
 **/
//core_err get_http_cli_platform_response(char** res_buff, size_t* len);

/**
 * @brief
 *
 * @return
 *
 **/
//core_err get_http_cli_platform_clear_client();

#endif /* HTTP_PLATFORM_H_ */
