/*
 * File Name: sntp_client_platform.h
 * File Path: /emmate/src/platform/esp/conn-proto/sntp-client/sntp_client_platform.h
 * Description:
 *
 *  Created on: 27-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef SNTP_CLIENT_PLATFORM_H_
#define SNTP_CLIENT_PLATFORM_H_

#include "core_config.h"
#include "core_common.h"
#include "core_error.h"

#include "lwip/err.h"
#include "lwip/apps/sntp.h"

/**
 * @brief initialize the SNTP Client
 *
 * @param sntp_server	pass the SNTP server name
 *
 **/
void initialize_sntp_platform(const char *sntp_server);

#endif /* SNTP_CLIENT_PLATFORM_H_ */
