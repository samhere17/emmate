/*
 * File Name: nw_interface_platform.h
 * File Path: /emmate/src/platform/esp/conn/nw_interface_platform.h
 * Description:
 *
 *  Created on: 05-May-2019
 *      Author: Rohan Dey
 */

#ifndef NW_INTERFACE_PLATFORM_H_
#define NW_INTERFACE_PLATFORM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_error.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#include "esp_interface.h"
#include "esp_wifi.h"

typedef enum {
	PLAT_NW_IF_WIFI_STA 	= ESP_IF_WIFI_STA, 		/**< ESP32 station interface */
	PLAT_NW_IF_WIFI_AP 		= ESP_IF_WIFI_AP,		/**< ESP32 soft-AP interface */
	PLAT_NW_IF_ETH 			= ESP_IF_ETH,			/**< ESP32 ethernet interface */
	PLAT_NW_IF_MAX			= ESP_IF_MAX
} plat_nw_interface;

/**
  * @brief     Get mac of specified interface
  *
  * @param      ifx  interface
  * @param[out] mac  store mac of the interface ifx
  *
  * @return
  *    - CORE_OK: succeed
  *    - PLAT_ERR_WIFI_NOT_INIT: WiFi is not initialized by esp_wifi_init
  *    - PLATFORM_ERR_INVALID_ARG: invalid argument
  *    - PLAT_ERR_WIFI_IF: invalid interface
  */
core_err get_nw_iface_mac_platform(plat_nw_interface ifx, uint8_t mac[6]);

#ifdef __cplusplus
}
#endif

#endif /* NW_INTERFACE_PLATFORM_H_ */
