/*
 * File Name: wifi_platform.h
 * File Path: /emmate/src/platform/esp/conn/wifi/wifi_platform.h
 * Description:
 *
 *  Created on: 24-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef WIFI_PLATFORM_H_
#define WIFI_PLATFORM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_interface.h"

//#include "esp32_wifi_core.h"

typedef core_err (*WiFiEvents) (void *ctx, system_event_t *event);

typedef void (*WifiEventHandler) (uint8_t event_id);

#define WIFI_PLAT_EVENT_WIFI_READY				SYSTEM_EVENT_WIFI_READY               /**< ESP32 WiFi ready */
#define WIFI_PLAT_EVENT_SCAN_DONE				SYSTEM_EVENT_SCAN_DONE                /**< ESP32 finish scanning AP */
#define WIFI_PLAT_EVENT_STA_START				SYSTEM_EVENT_STA_START                /**< ESP32 station start */
#define WIFI_PLAT_EVENT_STA_STOP				SYSTEM_EVENT_STA_STOP                 /**< ESP32 station stop */
#define WIFI_PLAT_EVENT_STA_CONNECTED			SYSTEM_EVENT_STA_CONNECTED            /**< ESP32 station connected to AP */
#define WIFI_PLAT_EVENT_STA_DISCONNECTED		SYSTEM_EVENT_STA_DISCONNECTED         /**< ESP32 station disconnected from AP */
#define WIFI_PLAT_EVENT_STA_AUTHMODECHANGE		SYSTEM_EVENT_STA_AUTHMODE_CHANGE      /**< the auth mode of AP connected by ESP32 station changed */
#define WIFI_PLAT_EVENT_STA_GOT_IP				SYSTEM_EVENT_STA_GOT_IP               /**< ESP32 station got IP from connected AP */
#define WIFI_PLAT_EVENT_STA_LOST_IP				SYSTEM_EVENT_STA_LOST_IP              /**< ESP32 station lost IP and the IP is reset to 0 */
#define WIFI_PLAT_EVENT_STA_WPS_ER_SUCCESS		SYSTEM_EVENT_STA_WPS_ER_SUCCESS       /**< ESP32 station wps succeeds in enrollee mode */
#define WIFI_PLAT_EVENT_STA_WPS_ER_FAILED		SYSTEM_EVENT_STA_WPS_ER_FAILED        /**< ESP32 station wps fails in enrollee mode */
#define WIFI_PLAT_EVENT_STA_WPS_ER_TIMEOUT		SYSTEM_EVENT_STA_WPS_ER_TIMEOUT       /**< ESP32 station wps timeout in enrollee mode */
#define WIFI_PLAT_EVENT_STA_WPS_ER_PIN			SYSTEM_EVENT_STA_WPS_ER_PIN           /**< ESP32 station wps pin code in enrollee mode */
#define WIFI_PLAT_EVENT_AP_START				SYSTEM_EVENT_AP_START                 /**< ESP32 soft-AP start */
#define WIFI_PLAT_EVENT_AP_STOP					SYSTEM_EVENT_AP_STOP                  /**< ESP32 soft-AP stop */
#define WIFI_PLAT_EVENT_AP_STACONNECTED			SYSTEM_EVENT_AP_STACONNECTED          /**< a station connected to ESP32 soft-AP */
#define WIFI_PLAT_EVENT_AP_STADISCONNECTED		SYSTEM_EVENT_AP_STADISCONNECTED       /**< a station disconnected from ESP32 soft-AP */
#define WIFI_PLAT_EVENT_AP_STAIPASSIGNED		SYSTEM_EVENT_AP_STAIPASSIGNED         /**< ESP32 soft-AP assign an IP to a connected station */
#define WIFI_PLAT_EVENT_AP_PROBEREQRECVED		SYSTEM_EVENT_AP_PROBEREQRECVED        /**< Receive probe request packet in soft-AP interface */
#define WIFI_PLAT_EVENT_GOT_IP6					SYSTEM_EVENT_GOT_IP6                  /**< ESP32 station or ap or ethernet interface v6IP addr is preferred */
#define WIFI_PLAT_EVENT_ETH_START				SYSTEM_EVENT_ETH_START                /**< ESP32 ethernet start */
#define WIFI_PLAT_EVENT_ETH_STOP				SYSTEM_EVENT_ETH_STOP                 /**< ESP32 ethernet stop */
#define WIFI_PLAT_EVENT_ETH_CONNECTED			SYSTEM_EVENT_ETH_CONNECTED            /**< ESP32 ethernet phy link up */
#define WIFI_PLAT_EVENT_ETH_DISCONNECTED		SYSTEM_EVENT_ETH_DISCONNECTED         /**< ESP32 ethernet phy link down */
#define WIFI_PLAT_EVENT_ETH_GOT_IP				SYSTEM_EVENT_ETH_GOT_IP               /**< ESP32 ethernet got IP from connected AP */
#define WIFI_PLAT_EVENT_MAX						SYSTEM_EVENT_MAX


typedef enum {
    WIFI_PLATFORM_MODE_NULL = 	WIFI_MODE_NULL,		/**< null mode */
	WIFI_PLATFORM_MODE_STA = 	WIFI_MODE_STA,		/**< WiFi station mode */
	WIFI_PLATFORM_MODE_AP = 	WIFI_MODE_AP,		/**< WiFi soft-AP mode */
	WIFI_PLATFORM_MODE_APSTA = 	WIFI_MODE_APSTA,	/**< WiFi station + soft-AP mode */
	WIFI_PLATFORM_MODE_MAX = 	WIFI_MODE_MAX
} WifiPlatformModes;

typedef enum {
	ESP_BOARD_IF_WIFI_STA = ESP_IF_WIFI_STA,     /**< ESP32 station interface */
	ESP_BOARD_IF_WIFI_AP = ESP_IF_WIFI_AP,          /**< ESP32 soft-AP interface */
	ESP_BOARD_IF_ETH =	ESP_IF_ETH,              /**< ESP32 ethernet interface */
	ESP_BOARD_IF_MAX =	ESP_IF_MAX
} ESP_BOARD_INTERFACE;


typedef struct {
	WifiPlatformModes wifi_mode;
//	wifi_config_t wifi_cfg;
	WifiEventHandler event_handler;
} WifiPlatformHelper;

/**
  * @brief    	Set the SSID and Password of the Access Point to connect. This can be called anytime after Wi-Fi is started.
  * 			This function internally calls esp_wifi_set_config()
  *
  * @return
  *    - CORE_OK: 	success
  *    - CORE_FAIL: failure
  */
core_err set_wifi_platform_configs(char* ssid, char* password, ESP_BOARD_INTERFACE esp_wifi_if_type);

/**
  * @brief     Connect the ESP32 WiFi station to the AP.
  *
  * @attention 1. This API only impact WIFI_MODE_STA or WIFI_MODE_APSTA mode
  * @attention 2. If the ESP32 is connected to an AP, call esp_wifi_disconnect to disconnect.
  * @attention 3. The scanning triggered by esp_wifi_start_scan() will not be effective until connection between ESP32 and the AP is established.
  *               If ESP32 is scanning and connecting at the same time, ESP32 will abort scanning and return a warning message and error
  *               number ESP_ERR_WIFI_STATE.
  *               If you want to do reconnection after ESP32 received disconnect event, remember to add the maximum retry time, otherwise the called
  *               scan will not work. This is especially true when the AP doesn't exist, and you still try reconnection after ESP32 received disconnect
  *               event with the reason code WIFI_REASON_NO_AP_FOUND.
  *
  * @return
  *    - CORE_OK: succeed
  *    - PLAT_ERR_WIFI_NOT_INIT: WiFi is not initialized by esp_wifi_init
  *    - PLAT_ERR_WIFI_NOT_STARTED: WiFi is not started by esp_wifi_start
  *    - PLAT_ERR_WIFI_CONN: WiFi internal error, station or soft-AP control block wrong
  *    - PLAT_ERR_WIFI_SSID: SSID of AP which station connects is invalid
  */
void connect_wifi_platform();

/**
  * @brief  Start WiFi according to current configuration
  *         If mode is WIFI_MODE_STA, it create station control block and start station
  *         If mode is WIFI_MODE_AP, it create soft-AP control block and start soft-AP
  *         If mode is WIFI_MODE_APSTA, it create soft-AP and station control block and start soft-AP and station
  *
  * @return
  *    - CORE_OK: succeed
  *    - PLAT_ERR_WIFI_NOT_INIT: WiFi is not initialized by esp_wifi_init
  *    - PLATFORM_ERR_INVALID_ARG: invalid argument
  *    - PLATFORM_ERR_NO_MEM: out of memory
  *    - PLAT_ERR_WIFI_CONN: WiFi internal error, station or soft-AP control block wrong
  *    - CORE_FAIL: other WiFi internal errors
  */
core_err start_wifi_platform();

/**
  * @brief  Init WiFi
  *         Alloc resource for WiFi driver, such as WiFi control structure, RX/TX buffer,
  *         WiFi NVS structure etc, this WiFi also start WiFi task
  *
  * @param	WifiPlatformModes: init the underlying wi-fi driver as per this mode
  * @param	WifiEventHandler: handler where events will be received
  * @param	ssid
  * @param	pwd
  *
  * @return
  *    - CORE_OK: succeed
  *    - PLATFORM_ERR_NO_MEM: out of memory
  */
core_err init_wifi_platform(WifiPlatformModes wifi_mode, WifiEventHandler handler, char* ssid, char* pwd);

/**
  * @brief  Deinit WiFi
  *         Free all resource allocated in esp_wifi_init and stop WiFi task
  *
  * @attention 1. This API should be called if you want to remove WiFi driver from the system
  *
  * @return
  *    - CORE_OK: succeed
  *    - PLAT_ERR_WIFI_NOT_INIT: WiFi is not initialized by esp_wifi_init
  */
core_err deinit_wifi_platform();

/**
  * @brief  Get current operating mode of WiFi
  *
  * @param[out]  mode  store current WiFi mode
  *
  * @return
  *    - CORE_OK: succeed
  *    - PLAT_ERR_WIFI_NOT_INIT: WiFi is not initialized by esp_wifi_init
  *    - PLATFORM_ERR_INVALID_ARG: invalid argument
  */
core_err get_wifi_platform_mode(WifiPlatformModes *mode);

/**
  * @brief  Stop WiFi
  *         If mode is WIFI_MODE_STA, it stop station and free station control block
  *         If mode is WIFI_MODE_AP, it stop soft-AP and free soft-AP control block
  *         If mode is WIFI_MODE_APSTA, it stop station/soft-AP and free station/soft-AP control block
  *
  * @return
  *    - CORE_OK: succeed
  *    - PLAT_ERR_WIFI_NOT_INIT: WiFi is not initialized by esp_wifi_init
  */
core_err stop_wifi_platform(void);

/**
  * @brief     Disconnect the ESP32 WiFi station from the AP.
  *
  * @return
  *    - CORE_OK: succeed
  *    - PLAT_ERR_WIFI_NOT_INIT: WiFi was not initialized by esp_wifi_init
  *    - PLAT_ERR_WIFI_NOT_STARTED: WiFi was not started by esp_wifi_start
  *    - CORE_FAIL: other WiFi internal errors
  */
core_err disconnect_wifi_platform(void);

/**
  * @brief     Get the Wi-Fi STA interface's MAC address
  *
  * @param[out]  mac  WiFi STA interface's MAC address (6 bytes) returned as an out parameter.
  *
  * @return
  * 	CORE_OK: succeed
  * 	PLAT_ERR_WIFI_NOT_INIT: WiFi is not initialized by esp_wifi_init
  * 	PLATFORM_ERR_INVALID_ARG: invalid argument
  * 	PLAT_ERR_WIFI_IF: invalid interface
  */
core_err get_wifi_pf_sta_mac(uint8_t *mac);

#ifdef __cplusplus
}
#endif

#endif /* WIFI_PLATFORM_H_ */
