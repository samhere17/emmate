/*
 * ble_platform.c
 *
 *	File Path: /emmate/src/conn/ble/esp-idf/ble_platform.c
 *
 *	Project Name: EmMate
 *	
 *  Created on: 16-Apr-2019
 *
 *      Author: Noyel Seth
 */

#include "ble_platform.h"
#include "esp_bt_device.h"
#include "gatt_utils.h"
#include "gatt_service.h"
#include "ble_constant.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include <string.h>

#define TAG LTAG_CONN_BLE

static BLE_CONNECTION_STATUS is_ble_connected = BLE_NOT_CONNECTED;

static BLE_CONNECT_EVENT ble_connect_event_handler;

/**** Static Functions ****/

/**
 * @brief Config ESP32 BT_Controlller with default configuration
 *
 * @return
 *
 **/
static core_err configure_esp32_ble() {
	esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
	core_err ret = esp_bt_controller_init(&bt_cfg);
	if (ret) {
		ESP_LOGE(TAG, "Error: (%s) %s initialize controller failed\n", esp_err_to_name(ret), __func__);
	}
	return ret;
}

/**
 * @brief de-Config ESP32 BT_Controlller
 *
 * @return
 *
 **/
static core_err deconfigure_esp32_ble() {
	core_err ret = esp_bt_controller_deinit();
	if (ret) {
		ESP_LOGE(TAG, "Error: (%s) %s de-initialize controller failed\n", esp_err_to_name(ret), __func__);
	}
	return ret;
}

/**
 * @brief	Set Bluetooth mode BLE
 *
 * @return
 *
 **/
static core_err enable_ble_mode() {
	core_err ret = esp_bt_controller_enable(ESP_BT_MODE_BLE);
	if (ret) {
		ESP_LOGE(TAG, "Error: (%s) %s enable controller failed\n", esp_err_to_name(ret), __func__);
	}
	return ret;
}

/**
 * @brief	Unset Bluetooth mode
 *
 * @return
 *
 **/
static core_err disable_ble_mode() {
	core_err ret = esp_bt_controller_disable();
	if (ret) {
		ESP_LOGE(TAG, "Error: (%s) %s disable controller failed\n", esp_err_to_name(ret), __func__);
	}
	return ret;
}

///**
// * @brief	Initialize Esp32 bluedroid
// *
// * @return
// *
// **/
//static core_err init_ble() {
//	core_err ret = esp_bluedroid_init();
//	if (ret) {
//		ESP_LOGE(TAG, "Error: (%s) %s init bluetooth failed\n",
//				esp_err_to_name(ret), __func__);
//		return ret;
//	}
//	return ret;
//}
//
///**
// * @brief		de-Initialize Esp32 bluedroid
// *
// * @return
// *
// **/
//static core_err deinit_ble() {
//	core_err ret = esp_bluedroid_deinit();
//	if (ret) {
//		ESP_LOGE(TAG, "Error: (%s) %s init bluetooth failed\n",
//				esp_err_to_name(ret), __func__);
//		return ret;
//	}
//	return ret;
//}

/**
 * @brief		Enable ESP32 Ble Module
 *
 * @return
 *
 **/
static core_err enable_bt() {
	core_err ret;
	ret = esp_bluedroid_init();
	if (ret) {
		ESP_LOGE(TAG, "Error: (%s) %s init bluetooth failed\n", esp_err_to_name(ret), __func__);
		return ret;
	}

	ret = esp_bluedroid_enable();
	if (ret) {
		ESP_LOGE(TAG, "Error: (%s) %s enable bluetooth failed\n", esp_err_to_name(ret), __func__);
		return ret;
	}

	return ret;
}

/**
 * @brief	Disable ESP32 Ble Module
 *
 * @return
 *
 **/
static core_err disable_bt() {
	core_err ret;
	ret = esp_bluedroid_disable();
	if (ret) {
		ESP_LOGE(TAG, "Error: (%s) %s disable bluetooth failed\n", esp_err_to_name(ret), __func__);
		return ret;
	}

	ret = esp_bluedroid_deinit();
	if (ret) {
		ESP_LOGE(TAG, "Error: (%s) %s de-init bluetooth failed\n", esp_err_to_name(ret), __func__);
		return ret;
	}
	return ret;
}

///**
// * @brief		Config ESP32 Ble Module
// *
// * @return
// *
// **/
//static core_err config_ble() {
//	core_err err = CORE_FAIL;
//	err = configure_esp32_ble();
//	ESP_ERROR_CHECK(err);
//	err = enable_ble_mode();
//	ESP_ERROR_CHECK(err);
//	return err;
//}
//
///**
// * @brief		de_Config ESP32 Ble Module
// *
// * @return
// *
// **/
//static core_err deconfig_ble() {
//	core_err err = CORE_FAIL;
//	err = disable_ble_mode();
//	ESP_ERROR_CHECK(err);
//	err = deconfigure_esp32_ble();
//	ESP_ERROR_CHECK(err);
//	return err;
//}

/**
 * @brief	Setup ble GATT service processes
 *
 * @return
 *
 **/
static core_err setup_bt_process() {

	core_err ret = esp_ble_gatts_register_callback(gatts_event_handler);
	if (ret) {
		ESP_LOGE(TAG, "gatts register error, error code = %x, %s\n", ret, esp_err_to_name(ret));
		return ret;
	}
	ret = esp_ble_gap_register_callback(gap_event_handler);
	if (ret) {
		ESP_LOGE(TAG, "gap register error, error code = %x, %s\n", ret, esp_err_to_name(ret));
		return ret;
	}
	ret = esp_ble_gatts_app_register(ESP_SPP_APP_ID);
	if (ret) {
		ESP_LOGE(TAG, "gatts app register error, error code = %x, %s\n", ret, esp_err_to_name(ret));
		return ret;
	}
	return ret;
}

/***************************************************************************************************/

/**
 * @brief
 *
 * @return
 *
 **/
uint8_t ble_platform_get_ble_max_name_size() {
	return BLE_NAME_MAX_LENGTH;
}

/**
 * @brief
 *
 * @return
 *
 **/
core_err ble_platform_set_ble_adv_name(char* pble_adv) {
	return gatt_utils_set_ble_adv_data(pble_adv);
}

/**
 * @brief		Config ESP32 Ble Module
 *
 * @return
 *
 **/
core_err ble_platform_config_ble() {
	core_err err = CORE_FAIL;
	err = configure_esp32_ble();
	ESP_ERROR_CHECK(err);
	err = enable_ble_mode();
	ESP_ERROR_CHECK(err);
	return err;
}

/**
 * @brief		de_Config ESP32 Ble Module
 *
 * @return
 *
 **/
core_err ble_platform_deconfig_ble() {
	core_err err = CORE_FAIL;
	err = disable_ble_mode();
	ESP_ERROR_CHECK(err);
	err = deconfigure_esp32_ble();
	ESP_ERROR_CHECK(err);
	return err;
}

/**
 * @brief Config the BLE and Enable
 *
 * @return  ESP_OK if all steps complete, otherwise show the error message and abort the process.
 *
 **/
core_err ble_platform_enable_ble(uint8_t *bt_mac) {
// BLE Configaration
	esp_err_t err;
//err = config_ble();
//ESP_ERROR_CHECK(err);
	err = enable_bt();
	ESP_ERROR_CHECK(err);
	err = setup_bt_process();
	ESP_ERROR_CHECK(err);
#if 1
	/* Get bluetooth device address.  Must use after "esp_bluedroid_enable"*/
	CORE_LOGI(TAG, "Trying to get Bluetooth MAC");
	if(bt_mac!=NULL){
	memcpy(bt_mac, esp_bt_dev_get_address(), BLE_MAC_LEN);
	CORE_LOGI(TAG, "Got bluetooth mac = %02x:%02x:%02x:%02x:%02x:%02x", bt_mac[0], bt_mac[1], bt_mac[2], bt_mac[3], bt_mac[4],
				bt_mac[5]);
	}else{
		CORE_LOGI(TAG, "Bluetooth-Mac variable is NULL");
	}
#endif
	return err;
}

/**
 * @brief deConfig the BLE and disable
 *
 * @return  ESP_OK if all steps complete, otherwise show the error message and abort the process.
 *
 **/
core_err ble_platform_disable_ble() {
	esp_err_t err;
	err = disable_bt();
	ESP_ERROR_CHECK(err);
//err = deconfig_ble();
//ESP_ERROR_CHECK(err);
	return err;
}

void ble_platform_set_ble_dt_recv_q(QueueHandle *recv_queue) {
	set_spp_data_queue(recv_queue);
}

/**
 * @brief
 *
 * @return
 *
 **/
core_err ble_platform_set_connect_event_handler(BLE_CONNECT_EVENT handler) {
	if (handler != NULL) {
		ble_connect_event_handler = handler;
		return CORE_OK;
	}
	return CORE_FAIL;
}

/**
 * @brief
 *
 * @return
 *
 **/
void ble_platform_set_isble_connected(BLE_CONNECTION_STATUS status) {
	is_ble_connected = status;
	if (ble_connect_event_handler != NULL) {
		ble_connect_event_handler(status);
	}
}

/**
 * @brief
 *
 * @return
 *
 **/
BLE_CONNECTION_STATUS ble_platform_isble_connected() {
	return is_ble_connected;
}

