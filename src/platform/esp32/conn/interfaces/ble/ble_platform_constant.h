/*
 * ble_constant.h
 *
 *  Created on: 15-Apr-2019
 *      Author: iqubuntu02
 */

#ifndef SRC_CONN_BLE_ESP_IDF_BLE_CONSTANT_H_
#define SRC_CONN_BLE_ESP_IDF_BLE_CONSTANT_H_

#include "core_constant.h"

#define BLE_CMD_POSTFIX		"\r\n"

#define BLE_DATA_QUEUE_MAX_SIZE 		50

#define BLE_TIMEOUT_DURATION				DELAY_1_MIN * 1				// 5min Duration
#define BLE_BYTE_STREAM_RECV_TIMEOUT		DELAY_1_MIN * 2				// 2min Duration

#define BLE_DATA_CHUNK_SIZE		20

typedef enum{
	BLE_NOT_CONNECTED=0,
	BLE_CONNECTED,
}BLE_CONNECTION_STATUS;

#endif /* SRC_CONN_BLE_ESP_IDF_BLE_CONSTANT_H_ */
