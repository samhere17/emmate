/*
 * gatt_utils.c
 *
 *	File Path: /emmate/src/conn/ble/esp-idf/gatt_utils.c
 *
 *	Project Name: EmMate
 *	
 *  Created on: 16-Apr-2019
 *
 *      Author: Noyel Seth
 */

#include "gatt_utils.h"
#include <string.h>
#include "ble_platform_constant.h"
#include "gatt_service.h"
#include "ble_platform.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

static char* ble_adv_name=NULL;

/* Bluetooth advertising data basics
 * (https://www.silabs.com/community/wireless/bluetooth/knowledge-base.entry.html/2017/02/10/bluetooth_advertisin-hGsf)
 *
 * advertising payload 31 bytes long.
 *
 */

static uint8_t spp_adv_data[BLE_ADV_PAYLOAD_SIZE] = {

		//Length	//AD type		//Value
		0x02, 		0x01, 			0x06,

		//Length	//AD type		//Value
		0x03, 		0x03, 			0xF0, 0xAB,

		//Length	//AD type		//Value ("ESP32_SensorU")
		//0x0E,		0x09,			0x45,0x53,0x50,0x33,0x32,0x5f,0x53,0x65,0x6e,0x73,0x6f,0x72,0x55
	};

/**
 * @brief
 *
 * @return
 *
 **/
uint8_t* get_spp_adv_data(){
	return spp_adv_data;
}


/**
 * @brief
 *
 * @return
 *
 **/
uint8_t get_spp_adv_data_length(){
	return strlen((char*)spp_adv_data);
}



/**
 * @brief
 *
 * @return
 *
 **/
core_err gatt_utils_set_ble_adv_data(char* ble_name) {
	core_err err = CORE_FAIL;
	CORE_LOGI(LTAG_CONN_BLEGATT, "ble_device_name: %s\n", ble_name);
	ble_adv_name = ble_name;
	uint16_t default_adv_data_length = BLE_ADV_DATA_DEFAULT_LENGTH;
	uint8_t ble_spp_adv_data_length = default_adv_data_length + 1/*length of ble_name*/
	+ 1/*AD type*/
	+ strlen(ble_name)/*no of char in the ble_name*/;

	uint8_t idx = 0, count = 0;
	for (idx = 0; idx < BLE_ADV_DATA_DEFAULT_LENGTH; idx++) {

	}
	spp_adv_data[idx++] = strlen(ble_name) + 1; //length of BLE_NAME + length of BLE AD type CMD (0x09)
	spp_adv_data[idx++] = 0x09; // BLE AD type name for set ble_device custom name
	for (count = 0; count < strlen(ble_name); count++) {
		spp_adv_data[idx++] = ble_name[count];
	}

	for (idx = 0; idx < ble_spp_adv_data_length; idx++) {
		CORE_LOGD(LTAG_CONN_BLEGATT, "0x%02X,", spp_adv_data[idx]);
	}
	CORE_LOGI(LTAG_CONN_BLEGATT, "\nSizeof %d, %d, %d\n", sizeof(spp_adv_data),
			ble_spp_adv_data_length, strlen((char*)spp_adv_data));
	err = CORE_OK;

	return err;
}



/**
 * @brief
 *
 * @return
 *
 **/
char* get_ble_adv_name(){
	return ble_adv_name;
}



/**
 * @brief
 *
 * @return
 *
 **/
core_err ble_send_response(char* res) {
	core_err err=CORE_FAIL;
	CORE_LOGI(LTAG_CONN_BLEGATT, "%s : %s\n\n", __func__, res);
	char msg[BLE_DATA_CHUNK_SIZE+1] = { 0 };
	if (ble_platform_isble_connected() == BLE_CONNECTED) {
		if (strlen(res) > BLE_DATA_CHUNK_SIZE) {
			while (*res != '\0') {
				int idx = 0;
				memset(msg, 0x00, BLE_DATA_CHUNK_SIZE);
				for (idx = 0; idx < BLE_DATA_CHUNK_SIZE; idx++) {
					char ch = *res;
					msg[idx] = ch;
					res++;
					if (*res == '\0') {
						break;
					}
				}
				CORE_LOGI(LTAG_CONN_BLEGATT, "MSG = %s\n", msg);
				err = gatt_send_ble_response((uint8_t *) msg, strlen(msg));
			}
		} else {
			err = gatt_send_ble_response((uint8_t *) res, strlen(res));
		}
	}
	return err;
}
