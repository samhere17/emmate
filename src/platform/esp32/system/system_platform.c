/*
 * File Name: system_platform.c
 * File Path: /emmate/src/platform/esp/system/system_platform.c
 * Description:
 *
 *  Created on: 04-May-2019
 *      Author: Rohan Dey
 */


#include "system_platform.h"

void core_platform_restart() {
	esp_restart();
}
