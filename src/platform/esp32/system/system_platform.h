/*
 * File Name: system_platform.h
 * File Path: /emmate/src/platform/esp/system/system_platform.h
 * Description:
 *
 *  Created on: 04-May-2019
 *      Author: Rohan Dey
 */

#ifndef SYSTEM_PLATFORM_H_
#define SYSTEM_PLATFORM_H_

#include "esp_system.h"

/**
  * @brief  Restart PRO and APP CPUs.
  *
  * This function can be called both from PRO and APP CPUs.
  * After successful restart, CPU reset reason will be SW_CPU_RESET.
  * This function does not return.
  *
  */
void core_platform_restart();

#endif /* SYSTEM_PLATFORM_H_ */
