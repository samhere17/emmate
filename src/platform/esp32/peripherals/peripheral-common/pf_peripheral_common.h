/*
 * File Name: esp_pf_intr_alloc.h
 * File Path: /emmate/src/platform/esp32/peripherals/include/esp_pf_intr_alloc.h
 * Description:
 *
 *  Created on: 30-May-2019
 *      Author: Noyel Seth
 */

#ifndef ESP_PF_INTR_ALLOC_H_
#define ESP_PF_INTR_ALLOC_H_

#include "esp_intr_alloc.h"

//Keep the LEVELx values as they are here; they match up with (1<<level)
#define PF_INTR_FLAG_LEVEL1 		ESP_INTR_FLAG_LEVEL1	///< Accept a Level 1 interrupt vector (lowest priority)
#define PF_INTR_FLAG_LEVEL2 		ESP_INTR_FLAG_LEVEL2	///< Accept a Level 2 interrupt vector
#define PF_INTR_FLAG_LEVEL3 		ESP_INTR_FLAG_LEVEL3	///< Accept a Level 3 interrupt vector
#define PF_INTR_FLAG_LEVEL4 		ESP_INTR_FLAG_LEVEL4	///< Accept a Level 4 interrupt vector
#define PF_INTR_FLAG_LEVEL5 		ESP_INTR_FLAG_LEVEL5	///< Accept a Level 5 interrupt vector
#define PF_INTR_FLAG_LEVEL6 		ESP_INTR_FLAG_LEVEL6	///< Accept a Level 6 interrupt vector
#define PF_INTR_FLAG_NMI 			ESP_INTR_FLAG_NMI			///< Accept a Level 7 interrupt vector (highest priority)
#define PF_INTR_FLAG_SHARED 		ESP_INTR_FLAG_SHARED	///< Interrupt can be shared between ISRs
#define PF_INTR_FLAG_EDGE 			ESP_INTR_FLAG_EDGE		///< Edge-triggered interrupt
#define PF_INTR_FLAG_IRAM 			ESP_INTR_FLAG_IRAM			///< ISR can be called if cache is disabled
#define PF_INTR_FLAG_INTRDISABLED 	ESP_INTR_FLAG_INTRDISABLED		///< Return with this interrupt disabled

#define PF_INTR_FLAG_LOWMED 		ESP_INTR_FLAG_LOWMED ///< Low and medium prio interrupts. These can be handled in C.
#define PF_INTR_FLAG_HIGH 			ESP_INTR_FLAG_HIGH ///< High level interrupts. Need to be handled in assembly.

#define PF_INTR_FLAG_LEVELMASK 		ESP_INTR_FLAG_LEVELMASK	///< Mask for all level flags

#endif /* ESP_PF_INTR_ALLOC_H_ */
