/*
 * File Name: i2c_platform.h
 * File Path: /emmate/src/platform/esp32/peripherals/i2c/i2c_platform.h
 * Description:
 *
 *  Created on: 30-May-2019
 *      Author: Noyel Seth
 */

#ifndef I2C_PLATFORM_H_
#define I2C_PLATFORM_H_

#include "driver/i2c.h"
#include "i2c_core_local.h"
#include "core_error.h"

#ifdef __cplusplus
extern "C" {
#endif

#define I2C_PF_APB_CLK_FREQ		I2C_APB_CLK_FREQ /*!< I2C source clock is APB clock, 80MHz */
#define I2C_PF_FIFO_LEN			I2C_FIFO_LEN /*!< I2C hardware fifo length */

typedef enum {
	I2C_PF_MODE_SLAVE = I2C_MODE_SLAVE, /*!< I2C slave mode */
	I2C_PF_MODE_MASTER = I2C_MODE_MASTER, /*!< I2C master mode */
	I2C_PF_MODE_MAX = I2C_MODE_MAX,
} i2c_pf_mode_t;

typedef enum {
	I2C_PF_MASTER_WRITE = I2C_MASTER_WRITE, /*!< I2C write data */
	I2C_PF_MASTER_READ = I2C_MASTER_READ, /*!< I2C read data */
} i2c_pf_rw_t;

typedef enum {
	I2C_PF_DATA_MODE_MSB_FIRST = I2C_DATA_MODE_MSB_FIRST, /*!< I2C data msb first */
	I2C_PF_DATA_MODE_LSB_FIRST = I2C_DATA_MODE_LSB_FIRST, /*!< I2C data lsb first */
	I2C_PF_DATA_MODE_MAX = I2C_DATA_MODE_MAX,
} i2c_pf_trans_mode_t;

typedef enum {
	I2C_PF_CMD_RESTART = I2C_CMD_RESTART, /*!<I2C restart command */
	I2C_PF_CMD_WRITE = I2C_CMD_WRITE, /*!<I2C write command */
	I2C_PF_CMD_READ = I2C_CMD_READ, /*!<I2C read command */
	I2C_PF_CMD_STOP = I2C_CMD_STOP, /*!<I2C stop command */
	I2C_PF_CMD_END = I2C_CMD_END, /*!<I2C end command */
} i2c_pf_opmode_t;

typedef enum {
	I2C_PF_NUM_0 = I2C_NUM_0, /*!< I2C port 0 */
	I2C_PF_NUM_1 = I2C_NUM_1, /*!< I2C port 1 */
	I2C_PF_NUM_MAX = I2C_NUM_MAX
} i2c_pf_port_t;

typedef enum {
	I2C_PF_ADDR_BIT_7 = I2C_ADDR_BIT_7, /*!< I2C 7bit address for slave mode */
	I2C_PF_ADDR_BIT_10 = I2C_ADDR_BIT_10, /*!< I2C 10bit address for slave mode */
	I2C_PF_ADDR_BIT_MAX = I2C_ADDR_BIT_MAX,
} i2c_pf_addr_mode_t;

typedef enum {
	I2C_PF_MASTER_ACK = I2C_MASTER_ACK, /*!< I2C ack for each byte read */
	I2C_PF_MASTER_NACK = I2C_MASTER_NACK, /*!< I2C nack for each byte read */
	I2C_PF_MASTER_LAST_NACK = I2C_MASTER_LAST_NACK, /*!< I2C nack for the last byte*/
	I2C_PF_MASTER_ACK_MAX = I2C_MASTER_ACK_MAX,
} i2c_pf_ack_type_t;

/**
 * @brief I2C initialization parameters
 */
typedef i2c_config_t i2c_pf_config_t;

typedef i2c_cmd_handle_t i2c_pf_cmd_handle_t; /*!< I2C command handle  */

/**
 * @brief I2C driver install
 *
 * @param i2c_num I2C port number
 * @param mode I2C mode( master or slave )
 * @param slv_rx_buf_len receiving buffer size for slave mode
 *        @note
 *        Only slave mode will use this value, driver will ignore this value in master mode.
 * @param slv_tx_buf_len sending buffer size for slave mode
 *        @note
 *        Only slave mode will use this value, driver will ignore this value in master mode.
 * @param intr_alloc_flags Flags used to allocate the interrupt. One or multiple (ORred)
 *            ESP_INTR_FLAG_* values. See esp_intr_alloc.h for more info.
 *        @note
 *        In master mode, if the cache is likely to be disabled(such as write flash) and the slave is time-sensitive,
 *        `ESP_INTR_FLAG_IRAM` is suggested to be used. In this case, please use the memory allocated from internal RAM in i2c read and write function,
 *        because we can not access the psram(if psram is enabled) in interrupt handle function when cache is disabled.
 *
 * @return
 *     - ESP_OK   Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Driver install error
 */
#define i2c_pf_driver_install(i2c_num, mode, slv_rx_buf_len, slv_tx_buf_len, intr_alloc_flags)						i2c_driver_install(/*i2c_port_t*/ i2c_num, /*i2c_mode_t*/ mode, /*size_t*/ slv_rx_buf_len, /*size_t*/ slv_tx_buf_len, /*int*/ intr_alloc_flags);

/**
 * @brief I2C driver delete
 *
 * @param i2c_num I2C port number
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_driver_delete(i2c_num)			i2c_driver_delete(/*i2c_port_t*/ i2c_num)

/**
 * @brief I2C parameter initialization
 *
 * @param i2c_num I2C port number
 * @param i2c_conf pointer to I2C parameter settings
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_param_config(i2c_num, i2c_conf)				i2c_param_config(/*i2c_port_t*/ i2c_num, /*const i2c_config_t* */ i2c_conf)

/**
 * @brief reset I2C tx hardware fifo
 *
 * @param i2c_num I2C port number
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_reset_tx_fifo(i2c_num)			i2c_reset_tx_fifo(/*i2c_port_t*/ i2c_num)

/**
 * @brief reset I2C rx fifo
 *
 * @param i2c_num I2C port number
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_reset_rx_fifo(i2c_num)	i2c_reset_rx_fifo(/*i2c_port_t*/ i2c_num)

/**
 * @brief I2C isr handler register
 *
 * @param i2c_num I2C port number
 * @param fn isr handler function
 * @param arg parameter for isr handler function
 * @param intr_alloc_flags Flags used to allocate the interrupt. One or multiple (ORred)
 *            ESP_INTR_FLAG_* values. See esp_intr_alloc.h for more info.
 * @param handle handle return from esp_intr_alloc.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_isr_register(i2c_num, fn, arg, intr_alloc_flags, handle)			i2c_isr_register(/*i2c_port_t*/ i2c_num, /*void (* */ fn /*)(void*)*/, /*void* */ arg, /*int*/ intr_alloc_flags, /*intr_handle_t* */ handle)

/**
 * @brief to delete and free I2C isr.
 *
 * @param handle handle of isr.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_isr_free(handle)				i2c_isr_free(/*intr_handle_t*/ handle)

/**
 * @brief Configure GPIO signal for I2C sck and sda
 *
 * @param i2c_num I2C port number
 * @param sda_io_num GPIO number for I2C sda signal
 * @param scl_io_num GPIO number for I2C scl signal
 * @param sda_pullup_en Whether to enable the internal pullup for sda pin
 * @param scl_pullup_en Whether to enable the internal pullup for scl pin
 * @param mode I2C mode
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_set_pin(i2c_num, sda_io_num, scl_io_num, \
                      sda_pullup_en, scl_pullup_en, mode)				i2c_set_pin(/*i2c_port_t*/ i2c_num, /*int*/ sda_io_num, /*int*/ scl_io_num,\
                              	  	  	  	  	  	  	  	  	  /*gpio_pullup_t*/ sda_pullup_en, /*gpio_pullup_t*/ scl_pullup_en, /*i2c_mode_t*/ mode)

/**
 * @brief Create and init I2C command link
 *        @note
 *        Before we build I2C command link, we need to call i2c_cmd_link_create() to create
 *        a command link.
 *        After we finish sending the commands, we need to call i2c_cmd_link_delete() to
 *        release and return the resources.
 *
 * @return i2c command link handler
 */
#define i2c_pf_cmd_link_create()			i2c_cmd_link_create()

/**
 * @brief Free I2C command link
 *        @note
 *        Before we build I2C command link, we need to call i2c_cmd_link_create() to create
 *        a command link.
 *        After we finish sending the commands, we need to call i2c_cmd_link_delete() to
 *        release and return the resources.
 *
 * @param cmd_handle I2C command handle
 */
#define i2c_pf_cmd_link_delete(cmd_handle)				i2c_cmd_link_delete(/*i2c_cmd_handle_t*/ cmd_handle)

/**
 * @brief Queue command for I2C master to generate a start signal
 *        @note
 *        Only call this function in I2C master mode
 *        Call i2c_master_cmd_begin() to send all queued commands
 *
 * @param cmd_handle I2C cmd link
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_master_start(cmd_handle)				i2c_master_start(/*i2c_cmd_handle_t*/ cmd_handle);

/**
 * @brief Queue command for I2C master to write one byte to I2C bus
 *        @note
 *        Only call this function in I2C master mode
 *        Call i2c_master_cmd_begin() to send all queued commands
 *
 * @param cmd_handle I2C cmd link
 * @param data I2C one byte command to write to bus
 * @param ack_en enable ack check for master
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_master_write_byte(cmd_handle, data, ack_en)				i2c_master_write_byte(/*i2c_cmd_handle_t*/ cmd_handle, /*uint8_t*/ data, /*bool*/ ack_en)

/**
 * @brief Queue command for I2C master to write buffer to I2C bus
 *        @note
 *        Only call this function in I2C master mode
 *        Call i2c_master_cmd_begin() to send all queued commands
 *
 * @param cmd_handle I2C cmd link
 * @param data data to send
 *        @note
 *        If the psram is enabled and intr_flag is `ESP_INTR_FLAG_IRAM`, please use the memory allocated from internal RAM.
 * @param data_len data length
 * @param ack_en enable ack check for master
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_master_write(cmd_handle, data, data_len, ack_en)				i2c_master_write(/*i2c_cmd_handle_t*/ cmd_handle, /*uint8_t* */ data, /*size_t*/ data_len, /*bool*/ ack_en)

/**
 * @brief Queue command for I2C master to read one byte from I2C bus
 *        @note
 *        Only call this function in I2C master mode
 *        Call i2c_master_cmd_begin() to send all queued commands
 *
 * @param cmd_handle I2C cmd link
 * @param data pointer accept the data byte
 *        @note
 *        If the psram is enabled and intr_flag is `ESP_INTR_FLAG_IRAM`, please use the memory allocated from internal RAM.
 * @param ack ack value for read command
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_master_read_byte(cmd_handle, data, ack)				i2c_master_read_byte(/*i2c_cmd_handle_t*/ cmd_handle, /*uint8_t* */ data, /*i2c_ack_type_t*/ ack)

/**
 * @brief Queue command for I2C master to read data from I2C bus
 *        @note
 *        Only call this function in I2C master mode
 *        Call i2c_master_cmd_begin() to send all queued commands
 *
 * @param cmd_handle I2C cmd link
 * @param data data buffer to accept the data from bus
 *        @note
 *        If the psram is enabled and intr_flag is `ESP_INTR_FLAG_IRAM`, please use the memory allocated from internal RAM.
 * @param data_len read data length
 * @param ack ack value for read command
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_master_read( cmd_handle, data, data_len, ack)		i2c_master_read(/*i2c_cmd_handle_t*/ cmd_handle, /*uint8_t* */ data, /*size_t*/ data_len, /*i2c_ack_type_t*/ ack)

/**
 * @brief Queue command for I2C master to generate a stop signal
 *        @note
 *        Only call this function in I2C master mode
 *        Call i2c_master_cmd_begin() to send all queued commands
 *
 * @param cmd_handle I2C cmd link
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_master_stop(cmd_handle)			i2c_master_stop(/*i2c_cmd_handle_t*/ cmd_handle)

/**
 * @brief I2C master send queued commands.
 *        This function will trigger sending all queued commands.
 *        The task will be blocked until all the commands have been sent out.
 *        The I2C APIs are not thread-safe, if you want to use one I2C port in different tasks,
 *        you need to take care of the multi-thread issue.
 *        @note
 *        Only call this function in I2C master mode
 *
 * @param i2c_num I2C port number
 * @param cmd_handle I2C command handler
 * @param ticks_to_wait maximum wait ticks.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Sending command error, slave doesn't ACK the transfer.
 *     - ESP_ERR_INVALID_STATE I2C driver not installed or not in master mode.
 *     - ESP_ERR_TIMEOUT Operation timeout because the bus is busy.
 */
#define i2c_pf_master_cmd_begin(i2c_num, cmd_handle, ticks_to_wait)				i2c_master_cmd_begin(/*i2c_port_t*/ i2c_num, /*i2c_cmd_handle_t*/ cmd_handle, /*TickType_t*/ ticks_to_wait)

/**
 * @brief I2C slave write data to internal ringbuffer, when tx fifo empty, isr will fill the hardware
 *        fifo from the internal ringbuffer
 *        @note
 *        Only call this function in I2C slave mode
 *
 * @param i2c_num I2C port number
 * @param data data pointer to write into internal buffer
 * @param size data size
 * @param ticks_to_wait Maximum waiting ticks
 *
 * @return
 *     - ESP_FAIL(-1) Parameter error
 *     - Others(>=0) The number of data bytes that pushed to the I2C slave buffer.
 */
#define i2c_pf_slave_write_buffer(i2c_num, data, size, ticks_to_wait)				i2c_slave_write_buffer(/*i2c_port_t*/ i2c_num, /*uint8_t* */ data, /*int*/ size, /*TickType_t*/ ticks_to_wait)

/**
 * @brief I2C slave read data from internal buffer. When I2C slave receive data, isr will copy received data
 *        from hardware rx fifo to internal ringbuffer. Then users can read from internal ringbuffer.
 *        @note
 *        Only call this function in I2C slave mode
 *
 * @param i2c_num I2C port number
 * @param data data pointer to write into internal buffer
 * @param max_size Maximum data size to read
 * @param ticks_to_wait Maximum waiting ticks
 *
 * @return
 *     - ESP_FAIL(-1) Parameter error
 *     - Others(>=0) The number of data bytes that read from I2C slave buffer.
 */
#define i2c_pf_slave_read_buffer(i2c_num, data, max_size, ticks_to_wait)				i2c_slave_read_buffer(/*i2c_port_t*/ i2c_num, /*uint8_t* */ data, /*size_t*/ max_size, /*TickType_t*/ ticks_to_wait);

/**
 * @brief set I2C master clock period
 *
 * @param i2c_num I2C port number
 * @param high_period clock cycle number during SCL is high level, high_period is a 14 bit value
 * @param low_period clock cycle number during SCL is low level, low_period is a 14 bit value
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_set_period(i2c_num, high_period, low_period)				i2c_set_period(/*i2c_port_t*/ i2c_num, /*int*/ high_period, /*int*/ low_period)

/**
 * @brief get I2C master clock period
 *
 * @param i2c_num I2C port number
 * @param high_period pointer to get clock cycle number during SCL is high level, will get a 14 bit value
 * @param low_period pointer to get clock cycle number during SCL is low level, will get a 14 bit value
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_get_period(i2c_num, high_period, low_period)			i2c_get_period(/*i2c_port_t*/ i2c_num, /*int* */ high_period, /*int* */ low_period)

/**
 * @brief enable hardware filter on I2C bus
 *        Sometimes the I2C bus is disturbed by high frequency noise(about 20ns), or the rising edge of
 *        the SCL clock is very slow, these may cause the master state machine broken. enable hardware
 *        filter can filter out high frequency interference and make the master more stable.
 *        @note
 *        Enable filter will slow the SCL clock.
 *
 * @param i2c_num I2C port number
 * @param cyc_num the APB cycles need to be filtered(0<= cyc_num <=7).
 *        When the period of a pulse is less than cyc_num * APB_cycle, the I2C controller will ignore this pulse.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_filter_enable(i2c_num, cyc_num)						i2c_filter_enable(/*i2c_port_t*/ i2c_num, /*uint8_t*/ cyc_num)

/**
 * @brief disable filter on I2C bus
 *
 * @param i2c_num I2C port number
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_filter_disable(i2c_num)				i2c_filter_disable(/*i2c_port_t*/ i2c_num)

/**
 * @brief set I2C master start signal timing
 *
 * @param i2c_num I2C port number
 * @param setup_time clock number between the falling-edge of SDA and rising-edge of SCL for start mark, it's a 10-bit value.
 * @param hold_time clock num between the falling-edge of SDA and falling-edge of SCL for start mark, it's a 10-bit value.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_set_start_timing(i2c_num, setup_time, hold_time)				i2c_set_start_timing(/*i2c_port_t*/ i2c_num, /*int*/ setup_time, /*int*/ hold_time)

/**
 * @brief get I2C master start signal timing
 *
 * @param i2c_num I2C port number
 * @param setup_time pointer to get setup time
 * @param hold_time pointer to get hold time
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_get_start_timing(i2c_num, setup_time, hold_time)		i2c_get_start_timing(/*i2c_port_t*/ i2c_num, /*int* */ setup_time, /*int* */ hold_time)

/**
 * @brief set I2C master stop signal timing
 *
 * @param i2c_num I2C port number
 * @param setup_time clock num between the rising-edge of SCL and the rising-edge of SDA, it's a 10-bit value.
 * @param hold_time clock number after the STOP bit's rising-edge, it's a 14-bit value.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_set_stop_timing(i2c_num, setup_time, hold_time)		i2c_set_stop_timing(/*i2c_port_t*/ i2c_num, /*int*/ setup_time, /*int*/ hold_time)

/**
 * @brief get I2C master stop signal timing
 *
 * @param i2c_num I2C port number
 * @param setup_time pointer to get setup time.
 * @param hold_time pointer to get hold time.
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_get_stop_timing(i2c_num, setup_time, hold_time)	i2c_get_stop_timing(/*i2c_port_t*/ i2c_num, /*int* */ setup_time, /*int* */ hold_time)

/**
 * @brief set I2C data signal timing
 *
 * @param i2c_num I2C port number
 * @param sample_time clock number I2C used to sample data on SDA after the rising-edge of SCL, it's a 10-bit value
 * @param hold_time clock number I2C used to hold the data after the falling-edge of SCL, it's a 10-bit value
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_set_data_timing(i2c_num, sample_time, hold_time)		i2c_set_data_timing(/*i2c_port_t*/ i2c_num, /*int*/ sample_time, /*int*/ hold_time)

/**
 * @brief get I2C data signal timing
 *
 * @param i2c_num I2C port number
 * @param sample_time pointer to get sample time
 * @param hold_time pointer to get hold time
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_get_data_timing(i2c_num, sample_time, hold_time)		i2c_get_data_timing(/*i2c_port_t*/ i2c_num, /*int* */ sample_time, /*int* */ hold_time)

/**
 * @brief set I2C timeout value
 * @param i2c_num I2C port number
 * @param timeout timeout value for I2C bus (unit: APB 80Mhz clock cycle)
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_set_timeout(i2c_num, timeout)		i2c_set_timeout(/*i2c_port_t*/ i2c_num, /*int*/ timeout)

/**
 * @brief get I2C timeout value
 * @param i2c_num I2C port number
 * @param timeout pointer to get timeout value
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_get_timeout(i2c_num, timeout)			i2c_get_timeout(/*i2c_port_t*/ i2c_num, /*int*	*/ timeout)
/**
 * @brief set I2C data transfer mode
 *
 * @param i2c_num I2C port number
 * @param tx_trans_mode I2C sending data mode
 * @param rx_trans_mode I2C receving data mode
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_set_data_mode(i2c_num, tx_trans_mode, rx_trans_mode)			i2c_set_data_mode(/*i2c_port_t*/ i2c_num, /*i2c_trans_mode_t*/ tx_trans_mode, /*i2c_trans_mode_t*/ rx_trans_mode)

/**
 * @brief get I2C data transfer mode
 *
 * @param i2c_num I2C port number
 * @param tx_trans_mode pointer to get I2C sending data mode
 * @param rx_trans_mode pointer to get I2C receiving data mode
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 */
#define i2c_pf_get_data_mode(i2c_num, tx_trans_mode, rx_trans_mode)			i2c_get_data_mode(/*i2c_port_t*/ i2c_num, /*i2c_trans_mode_t* */tx_trans_mode, /*i2c_trans_mode_t* */ rx_trans_mode)

/******************************************************************************************************************************************************************************************************/

///**
// * @brief I2C connection initialization parameters
// */
//typedef struct {
//	uint32_t i2c_port; /*!< I2C platform port  */
//	uint32_t i2c_mode; /*!< I2C mode */
//
//	uint32_t sda_io_num; /*!< GPIO number for I2C sda signal */
//	uint32_t scl_io_num; /*!< GPIO number for I2C scl signal */
//
//	bool is_internal_pullup_enable;		/*!< GPIO's Internal pull-up mode enable variable for I2C sda/scl signal*/
//
//	union {
//		struct {
//			uint32_t clk_speed; /*!< I2C clock frequency for master mode, (no higher than 1MHz for now) */
//		} master;
//		struct {
//			uint8_t addr_10bit_en; /*!< I2C 10bit address mode enable for slave mode */
//			uint16_t slave_addr; /*!< I2C address for slave mode */
//		} slave;
//	};
//} I2C_CONN_DATA;

/**
 * @brief initialize I2C peripheral
 *
 * @param i2c_conn_data		I2C connection driver data
 *
 * @return
 *     - CORE_OK 						Success
 *     - CORE_ERR_INVALID_ARG 			Parameter error
 *     - CORE_FAIL 						Driver install error
 */
core_err init_platform_i2c(I2C_CONN_DATA *i2c_conn_data);

/**
 * @brief I2C master to write buffer to I2C slave
 *
 * @param i2c_port		I2C port number ( I2C_CORE_NUM_0 , I2C_CORE_NUM_1 )
 * @param slave_addr	I2C slave address
 * @param cmd_addr		I2C slave control/data command address
 * @param data_wr		I2C write data buffer
 * @param size			I2C write data buffer size
 *
 * @return
 *     - CORE_OK 					Success
 *     - CORE_ERR_INVALID_ARG 		Parameter error
 *     - CORE_FAIL 					Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 		I2C	driver not installed or not in master mode.
 *     - CORE_ERR_TIMEOUT 			Operation timeout because the bus is busy.
 */
core_err i2c_platform_master_write_slave(uint32_t i2c_port_num, uint8_t slave_addr, uint8_t cmd_addr, uint8_t *data_wr,
		size_t size);

/**
 * @brief I2C master read buffer to I2C slave
 *
 * @param i2c_port		          	I2C port number ( I2C_CORE_NUM_0 , I2C_CORE_NUM_1 )
 * @param slave_addr	          	I2C slave address
 * @param cmd_addr		          	I2C slave control/data command address
 * @param data_rd		          	I2C read data buffer
 * @param size			          	I2C read data buffer size
 * @param delay_before_restart		time delay before restart operation in I2C read process
 *
 * @return
 *     - CORE_OK 					Success
 *     - CORE_ERR_INVALID_ARG 		Parameter error
 *     - CORE_FAIL 					Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 		I2C	driver not installed or not in master mode.
 *     - CORE_ERR_TIMEOUT 			Operation timeout because the bus is busy.
 */
core_err i2c_platform_master_read_slave(uint32_t i2c_port_num, uint8_t slave_addr, uint8_t cmd_addr, uint8_t *data_rd,
		size_t size, uint32_t delay_before_restart);


/**
 * @brief I2C master read buffer without write the cmd address to I2C slave
 *
 * @param i2c_port		          	I2C port number ( I2C_CORE_NUM_0 , I2C_CORE_NUM_1 )
 * @param slave_addr	          	I2C slave address
 * @param data_rd		          	I2C read data buffer
 * @param size			          	I2C read data buffer size
 *
 * @return
 *     - CORE_OK 					Success
 *     - CORE_ERR_INVALID_ARG 		Parameter error
 *     - CORE_FAIL 					Sending command error, slave doesn’t ACK the transfer.
 *     - CORE_ERR_INVALID_STATE 	I2C	driver not installed or not in master mode.
 *     - CORE_ERR_TIMEOUT 			Operation timeout because the bus is busy.
 */
core_err i2c_platform_master_read_slave_without_write(uint32_t i2c_port_num, uint8_t slave_addr, uint8_t *data_rd,
		size_t size);

/**
 * @brief de-initialize I2C peripheral
 *
 * @param i2c_conn_data		I2C connection driver data
 *
 * @return
 *     - CORE_OK 						Success
 *     - CORE_ERR_INVALID_ARG 			Parameter error
 */
core_err deinit_platform_i2c(I2C_CONN_DATA *i2c_conn_data);

#ifdef __cplusplus
}
#endif

#endif /* I2C_PLATFORM_H_ */
