/*
 * File Name: button_platform.h
 * File Path: /emmate/src/platform/esp/hmi/button/button_platform.h
 * Description:
 *
 *  Created on: 10-May-2019
 *      Author: Noyel Seth
 */

#ifndef BUTTON_PLATFORM_H_
#define BUTTON_PLATFORM_H_

#include "core_common.h"
#include "core_config.h"

#define BSP_INIT_BUTTONS (1 << 1) /**< Enable buttons during initialization (@ref bsp_board_init).*/

/**
 * @brief  Get the state of a Button
 *
 * @param[in]	button_idx Button index
 *
 * @return
 * 		- true
 * 		- false
 */
bool get_bsp_board_btn_state(uint32_t button_idx);

/**
 * @brief  Get the connected Buttons's Index number
 *
 * @param[in]	pin_number connected Button's GPIO number
 *
 * @return
 * 		- led_idx number
 */
uint32_t pin_to_btn_idx_bsp_board(uint32_t pin_number);


/**
 * @brief  Initialize the Buttons
 *
 * @param[in]	init_flags Button initialization flag
 *
 */
void init_platform_buttons(uint32_t init_flags);

#endif /* BUTTON_PLATFORM_H_ */
