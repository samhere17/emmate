/*
 * File Name: led_platform.h
 * File Path: /emmate/src/hmi/led/esp-idf/led_platform.h
 * Description:
 *
 *  Created on: 14-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef LED_PLATFORM_H_
#define LED_PLATFORM_H_

#include "core_common.h"
#include "core_config.h"
#include "core_error.h"

#define BSP_INIT_LEDS    (1 << 0) /**< Enable LEDs during initialization (@ref bsp_board_init).*/

/**
 * @brief  Get the state of a LED
 *
 * @param[in]	led_idx LED index
 *
 * @return
 * 		- true
 * 		- false
 */
bool get_bsp_board_led_state(uint32_t led_idx);

/**
 * @brief  Switch On the LED
 *
 * @param[in]	led_idx LED index
 *
 */
void on_bsp_board_led(uint32_t led_idx);

/**
 * @brief  Switch Off the LED
 *
 * @param[in]	led_idx LED index
 *
 */
void off_bsp_board_led(uint32_t led_idx);

/**
 * @brief  Switch Off all LEDs
 *
 */
void off_bsp_board_leds(void);

/**
 * @brief  Switch On all LEDs
 *
 */
void on_bsp_board_leds(void);

/**
 * @brief  Toggle all LED's state
 *
 * @param[in]	led_idx LED index
 *
 */
void invert_bsp_board_led(uint32_t led_idx);

/**
 * @brief  Get the connected LED's GPIO number
 *
 * @param[in]	led_idx LED index
 *
 * @return
 * 		- GPIO number
 *
 */
uint32_t led_idx_to_pin_bsp_board(uint32_t led_idx);

/**
 * @brief  Get the connected LED's Index number
 *
 * @param[in]	pin_number connected LED's GPIO number
 *
 * @return
 * 		- led_idx number
 */
uint32_t pin_to_led_idx_bsp_board(uint32_t pin_number);

/**
 * @brief  Initialize the LED's
 *
 * @param[in]	init_flags LED initialization flag
 *
 */
void init_platform_leds(uint32_t init_flags);

#endif /* LED_PLATFORM_H_ */
