/*
 * File Name: threading_platform.h
 * File Path: /emmate/src/threading/esp-idf/threading_platform.h
 * Description:
 *
 *  Created on: 14-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef THREADING_PLATFORM_H_
#define THREADING_PLATFORM_H_

#include "core_common.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"

/* Thread/Task related datatypes, enums and macros */
#define TICK_RATE_TO_MS			portTICK_RATE_MS
#define THREADING_MAX_DELAY		portMAX_DELAY

typedef TaskHandle_t		ThreadHandle;
typedef TickType_t			TickType;
typedef BaseType_t			BaseType;
typedef SemaphoreHandle_t	SemaphoreHandle;

/** Thread states returned by GET_THREAD_STATE. */
typedef enum
{
	TH_RUNNING = eRunning,		/*!< A task is querying the state of itself, so must be running. */
	TH_READY = eReady,			/*!< The task being queried is in a read or pending ready list. */
	TH_BLOCKED = eBlocked,		/*!< The task being queried is in the Blocked state. */
	TH_SUSPENDED = eSuspended,	/*!< The task being queried is in the Suspended state, or is in the Blocked state with an infinite time out. */
	TH_DELETED = eDeleted		/*!< The task being queried has been deleted, but its TCB has not yet been freed. */
} THREAD_STATE;


/**
 * TaskPfCreate() can only be used to create a task that has unrestricted
 * access to the entire microcontroller memory map.  Systems that include MPU
 * support can alternatively create an MPU constrained task using
 * xTaskCreateRestricted().
 *
 * @param func Pointer to the task entry function.  Tasks
 * must be implemented to never return (i.e. continuous loop).
 *
 * @param name A descriptive name for the task.  This is mainly used to
 * facilitate debugging.  Max length defined by configMAX_TASK_NAME_LEN - default
 * is 16.
 *
 * @param stack_depth The size of the task stack specified as the number of
 * bytes. Note that this differs from vanilla FreeRTOS.
 *
 * @param params Pointer that will be used as the parameter for the task
 * being created.
 *
 * @param priority The priority at which the task should run.  Systems that
 * include MPU support can optionally create tasks in a privileged (system)
 * mode by setting bit portPRIVILEGE_BIT of the priority parameter.  For
 * example, to create a privileged task at priority 2 the uxPriority parameter
 * should be set to ( 2 | portPRIVILEGE_BIT ).
 *
 * @param thread_handle Used to pass back a handle by which the created task
 * can be referenced.
 *
 * @return pdPASS if the task was successfully created and added to a ready
 * list, otherwise an error code defined in the file projdefs.h
 *
 * @note If program uses thread local variables (ones specified with "__thread" keyword)
 * then storage for them will be allocated on the task's stack.
 *
 */
#define TaskPfCreate(func, name, stack_depth, params, priority, thread_handle) \
								xTaskCreate(func, name, stack_depth, params, priority, thread_handle)


/**
 * Create a new task with a specified affinity.
 *
 * This function is similar to TaskPfCreate, but allows setting task affinity
 * in SMP system.
 *
 *  @param func Pointer to the task entry function.  Tasks
 * must be implemented to never return (i.e. continuous loop).
 *
 * @param name A descriptive name for the task.  This is mainly used to
 * facilitate debugging.  Max length defined by configMAX_TASK_NAME_LEN - default
 * is 16.
 *
 * @param stack_depth The size of the task stack specified as the number of
 * bytes. Note that this differs from vanilla FreeRTOS.
 *
 * @param params Pointer that will be used as the parameter for the task
 * being created.
 *
 * @param priority The priority at which the task should run.  Systems that
 * include MPU support can optionally create tasks in a privileged (system)
 * mode by setting bit portPRIVILEGE_BIT of the priority parameter.  For
 * example, to create a privileged task at priority 2 the uxPriority parameter
 * should be set to ( 2 | portPRIVILEGE_BIT ).
 *
 * @param thread_handle Used to pass back a handle by which the created task
 * can be referenced.

 * @param core_id If the value is tskNO_AFFINITY, the created task is not
 * pinned to any CPU, and the scheduler can run it on any core available.
 * Other values indicate the index number of the CPU which the task should
 * be pinned to. Specifying values larger than (portNUM_PROCESSORS - 1) will
 * cause the function to fail.
 *
 * @return pdPASS if the task was successfully created and added to a ready
 * list, otherwise an error code defined in the file projdefs.h
 *
 */
#define TaskPfCreatePinnedToCore(func, name, stack_depth, params, priority, thread_handle, core_id) \
								xTaskCreatePinnedToCore(func, name, stack_depth, params, priority, thread_handle, core_id)

/**
 * Obtain the state of any task.
 *
 * @param thread_handle Handle of the task to be queried.
 *
 * @return The state of thread_handle at the time the function was called.  Note the
 * state of the task might change between the function being called, and the
 * functions return value being tested by the calling task.
 */
#define TaskPfGetState(thread_handle)		eTaskGetState(thread_handle) /* Returns a THREAD_STATE */


/**
 * Remove a task from the RTOS real time kernel's management.
 *
 * @note The idle task is responsible for freeing the kernel allocated
 * memory from tasks that have been deleted.  It is therefore important that
 * the idle task is not starved of microcontroller processing time if your
 * application makes any calls to TaskPfDelete ().  Memory allocated by the
 * task code is not automatically freed, and should be freed before the task
 * is deleted.
 *
 * See the demo application file death.c for sample code that utilises
 * TaskPfDelete ().
 *
 * @param thread_handle The handle of the task to be deleted.  Passing NULL will
 * cause the calling task to be deleted.
 */
#define TaskPfDelete(thread_handle)			vTaskDelete(thread_handle)

/**
 * Get tick count
 *
 * @return The count of ticks.
 */
#define TaskPfGetTickCount					xTaskGetTickCount /* Returns a TickType */

/**
 * Delay a task for a given number of ticks.
 *
 * The actual time that the task remains blocked depends on the tick rate.
 * The constant portTICK_PERIOD_MS can be used to calculate real time from
 * the tick rate - with the resolution of one tick period.
 *
 * INCLUDE_vTaskDelay must be defined as 1 for this function to be available.
 * See the configuration section for more information.
 *
 * vTaskDelay() specifies a time at which the task wishes to unblock relative to
 * the time at which vTaskDelay() is called.  For example, specifying a block
 * period of 100 ticks will cause the task to unblock 100 ticks after
 * vTaskDelay() is called.  vTaskDelay() does not therefore provide a good method
 * of controlling the frequency of a periodic task as the path taken through the
 * code, as well as other task and interrupt activity, will effect the frequency
 * at which vTaskDelay() gets called and therefore the time at which the task
 * next executes.  See vTaskDelayUntil() for an alternative API function designed
 * to facilitate fixed frequency execution.  It does this by specifying an
 * absolute time (rather than a relative time) at which the calling task should
 * unblock.
 *
 * @param ticks_to_delay The amount of time, in tick periods, that
 * the calling task should block.
 *
 */
#define TaskPfDelay(ticks_to_delay)			vTaskDelay(ticks_to_delay)

/**
 * Get task name
 *
 * @return The text (human readable) name of the task referenced by the handle
 * xTaskToQuery.  A task can query its own name by either passing in its own
 * handle, or by setting xTaskToQuery to NULL.  INCLUDE_pcTaskGetTaskName must be
 * set to 1 in FreeRTOSConfig.h for pcTaskGetTaskName() to be available.
 */
#define TaskPfGetTaskName(thread_handle)			pcTaskGetTaskName(thread_handle) /* Returns (char *) */

/**
 * Returns the high water mark of the stack associated with xTask.
 *
 * INCLUDE_uxTaskGetStackHighWaterMark must be set to 1 in FreeRTOSConfig.h for
 * this function to be available.
 *
 * High water mark is the minimum free stack space there has been (in bytes
 * rather than words as found in vanilla FreeRTOS) since the task started.
 * The smaller the returned number the closer the task has come to overflowing its stack.
 *
 * @param xTask Handle of the task associated with the stack to be checked.
 * Set xTask to NULL to check the stack of the calling task.
 *
 * @return The smallest amount of free stack space there has been (in bytes
 * rather than words as found in vanilla FreeRTOS) since the task referenced by
 * xTask was created.
 */
#define TaskPfGetStackHighWaterMark(thread_handle) 	uxTaskGetStackHighWaterMark(thread_handle) /* Returns int */

/**
 * Suspend a task.
 *
 * INCLUDE_vTaskSuspend must be defined as 1 for this function to be available.
 * See the configuration section for more information.
 *
 * When suspended, a task will never get any microcontroller processing time,
 * no matter what its priority.
 *
 * Calls to vTaskSuspend are not accumulative -
 * i.e. calling vTaskSuspend () twice on the same task still only requires one
 * call to vTaskResume () to ready the suspended task.
 *
 * @param xTaskToSuspend Handle to the task being suspended.  Passing a NULL
 * handle will cause the calling task to be suspended.
 *
 */
#define TaskPfSuspend(thread_handle)			vTaskSuspend(thread_handle)

/**
 * Resumes a suspended task.
 *
 * INCLUDE_vTaskSuspend must be defined as 1 for this function to be available.
 * See the configuration section for more information.
 *
 * A task that has been suspended by one or more calls to vTaskSuspend ()
 * will be made available for running again by a single call to
 * vTaskResume ().
 *
 * @param xTaskToResume Handle to the task being readied.
 *
 */
#define TaskPfResume(thread_handle)			vTaskResume(thread_handle)


/* Queue related datatypes and macros */
/**
 * Creates a new queue instance.  This allocates the storage required by the
 * new queue and returns a handle for the queue.
 *
 * @param uxQueueLength The maximum number of items that the queue can contain.
 *
 * @param uxItemSize The number of bytes each item in the queue will require.
 * Items are queued by copy, not by reference, so this is the number of bytes
 * that will be copied for each posted item.  Each item on the queue must be
 * the same size.
 *
 * @return If the queue is successfully create then a handle to the newly
 * created queue is returned.  If the queue cannot be created then 0 is
 * returned.
 */
#define QueuePfCreate(queue_len, item_size) 				xQueueCreate(queue_len, item_size)

/**
 * This is a macro that calls the xQueueGenericReceive() function.
 *
 * Receive an item from a queue.  The item is received by copy so a buffer of
 * adequate size must be provided.  The number of bytes copied into the buffer
 * was defined when the queue was created.
 *
 * Successfully received items are removed from the queue.
 *
 * This function must not be used in an interrupt service routine.  See
 * xQueueReceiveFromISR for an alternative that can.
 *
 * @param queue The handle to the queue from which the item is to be
 * received.
 *
 * @param buffer Pointer to the buffer into which the received item will
 * be copied.
 *
 * @param ticks_to_wait The maximum amount of time the task should block
 * waiting for an item to receive should the queue be empty at the time
 * of the call.	 xQueueReceive() will return immediately if xTicksToWait
 * is zero and the queue is empty.  The time is defined in tick periods so the
 * constant portTICK_PERIOD_MS should be used to convert to real time if this is
 * required.
 *
 * @return pdTRUE if an item was successfully received from the queue,
 * otherwise pdFALSE.
 */
#define QueuePfReceive(queue, buffer, ticks_to_wait) 		xQueueReceive(queue, buffer, ticks_to_wait)

/**
 * This is a macro that calls xQueueGenericSend().  It is included for
 * backward compatibility with versions of FreeRTOS.org that did not
 * include the xQueueSendToFront() and xQueueSendToBack() macros.  It is
 * equivalent to xQueueSendToBack().
 *
 * Post an item on a queue.  The item is queued by copy, not by reference.
 * This function must not be called from an interrupt service routine.
 * See xQueueSendFromISR () for an alternative which may be used in an ISR.
 *
 * @param queue The handle to the queue on which the item is to be posted.
 *
 * @param item_to_queue A pointer to the item that is to be placed on the
 * queue.  The size of the items the queue will hold was defined when the
 * queue was created, so this many bytes will be copied from pvItemToQueue
 * into the queue storage area.
 *
 * @param ticks_to_wait The maximum amount of time the task should block
 * waiting for space to become available on the queue, should it already
 * be full.  The call will return immediately if this is set to 0 and the
 * queue is full.  The time is defined in tick periods so the constant
 * portTICK_PERIOD_MS should be used to convert to real time if this is required.
 *
 * @return pdTRUE if the item was successfully posted, otherwise errQUEUE_FULL.
 */
#define QueuePfSend(queue, item_to_queue, ticks_to_wait) 	xQueueSend(queue, item_to_queue, ticks_to_wait)


/**
 * This is a macro that calls xQueueGenericSendFromISR().  It is included
 * for backward compatibility with versions of FreeRTOS.org that did not
 * include the xQueueSendToBackFromISR() and xQueueSendToFrontFromISR()
 * macros.
 *
 * Post an item to the back of a queue.  It is safe to use this function from
 * within an interrupt service routine.
 *
 * Items are queued by copy not reference so it is preferable to only
 * queue small items, especially when called from an ISR.  In most cases
 * it would be preferable to store a pointer to the item being queued.
 *
 * @param queue The handle to the queue on which the item is to be posted.
 *
 * @param item_to_queue A pointer to the item that is to be placed on the
 * queue.  The size of the items the queue will hold was defined when the
 * queue was created, so this many bytes will be copied from pvItemToQueue
 * into the queue storage area.
 *
 * @param[out] pxHigherPriorityTaskWoken xQueueSendFromISR() will set
 * *pxHigherPriorityTaskWoken to pdTRUE if sending to the queue caused a task
 * to unblock, and the unblocked task has a priority higher than the currently
 * running task.  If xQueueSendFromISR() sets this value to pdTRUE then
 * a context switch should be requested before the interrupt is exited.
 *
 * @return pdTRUE if the data was successfully sent to the queue, otherwise
 * errQUEUE_FULL.
 *
 */
#define QueuePfSendFromISR(queue,item_to_queue,pxHigherPriorityTaskWoken) xQueueSendFromISR(/*QueueHandle_t*/ queue, /*const void * */item_to_queue, /*BaseType_t * */pxHigherPriorityTaskWoken)

/**
 * Reset a queue back to its original empty state.  pdPASS is returned if the
 * queue is successfully reset.  pdFAIL is returned if the queue could not be
 * reset because there are tasks blocked on the queue waiting to either
 * receive from the queue or send to the queue.
 *
 * @param queue The queue to reset
 * @return always returns pdPASS
 */
#define QueuePfReset(queue) 								xQueueReset(queue)

/**
 * Delete a queue - freeing all the memory allocated for storing of items
 * placed on the queue.
 *
 * @param queue A handle to the queue to be deleted.

 */
#define QueuePfDelete(queue) 								vQueueDelete(queue)

/**
 * Return the number of messages stored in a queue.
 *
 * @param queue A handle to the queue being queried.
 *
 * @return The number of messages available in the queue.

 */
#define QueuePfMessagesWaiting(queue)							uxQueueMessagesWaiting(queue)	/* Returns int*/


/**
 * <i>Macro</i> that implements a mutex semaphore by using the existing queue
 * mechanism.
 *
 * Internally, within the FreeRTOS implementation, mutex semaphores use a block
 * of memory, in which the mutex structure is stored.  If a mutex is created
 * using xSemaphoreCreateMutex() then the required memory is automatically
 * dynamically allocated inside the xSemaphoreCreateMutex() function.  (see
 * http://www.freertos.org/a00111.html).  If a mutex is created using
 * xSemaphoreCreateMutexStatic() then the application writer must provided the
 * memory.  xSemaphoreCreateMutexStatic() therefore allows a mutex to be created
 * without using any dynamic memory allocation.
 *
 * Mutexes created using this function can be accessed using the xSemaphoreTake()
 * and xSemaphoreGive() macros.  The xSemaphoreTakeRecursive() and
 * xSemaphoreGiveRecursive() macros must not be used.
 *
 * This type of semaphore uses a priority inheritance mechanism so a task
 * 'taking' a semaphore MUST ALWAYS 'give' the semaphore back once the
 * semaphore it is no longer required.
 *
 * Mutex type semaphores cannot be used from within interrupt service routines.
 *
 * See vSemaphoreCreateBinary() for an alternative implementation that can be
 * used for pure synchronisation (where one task or interrupt always 'gives' the
 * semaphore and another always 'takes' the semaphore) and from within interrupt
 * service routines.
 *
 * @return If the mutex was successfully created then a handle to the created
 * semaphore is returned.  If there was not enough heap to allocate the mutex
 * data structures then NULL is returned.
 *
 * Example usage:
 * @code{c}
 *  SemaphoreHandle_t xSemaphore;
 *
 *  void vATask( void * pvParameters )
 *  {
 *     // Semaphore cannot be used before a call to xSemaphoreCreateMutex().
 *     // This is a macro so pass the variable in directly.
 *     xSemaphore = xSemaphoreCreateMutex();
 *
 *     if( xSemaphore != NULL )
 *     {
 *         // The semaphore was created successfully.
 *         // The semaphore can now be used.
 *     }
 *  }
 * @endcode
 * \ingroup Semaphores
 */
#define	SemaphorePfCreateMutex() 										xSemaphoreCreateMutex()

/**
 * Delete a semaphore.  This function must be used with care.  For example,
 * do not delete a mutex type semaphore if the mutex is held by a task.
 *
 * @param xSemaphore A handle to the semaphore to be deleted.
 *
 * \ingroup Semaphores
 */
#define	SemaphorePfDelete( xSemaphore )									vSemaphoreDelete( xSemaphore )

/**
 * <i>Macro</i> to release a semaphore.  The semaphore must have previously been
 * created with a call to vSemaphoreCreateBinary(), xSemaphoreCreateMutex() or
 * xSemaphoreCreateCounting(). and obtained using sSemaphoreTake().
 *
 * This macro must not be used from an ISR.  See xSemaphoreGiveFromISR () for
 * an alternative which can be used from an ISR.
 *
 * This macro must also not be used on semaphores created using
 * xSemaphoreCreateRecursiveMutex().
 *
 * @param xSemaphore A handle to the semaphore being released.  This is the
 * handle returned when the semaphore was created.
 *
 * @return pdTRUE if the semaphore was released.  pdFALSE if an error occurred.
 * Semaphores are implemented using queues.  An error can occur if there is
 * no space on the queue to post a message - indicating that the
 * semaphore was not first obtained correctly.
 *
 * Example usage:
 * @code{c}
 *  SemaphoreHandle_t xSemaphore = NULL;
 *
 *  void vATask( void * pvParameters )
 *  {
 *     // Create the semaphore to guard a shared resource.
 *     vSemaphoreCreateBinary( xSemaphore );
 *
 *     if( xSemaphore != NULL )
 *     {
 *         if( xSemaphoreGive( xSemaphore ) != pdTRUE )
 *         {
 *             // We would expect this call to fail because we cannot give
 *             // a semaphore without first "taking" it!
 *         }
 *
 *         // Obtain the semaphore - don't block if the semaphore is not
 *         // immediately available.
 *         if( xSemaphoreTake( xSemaphore, ( TickType_t ) 0 ) )
 *         {
 *             // We now have the semaphore and can access the shared resource.
 *
 *             // ...
 *
 *             // We have finished accessing the shared resource so can free the
 *             // semaphore.
 *             if( xSemaphoreGive( xSemaphore ) != pdTRUE )
 *             {
 *                 // We would not expect this call to fail because we must have
 *                 // obtained the semaphore to get here.
 *             }
 *         }
 *     }
 *  }
 * @endcode
 * \ingroup Semaphores
 */
#define SemaphorePfGive( xSemaphore )									xSemaphoreGive( xSemaphore )


/**
 * <i>Macro</i> to obtain a semaphore.  The semaphore must have previously been
 * created with a call to vSemaphoreCreateBinary(), xSemaphoreCreateMutex() or
 * xSemaphoreCreateCounting().
 *
 * @param xSemaphore A handle to the semaphore being taken - obtained when
 * the semaphore was created.
 *
 * @param xBlockTime The time in ticks to wait for the semaphore to become
 * available.  The macro portTICK_PERIOD_MS can be used to convert this to a
 * real time.  A block time of zero can be used to poll the semaphore.  A block
 * time of portMAX_DELAY can be used to block indefinitely (provided
 * INCLUDE_vTaskSuspend is set to 1 in FreeRTOSConfig.h).
 *
 * @return pdTRUE if the semaphore was obtained.  pdFALSE
 * if xBlockTime expired without the semaphore becoming available.
 *
 * Example usage:
 * @code{c}
 *  SemaphoreHandle_t xSemaphore = NULL;
 *
 *  // A task that creates a semaphore.
 *  void vATask( void * pvParameters )
 *  {
 *     // Create the semaphore to guard a shared resource.
 *     vSemaphoreCreateBinary( xSemaphore );
 *  }
 *
 *  // A task that uses the semaphore.
 *  void vAnotherTask( void * pvParameters )
 *  {
 *     // ... Do other things.
 *
 *     if( xSemaphore != NULL )
 *     {
 *         // See if we can obtain the semaphore.  If the semaphore is not available
 *         // wait 10 ticks to see if it becomes free.
 *         if( xSemaphoreTake( xSemaphore, ( TickType_t ) 10 ) == pdTRUE )
 *         {
 *             // We were able to obtain the semaphore and can now access the
 *             // shared resource.
 *
 *             // ...
 *
 *             // We have finished accessing the shared resource.  Release the
 *             // semaphore.
 *             xSemaphoreGive( xSemaphore );
 *         }
 *         else
 *         {
 *             // We could not obtain the semaphore and can therefore not access
 *             // the shared resource safely.
 *         }
 *     }
 *  }
 * @endcode
 * \ingroup Semaphores
 */
#define SemaphorePfTake( xSemaphore, xTicksToWait )						xSemaphoreTake( xSemaphore, xTicksToWait )


/**********************************************************************************************/

/*
 * Wrapper function and structures for the FreeRtos portmacro.h
 */

/* "mux" data structure (spinlock) */
typedef portMUX_TYPE 	pf_portMUX_TYPE;


 /* Calling pf*_CRITICAL from ISR context would cause an assert failure.
  * If the parent function is called from both ISR and Non-ISR context then call pf*_CRITICAL_SAFE
  */
#define pf_portENTER_CRITICAL(mux)    	portENTER_CRITICAL(mux)
#define pf_portEXIT_CRITICAL(mux)		portEXIT_CRITICAL(mux)


 // Keep this in sync with the portMUX_TYPE struct definition please.

#define pf_portMUX_INITIALIZER_UNLOCKED		portMUX_INITIALIZER_UNLOCKED

#endif /* THREADING_PLATFORM_H_ */
