/**
 * Company:
 *  Iquester Solutions LLP
 *
 * Project:
 *   EmMate
 *
 * File Name:
 *   nvs_helper.c
 *
 * Created On:
 *   29-Mar-2019
 *
 * Author:
 *   Noyel Seth
 *
 * Summary:
 *
 *
 * Description:
 *
 *
 **/

#include <stdio.h>
#include <string.h>

#include "core_config.h"
#include "config.h"
#include "core_error.h"
#include "core_constant.h"
#include "nvs_helper.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "threading.h"

#define TAG LTAG_PERSISTENT_NVS

/* Globals */
static nvs_handle nvs_hndl;

/* Static Functions */
static core_err open_nvs_handle();
static void close_nvs_handle();

static core_err open_nvs_handle() {
	core_err err = nvs_open("storage", NVS_READWRITE, &nvs_hndl);
	if (err != CORE_OK) {
		CORE_LOGE(TAG, "Error (%s) opening NVS handle!", esp_err_to_name(err));
	} else {
		CORE_LOGD(TAG, "(%s) opening NVS handle!", esp_err_to_name(err));
	}
	return err;
}

static void close_nvs_handle() {
	// Close
	if (nvs_hndl != 0) {
		nvs_close(nvs_hndl);
	}
}

/* Initialize the default ESP32 NVS partition */
core_err init_nvs() {
	core_err err = nvs_flash_init();
	if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		// NVS partition was truncated and needs to be erased
		// Retry nvs_flash_init
		ESP_ERROR_CHECK(nvs_flash_erase());
		err = nvs_flash_init();
	}
	err = open_nvs_handle();
	if (err != CORE_OK) {
		CORE_LOGE(TAG, "Error (%s) opening NVS handle!", esp_err_to_name(err));
	}

	return err;
}

void deinit_nvs() {
	close_nvs_handle();
}

core_err erase_nvs(){
	core_err err = nvs_flash_erase();
	return err;
}

core_err read_nvsdata_by_key(char *key, void* value, size_t * val_size) {
	core_err err;

	CORE_LOGD(TAG, "Reading data from NVS ... ");

	TaskDelay(DELAY_250_MSEC / TICK_RATE_TO_MS);

	size_t size = 0;
	err = nvs_get_blob(nvs_hndl, (const char*)key, NULL, &size);

	switch (err) {
	case CORE_OK:
		CORE_LOGD(TAG, "Size = %d", size);
		break;
	case ESP_ERR_NVS_NOT_FOUND:
		CORE_LOGE(TAG, "The value is not initialized yet!");
		break;
	default:
		CORE_LOGE(TAG, "Error (%s) reading!", esp_err_to_name(err));
		break;
	}

	if (size != 0) {
		*val_size = size;
		err = nvs_get_blob(nvs_hndl, key, value, val_size);
		switch (err) {
		case CORE_OK:
			CORE_LOGD(TAG, "Done");
			break;
		case ESP_ERR_NVS_NOT_FOUND:
			CORE_LOGE(TAG, "The key %s is not initialized yet!", key);
			break;
		default:
			CORE_LOGE(TAG, "Error (%s) reading!", esp_err_to_name(err));
		}
	}
	return err;
}

core_err write_nvsdata_by_key(char *key, void *set_data, size_t len) {
	core_err err;

	err = nvs_erase_key(nvs_hndl, key);
	err = nvs_set_blob(nvs_hndl, key,(const void*) set_data, len);

	if (err == CORE_OK) {
		CORE_LOGI(TAG, "Committing %s to NVS ... ", key);

		err = nvs_commit(nvs_hndl);
		if (err != CORE_OK) {
			CORE_LOGE(TAG, "Failed! to set key: %s", key);
		} else {
			CORE_LOGI(TAG, "Successfully commited %s", key);
		}
	} else {
		CORE_LOGE(TAG, "Failed! to set key:%s", key);
	}
	return err;
}

