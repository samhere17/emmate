/*
 * persistent_mem.c
 *
 *  Created on: 11-Apr-2019
 *      Author: Rohan Dey
 */

#include <string.h>
#include <stdlib.h>
#include "core_error.h"
#include "persistent_mem.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#include "persistent_mem_errors.h"
#if CONFIG_USE_DEVCONFIG
#include "device_config.h"
#endif
#if ((CONFIG_USE_SDMMC_HOST) && (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_DATA_STORAGE == CONFIG_PERSISTENT_MEMORY_SDMMC))
#include "fatfs_helper.h"
#endif
#if CONFIG_USE_OTA
#include "fota_core.h"
#endif
#include "gpio_core.h"

#define TAG LTAG_PERSISTENT

/* Static Functions */
static core_err read_devconfig_by_key(char *key, void* buf, uint16_t buflen);
static core_err write_devconfig_by_key(char *key, void* buf, size_t buff_len);

static core_err read_devconfig_by_key(char *key, void* buf, uint16_t buflen) {
	core_err ret = CORE_FAIL;
	size_t data_len = 0;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	memset(buf, 0x00, buflen);
	if ((ret = read_nvsdata_by_key(key, buf, &data_len)) == CORE_OK) {
		// TODO:
	} else {
		CORE_LOGE(TAG, "Error: read_devconfig_by_key failed for [key] = %s!", key);
	}
#endif
	return ret;
}

static core_err write_devconfig_by_key(char *key, void* buf, size_t buff_len) {
	core_err ret = CORE_FAIL;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = write_nvsdata_by_key(key, buf, buff_len)) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Error: write_nvsdata_by_key failed for [key] = %s!", key);
	}
#endif

	return ret;
}


/*
 *
 */
core_err read_appconfig_from_persistent_mem_by_key(char *key, void* buf, size_t buflen) {
	core_err ret = CORE_FAIL;
	size_t data_len = 0;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	memset(buf, 0x00, buflen);
	if ((ret = read_nvsdata_by_key(key, buf, &data_len)) == CORE_OK) {
		// TODO:
	} else {
		CORE_LOGE(TAG, "Error: read_devconfig_by_key failed for [key] = %s!", key);
	}
#endif
	return ret;
}

/*
 *
 */
core_err write_appconfig_from_persistent_mem_by_key(char *key, void* buf, size_t buff_len) {
	core_err ret = CORE_FAIL;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = write_nvsdata_by_key(key, buf, buff_len)) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Error: write_nvsdata_by_key failed for [key] = %s!", key);
	}
#endif

	return ret;
}

/*
 * read_opermode_from_persistent_mem function reads only the operation mode from the persistent memory.
 * For the very first boot there will be no operating mode set in the memory. In this case this function will
 * set the current operating mode as CONFIG_MODE and write into the persistent memory.
 *
 * This function should be called right after systems boots so that the correct operating mode is selected.
 * */
uint8_t read_opermode_from_persistent_mem() {
	core_err ret = CORE_FAIL;

	uint8_t opermode;

	if ((ret = read_devconfig_by_key(OPERMODE_NVS_KEY, (void*) &opermode, 1)) == CORE_OK) {
		return opermode;
	}
	/* If the OPERMODE_NVS_KEY is not found then set the opermode to CONFIG_MODE and write the same to the
	 * persistent memory */
	else if (ret == CORE_ERR_NVS_NOT_FOUND) {
		CORE_LOGI(TAG, "Operation mode not set. Setting current operation mode to CONFIG_MODE ");
		opermode = SYSTEM_CONFIG_MODE_FACTORY;
		if ((ret = write_devconfig_by_key(OPERMODE_NVS_KEY, (void*) &opermode, sizeof(opermode))) == CORE_OK) {
			return opermode;
		} else {
			return CORE_FAIL;
		}
	} else {
		return CORE_FAIL;
	}

}

#if CONFIG_USE_DEVCONFIG
/*
 * read_devconfig_from_persistent_mem reads only the 1st level of device configurations from the persistent memory
 * The 1st level of device configurations will include the device id, and configs required for the
 * selected network interface. These configurations will be used to do the 1st level OTA.
 * */
core_err read_devconfig_from_persistent_mem(DeviceConfig *devcfg) {
	core_err ret = CORE_FAIL;

#if CONFIG_USE_BLE
#endif

#if CONFIG_USE_WIFI
//	if ((ret = read_devconfig_by_key(WIFI_SSID_NVS_KEY, (void*) devcfg->wifi_cfg.wifi_ssid,
//			devcfg->wifi_cfg.wifi_ssid_len)) == CORE_OK)
//		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
//
//	if ((ret = read_devconfig_by_key(WIFI_PWD_NVS_KEY, (void*) devcfg->wifi_cfg.wifi_pwd, devcfg->wifi_cfg.wifi_pwd_len))
//			== CORE_OK)
//		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);


	if ((ret = read_wifi_credentials()) == CORE_OK) {
		// TODO: Need to change the checklist functionality, now calling 2 times to support existing code
		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
	}

#endif

#if IFACE_NBIOT
#endif

#if IFACE_GSM
#endif

	if (ret == CORE_ERR_NVS_NOT_FOUND) {
		ret = CORE_ERR_NOT_FOUND;
	}
	return ret;
}

core_err write_devconfig_to_persistent_mem(DeviceConfig *devcfg) {
	core_err ret = CORE_FAIL;
#if CONFIG_USE_BLE
#endif

#if CONFIG_USE_WIFI
//	if ((ret = write_devconfig_by_key(WIFI_SSID_NVS_KEY, (void*) devcfg->wifi_cfg.wifi_ssid,
//			devcfg->wifi_cfg.wifi_ssid_len)) == CORE_OK) {
//		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
//	}
//
//	if ((ret = write_devconfig_by_key(WIFI_PWD_NVS_KEY, (void*) devcfg->wifi_cfg.wifi_pwd,
//			devcfg->wifi_cfg.wifi_pwd_len)) == CORE_OK) {
//		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
//	}

	if ((ret = save_wifi_config(&devcfg->wifi_cfg)) == CORE_OK) {
		// TODO: Need to change the checklist functionality, now calling 2 times to support existing code
		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
		SET_DEVCONFIG_CHECKLIST(devcfg->dev_conf_checklist, devcfg->checklist_mask);
	}
#endif

#if IFACE_NBIOT
#endif

#if IFACE_GSM
#endif

#if AWS_CRED
#endif

	if (ret == CORE_ERR_NVS_NOT_FOUND) {
		ret = CORE_ERR_NOT_FOUND;
	}
	return ret;
}
#endif

/*
 * write_opermode_to_persistent_mem function writes the opermode param into the persistent memory
 *
 * */
core_err write_opermode_to_persistent_mem(SYSTEM_OPERATION_MODES opermode) {
	core_err ret = CORE_FAIL;

	if ((ret = write_devconfig_by_key(OPERMODE_NVS_KEY, (void*) &opermode, sizeof(opermode))) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Error: write_opermode_to_persistent_mem failed!. %s, %d", (char*)__FILE__, __LINE__);
	}

	return ret;
}

#if CONFIG_USE_WIFI
/*
 * Read the wifi credentials data from persistent memory
 */
core_err read_wifi_credentials_from_persistent_mem(WiFiCredentials *wifi_cred) {
	core_err ret = CORE_FAIL;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = read_devconfig_by_key(WIFI_CRED_NVS_KEY, (void*) wifi_cred, sizeof(WiFiCredentials))) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Reading WiFiCredentials from persistent memory failed");
	}
#endif

	return ret;
}

/*
 * Write the wifi credentials data into persistent memory
 */
core_err write_wifi_credentials_to_persistent_mem(WiFiCredentials *wifi_cred) {
	core_err ret = CORE_FAIL;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = write_devconfig_by_key(WIFI_CRED_NVS_KEY, (void*) wifi_cred, sizeof(WiFiCredentials))) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Writing WiFiCredentials to persistent memory failed");
	}
#endif

	return ret;
}
#endif

/*
 * Read the somthing id from persistent memory
 */
core_err read_somthing_id_from_persistent_mem(char *somthing_id) {
	core_err ret = CORE_FAIL;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = read_devconfig_by_key(SOMTHING_ID_NVS_KEY, (void*) somthing_id, SOMTHING_ID_LEN)) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Reading SOM-Thing ID from persistent memory failed");
	}
#endif

	return ret;
}

/*
 * Write the somthing id into persistent memory
 */
core_err write_somthing_id_to_persistent_mem(char *somthing_id) {
	core_err ret = CORE_FAIL;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = write_devconfig_by_key(SOMTHING_ID_NVS_KEY, (void*) somthing_id, SOMTHING_ID_LEN)) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Writing SOM-Thing ID to persistent memory failed");
	}
#endif

	return ret;
}

#if CONFIG_USE_OTA
/*
 * Read the fota id from persistent memory
 */
core_err read_fotaid_from_persistent_mem(char *fota_id) {
	core_err ret = CORE_FAIL;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = read_devconfig_by_key(FOTA_ID_NVS_KEY, (void*) fota_id, FOTA_ID_LEN)) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Reading FOTA ID from persistent memory failed");
	}
#endif

	return ret;
}

/*
 * Write the fota id into persistent memory
 */
core_err write_fotaid_to_persistent_mem(char *fota_id) {
	core_err ret = CORE_FAIL;

#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	if ((ret = write_devconfig_by_key(FOTA_ID_NVS_KEY, (void*) fota_id, FOTA_ID_LEN)) == CORE_OK) {

	} else {
		CORE_LOGE(TAG, "Writing FOTA ID to persistent memory failed");
	}
#endif

	return ret;
}
#endif

core_err init_persistent_mem_for_config() {
	core_err ret = CORE_FAIL;

	int16_t config_mem_type = CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE;

	if (config_mem_type == CONFIG_PERSISTENT_MEMORY_NVS) {
		/* Initialize NVS */
		ret = init_nvs();
	} else if (config_mem_type == CONFIG_PERSISTENT_MEMORY_SDMMC) {
		// Initialize SD
	}

	return ret;
}

core_err reset_persistent_mem_for_reconfig() {
	core_err ret = CORE_FAIL;
#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	ret = write_opermode_to_persistent_mem(SYSTEM_CONFIG_MODE_FACTORY);
#endif
	return ret;
}

core_err erase_configs_from_persistent_mem() {
	core_err ret = CORE_FAIL;
#if (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_CONFIG_STORAGE == CONFIG_PERSISTENT_MEMORY_NVS)
	ret = erase_nvs();
#endif
	return ret;
}

core_err init_persistent_mem_for_data_storage() {
	core_err ret = CORE_FAIL;

#if ((CONFIG_USE_SDMMC_HOST) && (CONFIG_PERSISTENT_MEMORY_TYPE_FOR_DATA_STORAGE == CONFIG_PERSISTENT_MEMORY_SDMMC))

#if CONFIG_PLATFORM_ESP_IDF	/* For ESP, SPI mode is used to save pins */

#else
	gpio_io_set_pull_mode(SDMMC_CMD, GPIO_IO_PULLUP_ONLY);
	gpio_io_set_pull_mode(SDMMC_D0, GPIO_IO_PULLUP_ONLY);
	gpio_io_set_pull_mode(SDMMC_D1, GPIO_IO_PULLUP_ONLY);
	gpio_io_set_pull_mode(SDMMC_D2, GPIO_IO_PULLUP_ONLY);
	gpio_io_set_pull_mode(SDMMC_D3, GPIO_IO_PULLUP_ONLY);
#endif
	// TODO: Card Detect pin has to be checked before calling this function and relevant error code has to be returned.
	ret = init_and_mount_fatfs_on_sdmmc(CONFIG_SDMMC_PARTITION_NAME, false, SDMMC_MAX_NUM_FILES_TO_OPEN);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Failed to Initialize/Mount FAT File System on SD/MMC Card!");
		ret = CORE_FAIL;
	} else {
		CORE_LOGI(TAG, "Initialize/Mount FAT File System on SD Card succeeded");
	}
#endif

	return ret;
}
