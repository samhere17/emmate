/*
 * sdmmc_host_core.h
 *
 *  Created on: 03-Jul-2019
 *      Author: Rohan Dey
 */

#ifndef SDMMC_HOST_CORE_H_
#define SDMMC_HOST_CORE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "sdmmc_host_platform.h"

#define SDMMC_CORE_FREQ_DEFAULT		SDMMC_PF_FREQ_DEFAULT		/*!< SD/MMC Default speed (limited by clock divider) */
#define SDMMC_CORE_FREQ_HIGHSPEED	SDMMC_PF_FREQ_HIGHSPEED		/*!< SD High speed (limited by clock divider) */
#define SDMMC_CORE_FREQ_PROBING		SDMMC_PF_FREQ_PROBING		/*!< SD/MMC probing speed */
#define SDMMC_CORE_FREQ_52M			SDMMC_PF_FREQ_52M			/*!< MMC 52MHz speed */
#define SDMMC_CORE_FREQ_26M			SDMMC_PF_FREQ_26M			/*!< MMC 26MHz speed */

/**
 * @brief Default sdmmc_host_core_t structure initializer for SPI peripheral
 *
 */
#define SDSPI_HOST_CORE_DEFAULT()			SDSPI_HOST_PF_DEFAULT()

/**
 * @brief Macro defining default sdmmc_core_slot_config_t configuration of SPI host slot
 */
#define SDSPI_CORE_SLOT_CONFIG_DEFAULT()	SDSPI_PF_SLOT_CONFIG_DEFAULT()

/**
 * @brief Default sdmmc_host_core_t structure initializer for SDMMC peripheral
 *
 * Uses SDMMC peripheral, with 4-bit mode enabled, and max frequency set to 20MHz
 */
#define SDMMC_HOST_CORE_DEFAULT()			SDMMC_HOST_PF_DEFAULT()

/**
 * @brief Macro defining default sdmmc_core_slot_config_t configuration of SDMMC host slot
 */
#define SDMMC_CORE_SLOT_CONFIG_DEFAULT()	SDMMC_PF_SLOT_CONFIG_DEFAULT()

/**
 * SD/MMC Host description
 *
 * This structure defines properties of SD/MMC host and functions
 * of SD/MMC host which can be used by upper layers.
 */
typedef sdmmc_host_pf_t	sdmmc_host_core_t;

/**
 * SD/MMC card information structure
 */
typedef sdmmc_pf_card_t	sdmmc_core_card_t;

/**
 * Extra configuration for SDMMC peripheral slot
 */
typedef sdmmc_pf_slot_config_t	sdmmc_core_slot_config_t;

/**
 * Extra configuration for SPI host
 */
typedef sdspi_pf_slot_config_t	sdspi_core_slot_config_t;


#ifdef __cplusplus
}
#endif

#endif /* SDMMC_HOST_CORE_H_ */
