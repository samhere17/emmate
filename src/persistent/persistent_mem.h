/*
 * persistent_mem.h
 *
 *  Created on: 11-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef PERSISTENT_MEM_H_
#define PERSISTENT_MEM_H_

#include "core_config.h"
#if CONFIG_USE_NVS
#include "nvs_helper.h"
#endif
#if CONFIG_USE_SDMMC_HOST
#include "sdmmc_host_core.h"
#endif

#include "system.h"

/**
 * @brief Maximum number of files that can be opened using FATFS in a SD/MMC card
 * */
#define SDMMC_MAX_NUM_FILES_TO_OPEN		CONFIG_MAX_NUM_FILES_TO_OPEN

/**
 * @brief 	Name of the partition to be created when the FATFS is mounted on a SD/MMC card
 * 			This partition will be used for the entire system. All files created will fall under
 * 			this partition
 * */
//#define CONFIG_SDMMC_PARTITION_NAME	"/emmate"

/**
 * @brief read_opermode_from_persistent_mem function reads only the operation mode from the persistent memory.
 * For the very first boot there will be no operating mode set in the memory. In this case this function will
 * set the current operating mode as CONFIG_MODE and write into the persistent memory.
 *
 * This function should be called right after systems boots so that the correct operating mode is selected.
 * */
/**
 * @brief read_opermode_from_persistent_mem function reads only the operation mode from the persistent memory.
 * For the very first boot there will be no operating mode set in the memory. In this case this function will
 * set the current operating mode as CONFIG_MODE and write into the persistent memory.
 *
 * @return
 * 		- SYSTEM_OPERATION_MODES if value was get successfully
 *      - CORE_ERR_NVS_INVALID_HANDLE if handle has been closed or is NULL
 *      - CORE_ERR_NVS_READ_ONLY if storage handle was opened as read only
 *      - CORE_ERR_NVS_INVALID_NAME if key name doesn’t satisfy constraints
 *      - CORE_ERR_NVS_NOT_ENOUGH_SPACE if there is not enough space in the underlying storage to save the value
 *      - CORE_ERR_NVS_REMOVE_FAILED if the value wasn’t updated because flash write operation has failed. The value was written however, and update will be finished after re-initialization of nvs, provided that flash operation doesn’t fail again.
 *      - CORE_ERR_NVS_VALUE_TOO_LONG if the string value is too long
 *
 * */
uint8_t read_opermode_from_persistent_mem();

#if CONFIG_USE_DEVCONFIG
/**
 * @brief read_devconfig_from_persistent_mem reads only the 1st level of device configurations from the persistent memory
 * The 1st level of device configurations will include the device id, and configs required for the
 * selected network interface. These configurations will be used to do the 1st level OTA.
 *
 * @param[out]	devcfg	DeviceConfig data to get from persistent memory
 *
 * @return
 *		- CORE_OK if value was get successfully
 *      - CORE_ERR_NVS_INVALID_HANDLE if handle has been closed or is NULL
 *      - CORE_ERR_NVS_READ_ONLY if storage handle was opened as read only
 *      - CORE_ERR_NVS_INVALID_NAME if key name doesn’t satisfy constraints
 *      - CORE_ERR_NVS_NOT_ENOUGH_SPACE if there is not enough space in the underlying storage to save the value
 *      - CORE_ERR_NVS_REMOVE_FAILED if the value wasn’t updated because flash write operation has failed. The value was written however, and update will be finished after re-initialization of nvs, provided that flash operation doesn’t fail again.
 *      - CORE_ERR_NVS_VALUE_TOO_LONG if the string value is too long
 *
 * */
core_err read_devconfig_from_persistent_mem(DeviceConfig *devcfg);

/**
 * @brief write_devconfig_to_persistent_mem writes only the 1st level of device configurations from the persistent memory
 *
 * @param[in]	devcfg	DeviceConfig data to set into persistent memory
 *
 * @return
 *		- CORE_OK if value was set successfully
 *      - CORE_ERR_NVS_INVALID_HANDLE if handle has been closed or is NULL
 *      - CORE_ERR_NVS_READ_ONLY if storage handle was opened as read only
 *      - CORE_ERR_NVS_INVALID_NAME if key name doesn’t satisfy constraints
 *      - CORE_ERR_NVS_NOT_ENOUGH_SPACE if there is not enough space in the underlying storage to save the value
 *      - CORE_ERR_NVS_REMOVE_FAILED if the value wasn’t updated because flash write operation has failed. The value was written however, and update will be finished after re-initialization of nvs, provided that flash operation doesn’t fail again.
 *      - CORE_ERR_NVS_VALUE_TOO_LONG if the string value is too long
 *
 * */
core_err write_devconfig_to_persistent_mem(DeviceConfig *devcfg);
#endif
/**
 * @brief write app-configuration to persistent NVS memory
 *
 * @param[in]	key		NVS key for set data into persistent memory
 *
 * @param[in]	buf		Data buffer
 *
 * @param[in]	buflen	Data buffer length
 *
 * @return
 *		- CORE_OK 	if value was set successfully
 *		- CORE_FAIL if value was not set successfully
 *		- CORE_ERR_NVS_INVALID_HANDLE if handle has been closed or is NULL
 *		- CORE_ERR_NVS_READ_ONLY if storage handle was opened as read only
 *		- CORE_ERR_NVS_INVALID_NAME if key name doesn’t satisfy constraints
 *		- CORE_ERR_NVS_NOT_ENOUGH_SPACE if there is not enough space in the underlying storage to save the value
 *      - CORE_ERR_NVS_REMOVE_FAILED if the value wasn’t updated because flash write operation has failed. The value was written however, and update will be finished after re-initialization of nvs, provided that flash operation doesn’t fail again.
 *      - CORE_ERR_NVS_VALUE_TOO_LONG if the string value is too long
 *
 */
core_err read_appconfig_from_persistent_mem_by_key(char *key, void* buf, size_t buflen);

/**
 * @brief read app-configuration from persistent NVS memory
 *
 * @param[in]	key		NVS key for get data from persistent memory
 *
 * @param[out]	buf		Data buffer
 *
 * @param[in]	buflen	Data buffer length
 *
 * @return
 *		- CORE_OK 	if value was set successfully
 *		- CORE_FAIL if value was not set successfully
 *		- CORE_ERR_NVS_INVALID_HANDLE if handle has been closed or is NULL
 *		- CORE_ERR_NVS_READ_ONLY if storage handle was opened as read only
 *		- CORE_ERR_NVS_INVALID_NAME if key name doesn’t satisfy constraints
 *		- CORE_ERR_NVS_NOT_ENOUGH_SPACE if there is not enough space in the underlying storage to save the value
 *      - CORE_ERR_NVS_REMOVE_FAILED if the value wasn’t updated because flash write operation has failed. The value was written however, and update will be finished after re-initialization of nvs, provided that flash operation doesn’t fail again.
 *      - CORE_ERR_NVS_VALUE_TOO_LONG if the string value is too long
 *
 */
core_err write_appconfig_from_persistent_mem_by_key(char *key, void* buf, size_t buff_len);

/**
 * @brief write_opermode_to_persistent_mem function writes the SYSTEM_OPERATION_MODES into the persistent memory
 *
 * @param[in]	opermode	SYSTEM_OPERATION_MODES value to set into persistent memory
 *
 * @return
 *		- CORE_OK if value was set successfully
 *      - CORE_ERR_NVS_INVALID_HANDLE if handle has been closed or is NULL
 *      - CORE_ERR_NVS_READ_ONLY if storage handle was opened as read only
 *      - CORE_ERR_NVS_INVALID_NAME if key name doesn’t satisfy constraints
 *      - CORE_ERR_NVS_NOT_ENOUGH_SPACE if there is not enough space in the underlying storage to save the value
 *      - CORE_ERR_NVS_REMOVE_FAILED if the value wasn’t updated because flash write operation has failed. The value was written however, and update will be finished after re-initialization of nvs, provided that flash operation doesn’t fail again.
 *      - CORE_ERR_NVS_VALUE_TOO_LONG if the string value is too long
 *
 * */
core_err write_opermode_to_persistent_mem(SYSTEM_OPERATION_MODES opermode);

#if CONFIG_USE_WIFI
/**
 * @brief This function reads the Wi-Fi Credentials from the selected persistent memory and returns the same via out parameter
 *
 * @param[out]	wifi_cred	Pointer to WiFiCredentials to be populated and returned as an out parameter
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err read_wifi_credentials_from_persistent_mem(WiFiCredentials *wifi_cred);

/**
 * @brief This function writes the Wi-Fi Credentials to the selected persistent memory
 *
 * @param[in]	wifi_cred	Pointer to WiFiCredentials to be saved in the memory
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err write_wifi_credentials_to_persistent_mem(WiFiCredentials *wifi_cred);
#endif

/**
 * @brief This function reads the SOM Thing ID from the selected persistent memory and returns the same via out parameter
 *
 * @param[out]	somthing_id	Pointer to char to be populated and returned as an out parameter. Length of somthing_id is defined by SOMTHING_ID_LEN
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err read_somthing_id_from_persistent_mem(char *somthing_id);

/**
 * @brief This function writes the SOM Thing ID to the selected persistent memory
 *
 * @param[in]	somthing_id	Pointer to char to be saved in the memory. Length of somthing_id is defined by SOMTHING_ID_LEN
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err write_somthing_id_to_persistent_mem(char *somthing_id);

#if CONFIG_USE_OTA
/**
 * @brief This function reads the FOTA ID from the selected persistent memory and returns the same via out parameter
 *
 * @param[out]	fota_id	Pointer to char to be populated and returned as an out parameter.
 * 						Length of somthing_id is defined by FOTA_ID_LEN
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err read_fotaid_from_persistent_mem(char *fota_id);

/**
 * @brief This function writes the FOTA ID to the selected persistent memory
 *
 * @param[in]	fota_id	Pointer to char to be saved in the memory. Length of somthing_id is defined by FOTA_ID_LEN
 *
 * @return
 *		- CORE_OK 		if successful
 *		- CORE_FAIL		on failure
 *
 * */
core_err write_fotaid_to_persistent_mem(char *fota_id);
#endif

/**
 * @brief initialize the all persistent memory modules
 *
 * @return
 * 		- CORE_OK		On success
 * 		- CORE_FAIL		On failure
 *
 * */
core_err init_persistent_mem_for_config();

/**
 *	@brief 	Erases the current System Operation Mode and sets it to SYSTEM_CONFIG_MODE_FACTORY so that Device Configuration
 *			can be done again
 *	@return
 * 		- CORE_OK		On success
 * 		- CORE_FAIL		On failure
 */
core_err reset_persistent_mem_for_reconfig();

/**
 *	@brief Erases all configurations from the persistent memory
 *	@return
 * 		- CORE_OK		On success
 * 		- CORE_FAIL		On failure
 */
core_err erase_configs_from_persistent_mem();

/**
 *	@brief Initializes the selected storage to be used for storing data in the application
 *	@return
 * 		- CORE_OK		On success
 * 		- CORE_FAIL		On failure
 */
core_err init_persistent_mem_for_data_storage();

#endif /* PERSISTENT_MEM_H_ */
