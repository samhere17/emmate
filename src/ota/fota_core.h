/*
 * File Name: ota_core.h
 * File Path: /emmate/src/ota/ota_core.h
 * Description:
 *
 *  Created on: 25-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef FOTA_CORE_H_
#define FOTA_CORE_H_

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "systime.h"

#if CONFIG_PLATFORM_ESP_IDF
#include <time.h>
#include <sys/time.h>
#endif

#include "ota_platform.h"

#define FOTA_ID_LEN				20
#define MAX_FOTA_REQUEST_COUNT	10

/** OTA status to be sent to server */
typedef enum {
	FOTA_STATUS_NEW = 1,
	FOTA_STATUS_AVAILABLE = 2,
	FOTA_STATUS_NEEDED = 3,
	FOTA_STATUS_DOWNLOADING = 30,
	FOTA_STATUS_DOWNLOADED = 31,
	FOTA_STATUS_DOWNLOAD_FAILED = 32,
	FOTA_STATUS_UPDATING = 20,
	FOTA_STATUS_UPDATED = 21,
	FOTA_STATUS_UPDATE_FAILED = 22,
	FOTA_STATUS_UNKNOWN = 0,
} FOTA_STATUSES;

/** Wrapper typedefs for abstracting the platform specific names */

#define OTA_ERR_HTTPS_OTA_IN_PROGRESS	OTA_PF_ERR_HTTPS_OTA_IN_PROGRESS
#define OTA_ERR_OTA_VALIDATE_FAILED		OTA_PF_ERR_OTA_VALIDATE_FAILED
#define OTA_ERR_FLASH_OP_TIMEOUT		OTA_PF_ERR_FLASH_OP_TIMEOUT
#define OTA_ERR_FLASH_OP_FAIL			OTA_PF_ERR_FLASH_OP_FAIL

//typedef ota_pf_img_states_t	ota_img_states_t;
typedef ota_pf_partition_t ota_partition_t;

typedef ota_pf_https_ota_handle_t	ota_https_ota_handle_t;

typedef ota_pf_https_ota_config_t	ota_https_ota_config_t;

/** Wrapper typedefs for abstracting the platform specific names */
/// OTA_DATA states for checking operability of the app.
typedef enum {
	OTA_IMG_NEW = OTA_PF_IMG_NEW, /*!< Monitor the first boot. In bootloader this state is changed to ESP_OTA_IMG_PENDING_VERIFY. */
	OTA_IMG_PENDING_VERIFY = OTA_PF_IMG_PENDING_VERIFY, /*!< First boot for this app was. If while the second boot this state is then it will be changed to ABORTED. */
	OTA_IMG_VALID = OTA_PF_IMG_VALID, /*!< App was confirmed as workable. App can boot and work without limits. */
	OTA_IMG_INVALID = OTA_PF_IMG_INVALID, /*!< App was confirmed as non-workable. This app will not selected to boot at all. */
	OTA_IMG_ABORTED = OTA_PF_IMG_ABORTED, /*!< App could not confirm the workable or non-workable. In bootloader IMG_PENDING_VERIFY state will be changed to IMG_ABORTED. This app will not selected to boot at all. */
	OTA_IMG_UNDEFINED = OTA_PF_IMG_UNDEFINED, /*!< Undefined. App can boot and work without limits. */
} ota_img_states_t;

/* Wrapper typedefs for abstracting the platform specific names */
/**
 * @brief Get partition info of currently running app
 *
 * @return
 * 		Pointer to info for partition structure, or NULL if no partition is found or flash read operation failed.
 * 		Returned pointer is valid for the lifetime of the application.
 */
#define ota_get_running_partition()						ota_pf_get_running_partition()
/**
 * @brief Returns state for given partition
 *
 * @param[in]	partition	Pointer to partition
 * @param[in]	ota_state	state of partition (if this partition has a record in otadata
 *
 * @return
 * 		CORE_OK: Successful.
 * 		CORE_ERR_INVALID_ARG: partition or ota_state arguments were NULL.
 * 		CORE_ERR_NOT_SUPPORTED: partition is not ota.
 * 		CORE_ERR_NOT_FOUND: Partition table does not have otadata or state was not found for given partition.
 */
#define ota_get_state_partition(partition, ota_state)	ota_pf_get_state_partition(partition, ota_state)
/**
 * @brief This function is called to indicate that the running app is working well.
 *
 * @return
 * 		CORE_OK: If successful.
 */
#define ota_mark_app_valid_cancel_rollback()			ota_pf_mark_app_valid_cancel_rollback()
/**
 * @brief 	This function is called to roll back to the previously workable app with reboot.
 * 			If rollback is successful then device will reset else API will return with error code.
 * 			Checks applications on a flash drive that can be booted in case of rollback.
 * 			If the flash does not have at least one app (except the running app) then rollback is not possible.
 *
 * @return
 * 		CORE_FAIL: if not successful.
 * 		ESP_ERR_OTA_ROLLBACK_FAILED: The rollback is not possible due to flash does not have any apps.
 */
#define ota_mark_app_invalid_rollback_and_reboot()		ota_pf_mark_app_invalid_rollback_and_reboot()

/**
 * @brief 	HTTPS OTA Firmware upgrade.
 * 			Note
 * 			----
 * 			For secure HTTPS updates, the cert_pem member of config structure must be set to the server certificate.
 *
 * @param[in]	config	 pointer to HttpClientConfig structure
 *
 * @return
 * 		CORE_OK: OTA data updated, next reboot will use specified partition.
 * 		CORE_FAIL: For generic failure.
 * 		CORE_ERR_INVALID_ARG: Invalid argument
 * 		ESP_ERR_OTA_VALIDATE_FAILED: Invalid app image
 * 		CORE_ERR_NO_MEM: Cannot allocate memory for OTA operation.
 * 		ESP_ERR_FLASH_OP_TIMEOUT or ESP_ERR_FLASH_OP_FAIL: Flash write failed.
*/
#define ota_perform_https_ota(config)	 				ota_pf_https_ota(config)

/**
 * @brief    Start HTTPS OTA Firmware upgrade
 *
 * This function initializes HTTPS OTA context and establishes HTTPS connection.
 * This function must be invoked first. If this function returns successfully, then `ota_https_ota_perform` should be
 * called to continue with the OTA process and there should be a call to `ota_https_ota_finish` on
 * completion of OTA operation or on failure in subsequent operations.
 * This API supports URL redirection, but if CA cert of URLs differ then it
 * should be appended to `cert_pem` member of `http_config`, which is a part of `ota_config`.
 *
 * @param[in]   ota_config       pointer to HttpClientConfig structure
 * @param[out]  handle           pointer to an allocated data of type `ota_https_ota_handle_t`
 *                               which will be initialised in this function
 *
 * @note     This API is blocking, so setting `is_async` member of `http_config` structure will
 *           result in an error.
 *
 * @return
 *    - CORE_OK: HTTPS OTA Firmware upgrade context initialised and HTTPS connection established
 *    - CORE_FAIL: For generic failure.
 *    - CORE_ERR_INVALID_ARG: Invalid argument (missing/incorrect config, certificate, etc.)
 */
#define ota_https_ota_begin(ota_config, handle)				ota_pf_https_ota_begin(ota_config, handle)

/**
 * @brief    Read image data from HTTP stream and write it to OTA partition
 *
 * This function reads image data from HTTP stream and writes it to OTA partition. This function
 * must be called only if ota_https_ota_begin() returns successfully.
 * This function must be called in a loop since it returns after every HTTP read operation thus
 * giving you the flexibility to stop OTA operation midway.
 *
 * @param[in]  https_ota_handle  pointer to ota_https_ota_handle_t structure
 *
 * @return
 *    - OTA_ERR_HTTPS_OTA_IN_PROGRESS: OTA update is in progress, call this API again to continue.
 *    - CORE_OK: OTA update was successful
 *    - CORE_FAIL: OTA update failed
 *    - CORE_ERR_INVALID_ARG: Invalid argument
 *    - OTA_ERR_OTA_VALIDATE_FAILED: Invalid app image
 *    - CORE_ERR_NO_MEM: Cannot allocate memory for OTA operation.
 *    - OTA_ERR_FLASH_OP_TIMEOUT or OTA_ERR_FLASH_OP_FAIL: Flash write failed.
 */
#define ota_https_ota_perform(https_ota_handle)				ota_pf_https_ota_perform(https_ota_handle)

/**
 * @brief    Clean-up HTTPS OTA Firmware upgrade and close HTTPS connection
 *
 * This function closes the HTTP connection and frees the HTTPS OTA context.
 * This function switches the boot partition to the OTA partition containing the
 * new firmware image.
 *
 * @note     If this API returns successfully, core_system_restart() must be called to
 *           boot from the new firmware image
 *
 * @param[in]  https_ota_handle   pointer to ota_https_ota_handle_t structure
 *
 * @return
 *    - CORE_OK: Clean-up successful
 *    - CORE_ERR_INVALID_STATE
 *    - CORE_ERR_INVALID_ARG: Invalid argument
 *    - OTA_ERR_OTA_VALIDATE_FAILED: Invalid app image
 */
#define ota_https_ota_finish(https_ota_handle)				ota_pf_https_ota_finish(https_ota_handle)

/*
* @brief  This function returns OTA image data read so far.
*
* @note   This API should be called only if `ota_https_ota_perform()` has been called atleast once
*
* @param[in]   https_ota_handle   pointer to ota_https_ota_handle_t structure
*
* @return
*    - -1    On failure
*    - total bytes read so far
*/
#define ota_https_ota_get_image_len_read(https_ota_handle)	ota_pf_https_ota_get_image_len_read(https_ota_handle)

/*
 * Sample OTA JSON
 * ---------------
 * {
 * 		"stat":true,
 * 		"len":100,	or "link_len":100,
 * 		"link" : "https://dev.iquesters.com/emb_api/test_apis/upload/firmware.hex",
 * 		"cert_len":2048,					//add if certificate's length needed
 * 		"cert":"ndfjhdfhflhfsdfh.....",		//add if certificate needed
 * 		"ver" : "v0.1",
 * 		"fname" : "firmware.hex",
 * 		"fsize" : 100000,
 * 		"sch" : "1556797762"
 * 	}
 */

/* OTA JSON keys */
//#define OTA_KEY_STATUS				"stat"
//#define OTA_KEY_PERMALINK_LEN		"len"
//#define OTA_KEY_PERMALINK			"link"
//#define OTA_KEY_VER					"ver"
//#define OTA_KEY_FILENAME			"fname"
//#define OTA_KEY_FILESIZE			"fsize"
//#define OTA_KEY_TSTAMP				"sch"

/*
 * @brief	Data structure to provide all FOTA related info
 *
 * @note	FOTA Info response JSON structure:
 *
 *	{
 *		"id": "",						// Fota ID: all fota status activities to be made using this id
 *		"url_len": 63,
 *		"url": "https://dev.iqueasters.com/emb_api/test_pis/upload/firmware.hex",
 *		"cert_len": 0,
 *		"cert": null,
 *		"ver": "0.0.0.1",
 *		"fname": "firmware.hex",
 *		"fsize": 18410,
 *		"sch": "1559735400"
 *	}
 *
 * */
typedef struct {
	char id[FOTA_ID_LEN+1];					/**< FOTA id for communicating with server */
//	int stat;								/**< FOTA Status. true = new firmware available, false = do nothing */
	double url_len;							/**< Length of FOTA's URL */
	char *url;								/**< Length of the permalink for getting the new firmware */
	double cert_len;						/**< Length of the certificate to follow */
	char *cert;								/**< The HTTPS certificate with which the OTA has to be fetched */
	char ver[CORE_VERSION_NUMBER_LEN+1];	/**< The new firmware's version. To be provided by remote host */
	char fname[CONFIG_FIRMWARE_FILENAME_MAX_LEN];	/**< The new firmware's file name. To be provided by remote host */
	double fsize;							/**< The new firmware's size. To be provided by remote host */
#if CONFIG_PLATFORM_ESP_IDF
	struct tm sch;							/**< Scheduled time when the OTA needs to take place */
#endif
} FotaInfo;

/**
 * @brief 	This function queries the server to get OTA info and returns a bool value through the out param
 *
 * @param[out]	is_ota_reqd	 Whether or not the OTA Update is required
 *
 * @return
 * 		CORE_OK
 * 		CORE_FAIL
 *
*/
core_err get_ota_info(int *is_ota_reqd);

/**
 * @brief 	This function starts the OTA process by starting a dedicated OTA Update thread
 *
 * @return
 * 		CORE_OK
 * 		CORE_FAIL
 * 		CORE_ERR_NO_MEM
*/
core_err start_fota_task(FotaInfo *recv_fota_info);

#endif /* FOTA_CORE_H_ */
