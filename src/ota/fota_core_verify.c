/*
 * fota_core_verify.c
 *
 *  Created on: 09-Aug-2019
 *      Author: Rohan Dey
 */
#include "fota_core_verify.h"
#include "fota_core_helper.h"
#if CONFIG_USE_PERSISTENT
#include "persistent_mem.h"
#endif
#include "system_utils.h"
#include "event_group_core.h"
#include "conn.h"

#define TAG LTAG_OTA

/*
 * Get the Current Running Image's state.
 *
 * Note For ESP32:
 * 		Enable OTA Rollbackfrom make menuconfig->Bootloader Config->Enable app rollback support
 */
ota_img_states_t get_running_fota_img_state() {

	ota_img_states_t state = OTA_IMG_INVALID;
	const ota_partition_t *current_partition = NULL;

	current_partition = ota_get_running_partition();

	// Display the running partition
	CORE_LOGI(TAG, "Running partition: %s", current_partition->label);

	core_err ret = ota_get_state_partition(current_partition, &state);
	if (ret == CORE_OK) {
		CORE_LOGI(TAG, "Current ota state 0x%x", state);
		if (OTA_IMG_PENDING_VERIFY == state || OTA_IMG_VALID == state) {
			return state;
		}
	} else if (CORE_ERR_INVALID_ARG) {
		// TODO: error handle
	} else if (CORE_ERR_NOT_SUPPORTED) {
		// TODO: error handle
	} else if (CORE_ERR_NOT_FOUND) {
		// TODO: error handle
	}

	/* If return value of ota_get_state_partition() is not CORE_OK, then handle the error. */
	//state = OTA_IMG_INVALID;
	return state;
}

static core_err img_verify() {
	core_err ret = CORE_FAIL;

	/*
	 * TODO: Verify the Downloaded & Running Image
	 *
	 */
	CORE_LOGW(TAG, "Image Verify successful\r\n");
	ret = CORE_OK;

	return ret;
}

/**
 * @brief OTA Image Verification Process
 *
 * This function user for verify the Current Running Image
 * to find out the Fault in it.
 * If Any error occur, Then the system will rollback to
 * last running & valid Image or else go on with the new Image..
 */
void verify_running_firmware() {
	CORE_LOGI(TAG, "Verifying the newly started firmware image");

	ota_img_states_t state = get_running_fota_img_state();
	if (state == OTA_IMG_PENDING_VERIFY) {
		CORE_LOGI(TAG, "Firmware image is pending to be verified");

		char fota_id[FOTA_ID_LEN+1];
		core_err ret = read_fotaid_from_persistent_mem(fota_id);
		if (ret == CORE_FAIL) {
			// TODO: Handle error
			CORE_LOGE(TAG, "read_fotaid_from_persistent_mem failed. Don't know what to do yet!");
			return;
		}
		/* Wait until we have a network IP */
		event_group_wait_bits(get_system_evtgrp_hdl(), CONN_GOT_IP_BIT, false, true, EventMaxDelay);

		TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
		while (get_network_conn_status() != NETCONNSTAT_CONNECTED) {}

		/* execute OTA Image Verification for validate that Its Ours release Binary Image. */
		ret = img_verify();
		if (CORE_OK == ret) {
			CORE_LOGI(TAG, "The image is verified and OK to run, informing the server");
			ota_mark_app_valid_cancel_rollback();

			/* Send FOTA_STATUS_UPDATED status to server */
			_send_fota_status_to_server(fota_id, FOTA_STATUS_UPDATED, 0);

		} else {
			CORE_LOGE(TAG, "The image could not be verified! Inform the server and rollback to previous version");
			ota_mark_app_invalid_rollback_and_reboot();

			/* Send FOTA_STATUS_UPDATE_FAILED status to server */
			_send_fota_status_to_server(fota_id, FOTA_STATUS_UPDATE_FAILED, 0);
		}
	} else if (state == OTA_IMG_VALID) {
		CORE_LOGI(TAG, "The running firmware is already verified, no need to verify it again...");
		// Image is Valid, So no need to execute Self-test & Image Verification. Good to Continue
	}
}
