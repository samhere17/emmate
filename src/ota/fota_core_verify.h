/*
 * fota_core_verify.h
 *
 *  Created on: 09-Aug-2019
 *      Author: Rohan Dey
 */

#ifndef SRC_OTA_FOTA_CORE_VERIFY_H_
#define SRC_OTA_FOTA_CORE_VERIFY_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"
#include "fota_core.h"

/**
 * @brief 	Get the Current Running Image's state
 *
 *
 * @return
 * 		ota_img_states_t	See the enum documentation for details
*/
ota_img_states_t get_running_fota_img_state();

/**
 * @brief OTA Image Verification Process
 *
 * This function is used for verifying the currently running image.
 * If any error occurs, then the system will rollback to the last running & valid image, else this image will be continued
 */
void verify_running_firmware();


#ifdef __cplusplus
}
#endif

#endif /* SRC_OTA_FOTA_CORE_VERIFY_H_ */
