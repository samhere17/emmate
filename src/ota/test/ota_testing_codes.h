/*
 * File Name: testing_codes.h
 * File Path: /emmate/src/testing-codes/testing_codes.h
 * Description:
 *
 *  Created on: 09-May-2019
 *      Author: Noyel Seth
 */

#ifndef SRC_TESTING_CODES_TESTING_CODES_H_
#define SRC_TESTING_CODES_TESTING_CODES_H_

#include "core_config.h"

#if TESTING

#include "http_client_core.h"


void config_ota_tesing_http_cli_data(HttpClientData *cnfdata);

#endif

#endif /* SRC_TESTING_CODES_TESTING_CODES_H_ */
