/*
 * core_constant.h
 *
 *  Created on: 13-Apr-2019
 *      Author: iqubuntu02
 */

#ifndef CORE_CONSTANT_H_
#define CORE_CONSTANT_H_

#ifdef __cplusplus
extern "C" {
#endif

/***************** Macros of Miliseconds **************************/
#define DELAY_5_MSEC	5
#define DELAY_10_MSEC	10
#define DELAY_20_MSEC	20
#define DELAY_100_MSEC	100
#define DELAY_250_MSEC	250
#define DELAY_500_MSEC	500
#define DELAY_600_MSEC	600

/***************** Macros of Seconds **************************/
#define DELAY_1_SEC		1000
#define DELAY_2_SEC		2000
#define DELAY_3_SEC		3000
#define DELAY_4_SEC		4000
#define DELAY_5_SEC		5000
#define DELAY_6_SEC		6000
#define DELAY_7_SEC		7000
#define DELAY_8_SEC		8000
#define DELAY_9_SEC		9000
#define DELAY_10_SEC	10000
#define DELAY_20_SEC	20000
#define DELAY_30_SEC	30000
#define DELAY_40_SEC	40000
#define DELAY_50_SEC	50000

/***************** Macros of Minutes **************************/
#define DELAY_1_MIN		DELAY_1_SEC * 60
#define DELAY_2_MIN		DELAY_1_MIN * 2
#define DELAY_3_MIN		DELAY_1_MIN * 3
#define DELAY_4_MIN		DELAY_1_MIN * 4
#define DELAY_5_MIN		DELAY_1_MIN * 5
#define DELAY_6_MIN		DELAY_1_MIN * 6
#define DELAY_7_MIN		DELAY_1_MIN * 7
#define DELAY_8_MIN		DELAY_1_MIN * 8
#define DELAY_9_MIN		DELAY_1_MIN * 9
#define DELAY_10_MIN	DELAY_1_MIN * 10
#define DELAY_15_MIN	DELAY_1_MIN * 15
#define DELAY_30_MIN	DELAY_1_MIN * 30
#define DELAY_45_MIN	DELAY_1_MIN * 45

/***************** Macros of Hours **************************/
#define DELAY_1_HOUR	DELAY_1_MIN * 60
#define DELAY_2_HOUR	DELAY_1_HOUR * 2
#define DELAY_3_HOUR	DELAY_1_HOUR * 3
#define DELAY_4_HOUR	DELAY_1_HOUR * 4
#define DELAY_5_HOUR	DELAY_1_HOUR * 5
#define DELAY_6_HOUR	DELAY_1_HOUR * 6
#define DELAY_7_HOUR	DELAY_1_HOUR * 7
#define DELAY_8_HOUR	DELAY_1_HOUR * 8
#define DELAY_9_HOUR	DELAY_1_HOUR * 9
#define DELAY_10_HOUR	DELAY_1_HOUR * 10

/***************** Threads Priority's Macros **************************/
#define THREAD_PRIORITY_1	1
#define THREAD_PRIORITY_2	2
#define THREAD_PRIORITY_3	3
#define THREAD_PRIORITY_4	4
#define THREAD_PRIORITY_5	5
#define THREAD_PRIORITY_6	6
#define THREAD_PRIORITY_7	7
#define THREAD_PRIORITY_8	8
#define THREAD_PRIORITY_9	9
#define THREAD_PRIORITY_10	10
#define THREAD_PRIORITY_11	11
#define THREAD_PRIORITY_12	12
#define THREAD_PRIORITY_13	13
#define THREAD_PRIORITY_14	14
#define THREAD_PRIORITY_15	15
#define THREAD_PRIORITY_16	16
#define THREAD_PRIORITY_17	17
#define THREAD_PRIORITY_18	18
#define THREAD_PRIORITY_19	19
#define THREAD_PRIORITY_20	20

/***************** Thread's Stack Size Macros **************************/
#define TASK_STACK_SIZE_512				512
#define TASK_STACK_SIZE_1K				1024*1
#define TASK_STACK_SIZE_2K				1024*2
#define TASK_STACK_SIZE_3K				1024*3
#define TASK_STACK_SIZE_4K				1024*4
#define TASK_STACK_SIZE_5K				1024*5
#define TASK_STACK_SIZE_6K				1024*6
#define TASK_STACK_SIZE_7K				1024*7
#define TASK_STACK_SIZE_8K				1024*8
#define TASK_STACK_SIZE_9K				1024*9

#define BLE_AUTH_PWD_NVS_KEY_COSNT 			"BLE_PWD"

/***************** Macros of Device ID **************************/
#define DEV_ID_LEN		32
#define SOMTHING_ID_LEN		20					/**< Maximum Length of a SOM's identification number */

/*
 * @brief Macros for Core and App Version
 * */
#define CORE_VERSION_NUMBER_LEN		25		/**< Maximum Length of the Core Framwork's version number */
#define APP_VERSION_NUMBER_LEN		25		/**< Maximum Length of the Application's version number */

/*
 * @brief Macros for SOM Registration
 * */
#define SOM_REG_LOCATION_LEN		10		/**< Maximum Length of SOM's registration location */
#define SOM_BT_PASSKEY_LEN			36		/**< Maximum Length of Bluetooth Passkey, to be used in BLE Config */

// Device ID NVS Key
#define DEVID_NVS_KEY			"DEVID"

// Wi-Fi related NVS Key
#define WIFI_SSID_NVS_KEY		"WIFI_SSID"
#define WIFI_PWD_NVS_KEY		"WIFI_PWD"
#define WIFI_CRED_NVS_KEY		"WIFI_CRED"

// SOMTHING related NVS Key
#define SOMTHING_ID_NVS_KEY		"SOMTHING_ID"

// FOTA related NVS Key
#define FOTA_ID_NVS_KEY			"FOTA_ID"

/***************** Macros of Operation Mode **************************/
#define OPERMODE_LEN				1
#define OPERMODE_NVS_KEY			"OPERMODE"

#ifdef __cplusplus
}
#endif

#endif /* CORE_CONSTANT_H_ */
