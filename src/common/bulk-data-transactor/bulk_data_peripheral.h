/*
 * File Name: bulk_data_peripheral.h
 * File Path: /emmate/src/common/bulk-data-transactor/bulk_data_peripheral.h
 * Description:
 *
 *  Created on: 12-Jun-2019
 *      Author: Rohan Dey
 */

#ifndef BULK_DATA_PERIPHERAL_H_
#define BULK_DATA_PERIPHERAL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_error.h"
#include "core_constant.h"

typedef enum {
	UART_BULK_DATA = 0x100,
	USB_BULK_DATA,
	BLE_BULK_DATA
} BULKDATA_PERIPHERALS;

#ifdef __cplusplus
}
#endif

#endif /* BULK_DATA_PERIPHERAL_H_ */
