# Bulk Data Transactor Module
The Bulk Data Transactor module is used to help an application to work with data streams easily. This module works in full duplex mode. An application can use this module to read or write data streams to/from a number of peripherals.

See the below block diagram for a better understanding

![image](block-diagram.png) 

## How to use the Bulk Data Transactor Module from an application

#### Header(s) to include

```
bulk_data_transactor.h
```

##### Sample APIs

