/*
 * File Name: bd_transactor_utils.c
 * File Path: /emmate/src/common/bulk-data-transactor/bd_transactor_utils.c
 * Description:
 *
 *  Created on: 12-Jun-2019
 *      Author: Rohan Dey
 */

#include "bd_transactor_utils.h"
#include "bulk_data_transactor.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#define TAG	LTAG_BDTRANSACTOR

core_err validate_bd_transaction_input(TransactionData *tdata) {
	core_err ret = CORE_FAIL;

	/* Check the Bulk Data Transaction Mode */
	if ((tdata->mode == BD_READONLY) || (tdata->mode == BD_WRITEONLY) || (tdata->mode == BD_READWRITE)) {
		CORE_LOGD(TAG, "Bulk Data Transaction Mode is OK ...");
	} else {
		CORE_LOGD(TAG, "Bulk Data Transaction Mode is NOT OK");
		return CORE_FAIL;
	}

	/* Check the Bulk Data Transaction Type */
	if ((tdata->type == TRANSACT_NUM_BYTES) || (tdata->type == TRANSACT_CHAR_TERMINATOR)
			|| (tdata->type == TRANSACT_STRING_TERMINATOR)) {
		CORE_LOGD(TAG, "Bulk Data Transaction Type is OK ...");
	} else {
		CORE_LOGD(TAG, "Bulk Data Transaction Type is NOT OK");
		return CORE_FAIL;
	}

	/* Check the Bulk Data TransactionEnd */
	switch (tdata->type) {
	case TRANSACT_NUM_BYTES:
		if (tdata->stream_end.num_bytes == 0) {
			CORE_LOGD(TAG, "Bulk Data TransactionEnd num_bytes is 0");
			return CORE_FAIL;
		}
		break;
	case TRANSACT_CHAR_TERMINATOR:
		if (tdata->stream_end.terminator == '\0') {
			CORE_LOGD(TAG, "Bulk Data TransactionEnd terminator is 0");
			return CORE_FAIL;
		}
		break;
	case TRANSACT_STRING_TERMINATOR:
		if (tdata->stream_end.delimiter == NULL) {
			CORE_LOGD(TAG, "Bulk Data TransactionEnd delimiter is NULL");
			return CORE_FAIL;
		}
		break;
	}

	/* Check the Bulk Data Peripheral */
	if ((tdata->peripheral == UART_BULK_DATA) || (tdata->peripheral == USB_BULK_DATA)
			|| (tdata->peripheral == BLE_BULK_DATA)) {
		CORE_LOGD(TAG, "Bulk Data Peripheral is OK ...");
	} else {
		CORE_LOGD(TAG, "Bulk Data Peripheral is NOT OK");
		return CORE_FAIL;
	}

	/* Check the Bulk Data Transaction callback */
	if (tdata->transaction_cb == NULL) {
		CORE_LOGD(TAG, "Bulk Data Transaction callback is NULL");
	}

	/* All is ok */
	ret = CORE_OK;

	return ret;
}
