/*
 * File Name: bd_transactor_utils.h
 * File Path: /emmate/src/common/bulk-data-transactor/bd_transactor_utils.h
 * Description:
 *
 *  Created on: 12-Jun-2019
 *      Author: Rohan Dey
 */

#ifndef BD_TRANSACTOR_UTILS_H_
#define BD_TRANSACTOR_UTILS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "core_config.h"
#include "core_common.h"
#include "core_error.h"
#include "core_constant.h"
#include "bulk_data_transactor.h"

core_err validate_bd_transaction_input(TransactionData *tdata);

#ifdef __cplusplus
}
#endif

#endif /* BD_TRANSACTOR_UTILS_H_ */
