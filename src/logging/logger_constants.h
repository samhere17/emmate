/*
 * File Name: logger_constants.h
 * Description:
 *
 *  Created on: 02-May-2019
 *      Author: Rohan Dey
 */

#ifndef LOGGING_CONSTANTS_H_
#define LOGGING_CONSTANTS_H_

/** Definitions for identifier (TAG) of all core modules */
#define LTAG_ALL_MODULE				"*"
#define LTAG_BOARDS					"boards"
#define LTAG_COMMON					"common"
#define LTAG_CONN					"conn"
#define LTAG_CONN_BLE				"ble"
#define LTAG_CONN_BLECONFIG			"ble-config"
#define LTAG_CONN_BLEGATT			"ble-gatt"
#define LTAG_CONN_WIFI				"wifi"
#define LTAG_CONN_WIFIPLAT			"wifi-plat"
#define LTAG_CONNPROTO_HTTPCLI		"http-cli"
#define LTAG_CONNPROTO_HTTPCLIAPI	"http-cli-api"
#define LTAG_CONNPROTO_SNTPCLI		"sntp-cli"
#define LTAG_EVENTGROUP				"event-group"
#define LTAG_HMI					"hmi"
#define LTAG_HMI_LED				"hmi-led"
#define LTAG_HMI_BUTTON				"hmi-button"
#define LTAG_SOM_REG				"som-reg"
#define LTAG_IOT					"iot"
#define LTAG_IOT_AWS				"aws-iot"
#define LTAG_OTA					"fota"
#define LTAG_INPROC					"inproc"
#define LTAG_PERSISTENT				"persistent"
#define LTAG_PERSISTENT_NVS			"nvs"
#define LTAG_SYSTEM					"system"
#define LTAG_SYSTEM_DEVCFG			"devcfg"
#define LTAG_SYSTEM_SYSCFG			"syscfg"
#define LTAG_SYSTEM_SYSINFO			"sysinfo"
#define LTAG_SYSTEM_SYSINIT			"sysinit"
#define LTAG_SYSTEM_SYSTIME			"systime"
#define LTAG_SYSHB					"syshb"
#define LTAG_APPCFG					"sys-appcfg"
#define LTAG_APPPOSTDATA			"sys-apppost"
#define LTAG_THREADING				"threading"
#define LTAG_UTILS					"core-utils"
#define LTAG_TCPIP					"tcpip-adaptor"
#define LTAG_PARSER_UTILS			"parser_utils"
#define LTAG_BTN_HELPER				"btn-helper"
#define LTAG_ADC					"adc-core"
#define LTAG_BDTRANSACTOR			"bd-transact"
#define LTAG_I2C					"i2c-core"
#define LTAG_FATFS					"fatfs-helper"
#define LTAG_HWIDENTIFY				"hw-identify"
#define LTAG_UART					"uart-core"
#define LTAG_GPIO					"gpio-core"

#endif /* LOGGING_CONSTANTS_H_ */
