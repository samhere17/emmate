/*
 * File Name: udp_log_streamer.c
 * Description:
 *
 *  Created on: 03-May-2019
 *      Author: Rohan Dey
 */

#include "udp_log_streamer.h"
#include "udp_log_streamer_platform.h"
#include "threading.h"
#include "module_thread_priorities.h"
#include "conn.h"
#include "logger_constants.h"
#include <string.h>

typedef struct {
	ThreadHandle udp_streamer_hndl;
	QueueHandle udp_streamer_que;
	bool udp_streamer_stat;			// true: if streamer is initialized and running, else false
	int udp_log_fd;
	struct sockaddr_in serveraddr;
} UDPStreamerCntl;

static UDPStreamerCntl m_usc;// = { 0, 0, false, 0 };
static char udp_buf[UDP_STREAMER_MAX_QUEUE_SIZE] = { 0 };

#if CONFIG_PLATFORM_ESP_IDF

/*
 * https://github.com/MalteJ/embedded-esp32-component-udp_logging
 * */
static int udp_logging_init(const char *ipaddr, unsigned long port) {
	struct timeval send_timeout = { 1, 0 };
	m_usc.udp_log_fd = 0;
	printf("udp-streamer: initializing udp logging...\r\n");
	if ((m_usc.udp_log_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		printf("udp-streamer: Cannot open socket!\r\n");
		return -1;
	}

	uint32_t ip_addr_bytes;
	inet_aton(ipaddr, &ip_addr_bytes);
	printf("udp-streamer: Logging to 0x%x\r\n", ip_addr_bytes);

	memset(&m_usc.serveraddr, 0, sizeof(m_usc.serveraddr));
	m_usc.serveraddr.sin_family = AF_INET;
	m_usc.serveraddr.sin_port = htons(port);
	m_usc.serveraddr.sin_addr.s_addr = ip_addr_bytes;

	int err = setsockopt(m_usc.udp_log_fd, SOL_SOCKET, SO_SNDTIMEO, (const char *) &send_timeout, sizeof(send_timeout));
	if (err < 0) {
		printf("udp-streamer: Failed to set SO_SNDTIMEO. Error %d\r\n", err);
	}

	return 0;
}

static void udp_logging_free() {
	int err = 0;
	if ((err = shutdown(m_usc.udp_log_fd, 2)) == 0) {
		printf("udp-streamer: UDP socket shutdown!\r\n");
	} else {
		printf("udp-streamer: Shutting-down UDP socket failed: %d!\r\n", err);
	}

	if ((err = close(m_usc.udp_log_fd)) == 0) {
		printf("udp-streamer: UDP socket closed!\r\n");
	} else {
		printf("udp-streamer: Closing UDP socket failed: %d!\r\n", err);
	}
	m_usc.udp_log_fd = 0;
}

static int udp_logging_send(const char *str, int len) {
	int err = 0;
//	int len;
//	char task_name[16];
//	char *cur_task = pcTaskGetTaskName(xTaskGetCurrentTaskHandle());
//	strncpy(task_name, cur_task, 16);
//	task_name[15] = 0;
//	if (strncmp(task_name, "tiT", 16) != 0)
//	{
//		len = vsprintf((char*)buf, str, l);
	if ((err = sendto(m_usc.udp_log_fd, str, len, 0, (struct sockaddr *) &m_usc.serveraddr, sizeof(m_usc.serveraddr)))
			< 0) {
//		show_socket_error_reason(m_usc.udp_log_fd);
		printf("udp-streamer: Freeing UDP Logging. sendto failed!\r\n");
		udp_logging_free();
		printf("udp-streamer: UDP Logging freed!\r\n");
		return err;
	}
//	}
//	return vprintf( str, l );
	return 0;
}

#else
static int udp_logging_init(const char *ipaddr, unsigned long port) {}
#endif	/* CONFIG_PLATFORM_ESP_IDF */

static void udp_log_streamer(void * params) {

	char buf[UDP_STREAMER_MIN_TRANSMIT_SIZE] = { 0 };
	int idx = 0;

	while (get_network_conn_status() != NETCONNSTAT_CONNECTED) {
		TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
	}
	udp_logging_init(UDP_LOG_STREAMER_SERVER, UDP_LOG_STREAMER_PORT);

	m_usc.udp_streamer_stat = true;

	while (1) {
		QueueReceive(m_usc.udp_streamer_que, (buf + idx++), THREADING_MAX_DELAY);
//		printf("udp-logging: idx = %d\r\n", idx);

		if (idx == UDP_STREAMER_MIN_TRANSMIT_SIZE) {
			int err = udp_logging_send(buf, (idx + 1));
			if (err < 0) {
				printf("udp-streamer: exited!");
				m_usc.udp_streamer_stat = false;
			}
			memset(buf, 0, sizeof(buf));
			idx = 0;

			TaskDelay(DELAY_100_MSEC / TICK_RATE_TO_MS);
		}
//		TaskDelay(DELAY_100_MSEC / TICK_RATE_TO_MS);
	}
}

core_err start_udp_log_streamer() {
	/* Create a byte queue */
	m_usc.udp_streamer_que = QueueCreate(UDP_STREAMER_MAX_QUEUE_SIZE, sizeof(char));
	if (m_usc.udp_streamer_que == 0) {
		printf("udp-streamer: Failed to create queue\r\n");
		return CORE_FAIL;
	}

	/* Create a thread to perform the notifications */
	BaseType thread_stat = TaskCreate(udp_log_streamer, "udp-log-streamer", TASK_STACK_SIZE_8K, NULL,
			THREAD_PRIORITY_UDP_LOG_STREAMER, &m_usc.udp_streamer_hndl);
	if (thread_stat == false) {
		printf("udp-streamer: Failed to create thread\r\n");
		return CORE_FAIL;
	}

	return CORE_OK;
}

static void reset_udp_log_streamer_cntl() {
	m_usc.udp_streamer_que = 0;
	m_usc.udp_streamer_hndl = 0;
	m_usc.udp_streamer_stat = false;
	m_usc.udp_log_fd = 0;
}

core_err kill_udp_log_streamer() {
	udp_logging_free();
	QueueDelete(m_usc.udp_streamer_que);
	TaskDelete(m_usc.udp_streamer_hndl);
	reset_udp_log_streamer_cntl();

	return CORE_OK;
}

bool is_udp_log_streamer_ready() {
	return m_usc.udp_streamer_stat;
}

#if 0
core_err send_log_to_udp_server(const char *buf, int len) {
	core_err ret = CORE_FAIL;

	if (len > UDP_STREAMER_MAX_QUEUE_SIZE) {
		printf("udp-streamer: Log too long, can't send to UDP\r\n");
		return CORE_FAIL;
	}

//	printf("udp-streamer: length of log = %d\r\n", len);
//	printf("udp-streamer: queuing %d bytes\r\n", len);
//	printf("%s", buf);

	if (buf != NULL) {
		bool stat = false;
		for (int i = 0; i < len; i++) {
			stat = QueueSend(m_usc.udp_streamer_que, (buf + i), 0/*10/TICK_RATE_TO_MS*/);
		}
	}

//	free(buf);

	return ret;
}
#else
void send_log_to_udp_streamer(const char *str, va_list list, int len) {
	if (len > UDP_STREAMER_MAX_QUEUE_SIZE) {
		printf("udp-streamer: Log is too long, can't send to UDP!\r\n");
		return;
	}

	if (str != NULL) {
		len = vsprintf((char*) udp_buf, str, list);

//		bool stat = false;
		for (int i = 0; i < len; i++) {
			QueueSend(m_usc.udp_streamer_que, (udp_buf + i), 0/*10/TICK_RATE_TO_MS*/);
		}
		memset(udp_buf, 0, sizeof(udp_buf));
	}
}
#endif
