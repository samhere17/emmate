/*
 * File Name: tcpip_adapter_core.h
 * File Path: /emmate/src/tcpip/tcpip_adapter_core.h
 * Description:
 *
 *  Created on: 06-May-2019
 *      Author: Rohan Dey
 */

#ifndef TCPIP_ADAPTER_CORE_H_
#define TCPIP_ADAPTER_CORE_H_

#include "tcpip_adapter_platform.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

typedef TcpipAdapterIpInfo_Pf TcpipAdapterIpInfo_Core;

typedef enum {
	CORE_TCPIP_ADAPTER_IF_STA 	= PF_TCPIP_ADAPTER_IF_STA,	/**< Wi-Fi STA (station) interface */
	CORE_TCPIP_ADAPTER_IF_AP 	= PF_TCPIP_ADAPTER_IF_AP,	/**< Wi-Fi soft-AP interface */
	CORE_TCPIP_ADAPTER_IF_ETH 	= PF_TCPIP_ADAPTER_IF_ETH,	/**< Ethernet interface */
	CORE_TCPIP_ADAPTER_IF_MAX 	= PF_TCPIP_ADAPTER_IF_MAX
} TCPIP_ADAPTER_IF_CORE;

/**
 * @brief  Get interface's IP address information
 *
 * If the interface is up, IP information is read directly from the TCP/IP stack.
 *
 * If the interface is down, IP information is read from a copy kept in the TCP/IP adapter
 * library itself.
 *
 * @param[in]   tcpip_if Interface to get IP information
 * @param[out]  ip_info If successful, IP information will be returned in this argument.
 *
 * @return
 *         - CORE_OK
 *         - CORE_FAIL
 */
core_err get_tcpip_ip_info(TCPIP_ADAPTER_IF_PF tcpip_if, TcpipAdapterIpInfo_Core *ip_info);

/**
 * @brief  Test if supplied interface is up or down
 *
 * @param[in]   tcpip_if Interface to test up/down status
 *
 * @return
 *         - true - Interface is up
 *         - false - Interface is down
 */
#define	is_tcpip_core_netif_up(tcpip_if) /*bool*/ is_tcpip_pf_netif_up(/*TCPIP_ADAPTER_IF_CORE*/ tcpip_if)

#endif /* TCPIP_ADAPTER_CORE_H_ */
