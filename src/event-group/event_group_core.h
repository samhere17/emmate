/*
 * File Name: event_group_core.h
 * File Path: /emmate/src/event-group/event_group_core.h
 * Description:
 *
 *  Created on: 27-Apr-2019
 *      Author: Rohan Dey
 */

#ifndef EVENT_GROUP_H_
#define EVENT_GROUP_H_

#include "core_common.h"
#include "core_constant.h"
#include "core_error.h"

#include "event_group_platform.h"

/* Wrapper typedefs for abstracting the platform specific names */

/**
 *
 * Type by which event groups are referenced.  For example, a call to
 * event_group_create() returns an EventGrpHandle variable that can then
 * be used as a parameter to other event group functions.
 *
 */
typedef EventGroupPfHandle	EventGrpHandle;

/*
 * The type that holds event bits always matches EventPfBits - therefore the
 * number of bits it holds is set by configUSE_16_BIT_TICKS (16 bits if set to 1,
 * 32 bits if set to 0.
 *
 */
typedef EventPfBits			EventBits;


#define EventMaxDelay		EventPfMaxDelay

//Event Platform Bits
#if( configUSE_16_BIT_TICKS == 0 )

#define EVT_BIT31	EVT_PTF_BIT31
#define EVT_BIT30	EVT_PTF_BIT30
#define EVT_BIT29	EVT_PTF_BIT29
#define EVT_BIT28	EVT_PTF_BIT28
#define EVT_BIT27	EVT_PTF_BIT27
#define EVT_BIT26	EVT_PTF_BIT26
#define EVT_BIT25	EVT_PTF_BIT25
#define EVT_BIT24	EVT_PTF_BIT24
#define EVT_BIT23	EVT_PTF_BIT23
#define EVT_BIT22	EVT_PTF_BIT22
#define EVT_BIT21	EVT_PTF_BIT21
#define EVT_BIT20	EVT_PTF_BIT20
#define EVT_BIT19	EVT_PTF_BIT19
#define EVT_BIT18	EVT_PTF_BIT18
#define EVT_BIT17	EVT_PTF_BIT17
#define EVT_BIT16	EVT_PTF_BIT16
#define EVT_BIT15	EVT_PTF_BIT15
#define EVT_BIT14	EVT_PTF_BIT14
#define EVT_BIT13	EVT_PTF_BIT13
#define EVT_BIT12	EVT_PTF_BIT12
#define EVT_BIT11	EVT_PTF_BIT11
#define EVT_BIT10	EVT_PTF_BIT10
#define EVT_BIT9	EVT_PTF_BIT9

#endif

#define EVT_BIT8	EVT_PTF_BIT8
#define EVT_BIT7	EVT_PTF_BIT7
#define EVT_BIT6	EVT_PTF_BIT6
#define EVT_BIT5	EVT_PTF_BIT5
#define EVT_BIT4	EVT_PTF_BIT4
#define EVT_BIT3	EVT_PTF_BIT3
#define EVT_BIT2	EVT_PTF_BIT2
#define EVT_BIT1	EVT_PTF_BIT1
#define EVT_BIT0	EVT_PTF_BIT0

/* Wrapper functions for abstracting the platform specific names */

/*
 * @brief	Creates a new RTOS event group, and returns a handle by which the newly created event group can be referenced.
 *
 * @return	If the event group was created then a handle to the event group is returned. If there was insufficient RTOS heap available to create the event group then NULL is returned.
 *
 */
#define	event_group_create() 										event_group_ptf_create()


/**
 * @brief [Potentially] block to wait for one or more bits to be set within a
 * previously created event group.
 *
 * This function cannot be called from an interrupt.
 *
 * @param eventgroup The event group in which the bits are being tested.  The
 * event group must have previously been created using a call to
 * event_group_create().
 *
 * @param bitstowaitfor A bitwise value that indicates the bit or bits to test
 * inside the event group.  For example, to wait for bit 0 and/or bit 2 set
 * bitstowaitfor to 0x05.  To wait for bits 0 and/or bit 1 and/or bit 2 set
 * bitstowaitfor to 0x07.  Etc.
 *
 * @param clearonexit If clearonexit is set to pdTRUE then any bits within
 * bitstowaitfor that are set within the event group will be cleared before
 * event_group_wait_bits() returns if the wait condition was met (if the function
 * returns for a reason other than a timeout).  If clearonexit is set to
 * pdFALSE then the bits set in the event group are not altered when the call to
 * event_group_wait_bits() returns.
 *
 * @param waitforallbits If waitforallbits is set to pdTRUE then
 * event_group_wait_bits() will return when either all the bits in waitforallbits
 * are set or the specified block time expires.  If waitforallbits is set to
 * pdFALSE then event_group_wait_bits() will return when any one of the bits set
 * in waitforallbits is set or the specified block time expires.  The block
 * time is specified by the xTicksToWait parameter.
 *
 * @param tickstowait The maximum amount of time (specified in 'ticks') to wait
 * for one/all (depending on the waitforallbits value) of the bits specified by
 * bitstowaitfor to become set.
 *
 * @return The value of the event group at the time either the bits being waited
 * for became set, or the block time expired.  Test the return value to know
 * which bits were set.  If event_group_wait_bits() returned because its timeout
 * expired then not all the bits being waited for will be set.  If
 * event_group_wait_bits() returned because the bits it was waiting for were set
 * then the returned value is the event group value before any bits were
 * automatically cleared in the case that xClearOnExit parameter was set to
 * pdTRUE.
 *
 */
#define event_group_wait_bits(eventgroup, \
		bitstowaitfor, clearonexit, waitforallbits, tickstowait)	event_group_ptf_wait_bits(eventgroup, bitstowaitfor, clearonexit, waitforallbits, tickstowait)


/**
 * @brief Set bits within an event group.
 * This function cannot be called from an interrupt.  event_group_set_bits_from_isr()
 * is a version that can be called from an interrupt.
 *
 * Setting bits in an event group will automatically unblock tasks that are
 * blocked waiting for the bits.
 *
 * @param eventgroup The event group in which the bits are to be set.
 *
 * @param bitstoset A bitwise value that indicates the bit or bits to set.
 * For example, to set bit 3 only, set bitstoset to 0x08.  To set bit 3
 * and bit 0 set bitstoset to 0x09.
 *
 * @return The value of the event group at the time the call to
 * event_group_set_bits() returns.  There are two reasons why the returned value
 * might have the bits specified by the uxBitsToSet parameter cleared.  First,
 * if setting a bit results in a task that was waiting for the bit leaving the
 * blocked state then it is possible the bit will be cleared automatically
 * (see the clearonexit parameter of event_group_wait_bits()).  Second, any
 * unblocked (or otherwise Ready state) task that has a priority above that of
 * the task that called event_group_set_bits() will execute and may change the
 * event group value before the call to event_group_set_bits() returns.
 *
 */
#define event_group_set_bits( eventgroup,bitstoset )				event_group_ptf_set_bits( eventgroup,bitstoset )

/**
 * @brief A version of event_group_set_bits() that can be called from an interrupt.
 *
 * Setting bits in an event group is not a deterministic operation because there
 * are an unknown number of tasks that may be waiting for the bit or bits being
 * set.  RTOS does not allow nondeterministic operations to be performed in
 * interrupts or from critical sections.  Therefore xEventGroupSetBitFromISR()
 * sends a message to the timer task to have the set operation performed in the
 * context of the timer task - where a scheduler lock is used in place of a
 * critical section.
 *
 * @param eventgroup The event group in which the bits are to be set.
 *
 * @param uxBitsToSet A bitwise value that indicates the bit or bits to set.
 * For example, to set bit 3 only, set uxBitsToSet to 0x08.  To set bit 3
 * and bit 0 set uxBitsToSet to 0x09.
 *
 * @param pxHigherPriorityTaskWoken As mentioned above, calling this function
 * will result in a message being sent to the timer daemon task.  If the
 * priority of the timer daemon task is higher than the priority of the
 * currently running task (the task the interrupt interrupted) then
 * *pxHigherPriorityTaskWoken will be set to pdTRUE by
 * xEventGroupSetBitsFromISR(), indicating that a context switch should be
 * requested before the interrupt exits.  For that reason
 * *pxHigherPriorityTaskWoken must be initialised to pdFALSE.  See the
 * example code below.
 *
 * @return If the request to execute the function was posted successfully then
 * pdPASS is returned, otherwise pdFALSE is returned.  pdFALSE will be returned
 * if the timer service queue was full.
 *
 */
#define event_group_set_bits_from_isr(eventgroup, bitstoset, \
		higherprioritytaskwoken )									event_group_ptf_set_bits_from_isr(eventgroup, bitstoset, higherprioritytaskwoken )

/**
 * @brief Clear bits within an event group.  This function cannot be called from an
 * interrupt.
 *
 * @param eventgroup The event group in which the bits are to be cleared.
 *
 * @param bitstoclear A bitwise value that indicates the bit or bits to clear
 * in the event group.  For example, to clear bit 3 only, set bitstoclear to
 * 0x08.  To clear bit 3 and bit 0 set bitstoclear to 0x09.
 *
 * @return The value of the event group before the specified bits were cleared.
 *
 */
#define event_group_clear_bits(eventgroup, bitstoclear)				event_group_ptf_clear_bits(eventgroup, bitstoclear)

/**
 * @brief A version of event_group_clear_bits() that can be called from an interrupt.
 *
 * Setting bits in an event group is not a deterministic operation because there
 * are an unknown number of tasks that may be waiting for the bit or bits being
 * set.  RTOS does not allow nondeterministic operations to be performed
 * while interrupts are disabled, so protects event groups that are accessed
 * from tasks by suspending the scheduler rather than disabling interrupts.  As
 * a result event groups cannot be accessed directly from an interrupt service
 * routine.  Therefore xEventGroupClearBitsFromISR() sends a message to the
 * timer task to have the clear operation performed in the context of the timer
 * task.
 *
 * @param eventgroup The event group in which the bits are to be cleared.
 *
 * @param bitstoclear A bitwise value that indicates the bit or bits to clear.
 * For example, to clear bit 3 only, set bitstoclear to 0x08.  To clear bit 3
 * and bit 0 set bitstoclear to 0x09.
 *
 * @return If the request to execute the function was posted successfully then
 * pdPASS is returned, otherwise pdFALSE is returned.  pdFALSE will be returned
 * if the timer service queue was full.
 *
 */
#define event_group_clear_bits_from_isr(eventgroup, bitstoclear)	event_group_ptf_clear_bits_from_isr(eventgroup, bitstoclear)


/**
 * @brief A version of event_group_get_bits() that can be called from an ISR.
 *
 * @param eventgroup The event group being queried.
 *
 * @return The event group bits at the time xEventGroupGetBitsFromISR() was called.
 */
#define event_group_get_bits(eventgroup)							event_group_ptf_get_bits(eventgroup)


/**
 * @brief A version of event_group_get_bits() that can be called from an ISR.
 *
 * @param eventgroup The event group being queried.
 *
 * @return The event group bits at the time event_group_get_bits_from_isr() was called.
 */
#define event_group_get_bits_from_isr(eventgroup)					event_group_ptf_get_bits_from_isr(eventgroup)

/**
 * @brief Atomically set bits within an event group, then wait for a combination of
 * bits to be set within the same event group.  This functionality is typically
 * used to synchronise multiple tasks, where each task has to wait for the other
 * tasks to reach a synchronisation point before proceeding.
 *
 * This function cannot be used from an interrupt.
 *
 * The function will return before its block time expires if the bits specified
 * by the bitstowait parameter are set, or become set within that time.  In
 * this case all the bits specified by bitstowait will be automatically
 * cleared before the function returns.
 *
 * @param eventgroup The event group in which the bits are being tested.  The
 * event group must have previously been created using a call to
 * event_group_create().
 *
 * @param bitstoset The bits to set in the event group before determining
 * if, and possibly waiting for, all the bits specified by the bitstowait
 * parameter are set.
 *
 * @param bitstowaitfor A bitwise value that indicates the bit or bits to test
 * inside the event group.  For example, to wait for bit 0 and bit 2 set
 * bitstowaitfor to 0x05.  To wait for bits 0 and bit 1 and bit 2 set
 * bitstowaitfor to 0x07.  Etc.
 *
 * @param tickstowait The maximum amount of time (specified in 'ticks') to wait
 * for all of the bits specified by bitstowaitfor to become set.
 *
 * @return The value of the event group at the time either the bits being waited
 * for became set, or the block time expired.  Test the return value to know
 * which bits were set.  If event_group_sync() returned because its timeout
 * expired then not all the bits being waited for will be set.  If
 * event_group_sync() returned because all the bits it was waiting for were
 * set then the returned value is the event group value before any bits were
 * automatically cleared.
 */
#define event_group_sync( eventgroup, bitstoset, \
		bitstowaitfor, tickstowait  )								event_group_ptf_sync( eventgroup, bitstoset, bitstowaitfor, tickstowait  )

/**
 * @brief Delete an event group that was previously created by a call to
 * event_group_create().  Tasks that are blocked on the event group will be
 * unblocked and obtain 0 as the event group's value.
 *
 * @param eventgroup The event group being deleted.
 */
#define event_group_delete(eventgroup)								event_group_ptf_delete(eventgroup)

#endif /* EVENT_GROUP_H_ */
