/*
 * File Name: board_ids.c
 * File Path: /emmate/src/boards/board_ids.c
 * Description:
 *
 *  Created on: 29-May-2019
 *      Author: Rohan Dey
 */

#include "board_ids.h"
#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif
#if CONFIG_USE_WIFI
#include "wifi.h"
#endif
#if CONFIG_USE_BLE
#include "ble.h"
#endif
#if IFACE_ETH
#endif
#if IFACE_GSM
#endif
#if CONFIG_USE_EEPROM_24AA02UID
#include "eeprom_24AA02UID.h"
#endif
#if CONFIG_USE_PERSISTENT
#include "persistent_mem.h"
#endif
#include "core_utils.h"
#include <string.h>

#define TAG	LTAG_BOARDS

static char core_somthing_id[SOMTHING_ID_LEN+1] = {0};

core_err read_save_somthing_id() {
	core_err err = CORE_FAIL;

	memset(core_somthing_id, 0, (SOMTHING_ID_LEN+1));

	// TODO: Checking for hard-wired SOM ID has to be made dynamic instead of compile time
#if CONFIG_USE_SILICON_ID_CHIP
#if CONFIG_USE_EEPROM_24AA02UID
	uint8_t uid[8] = {0};
	err = eeprom_24AA02UID_init();
	if (err != CORE_OK) {
		CORE_LOGE(TAG, "Failed to initialize EEPROM 24AA02UID");
		return CORE_FAIL;
	}

	err = eeprom_24AA02UID_read_64bit_uid_code(uid);
	if (err != CORE_OK) {
		CORE_LOGE(TAG, "Failed to read 64 bit UID ID from EEPROM 24AA02UID");
		return CORE_FAIL;
	}

	err = eeprom_24AA02UID_deinit();
	if (err != CORE_OK) {
		CORE_LOGE(TAG, "Failed to de-initialize EEPROM 24AA02UID");
		return CORE_FAIL;
	}
	err = hexbytearray_to_str(uid, sizeof(uid), core_somthing_id);
	if (err != CORE_OK) {
		CORE_LOGE(TAG, "Failed to convert HEX UID to String");
		return CORE_FAIL;
	}
#endif /* CONFIG_USE_EEPROM_24AA02UID */
#else
#if CONFIG_USE_PERSISTENT
	err = read_somthing_id_from_persistent_mem(core_somthing_id);
	if (err != CORE_OK) {
		memcpy(core_somthing_id, NO_HW_SOM_ID, strlen(NO_HW_SOM_ID));
	}
#else
	memcpy(core_somthing_id, NO_HW_SOM_ID, strlen(NO_HW_SOM_ID));
#endif /* CONFIG_USE_PERSISTENT */
#endif /* CONFIG_USE_SILICON_ID_CHIP */

	err = CORE_OK;
	return err;
}

core_err get_somthing_id(char *somthing_id) {
	core_err err = CORE_FAIL;

	if (somthing_id == NULL)
		return CORE_FAIL;

	/* If NO_SOM_ID is found, then return suitable error code */
	if (strcmp(core_somthing_id, NO_HW_SOM_ID) == 0) {
		return CORE_ERR_NOT_FOUND;
	}

	strcpy(somthing_id, core_somthing_id);
//	memcpy(som_id, core_som_id, SOM_ID_LEN);

	CORE_LOGD(TAG, "SOM ID: %s", somthing_id);
//	CORE_LOGI(TAG, "SOM ID: 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x", som_id[0], som_id[1], som_id[2], som_id[3], som_id[4],
//			som_id[5]);

	err = CORE_OK;
	return err;
}

core_err set_virtual_somthing_id(char *v_somthing_id) {
	if (v_somthing_id == NULL)
		return CORE_FAIL;

	strcpy(core_somthing_id, v_somthing_id);

	return CORE_OK;
}

bool is_hw_somthing_id_present() {
	/* If NO_SOM_ID is found, then return suitable error code */
	if (strcmp(core_somthing_id, NO_HW_SOM_ID) == 0) {
		return false;
	}
	return true;
}

#if CONFIG_USE_WIFI
core_err get_board_wifi_sta_mac(uint8_t *wifi_sta_mac) {
	core_err err = CORE_FAIL;

	if (wifi_sta_mac == NULL)
		return CORE_FAIL;

	err = get_wifi_sta_mac(wifi_sta_mac);
	CORE_LOGD(TAG, "Wi-Fi STA MAC: %02x:%02x:%02x:%02x:%02x:%02x", wifi_sta_mac[0], wifi_sta_mac[1], wifi_sta_mac[2],
			wifi_sta_mac[3], wifi_sta_mac[4], wifi_sta_mac[5]);
	if (err != CORE_OK) {
		CORE_LOGE(TAG, "Failed to get Wi-Fi STA MAC");
		err = CORE_FAIL;
	}
	return err;
}
#endif

#if CONFIG_USE_BLE
core_err get_board_bt_mac(uint8_t *bt_mac) {
	core_err err = CORE_FAIL;

	if (bt_mac == NULL)
		return CORE_FAIL;

	get_ble_mac(bt_mac);
	CORE_LOGD(TAG, "Bluetooth MAC: %02x:%02x:%02x:%02x:%02x:%02x", bt_mac[0], bt_mac[1], bt_mac[2], bt_mac[3], bt_mac[4],
			bt_mac[5]);
	err = CORE_OK;
	return err;
}
#endif

#if IFACE_ETH
core_err get_board_eth_mac(uint8_t *eth_mac) {
	core_err err = CORE_FAIL;

	if (eth_mac == NULL) return CORE_FAIL;

	err = // TODO:
	CORE_LOGD(TAG, "Ethernet MAC: %02x:%02x:%02x:%02x:%02x:%02x", eth_mac[0], eth_mac[1], eth_mac[2], eth_mac[3],
			eth_mac[4], eth_mac[5]);
	if (err != CORE_OK) {
		CORE_LOGE(TAG, "Failed to get Ethernet MAC");
		err = CORE_FAIL;
	}
	return err;
}
#endif

#if IFACE_GSM
core_err get_board_gsm_imei(long *gsm_imei) {
	core_err err = CORE_FAIL;

	if (eth_mac == NULL) return CORE_FAIL;

	err = // TODO:
	CORE_LOGD(TAG, "GSM IMEI: %d", gsm_imei);
	if (err != CORE_OK) {
		CORE_LOGE(TAG, "Failed to get GSM IMEI");
		err = CORE_FAIL;
	}
	return err;
}
#endif

