/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   multi_thread_switching.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework freeRtos example to demonstrate the multi-task switching
 *
 *
 *
 */

#include "multi_thread_switching.h"

#define TAG	"multi_thread_switching"

ThreadHandle thread_handle_1;
ThreadHandle thread_handle_2;
ThreadHandle thread_handle_3;


/********************************************** Module's Static Functions **********************************************************************/

/* Task1 with priority 1 */
static void task_1(void * param) {
	uint8_t count = 0;
	while (1) {
		CORE_LOGI(TAG, "Hello %s running..", __func__);
		count++;
		// Task delay of 2 secs
		TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);

		if (count > 100) {
			CORE_LOGI(TAG, "Hello %s going to end after 100times print..", __func__);
			break;
		}
	}
	/* Deleting the task_1 */
	TaskDelete(NULL);
}

/* Task2 with priority 2 */
static void task_2(void * param) {
	uint8_t count = 0;
	while (1) {
		CORE_LOGI(TAG, "Hello %s running..", __func__);
		count++;
		// Task delay of 1.5 secs
		TaskDelay((DELAY_1_SEC+DELAY_500_MSEC) / TICK_RATE_TO_MS);

		if (count > 100) {
			CORE_LOGI(TAG, "Hello %s going to end after 100times print..", __func__);
			break;
		}
	}
	/* Deleting the task_1 */
	TaskDelete(NULL);
}

/* idle_task with priorities 0 means This task will run when other High priority task are in Idle state */
static void idle_task(void * param) {
	uint8_t count = 0;
	while (1) {
		CORE_LOGI(TAG, "Hello %s running..", __func__);
		count++;
		TaskDelay(DELAY_500_MSEC / TICK_RATE_TO_MS);

		if (count > 100) {
			CORE_LOGI(TAG, "Hello %s going to end after 100times print..", __func__);
			break;
		}
	}
	/* Deleting the task_1 */
	TaskDelete(NULL);
}




/**************************************************************************************************************************/

void multi_thread_switching_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

	/* Create a task-1 with priorities 1 */
	BaseType xReturned = TaskCreate(task_1, "task_1", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_1, &thread_handle_1);

	if (xReturned == true) {
		// The task started
		CORE_LOGI(TAG, "task_1 started.....");
	} else {
		// failed to start the task
		CORE_LOGE(TAG, "Failed to start task_1.....");
	}

	/* Create a task-2 with priorities 2 */
	xReturned = TaskCreate(task_2, "task_2", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_2, &thread_handle_2);

	if (xReturned == true) {
		// The task started
		CORE_LOGI(TAG, "task_2 started.....");
	} else {
		// failed to start the task
		CORE_LOGE(TAG, "Failed to start task_2.....");
	}

	/* Create a idle_task with priorities 0 means This task will run when other High priority task are in Idle state */
	xReturned = TaskCreate(idle_task, "idle_task", TASK_STACK_SIZE_2K, NULL, 0, &thread_handle_2);

	if (xReturned == true) {
		// The task started
		CORE_LOGI(TAG, "idle_task started.....");
	} else {
		// failed to start the task
		CORE_LOGE(TAG, "Failed to start idle_task.....");
	}

}

void multi_thread_switching_loop() {
	//CORE_LOGI(TAG, "In %s", __func__);
}
