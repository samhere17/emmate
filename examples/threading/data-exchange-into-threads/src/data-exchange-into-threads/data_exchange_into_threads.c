/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   data_exchange_into_threads.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework freeRtos example to demonstrate the
 * 				Data exchange between multiple tasks
 *
 *
 *
 */

#include "data_exchange_into_threads.h"

#define TAG	"data_exchange_into_threads"

ThreadHandle thread_handle_1;
ThreadHandle thread_handle_2;

QueueHandle queue_handle;

/********************************************** Module's Static Functions **********************************************************************/

/* Task1 with priority 3 */
static void task_1(void * param) {
	uint16_t recv_byte;
	uint16_t send_byte = 1;
	while (1) {
		// Continue if Queue Initialize success
		if (queue_handle != NULL) {
			// wait for Queue to Receive Data from Task-2 or else get timeout and again wait after 1sec.
			if (QueueReceive(queue_handle, &recv_byte, DELAY_20_MSEC/TICK_RATE_TO_MS) == true) {

				CORE_LOGI(TAG, "Hello %s running.. and Queue-Receive value %d", __func__, recv_byte);

				// wait for 1sec before sending the data
				TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);

				// Send data to Task-2
				QueueSend(queue_handle, &send_byte, DELAY_10_MSEC/TICK_RATE_TO_MS);

				send_byte++;
			}
		} else {

			// wait for 1sec
			TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
		}
	}
	/* Deleting the task_1 */
	TaskDelete(NULL);
}

/* Task2 with priority 3 */
static void task_2(void * param) {
	uint16_t recv_byte;
	uint16_t send_byte = 5;
	while (1) {
		// Continue if Queue Initialize success
		if (queue_handle != NULL) {
			// wait for Queue to Receive Data from Task-1
			if (QueueReceive(queue_handle, &recv_byte, THREADING_MAX_DELAY) == true) {

				CORE_LOGI(TAG, "Hello %s running.. and Queue-Receive value %d", __func__, recv_byte);

				// wait for 5sec before sending the data
				TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);

				// Send data to Task-1
				QueueSend(queue_handle, &send_byte, DELAY_10_MSEC/TICK_RATE_TO_MS);
				send_byte += 5;
			}
		} else {
			// wait for 1sec
			TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
		}
	}
	/* Deleting the task_1 */
	TaskDelete(NULL);
}

/**************************************************************************************************************************/

void data_exchange_into_thread_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

	/* Create a Queue to communicate between tasks */
	queue_handle = QueueCreate(5, sizeof(uint16_t));
	if (queue_handle == NULL) {
		CORE_LOGE(TAG, "Failed to create Queue.....");
	} else {
		uint16_t send_byte = 200;
		/* 
			Send data to Task-1 & Task-2, 
		 	which task execute first, that task will receive the data first.
		*/
		QueueSend(queue_handle, &send_byte, DELAY_10_MSEC/TICK_RATE_TO_MS);
	}


	/* Create a task-1 with priorities 3 */
	BaseType xReturned = TaskCreate(task_1, "task_1", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_3, &thread_handle_1);

	if (xReturned == true) {
		// The task started
		CORE_LOGI(TAG, "task_1 started.....");
	} else {
		// failed to start the task
		CORE_LOGE(TAG, "Failed to start task_1.....");
	}

	/* Create a task-2 with priorities 3 */
	xReturned = TaskCreate(task_2, "task_2", TASK_STACK_SIZE_2K, NULL, THREAD_PRIORITY_3, &thread_handle_2);

	if (xReturned == true) {
		// The task started
		CORE_LOGI(TAG, "task_2 started.....");
	} else {
		// failed to start the task
		CORE_LOGE(TAG, "Failed to start task_2.....");
	}

}

void data_exchange_into_thread_loop() {
	//CORE_LOGI(TAG, "In %s", __func__);
}
