/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   data_exchange_into_threads.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework freeRtos example to demonstrate the
 * 				Data exchange between multiple tasks
 *
 *
 *
 */

#ifndef DATA_EXCHANGE_INTO_THREADS_H_
#define DATA_EXCHANGE_INTO_THREADS_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

/**
 * @brief	Init function for data_exchange_into_thread module
 *
 */
void data_exchange_into_thread_init();

/**
 * @brief	Execution function for data_exchange_into_thread module
 *
 */
void data_exchange_into_thread_loop();

#endif	/* DATA_EXCHANGE_INTO_THREADS_H_ */
