
#include "data_exchange_into_threads.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	// initialize My module
	data_exchange_into_thread_init();

	while(1){
		// Execute My module
		data_exchange_into_thread_loop();
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
