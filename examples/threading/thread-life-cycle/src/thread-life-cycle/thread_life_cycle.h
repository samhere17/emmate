/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   thread_life_cycle.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework freeRtos example to demonstrate the
 * task to create and run for 100time and then deleted.
 *
 *
 *
 */



#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

/**
 * @brief	Init function for thread_life_cycle module
 *
 */
void thread_life_cycle_init();

/**
 * @brief	Init function for thread_life_cycle module
 *
 */
void thread_life_cycle_loop();


