/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   thread_life_cycle.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework freeRtos example to demonstrate the
 * task to create and run for 100time and then deleted.
 *
 *
 *
 */

#include "core_common.h"
#include "core_constant.h"
#include "system.h"

#include "thread_life_cycle.h"

#define TAG	"thread_life_cycle"

ThreadHandle thread_handle;

/* Task1 with priority 3 */
static void task_1(void * param) {
	uint8_t count=0;
	while (1) {
		CORE_LOGI(TAG, "Hello %s running..", __func__);
		count++;
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);

		if(count > 100){
			CORE_LOGI(TAG, "Hello %s going to end after 100times print..", __func__);
			break;
		}
	}
	/* Deleting the task_1 */
	TaskDelete(NULL);
}

void thread_life_cycle_init() {
	CORE_LOGI(TAG, "In thread_life_cycle_init");

	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

	/* Create a task-1 with priorities 3 */
	BaseType xReturned = TaskCreate(task_1, "task_1", TASK_STACK_SIZE_2K, NULL,
			THREAD_PRIORITY_3, &thread_handle);

	if (xReturned == true) {
		// The task started
		CORE_LOGI(TAG, "task_1 started.....");
	} else {
		// failed to start the task
		CORE_LOGE(TAG, "Failed to start task_1-1.....");
	}
}

void thread_life_cycle_loop() {
	//CORE_LOGI(TAG, "In thread_life_cycle_loop");
}
