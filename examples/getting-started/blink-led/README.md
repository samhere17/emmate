# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

The required components and tools are:

1. A LED (Any color).
2. 220 Ohms Resistor. (For color code refer the connection diagram.)
3. Single strand wires.
4. Breadboard.
5. Power Source for power up the module/dev-board

Connect the Hardware as per the below diagram :

### For ESP32 DevKit-C V4

![ESP32 DevKit-C](fritzing/esp32_blink_led.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates a LED blink operation with the help of the GPIO peripheral of the EmMate Framework.

This example does the following things:

- Blinks the `SYSTEM_HMI_LED_MONO_RED` LED internally as a system notification. This must be configured properly in `src/thing/thing.h`
- Toggles the `CUSTOM_LED_1` LED at a 1 second interval. This must be configured properly in `src/thing/thing.h`
- Prints a log message every 1 second.
