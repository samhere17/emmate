# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

![ESP32 DevKit-C](fritzing/logging.png)

### For Other Hardware

Comming soon ...

## About this example

This should be the very first example to run after setting up the EmMate Development Environment.

This example does the following things:

- Blinks the `SYSTEM_HMI_LED_MONO_RED` LED internally as a system notification. This must be configured properly in `src/thing/thing.h`
- Prints "Hello World" in the console every 2 seconds