/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   json_array_create.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The creation of a array object into a JSON using structure.
 *
 *
 *
 */

#include "json_array_create.h"

#include "core_common.h"
#include "core_constant.h"
#include "system.h"

#include "core_utils.h"

// Include JSON Library
#include "parson.h"

#define TAG	"json_create"

/*
 *	Declaration of a global variables
 */
bool is_bool_val = true;
int integer_val = 1;
float float_val = 1;
double double_val = 1;

/*
 *	Declaration of a Example structure
 */
typedef struct {
	char board_id[50];
	bool is_bool_val;
	int integer_val;
	float float_val;
	double double_val;
	int integer_arr_val[5];
} EXAMPLE_STRUCT;

/********************************************** Module's Static Functions **********************************************************************/

static core_err create_json() {

	core_err ret = CORE_FAIL;

	char *your_thing_name = NULL;

	// populate the example_struct_data with data
	EXAMPLE_STRUCT example_struct_data = { .board_id = "board-001", .is_bool_val = is_bool_val, .integer_val =
			integer_val, .float_val = float_val, .double_val = double_val, .integer_arr_val = { 10, 22, 34, 14, 56 }, };

	// Store JSON string into this variable.
	char *serialized_string = NULL;
	char *serialized_string_pretty = NULL;

	// Creating JSON root obj
	JSON_Value *root_value = json_value_init_object();
	JSON_Object *root_object = json_value_get_object(root_value);

	if (json_object_set_string(root_object, GET_VAR_NAME(your_thing_name, NULL),
	YOUR_THING_NAME) != JSONSuccess) {
		// YOUR_THING_NAME failed add into the root object
		ret = CORE_FAIL;
	}

	if (json_object_set_string(root_object, GET_VAR_NAME(example_struct_data.board_id, "."),
			example_struct_data.board_id) != JSONSuccess) {
		// Board-id failed add into the root object
		ret = CORE_FAIL;
	}

	if (json_object_set_boolean(root_object, GET_VAR_NAME(example_struct_data.is_bool_val, "."),
			example_struct_data.is_bool_val) != JSONSuccess) {
		// Is-Bool-Value failed add into the root object
		ret = CORE_FAIL;
	}

	if (json_object_set_number(root_object, GET_VAR_NAME(example_struct_data.integer_val, "."),
			example_struct_data.integer_val) != JSONSuccess) {
		// Integer_value failed add into the root object
		ret = CORE_FAIL;
	}

	if (json_object_set_number(root_object, GET_VAR_NAME(example_struct_data.float_val, "."),
			example_struct_data.float_val) != JSONSuccess) {
		// Float_value failed add into the root object
		ret = CORE_FAIL;
	}

	if (json_object_set_number(root_object, GET_VAR_NAME(example_struct_data.double_val, "."),
			example_struct_data.double_val) != JSONSuccess) {
		// Double_value failed add into the root object
		ret = CORE_FAIL;
	}

	// Creating JSON root obj
	JSON_Value *arr_value = json_value_init_array();
	JSON_Array *arr_object = json_value_get_array(arr_value);

	for (int i = 0; i < 5; i++) {
		if (json_array_append_number(arr_object, example_struct_data.integer_arr_val[i]) != JSONSuccess) {
			// Failed add value into the array object
			ret = CORE_FAIL;
		}
	}

	if (json_object_set_value(root_object, GET_VAR_NAME(example_struct_data.integer_arr_val, "."), arr_value)
			!= JSONSuccess) {
		// arr_value failed add into the root object
		ret = CORE_FAIL;
	}

	// Get The JSON in serialized format
	serialized_string = json_serialize_to_string(root_value);
	size_t serialized_string_len = json_serialization_size(root_value);

	// Get The JSON in serialized pretty format
	serialized_string_pretty = json_serialize_to_string_pretty(root_value);
	size_t serialized_string_pretty_len = json_serialization_size_pretty(root_value);

	// Consol print of Serialized_string
	if (serialized_string != NULL) {
		CORE_LOGI(TAG, "Serialized JSON string's length: %d \r\n Serialized String: %s \r\n", serialized_string_len,
				serialized_string);
		json_free_serialized_string(serialized_string);
	}

	// Consol print of Serialized_string Pretty
	if (serialized_string_pretty != NULL) {
		CORE_LOGI(TAG, "Serialized Pretty JSON string's length: %d \r\n Pretty String: %s \r\n\r\n",
				serialized_string_pretty_len, serialized_string_pretty);
		json_free_serialized_string(serialized_string_pretty);
	}

	/* cleanup JSON memory allocation*/
	json_value_free(root_value);
	ret = CORE_OK;

	integer_val += 10;
	float_val += 10.25;
	double_val += 100.5;
	is_bool_val ^= true;

	return ret;
}

/*******************************************************************************************************************/

void json_array_create_init() {
	CORE_LOGI(TAG, "In %s", __func__);
	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);
}

void json_array_create_loop() {
	//CORE_LOGI(TAG, "In %s", __func__);

	// called create_json() to create the JSON
	create_json();

	TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);
}
