/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   json_create.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The creation of a JSON using structure.
 *
 *
 *
 */

#ifndef JSON_CREATE_H_
#define JSON_CREATE_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

/**
 * @brief	Init function for json_create module
 *
 */
void json_create_init();

/**
 * @brief	Execution function for json_create module
 *
 */
void json_create_loop();

#endif	/* JSON_CREATE_H_ */
