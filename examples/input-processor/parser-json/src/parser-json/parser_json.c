/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   parser_json.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The parsing of a JSON using.
 *
 *
 *
 */

#include "core_common.h"
#include "core_constant.h"
#include "system.h"

#include "core_utils.h"
#include "parser_json.h"
#include "parson.h"

#include <string.h>

#define TAG	"parser_json"

char* json_str =
		"{\"your_thing_name\": \"PARSER_JSON_THING_NAME\",\"board_id\": \"board-001\",\"is_bool_val\": false,\"integer_val\": 151,\"float_val\": 154.75,\"double_val\": 1508.5 }";

/*
 *	Declaration of a Example structure
 */
typedef struct {
	char board_id[50];
	bool is_bool_val;
	int integer_val;
	float float_val;
	double double_val;
} EXAMPLE_STRUCT;

/********************************************** Module's Static Functions **********************************************************************/

static core_err parse_json(char *json_str, EXAMPLE_STRUCT *example_struct_data) {
	core_err ret = CORE_FAIL;

	// Creating JSON root obj
	JSON_Value *root_value = NULL;
	JSON_Object *root_object = NULL;

	if (json_str != NULL) {
		// parse the JSON string
		root_value = json_parse_string(json_str);

		// check is It a Valid JSON
		if (json_value_get_type(root_value) != JSONObject) {
			CORE_LOGE(TAG, "JSON Value type not matched\r\n");
			return ret;
		}

		// Get JSON root-object from the parse JSON value
		root_object = json_value_get_object(root_value);

		JSON_Value *keys_values;

		// Get JSON key's data type by JSON object's KEY name "your_thing_name"
		keys_values = json_object_get_value(root_object, GET_VAR_NAME(your_thing_name, NULL));
		// validate the Key's value type
		if (json_value_get_type(keys_values) == JSONString) {
			if (json_object_get_string(root_object, GET_VAR_NAME(your_thing_name, NULL)) != NULL) {
				CORE_LOGI(TAG, "your_thing_name: %s",
						json_object_get_string(root_object, GET_VAR_NAME(your_thing_name, NULL)));
			}
		}

		// Get JSON key's data type by JSON object's KEY name "board_id"
		keys_values = json_object_get_value(root_object, GET_VAR_NAME(example_struct_data->board_id, "->"));
		// validate the Key's value type
		if (json_value_get_type(keys_values) == JSONString) {
			if (json_object_get_string(root_object, GET_VAR_NAME(example_struct_data->board_id, "->")) != NULL) {
				strcpy(example_struct_data->board_id,
						json_object_get_string(root_object, GET_VAR_NAME(example_struct_data->board_id, "->")));
			}
		}

		// Get JSON key's data type by JSON object's KEY name "integer_val"
		keys_values = json_object_get_value(root_object, GET_VAR_NAME(example_struct_data->integer_val, "->"));
		// validate the Key's value type
		if (json_value_get_type(keys_values) == JSONNumber) {
			example_struct_data->integer_val = json_object_get_number(root_object,
					GET_VAR_NAME(example_struct_data->integer_val, "->"));

		}

		// Get JSON key's data type by JSON object's KEY name "float_val"
		keys_values = json_object_get_value(root_object, GET_VAR_NAME(example_struct_data->float_val, "->"));
		// validate the Key's value type
		if (json_value_get_type(keys_values) == JSONNumber) {
			example_struct_data->float_val = json_object_get_number(root_object,
					GET_VAR_NAME(example_struct_data->float_val, "->"));

		}

		// Get JSON key's data type by JSON object's KEY name "double_val"
		keys_values = json_object_get_value(root_object, GET_VAR_NAME(example_struct_data->double_val, "->"));
		if (json_value_get_type(keys_values) == JSONNumber) {
			example_struct_data->double_val = json_object_get_number(root_object,
					GET_VAR_NAME(example_struct_data->double_val, "->"));

		}

		ret = CORE_OK;
	}

	if (root_value != NULL) {
		/* cleanup memory  */
		json_value_free(root_value);
	}

	return ret;
}

/*******************************************************************************************************************/

void parser_json_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

}

void parser_json_loop() {
	CORE_LOGI(TAG, "In %s", __func__);

	EXAMPLE_STRUCT example_struct_data = { 0 };

	// Print the Parsed Json String
	CORE_LOGI(TAG, "Parsed Json Data : %s", json_str);

	// called create_json() to create the JSON
	parse_json(json_str, &example_struct_data);

	// Print the parsed json result
	CORE_LOGI(TAG, "example_struct_data.board_id = %s", example_struct_data.board_id);
	CORE_LOGI(TAG, "example_struct_data.integer_val = %d", example_struct_data.integer_val);
	CORE_LOGI(TAG, "example_struct_data.float_val = %f", example_struct_data.float_val);
	CORE_LOGI(TAG, "example_struct_data.double_val = %lf \r\n\r\n", example_struct_data.double_val);
}
