/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   parser_json.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The parsing of a JSON using.
 *
 *
 *
 */

#ifndef PARSER_JSON_H_
#define PARSER_JSON_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

/**
 * @brief	Init function for parser_json module
 *
 */
void parser_json_init();

/**
 * @brief	Execution function for parser_json module
 *
 */
void parser_json_loop();

#endif	/* PARSER_JSON_H_ */
