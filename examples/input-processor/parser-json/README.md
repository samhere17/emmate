# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

![ESP32 DevKit-C](../fritzing/logging.png)

### For Other Hardware

Comming soon ...

## About this example
This example demonstrates how to use the `input-processor` APIs to parse a JSON string. The EmMate Framework internally uses `parson` json library.

In this example you will learn:

- How to create `JSON_Value` & `JSON_Object` from a json formatted string
- How to validate whether the input string is a `JSONObject` or not
- How to parse and get string, number and boolean values from the json
- How to populate a data structure and print it
