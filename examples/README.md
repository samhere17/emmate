# Getting Started with Examples

To setup, build, flash and run all the examples, please follow the below steps:

## Prerequisites

Please read the [Getting Started](https://mig.iquesters.com/?s=embedded&p=documentation) guide first for setting up your environment.

To run the examples you will need to download a release package of EmMate. Please visit [Downloads](https://mig.iquesters.com/?s=embedded&p=downloads) section to download the latest release package. If you already have downloaded one, then you may proceed. 

## Steps to Setup an Example

For this guide we will use the `hello-world` example located in `examples/getting-started/`. All the other examples can be setup, built and run in the same process. Please refer each example's README.md file to know more about it.

Also note that on our end we have downloaded and extracted the EmMate release package at. So we will be using this path in this guide.

```
/home/iquester/emmate-framework/esp32
```

1. To setup an example project simply copy a example from the EmMate release package and paste it at your preferred location.

```
$ cd /home/iquester/my-emmate-projects
$ cp -r /home/iquester/emmate-framework/esp32/examples/getting-started/hello-world .
```

2. Now create a directory called `setup` inside `hello-world`

```
$ cd hello-world
$ mkdir setup
```

3. Now copy the files `setup.sh` and `setup.conf`

```
$ cd setup
$ cp /home/iquester/emmate/esp32/setup/setup.sh .
$ cp /home/iquester/emmate/esp32/setup/setup.conf .
```

4. Open `setup.conf` file and specify the directory location of the extracted EmMate release package against the property `RELEASE_PKG_DIR`

For example, If the distribution package is extracted at `/home/iquester/emmate-framework/esp32/`, then the `RELEASE_PKG_DIR` will be `/home/iquester/emmate-framework/esp32/`

Change

```
RELEASE_PKG_DIR=<directory location of the extracted emmate package>
```

to

```
RELEASE_PKG_DIR=/home/iquester/emmate-framework/esp32/
```

5. Now run the `setup.sh` (shell script). This will create the required folder structure and copy other dependency files in your project.

```
$ ./setup.sh
```

6. Next verify that the setup was successful or not

```
$ cd ..
$ ls -l
```

The following content should appear. If it does then **you have successfully setup an EmMate Project**, else please redo the above steps.

```
total 32
drwxrwxr-x 4 iquester iquester 4096 Sep 13 15:56 build
-rw-rw-r-- 1 iquester iquester 4729 Sep 13 15:54 emmate_config
drwxrwxr-x 2 iquester iquester 4096 Sep 13 15:54 fritzing
drwxrwxr-x 4 iquester iquester 4096 Sep 13 15:56 platform
-rw-rw-r-- 1 iquester iquester  754 Sep 13 15:54 README.md
drwxrwxr-x 2 iquester iquester 4096 Sep 13 15:55 setup
drwxrwxr-x 5 iquester iquester 4096 Sep 13 15:56 src
```

Now the example `hello-world` is successfully setup. You may proceed to the next step to build the example.


## Steps to Build an Example

1. Change to the `build` directory

```
$ cd build
```

2. To build the example run the script `build.sh` like shown below:

```
$ ./build.sh -make
```

When you build an EmMate example/application for the first time, the **EmMate Development Framework Configuration** application will automatically open. With the help of this application you can configure the modules of EmMate Framework as per the needs of your application.

For all examples the required configurations are already provided in the file `emmate-config`. So **we will simply click Save and close the application**.

If any additional configurations are needed, you will need to do them and save the configurations and build your project again. To open the **EmMate Development Framework Configuration** tool again, please use the following command:

```
$ ./build.sh -menuconfig
```

Once the **EmMate Development Framework Configuration** tool is closed the `hello-world` example will start compiling. If all went ok then you will find the following message:

```
## Execute EmMate Make Successful ################################
```

Now you have successfully built your example/application using the EmMate Framework. Next you need to flash the binary file into a hardware.

## Steps to Flash and View Log Messages

1. First open the README.md file inside the example directory and follow the section **Prepare the Hardware**. In this case `hello-world`

2. Once your hardware is ready, run the following command:

```
$ ./build.sh -flash -log
```

It will take some time to flash the binary on to the hardware. Once successful you will be able to see the following log messages. And that's all, you have successfully completed the required steps. 

```
  ______           __  __       _         ______                                           _    
 |  ____|         |  \/  |     | |       |  ____|                                         | |   
 | |__   _ __ ___ | \  / | __ _| |_ ___  | |__ _ __ __ _ _ __ ___   _____      _____  _ __| | __
 |  __| | '_ ` _ \| |\/| |/ _` | __/ _ \ |  __| '__/ _` | '_ ` _ \ / _ \ \ /\ / / _ \| '__| |/ /
 | |____| | | | | | |  | | (_| | ||  __/ | |  | | | (_| | | | | | |  __/\ V  V / (_) | |  |   < 
 |______|_| |_| |_|_|  |_|\__,_|\__\___| |_|  |_|  \__,_|_| |_| |_|\___| \_/\_/ \___/|_|  |_|\_\
     _____                      _               _____       _       _   _                          
    |_   _|                    | |             / ____|     | |     | | (_)                         
      | |  __ _ _   _  ___  ___| |_ ___ _ __  | (___   ___ | |_   _| |_ _  ___  _ __  ___          
      | | / _` | | | |/ _ \/ __| __/ _ \ '__|  \___ \ / _ \| | | | | __| |/ _ \| '_ \/ __|         
     _| || (_| | |_| |  __/\__ \ ||  __/ |     ____) | (_) | | |_| | |_| | (_) | | | \__ \         
    |_____\__, |\__,_|\___||___/\__\___|_|    |_____/ \___/|_|\__,_|\__|_|\___/|_| |_|___/         
             | |                                                                                   
             |_| 

W (197) sysinfo: =============================================================================
W (207) sysinfo: System Information
W (207) sysinfo: =============================================================================
W (217) sysinfo: -----------------------------
W (217) sysinfo: Hardware Information
W (227) sysinfo: -----------------------------
W (227) sysinfo: Chip Name: ESP32
W (237) sysinfo: Chip Manufacturer: ESSPRESIF
W (237) sysinfo: Features:
W (247) sysinfo: 	WIFI : YES
W (247) sysinfo: 	BT : YES
W (247) sysinfo: 	BLE : YES
W (257) sysinfo: 	GSM 2G : NO
W (257) sysinfo: 	GSM 3G : NO
W (257) sysinfo: 	GSM 4G : NO
W (267) sysinfo: 	LORA : NO
W (267) sysinfo: 	NBIOT : NO
W (267) sysinfo: FLASH SIZE : 4194304 bytes
W (277) sysinfo: RAM SIZE : 532480 bytes
W (277) sysinfo: -----------------------------
W (287) sysinfo: SDK Information
W (287) sysinfo: -----------------------------
W (297) sysinfo: Running SDK : ESP-IDF
W (297) sysinfo: SDk Version : v4.0-dev-621-g81ca1c0-dirty
W (307) sysinfo: -----------------------------
W (307) sysinfo: Framework Information
W (317) sysinfo: -----------------------------
W (317) sysinfo: Core Version: 0.1.0.0.823
W (327) sysinfo: Application Version: NO APP VERSION FOUND
W (327) sysinfo: =============================================================================

```

3. If you want to erase the flash memory of the hardware, use the following command:

```
$ ./build.sh -erase-flash
```

4. To stop the log messages press following keys:

```
$ Ctrl-]
```