# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

![ESP32 DevKit-C](../../../fritzing/logging.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates JSON creation & HTTP Post functionality. It uses EmMate's `conn/protocols/http` and `input-processor/parson` APIs.

This example does the following things:

- Creates a dummy JSON string using `input-processor/parson` APIs
- Hits the URL `http://httpbin.org/post` with the created dummy JSON
- Gets the response
- Prints the response