/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   https_get.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The HTTP Post method
 *
 *
 *
 */

#include "https_post.h"
#include "http_client_core.h"
#include "http_constant.h"
#include <string.h>

#define TAG "https_post"
static core_err app_http_operation(char* url, int port, char *cert_pem, HTTP_CLIENT_METHOD method, char* host_server,
		char* user_agent, char* content_type, char* post_data, size_t post_data_len, char* response_data,
		size_t *response_len, size_t max_response_len, uint16_t *http_stat) {

	core_err ret = CORE_FAIL;

	/* Initialize a http client data to be used for doing a http request */
	HttpClientData http_cli_data;
	bzero(&http_cli_data, sizeof(HttpClientData));

	http_cli_data.http_cnf_data.url = url;
	http_cli_data.http_cnf_data.port = port;
	http_cli_data.http_cnf_data.cert_pem = cert_pem;
	http_cli_data.http_cnf_data.method = method;
	http_cli_data.http_cnf_data.host_server = host_server;
	http_cli_data.http_cnf_data.user_agent = user_agent;
	http_cli_data.http_cnf_data.content_type = content_type;
	http_cli_data.http_cnf_data.post_data = post_data;
	http_cli_data.http_cnf_data.post_data_len = post_data_len;
	http_cli_data.http_ctrl.http_recv_handler = NULL;  //http_response_handler;
	http_cli_data.http_ctrl.response_data = response_data;
	if (response_len != NULL)
		*response_len = 0;
	http_cli_data.http_ctrl.response_len = response_len;
	http_cli_data.http_ctrl.max_response_len = max_response_len;

	http_cli_data.http_ctrl.client_api_mode = _API_MODE_SINGLE_STEP;

	CORE_LOGD(TAG, "Going to init http client with the following params:");
	CORE_LOGD(TAG, "url: %s", http_cli_data.http_cnf_data.url);
	CORE_LOGD(TAG, "cert_pem: %s", http_cli_data.http_cnf_data.cert_pem ? "cert is present" : "no cert");
	CORE_LOGD(TAG, "host_server: %s", http_cli_data.http_cnf_data.host_server);
	CORE_LOGD(TAG, "port: %d", http_cli_data.http_cnf_data.port);
	CORE_LOGD(TAG, "method: %d", http_cli_data.http_cnf_data.method);
	CORE_LOGD(TAG, "user_agent: %s", http_cli_data.http_cnf_data.user_agent);
	CORE_LOGD(TAG, "content_type: %s", http_cli_data.http_cnf_data.content_type);
	CORE_LOGD(TAG, "max_response_len: %d", http_cli_data.http_ctrl.max_response_len);

	/* Initialize a http client instance */
	ret = init_http_client(&http_cli_data);
	if (ret != CORE_OK) {
		if (ret == CORE_FAIL)
			CORE_LOGE(TAG, "init_http_client failed");
		else if (ret == CORE_ERR_INVALID_STATE)
			CORE_LOGE(TAG, "Network connection is unavailable!");
		ret = CORE_FAIL;
		goto deinit;
	}

	/* Perform the http process */
	CORE_LOGD(TAG, "Going to perform http client action");
	ret = perform_http_client_process(&http_cli_data);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "perform_http_client_process failed");
		ret = CORE_FAIL;
		goto deinit;
	}
	CORE_LOGD(TAG, "perform_http_client_process is successful");

	/* Wait for the http event to finish */
	while (get_http_client_event_stat(&http_cli_data) != HTTP_CLIENT_EVENT_ON_FINISH) {
		CORE_LOGD(TAG, "waiting for http action to be finished");
		TaskDelay(DELAY_250_MSEC / TICK_RATE_TO_MS);
	}

	/* Check the http status code */
	*http_stat = get_http_status_code(&http_cli_data);
	CORE_LOGD(TAG, "http status: %d", *http_stat);
	if (*http_stat == HTTP_OK_RES_CODE) {
		ret = CORE_OK;
	} else {
		ret = CORE_FAIL;
	}

	deinit:
	/* Our http tasks are done, now deinitialize the http client instance */
	CORE_LOGD(TAG, "Going to deinitialize the http client");
	if (deinit_http_client(&http_cli_data) != CORE_OK) {
		CORE_LOGE(TAG, "deinit_http_client failed");
	}

	return ret;
}

void https_post_init() {

}

void https_post_loop() {
	char *json_buf = EXAMPLE_POST_DATA;
	int json_len = strlen(json_buf);
	char http_response[EXAMPLE_RESPONSE_LEN];
	size_t http_response_len = 0;
	size_t http_response_max_len = EXAMPLE_RESPONSE_LEN;
	uint16_t http_stat = 0;

	CORE_LOGI(TAG, "Posting Data: %s", json_buf);

	/* Do http operation */
	core_err ret = app_http_operation(EXAMPLE_URL, EXAMPLE_PORT, EXAMPLE_ROOTCA, HTTP_CLIENT_METHOD_POST,
	EXAMPLE_HOST, EXAMPLE_HTTP_USER_AGENT,
	EXAMPLE_CONTENT_TYPE, json_buf, json_len, http_response, &http_response_len, http_response_max_len, &http_stat);

	if (ret == CORE_OK) {
		CORE_LOGI(TAG, "Response: %.*s", http_response_len, http_response);
	} else {
		CORE_LOGE(TAG, "HTTP POST Failed!");
	}
}
