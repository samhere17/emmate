# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

![ESP32 DevKit-C](../../../fritzing/logging.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates HTTPS Post functionality. It uses EmMate's `conn/protocols/http` APIs.

This example does the following things:

- Hits the URL `https://dev.iquesters.com/emb_api/hitandget.php` with a dummy data
- Gets the response. In this example the HTTPS API will respond with the same data that is posted.
- Prints the response