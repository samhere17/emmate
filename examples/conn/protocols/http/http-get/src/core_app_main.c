#include "http_get.h"

#define TAG	"core_app_main"

void core_app_main(void * param) {
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG, "==================================================================");

	// initialize My module
	http_get_init();

	while (1) {
		while (get_network_conn_status() != NETCONNSTAT_CONNECTED) {
			TaskDelay(DELAY_2_SEC / TICK_RATE_TO_MS);
		}
		// Execute My module
		http_get_loop();

		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
