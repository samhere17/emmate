# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

![ESP32 DevKit-C](../../../fritzing/logging.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates HTTP Get functionality. It uses EmMate's `conn/protocols/http` APIs.

This example does the following things:

- Hits the URL `http://httpbin.org/ip`
- Gets the public IP address
- Prints the IP address