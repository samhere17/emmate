
#ifndef THING_H
#define THING_H

// LED information
#define LEDS_NUMBER		12  /*!< Number of leds used */

#define BSP_LED_1			21
#define BSP_LED_2			19
#define BSP_LED_3			18
#define BSP_LED_4			22
#define BSP_LED_5			23
#define BSP_LED_6			32
#define BSP_LED_7			4
#define BSP_LED_8			16
#define BSP_LED_9			17
#define BSP_LED_10			25
#define BSP_LED_11			26
#define BSP_LED_12			27

#define LEDS_ACTIVE_STATE 1
#define LEDS_LIST { BSP_LED_1, BSP_LED_2, BSP_LED_3, BSP_LED_4, BSP_LED_5, BSP_LED_6, BSP_LED_7, BSP_LED_8, BSP_LED_9, BSP_LED_10, BSP_LED_11, BSP_LED_12 }

/**********************************************************************************************/

/*
 * The following BUTTON is used by the EmMate Framework as a Factory Reset Button for deleting all saved configurations.
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will and can be used in any other purpose.
 * Before changing the SOM pin numbers, please note that 'HAVE_SYSTEM_HMI' has to be disabled in core_config.h
 * */
#define SYSTEM_RESET_BUTTON		SOM_PIN_51

/**/
#define BUTTONS_ACTIVE_STATE	1
/**/
#define BUTTONS_LIST		{ SYSTEM_RESET_BUTTON }

#endif /* THING_H */
