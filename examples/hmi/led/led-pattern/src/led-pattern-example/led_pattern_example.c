/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   led_pattern.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The a LED Pattern of police van in different pattern using led-helper lib
 *
 *
 *
 */

#include "led_pattern_example.h"

#define TAG  "led_pattern_example"

/********************************************** Module's Static Functions **********************************************************************/

static void start_led_pattern() {
	core_err ret;
	int num_leds = 4;

	/* Creating and starting an LED notification consists of 4 steps. These steps must be followed
	 * in sequential manner, else behaviour of the library is undefied.
	 *
	 * Note: If you have multiple LEDs, then please follow Step 2 & 3, in the order as shown below.
	 *
	 * */

	/* Step 1: init_led_notification(num_led, period); */
	ret = init_led_notification(num_leds, NOTIFY_TIME_FOREVER);
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "%s init_led_notification failed!", __func__);
		return;
	}

	/* Step 2: set_led_type_idx(led no., led_type, m_r_idx, g_idx, b_idx) */
	/* Step 3: map_patterns_to_led(led no., num_patterns, pattern1, pattern2, ...) */
	set_led_type_idx((num_leds - 3), MONO_LED, CUSTOM_LED_1, 0, 0);
	map_patterns_to_led((num_leds - 3), 1, LED_PATTERN1);

	set_led_type_idx((num_leds - 2), MONO_LED, CUSTOM_LED_2, 0, 0);
	map_patterns_to_led((num_leds - 2), 2, LED_PATTERN2, LED_PATTERN3);

	set_led_type_idx((num_leds - 1), MONO_LED, CUSTOM_LED_3, 0, 0);
	map_patterns_to_led((num_leds - 1), 3, LED_PATTERN4, LED_PATTERN5, LED_PATTERN4);

	set_led_type_idx((num_leds), MONO_LED, CUSTOM_LED_4, 0, 0);
	map_patterns_to_led((num_leds), 2, LED_PATTERN6, LED_PATTERN7);

	/* Step 4: Start the led notification */
	start_led_notification();
}

/***********************************************************************************************/
void led_pattern_init() {
	CORE_LOGI(TAG, "In %s", __func__);
	CORE_LOGI(TAG, "My thing name is: %s\r\n", YOUR_THING_NAME);

	/* Initialize the basic LEDs activity. (LED pin details are in thing/thing.h) */
	core_err ret = init_leds();
	if (ret != CORE_OK) {
		CORE_LOGE(TAG, "Failed to initilize the leds");
		return;
	}
}

void led_pattern_loop() {
	CORE_LOGI(TAG, "Executing LED pattern");

	start_led_pattern();
}
