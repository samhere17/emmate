#include "adc_pot.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");
	
	// initialize My module
	adc_pot_init();

	while(1){

		// Execute My module
		adc_pot_loop();

		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
	}
}
