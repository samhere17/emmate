/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   adc_pot.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The use of ADC to read voltage from potentiometer
 *
 *
 *
 */

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"
#include "adc_core_channel.h"
#include "adc_core.h"


/**
 * @brief	Init function for adc_pot module
 *
 */
void adc_pot_init();



/**
 * @brief	Execution function for adc_pot module
 *
 */
void adc_pot_loop();




