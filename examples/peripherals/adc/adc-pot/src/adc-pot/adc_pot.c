/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   adc_pot.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The use of ADC to read voltage from potentiometer
 *
 *
 *
 */

#include "adc_pot.h"

#define TAG "adc_pot"

static uint32_t prev_mV_data = 0;

/********************************************** Module's Static Functions **********************************************************************/



/*******************************************************************************************************************/
void adc_pot_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s\r\n", YOUR_THING_NAME);

	/* 
	 * Initialize the ADC with the configured data and start processing.
	 * ADC Channel: ADC_POT_DETECT_AN_GPIO
	 * ADC read interval in seconds: 5secs = 500millisec
	 * ADC read data width in bit: ADC_CORE_WIDTH_BIT_10
	 * ADC voltage attenuation level: ADC_CORE_ATTEN_DB_11 (ADC read upto max Volt 3.3v)
	 */
	core_err res = init_adc_peripheral(ADC_POT_DETECT_AN_GPIO, DELAY_500_MSEC, ADC_CORE_WIDTH_BIT_10, ADC_CORE_ATTEN_DB_11);
	if(res != CORE_OK){
		CORE_LOGI(TAG, "Failed to init 'ADC_POT_DETECT_AN_GPIO' ADC peripheral");
	}

}
void adc_pot_loop() {
	//CORE_LOGI(TAG, "*** Start of %s ***\r\n", __func__);

	// Declared integer variable to capture ADC raw data
	int raw_data = 0;

	// Declared uint32 variable to capture ADC mV data (mVolt convertion from raw data)     
	uint32_t mV_data = 0;

	// Collect the ADC raw data & mV data
	get_adc_peripheral_data(ADC_POT_DETECT_AN_GPIO, &raw_data, &mV_data); 

	// check whether output mV changes as of previous read mV value
	if (mV_data != prev_mV_data) {

		// if last read read mV different from the previous read mV, the this line will print. otherwise continue
		CORE_LOGI(TAG,"ADC Readings: Raw Data : %d, Voltage :%.2fV", raw_data, (float)mV_data/1000); 

		// Re-initialize the lase read mV to previous read mV variable
		prev_mV_data = mV_data; 
	}

	//CORE_LOGI(TAG, "*** End of %s ***\r\n", __func__);
}

