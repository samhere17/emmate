# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

![ESP32 DevKit-C](fritzing/esp32_adc_lm35.png)

### For Other Hardware

Comming soon ...

## About this example

