/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   lm35.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate 
 * The ambient temperature measurement using LM35
 *
 *
 *
 */

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"
#include "adc_core_channel.h"
#include "adc_core.h"

/**
 * @brief	Init function for lm35 module
 *
 */
void lm35_init();


/**
 * @brief	Execution function for lm35 module
 *
 */
void lm35_loop();
