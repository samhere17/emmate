/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   lm35.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate 
 * The ambient temperature measurement using LM35
 *
 *
 */


#include "lm35.h"

#define TAG "lm35"

static uint32_t prev_mV_data = 0;

/********************************************** Module's Static Functions **********************************************************************/



/*******************************************************************************************************************/

void lm35_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);
	
	/* 
	 * Initialize the ADC with the configured data and start processing.
	 * ADC Channel: ADC_LM35_DETECT_AN_GPIO
	 * ADC read interval in seconds: 2secs = 2000millisecs
	 * ADC read data width in bit: ADC_CORE_WIDTH_BIT_10
	 * ADC voltage attenuation level: ADC_CORE_ATTEN_DB_11 (ADC read upto max Volt 3.3v)
	 */
	core_err res = init_adc_peripheral(ADC_LM35_DETECT_AN_GPIO, DELAY_2_SEC, ADC_CORE_WIDTH_BIT_10, ADC_CORE_ATTEN_DB_11);
	if(res != CORE_OK){
		CORE_LOGE(TAG,"Failed to init 'ADC_LM35_DETECT_AN_GPIO' ADC peripheral");
	}
}



void lm35_loop() {
	CORE_LOGD(TAG, "*** Start of %s ***\r\n", __func__);

	// Declared integer variable to capture ADC raw data
	int raw_data = 0;

	// Declared uint32 variable to capture ADC mV data (mVolt convertion from raw data)     
	uint32_t mV_data = 0;

	// Collect the ADC raw data & mV data
	get_adc_peripheral_data(ADC_LM35_DETECT_AN_GPIO, &raw_data, &mV_data); 

	float temperature = 0;

	if (mV_data != prev_mV_data) {     // check whether output volatge changes as of previous value
		temperature = mV_data / 10;   // convert analog output into temperature
		/*
		 * if last read read mV different from the previous read mV, the this line will print. otherwise continue
		 */
		CORE_LOGI(TAG,"Ambient temperature :%f C\n", temperature); 

		prev_mV_data = mV_data;  // if ouput volatage change then again store new value in prev_mV_data
	}

	CORE_LOGD(TAG, "*** End of %s ***\r\n", __func__);
}
