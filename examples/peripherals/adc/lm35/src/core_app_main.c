#include "lm35.h"

#define TAG	"core_app_main"

void core_app_main(void * param) {
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG, "==================================================================");
	
	// initialize My module
	lm35_init();

	while (1) {

		// Execute My module
		lm35_loop();

		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
