
#include "eeprom_module.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	eeprom_module_init();

	while(1){
	eeprom_module_loop();
		TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);
	}
}
