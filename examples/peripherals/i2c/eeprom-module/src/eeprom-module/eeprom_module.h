/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   eeprom_module.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * EEPROM
 *
 *
 *
 */
#ifndef EEPROM_MODULE_H_
#define EEPROM_MODULE_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"
/**
 * @brief initialize I2C master peripheral
 *    Set SDA pin , SCL pin and clock speed in init_i2c_core_master()
 *    Set Slave address, Command address, data ( which write in slave ) and size of data in i2c_core_master_write_slave()
 * @return
 *
 **/
void eeprom_module_init();
/**
 * @brief Read data from slave using i2c_core_master_read_slave()
 * print the current time
 * @return
 *
 **/
void eeprom_module_loop();

#endif	/* EEPROM_MODULE_H_ */
