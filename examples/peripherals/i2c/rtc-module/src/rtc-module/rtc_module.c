/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   rtc_module.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * Real Time Clock
 *
 *
 *
 */
#include "rtc_module.h"
#include "i2c_core.h"
#define TAG	"rtc_module"

#define CLK_SPEED      100000       // clock speed 100kHz 
#define SLAVE_ADDRESS     0xD0      // Slave address of RTC-DS1307 0b11010000
#define COMMAND_ADDRESS    0x00     // command address

I2C_CORE_DRV_HANDLE i2c_drv_handle=NULL;

void rtc_module_init() {
	CORE_LOGI(TAG, "In your_module_init");

	CORE_LOGI(TAG, "Accessing your thing from thing.h in your-thing directory ...");
	CORE_LOGI(TAG, "Your thing name is: %s\r\n", YOUR_THING_NAME);

	core_err ret;

	// set time is 05:12:16 day saturday and date 15/06/19
	uint8_t data_wr[7] = { 0x16, 0x12, 0x05, 0x06, 0x15, 0x06, 0x19 };

	int i=0;
	uint8_t size =7;

	printf("Set Time is :\n");
	for (i = 2; i >=0; i--) {
		printf("%x", data_wr[i]);
		printf(":");
	}
	 switch(data_wr[3])
	    {
	        case 01:
	            printf("Monday");
	            break;
	        case 02:
	            printf("Tuesday");
	            break;
	        case 03:
	            printf("Wednesday");
	            break;
	        case 04:
	            printf("Thursday");
	            break;
	        case 05:
	            printf("Friday");
	            break;
	        case 06:
	            printf("Saturday");
	            break;
	        case 07:
	       	     printf("Sunday");
	       	     break;
	        default:
	            printf("Invalid weekday number.");
	    }
	 printf(" ");
		for (i = 4; i <6; i++) {
			printf("%x", data_wr[i]);
			printf("/");
		}
		printf("%x", data_wr[6]);
	printf("\n");
	i2c_drv_handle = init_i2c_core_master(I2C_CORE_NUM_1, SDA_PIN, SCL_PIN, CLK_SPEED, 1);
	if(i2c_drv_handle!=NULL){
		CORE_LOGI(TAG, "I2C driver initialize complete, for RTC-DS1307 module");
		ret = i2c_core_master_write_slave(i2c_drv_handle, SLAVE_ADDRESS, COMMAND_ADDRESS, data_wr, size);
		if (ret != CORE_OK) {
			CORE_LOGE(TAG, "Failed to set the time and date into RTC module");
		}else{
			CORE_LOGI(TAG, "Successfully set the time and date into RTC module");
		}
	}else{
		CORE_LOGE(TAG, "Failed to initialize the I2C driver");
	}
	CORE_LOGI(TAG, "Returning from your_module_init\r\n");
}

void rtc_module_loop() {
	CORE_LOGI(TAG, "In your_module_loop");
	core_err ret;
	uint8_t data_rd[7];
	uint8_t size=7;
     	int i = 0;
	if(i2c_drv_handle!=NULL)
	{
		ret = i2c_core_master_read_slave(i2c_drv_handle, SLAVE_ADDRESS, COMMAND_ADDRESS, data_rd, size, 0);
		if(ret == CORE_OK)
		{
			printf("Current Time is :\n");
			for (i = 2; i >=0; i--) {
				printf("%x", data_rd[i]);
				printf(":");
			}
			 switch(data_rd[3])
			 {
			        case 1:
			            printf("Monday");
			            break;
			        case 2:
			            printf("Tuesday");
			            break;
			        case 3:
			            printf("Wednesday");
			            break;
			        case 4:
			            printf("Thursday");
			            break;
			        case 5:
			            printf("Friday");
			            break;
			        case 6:
			            printf("Saturday");
			            break;
			        case 7:
			       	     printf("Sunday");
			       	     break;
			        default:
			            printf("Invalid weekday number.");
		    	}
			printf(" ");
			for (i = 4; i <6; i++) {
				printf("%x", data_rd[i]);
				printf("/");
			}
			printf("%x", data_rd[6]);
			printf("\n");
			printf("\n");
		}else{
			CORE_LOGE(TAG, "Failed to read data from RTC module\r\n");
		}
	}else{
		// Write Error Mesasges LOGs
	}
}
