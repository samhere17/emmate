/**
 * This is an example c file of a module
 * As an application developer your code should be written as modules similar to this.
 *
 */

#include "gpio_intr.h"
#include "gpio_helper_api.h"

#define TAG	"gpio_intr"

static bool sw_pressed = false;
static uint8_t toggle_bit = LOW;

/************************************ Modules IRS handler Function ***********************************/
void INTERRUPT_ATTRIBUTES isr_func(void* arg) {
	uint32_t gpio_num = (uint32_t) arg;
	// ISR handler body
	if(gpio_num == SWITCH_2_GPIO){
		// Switch Press detected
		sw_pressed = true;
	}

}

void gpio_intr_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG,
			"Accessing your thing from thing.h in your-thing directory ...");
	CORE_LOGI(TAG, "Your thing name is: %s", YOUR_THING_NAME);

	/* initializations Switch 2 GPIO */
	core_err res = configure_gpio(SWITCH_2_GPIO, GPIO_IO_MODE_INPUT,
			GPIO_IO_FLOATING);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "Failed to initialize Switch-2");
	} else {
		// Add ISR handler function
		add_gpio_isr(SWITCH_2_GPIO, GPIO_INTERRUPT_POSEDGE, isr_func);
	}

	/* initializations LED 3 GPIO */
	res = configure_gpio(LED_3_GPIO, GPIO_IO_MODE_OUTPUT, GPIO_IO_FLOATING);
	if (res != CORE_OK) {
		CORE_LOGE(TAG, "Failed to initialize LED-3");
	}

	CORE_LOGI(TAG, "Returning from %s", __func__);
}

void gpio_intr_loop() {
	CORE_LOGD(TAG, "In %s", __func__);

	if(sw_pressed == true){
		toggle_bit ^= HIGH;
		// Toggle the LED 3
		set_gpio_value(LED_3_GPIO, toggle_bit);
		sw_pressed = false;
	}
}
