
#include "gpio_intr.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	CORE_LOGI(TAG, "Calling gpio_intr_init() in your_module.c in your-module directory ...");
	gpio_intr_init();
	CORE_LOGI(TAG, "Returned from gpio_intr_init()");

	while(1){
		gpio_intr_loop();
		TaskDelay(DELAY_250_MSEC/ TICK_RATE_TO_MS);
	}
}
