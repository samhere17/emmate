/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   system_time.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * System Time
 *
 *
 *
 */

#include "system_time.h"
#include <string.h>

#define TAG "system_time"

void system_time_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

	// Hard-coded Date/Time in milliseconds
	uint64_t milisec_now = CURRENT_TIME_MILLIS;

	// Create structure timeval variable
	struct timeval now_tv;

	// set UTC time in seconds
	now_tv.tv_sec = milisec_now / 1000;

	// set UTC time's miliseconds
	now_tv.tv_usec = (milisec_now % 1000) * 1000;

	/* Set the system time */
	if (core_settimeofday(&now_tv, NULL) != 0) {
		CORE_LOGE(TAG, "Failed to set System time...");
	} else {
		CORE_LOGI(TAG, "Successfully set the System time...");
	}
}

void system_time_loop() {
	struct tm now;    // create a struct for reading system time

	get_systime(&now);   // read system time using this function

	char strftime_buf[64];
	// make time string from current-time structure data, in human readable format.
	strftime(strftime_buf, sizeof(strftime_buf), "%c", &now);

	CORE_LOGI(TAG, "Current System Time: %s\n", strftime_buf);
}

