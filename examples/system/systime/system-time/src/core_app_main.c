#include "system_time.h"

#define TAG	"core_app_main"

void core_app_main(void * param) {
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG, "==================================================================");

	system_time_init();

	while (1) {
		system_time_loop();
		TaskDelay(DELAY_1_SEC / TICK_RATE_TO_MS);
	}
}
