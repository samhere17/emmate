# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

![ESP32 DevKit-C](../fritzing/logging.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates, how to convert a millisecond value into a `struct tm` type variable using the systime module APIs.

See the function `millisecond_to_time_loop()` for better understanding.

To convert a desired millisecond value please open the file `millisecond_to_time/millisecond_to_time.h` and change the macro `CURRENT_TIME_MILLIS`