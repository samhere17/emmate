/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   millisecond_to_time.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * MilliSecond  to Time conversion
 *
 *
 *
 */

#include "millisecond_to_time.h"
#include <string.h>

#define TAG "millisecond_to_time"

void millisecond_to_time_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);
#if 0
	// Hard-coded Date/Time in milliseconds
	uint64_t milisec_now = CURRENT_TIME_MILLIS;

	// Create structure timeval variable
	struct timeval now_tv;

	// set UTC time in seconds
	now_tv.tv_sec = milisec_now / 1000;

	// set UTC time's miliseconds
	now_tv.tv_usec = (milisec_now % 1000) * 1000;

	/* Set the system time */
	if (core_settimeofday(&now_tv, NULL) != 0) {
		CORE_LOGE(TAG, "Failed to set System time...");
	} else {
		CORE_LOGI(TAG, "Successfully set the System time...");
	}
#endif
}

void millisecond_to_time_loop() {
	CORE_LOGI(TAG, "In %s", __func__);
	uint64_t millisecond_to_convert = CURRENT_TIME_MILLIS;
	struct tm converted_time;

	// get the Current system time
	convert_millis_to_tm(millisecond_to_convert, &converted_time);

	char strftime_buf[64];
	// make time string from current-time structure data, in human readable format.
	strftime(strftime_buf, sizeof(strftime_buf), "%c", &converted_time);

	// print the Converted Date/Time
	CORE_LOGI(TAG, "Milliseconds to convert: %lld", millisecond_to_convert);
	CORE_LOGI(TAG, "Converted Date-Time: %s", strftime_buf);
	CORE_LOGI(TAG,"");
}

