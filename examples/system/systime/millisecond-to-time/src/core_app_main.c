#include "millisecond_to_time.h"

#define TAG	"core_app_main"

void core_app_main(void * param) {
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG, "Starting application built on the Iquester EmMate Framework");
	CORE_LOGI(TAG, "");
	CORE_LOGI(TAG, "==================================================================");

	millisecond_to_time_init();

	while (1) {
		millisecond_to_time_loop();
		TaskDelay(DELAY_10_SEC / TICK_RATE_TO_MS);
	}
}
