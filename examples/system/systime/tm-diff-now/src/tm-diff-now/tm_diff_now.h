/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   tm_diff_now.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The time difference between current time and previous time.
 *
 *
 *
 */

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"

#define CURRENT_TIME_MILLIS		1560259909111	// UTC Time/Date : Tue Jun 11 2019 13:31:49

/**
 * @brief	Init function for tm_diff_now module
 *
 */
void tm_diff_now_init();

/**
 * @brief	Execution function for tm_diff_now module
 *
 */
void tm_diff_now_loop();
