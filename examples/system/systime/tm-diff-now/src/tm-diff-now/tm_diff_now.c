/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   tm_diff_now.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The time difference between current time and previous time.
 *
 *
 *
 */

#include "tm_diff_now.h"
#include "core_common.h"
#include "core_constant.h"
#include "system.h"
#include "systime.h"
#include <string.h>

#define TAG "tm_diff_now"

/********************************************** Module's Static Functions **********************************************************************/
static void get_curr_tm(struct tm *diff_time) {
	struct tm curr_tm;

	// Get the current time.
	if (get_systime(&curr_tm) != 0) {
		memset(diff_time, 0x00, sizeof(struct tm));
		CORE_LOGE(TAG, "Failed to fetch System's Current Time ...");
	} else {
		char strftime_buf[64];

		// make time string from current-time structure data, in human readable format.
		strftime(strftime_buf, sizeof(strftime_buf), "%c", &curr_tm);

		// print the Current Date/Time
		CORE_LOGI(TAG, "The current UTC date/time is: %s", strftime_buf);

		memcpy(diff_time, &curr_tm, sizeof(struct tm));
	}
}

/*******************************************************************************************************************/

void tm_diff_now_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

	// Hard-coded Date/Time in milliseconds
	uint64_t milisec_now = CURRENT_TIME_MILLIS;

	// Create structure timeval variable
	struct timeval now_tv;

	// set UTC time in seconds
	now_tv.tv_sec = milisec_now / 1000;

	// set UTC time's miliseconds
	now_tv.tv_usec = (milisec_now % 1000) * 1000;

	/* Set the system time */
	if (core_settimeofday(&now_tv, NULL) != 0) {
		CORE_LOGE(TAG, "Failed to set System time...");
	} else {
		CORE_LOGI(TAG, "Successfully set the System time...");
	}
}

void tm_diff_now_loop() {
	CORE_LOGI(TAG, "In %s", __func__);

	struct tm diff_time;

	// get the Current system time
	get_curr_tm(&diff_time);

	// wait
	CORE_LOGI(TAG, "Waiting for %d seconds to create a time difference", (DELAY_30_SEC)/1000);
	TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);

	// Find out the difference between last taken time and current system time.
	/*
	 * if diff_time > system-time then the difference value will be positive (means not yet time over/passed)
	 * if diff_time < system-time then the difference value will be negative (means time over/passed)
	 */
	double diff_sec = diff_time_with_now(&diff_time);

	CORE_LOGI(TAG, "Diff: %lf\r\n", diff_sec);
}
