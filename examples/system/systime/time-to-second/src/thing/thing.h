/*
 * This file will only contain comments for creating a custom thing.h
 */

#ifndef THING_H_
#define THING_H_

#include "som.h"

#define YOUR_THING_NAME	"Time To Second Example"

#define LEDS_NUMBER		3  /*!< Number of leds used */

/*
 * The following LEDs are used by the EmMate Framework for showing notifications during startup.
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will and can be used in any other purpose.
 * Before changing these SOM pin numbers, please note that 'HAVE_SYSTEM_HMI' has to be disabled in core_config.h
 * */
#define SYSTEM_HMI_LED_MONO_RED		SOM_PIN_85   /*!< Used by the Core Embedded Framework as RED LED */
#define SYSTEM_HMI_LED_GREEN		SOM_PIN_98   /*!< Used by the Core Embedded Framework as GREEN LED */
#define SYSTEM_HMI_LED_BLUE			SOM_PIN_97   /*!< Used by the Core Embedded Framework as BLUE LED */

/**/
#define LEDS_ACTIVE_STATE 	1
/**/
#define LEDS_LIST { SYSTEM_HMI_LED_MONO_RED, SYSTEM_HMI_LED_GREEN, SYSTEM_HMI_LED_BLUE }

#define BUTTONS_NUMBER	1

/*
 * The following BUTTON is used by the EmMate Framework as a Factory Reset Button for deleting all saved configurations.
 * The SOM pin numbers 'SOM_PIN_n' might be changed at will and can be used in any other purpose.
 * Before changing the SOM pin numbers, please note that 'HAVE_SYSTEM_HMI' has to be disabled in core_config.h
 * */
#define SYSTEM_RESET_BUTTON		SOM_PIN_51

/**/
#define BUTTONS_ACTIVE_STATE	1
/**/
#define BUTTONS_LIST		{ SYSTEM_RESET_BUTTON }

#endif /* THING_H_ */
