/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   time_conversion_to_sec.h
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * Time conversion to second
 *
 *
 *
 */
#ifndef TIME_CONVERSION_TO_SEC_H_
#define TIME_CONVERSION_TO_SEC_H_

#include "core_config.h"
#include "core_constant.h"
#include "system.h"
#include "core_logger.h"
#include "thing.h"
#include "systime.h"

#define CURRENT_TIME_MILLIS		1560259909111	// UTC Time/Date : Tue Jun 11 2019 13:31:49

/**
 * @brief set the time value and time zone
 *
 *
 */
void time_to_sec_init();
/**
 * @brief Pass the created structure convert_tm_to_seconds()
 *     read integer value from convert_tm_to_seconds()
 *     print unix second
 *
 */
void time_to_sec_loop();

#endif	/* TIME_CONVERSION_TO_SEC_H_ */
