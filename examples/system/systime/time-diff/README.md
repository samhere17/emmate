# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory
	
## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

### For ESP32 DevKit-C V4

![ESP32 DevKit-C](../fritzing/logging.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to calculate the difference between 2 `struct tm` time variable using the systime module APIs.

See the functions `time_diff_loop()` for better understanding.

To set a desired current time as the system time open the file `time-diff/time_diff.h` and change the macro `CURRENT_TIME_MILLIS`
