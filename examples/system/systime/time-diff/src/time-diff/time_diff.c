/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   time_diff.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The time difference between end-time and start-time
 *
 *
 *
 */

#include "time_diff.h"
#include "core_common.h"
#include "core_constant.h"
#include "system.h"
#include "systime.h"

#include <string.h>

#define TAG "time_diff"

/********************************************** Module's Static Functions **********************************************************************/

static void get_curr_tm(struct tm *now) {
	struct tm curr_tm;

	// Get the current time.
	if (get_systime(&curr_tm) != 0) {
		memset(now, 0x00, sizeof(struct tm));
		CORE_LOGE(TAG, "Failed to fetch System's Current Time ...");
	} else {
		char strftime_buf[64];

		// make time string from current-time structure data, in human readable format.
		strftime(strftime_buf, sizeof(strftime_buf), "%c", &curr_tm);

		// print the Current Date/Time
//		CORE_LOGI(TAG, "The current UTC date/time is: %s", strftime_buf);

		memcpy(now, &curr_tm, sizeof(struct tm));
	}
}

/*******************************************************************************************************************/

void time_diff_init() {
	CORE_LOGI(TAG, "In %s", __func__);

	CORE_LOGI(TAG, "My thing name is: %s", YOUR_THING_NAME);

	// Hard-coded Date/Time in milliseconds
	uint64_t milisec_now = CURRENT_TIME_MILLIS;

	// Create structure timeval variable
	struct timeval now_tv;

	// set UTC time in seconds
	now_tv.tv_sec = milisec_now / 1000;

	// set UTC time's miliseconds
	now_tv.tv_usec = (milisec_now % 1000) * 1000;

	/* Set the system time */
	if (core_settimeofday(&now_tv, NULL) != 0) {
		CORE_LOGE(TAG, "Failed to set System time...");
	} else {
		CORE_LOGI(TAG, "Successfully set the System time...");
	}
}

void time_diff_loop() {
	CORE_LOGI(TAG, "In %s", __func__);

	// Create structure tm variable for store start and end time.
	struct tm start_tm;
	struct tm end_tm;

	// get the Current system time into start-time structure
	get_curr_tm(&start_tm);

	// wait
	CORE_LOGI(TAG, "Waiting for %d seconds to create a time difference", (DELAY_30_SEC)/1000);
	TaskDelay(DELAY_30_SEC / TICK_RATE_TO_MS);

	// get the Current system time into end-time structure
	get_curr_tm(&end_tm);

	// Find out the difference between end-time and start-time
	/*
	 * if end_tm > start_tm then the difference value will be positive
	 * if end_tm < start_tm then the difference value will be negative
	 */
	double diff_sec = diff_between_time(&end_tm, &start_tm);

	CORE_LOGI(TAG, "Diff: %lf\r\n", diff_sec);
}
