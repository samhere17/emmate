/****************************************************************************************************
 ******************************	Iquester Solutions LLP Copyright Notice *****************************
 ****************************************************************************************************
 * File:   logger.c
 * Author: Iquester Solutions LLP
 * Website: https://www.iquesters.com/
 * Description: File contains the Iquester EmMate Framework example to demonstrate
 * The Logging options and Log levels
 *
 *
 *
 */

#include "logger.h"
#include <string.h>

#define ALL_MODULE_TAG	"*"

#define TAG "logger"
#define TAG_LOGGER_1 "logger_1"
#define TAG_LOGGER_2 "logger_2"

#define DELAY_TIME		DELAY_10_SEC


/********************************************** Module's Static Functions **********************************************************************/

void log_lines() {

	printf("######################################## LOG Lines Start ########################################\r\n");

	CORE_LOGE(TAG_LOGGER_1, "Example LOG:Core Logging Error");
	CORE_LOGW(TAG_LOGGER_1, "Example LOG:Core Logging Warning");
	CORE_LOGI(TAG_LOGGER_1, "Example LOG:Core Logging Info");
	CORE_LOGD(TAG_LOGGER_1, "Example LOG:Core Logging Debug");
	CORE_LOGV(TAG_LOGGER_1, "Example LOG:Core Logging Verbose");

	CORE_LOGI(TAG,"");

	CORE_LOGE(TAG_LOGGER_2, "Example LOG:Core Logging Error");
	CORE_LOGW(TAG_LOGGER_2, "Example LOG:Core Logging Warning");
	CORE_LOGI(TAG_LOGGER_2, "Example LOG:Core Logging Info");
	CORE_LOGD(TAG_LOGGER_2, "Example LOG:Core Logging Debug");
	CORE_LOGV(TAG_LOGGER_2, "Example LOG:Core Logging Verbose");

	printf("######################################## LOG Lines End ########################################\r\n\r\n");
}

/*******************************************************************************************************************/

void logger_init() {

	/*
	 * The logging module is by default initialized from within the system. Its default peripheral is Console.
	 * (Note: By default LOG level set for INFO for all modules.)
	 *
	 * The EmMate Framework supports the following logging peripherals
	 * 1. CORE_LOG_OUTPUT_CONSOLE
	 * 2. CORE_LOG_OUTPUT_FILE		(under development, to be completed in the next release)
	 * 3. CORE_LOG_OUTPUT_NETWORK 	(under development, to be completed in the next release)
	 *
	 */

	CORE_LOGI(TAG, "In %s", __func__);
	CORE_LOGI(TAG, "My thing name is: %s\r\n", YOUR_THING_NAME);
}

void logger_loop() {
	CORE_LOGI(TAG, "*** Start of %s ***\r\n", __func__);

	// print the application log lines
	log_lines();


	/*********************************************************************************/
	TaskDelay(DELAY_TIME / TICK_RATE_TO_MS);
	//CORE_LOGI(TAG, "/*********************************************************************************/");
	CORE_LOGI(TAG, "After waiting for 10 sec, we set LOG level NONE for all modules");
	/*
	 * set the all module's Log level to NONE. (Note: means no logs will show on configured logging peripherals)
	 */
	set_core_logger_level(ALL_MODULE_TAG, CORE_LOG_NONE);

	// print the application log lines
	log_lines();


	// set the 'TAG' level to Info display logs for Logger modules
	set_core_logger_level(TAG, CORE_LOG_INFO);






	/*********************************************************************************/
	TaskDelay(DELAY_TIME / TICK_RATE_TO_MS);
	//CORE_LOGI(TAG, "\r\n/*********************************************************************************/");
	CORE_LOGI(TAG, "After waiting for 10 sec, we set LOG level Debug for Tag: logger_1 ");
	/*
	 * set the logger_1 module Log level to Debug. (Note: means no logs will show on configured logging peripherals)
	 */
	set_core_logger_level(TAG_LOGGER_1, CORE_LOG_DEBUG);

	// print the application log lines
	log_lines();






	/*********************************************************************************/
	TaskDelay(DELAY_TIME / TICK_RATE_TO_MS);
	//CORE_LOGI(TAG, "\r\n /*********************************************************************************/");
	CORE_LOGI(TAG, "After waiting for 10 sec, we set Info for Tag: logger_2 ");
	/*
	 * set the logger_2 module's Log level to Warning.
	 */
	set_core_logger_level(TAG_LOGGER_2, CORE_LOG_INFO);

	// print the application log lines
	log_lines();






	/*********************************************************************************/
	TaskDelay(DELAY_TIME / TICK_RATE_TO_MS);
	//CORE_LOGI(TAG, "\r\n/*********************************************************************************/");
	CORE_LOGI(TAG, "After waiting for 10 sec, we set Error for Tag: logger_1 ");
	/*
	 * set the logger_1 module's Log level to Error.
	 */
	set_core_logger_level(TAG_LOGGER_1, CORE_LOG_ERROR);

	// print the application log lines
	log_lines();






	/*********************************************************************************/
	TaskDelay(DELAY_TIME / TICK_RATE_TO_MS);
	//CORE_LOGI(TAG, "\r\n/*********************************************************************************/");
	CORE_LOGI(TAG, "After waiting for 10 sec, we set LOG level Info for Tag: logger_1 & logger_2");
	/*
	 * set the logger_1 & logger_2 module's Log level to Info.
	 */
	set_core_logger_level(TAG_LOGGER_1, CORE_LOG_INFO);
	set_core_logger_level(TAG_LOGGER_2, CORE_LOG_INFO);

	// print the application log lines
	log_lines();

	CORE_LOGI(TAG, "*** End of %s ***\r\n", __func__);

	TaskDelay(DELAY_20_SEC / TICK_RATE_TO_MS);
}
