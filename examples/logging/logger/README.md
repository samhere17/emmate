# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so. 

The required components and tools are - 

1. Four LEDs (Any color).
2. Four Resistors (220 Ohms). (For color code refer the connection diagram.)
3. Single strand wires.
4. Breadboard.
5. Power Source for power up the module/dev-board

Connect the Hardware as per the below diagram :

** For ESP32 DevKit-C V4 **

![image](../fritzing/logging.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to use the `logging` module APIs to change the log levels at run time

This example shows:

- The different log levels and how they displays in the console
- How to change log levels of each module at runtime
