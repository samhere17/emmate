# Setup the Example

To setup, build and flash an example built on EmMate Framework please see the **README.md** file located at `emmate/examples` directory

## Prepare the Hardware

First you must prepare the hardware in order run this example. Please follow the below images to do so.

The required components and tools are - 

1. LM35
2. Single strand wires.
3. Breadboard.
4. ESP32 Dev Kit C
5. USB cord to flash and power the ESP32 module.

** For ESP32 DevKit-C V4 **

![image](fritzing/esp32_lm35_sensor.png)

### For Other Hardware

Comming soon ...

## About this example

This example demonstrates how to use the LM32 module APIs to read temperature data from a LM35 sensor.

Please see the functions `lm35_sensor_init()` and `lm35_sensor_loop()` for more info.
