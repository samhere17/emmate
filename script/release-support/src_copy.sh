#!/bin/bash

echo
echo "#####################################################################"

# setting the extglob
shopt -s extglob

echo

source $CONFIG_PATH

# checking requested platforms
echo Checking requested platforms
echo PLATFORM = $PLATFORM

platform_count=0

if [ ! -z "$PLATFORM" ]
then
	platform_count=${#PLATFORM[@]}
	echo Number of requested platforms = $platform_count
else
	echo No platforms were requested
fi

if [ $platform_count -le 0 ]
then
	echo Setting all platforms
	cd $SRC_DIR/platform
	echo */
	PLATFORM=(*/)
	echo PLATFORM = ${PLATFORM[@]}
	platform_count=${#PLATFORM[@]}
	echo Generating release for $platform_count platforms
fi

if [ $platform_count -gt 0 ]
then
	for ((i=0; i<$platform_count; i++))
	do
		echo
#		echo .....................................................................
		echo "#####################################################################"

		THIS_PLATFORM=${PLATFORM[$i],,}
		echo Generating release package for $THIS_PLATFORM...
		
		#checking the availability of the mentioned platform
		#goto platform source directory ie, src/platform/this_platform
		PLATFORM_SRC_DIR=$SRC_DIR/platform/$THIS_PLATFORM
		{
			cd $PLATFORM_SRC_DIR
		} || {
			echo platform src directory NOT FOUND for $THIS_PLATFORM
			echo $PLATFORM_SRC_DIR NOT FOUND
			continue
		}

		#platform src directory found
		echo $PLATFORM_SRC_DIR FOUND
		
		#copying the core src files
		DIST_PLATFORM_DIR=$ACTUAL_DIST_DIR/$THIS_PLATFORM
		DIST_SRC_DIR=$DIST_PLATFORM_DIR/src

		#creating the distribution src directory
		mkdir -p $DIST_SRC_DIR

		#goto source directory
		cd $SRC_DIR

		echo Copying src files from $SRC_DIR to $DIST_SRC_DIR
		if [ $BUILD_MODE = "dev" ]
		then
			rsync -av -u -r . $DIST_SRC_DIR --exclude platform --exclude core_config.h
			#cp -v -u -r !(platform) $DIST_SRC_DIR
		else
			rsync -av -r . $DIST_SRC_DIR --exclude platform --exclude core_config.h
			#cp -v -r !(platform) $DIST_SRC_DIR
		fi

		echo
		echo .....................................................................

		#copying the platform specific src files
		DIST_PLATFORM_SRC_DIR=$DIST_PLATFORM_DIR/platform

		#creating the distribution platform directory
		mkdir -p $DIST_PLATFORM_SRC_DIR

		cd $PLATFORM_SRC_DIR

		echo Copying platform src files from $PLATFORM_SRC_DIR to $DIST_PLATFORM_SRC_DIR
		if [ $BUILD_MODE = "dev" ]
		then
			cp -v -u -r . $DIST_PLATFORM_SRC_DIR
		else
			cp -v -r . $DIST_PLATFORM_SRC_DIR
		fi

		echo .....................................................................
		echo

		#copying the setup related files
		DIST_SETUP_FILES_DIR=$DIST_PLATFORM_DIR/setup

		#creating the distribution setup directory
		mkdir -p $DIST_SETUP_FILES_DIR

		#goto setup directory
		cd $PROJ_DIR/res/setup

		echo Copying src files from $PROJ_DIR/setup to $DIST_SETUP_FILES_DIR
		if [ $BUILD_MODE = "dev" ]
		then
			cp -v -u -r . $DIST_SETUP_FILES_DIR
		else
			cp -v -r . $DIST_SETUP_FILES_DIR
		fi

		echo
		echo .....................................................................
				
		echo
		echo Generating platform.conf file in $DIST_SETUP_FILES_DIR
		PLATFORM_CONF_FILE=$DIST_SETUP_FILES_DIR/files/build/build-support/platform.conf
		echo "# This is a Auto-Generated file. DO NOT TOUCH" > $PLATFORM_CONF_FILE
		echo "PLATFORM="$THIS_PLATFORM >> $PLATFORM_CONF_FILE

		echo .....................................................................
		echo
		
		echo .....................................................................
		echo
		
		#copying the platform-resources related files
		DIST_PLATFORM_RESOURCES_FILES_DIR=$DIST_PLATFORM_DIR/platform-resources
		
		#creating the distribution platform-resources directory
		mkdir -p $DIST_PLATFORM_RESOURCES_FILES_DIR
		
		#goto platform-resources/platform directory
		cd $PROJ_DIR/res/platform-resources/$THIS_PLATFORM
		
		echo Copying src files from $PROJ_DIR/res/platform-resources/$THIS_PLATFORM to $DIST_PLATFORM_RESOURCES_FILES_DIR
		if [ $BUILD_MODE = "dev" ]
		then
			cp -v -u -r . $DIST_PLATFORM_RESOURCES_FILES_DIR
		else
			cp -v -r . $DIST_PLATFORM_RESOURCES_FILES_DIR
		fi
		
		echo
		echo .....................................................................

		#copying the examples related files
		DIST_EXAMPLES_FILES_DIR=$DIST_PLATFORM_DIR/examples

		#creating the distribution examples directory
		mkdir -p $DIST_EXAMPLES_FILES_DIR

		#goto examples directory
		cd $PROJ_DIR/examples

		echo Copying src files from $PROJ_DIR/examples to $DIST_EXAMPLES_FILES_DIR
		if [ $BUILD_MODE = "dev" ]
		then
			cp -v -u -r . $DIST_EXAMPLES_FILES_DIR
		else
			cp -v -r . $DIST_EXAMPLES_FILES_DIR
		fi

		echo .....................................................................
		echo
	done
fi

echo "#####################################################################"
echo
