#!/bin/bash

#setting current directory
export CURR_DIR=`pwd`

#importing Configuration File
export CONFIG_PATH=$CURR_DIR/release.conf

source $CONFIG_PATH
export $(grep --regexp ^[A-Z] $CONFIG_PATH | cut -d= -f1)

export CURRENT_DATE=`date +%d-%b-%Y`
export CURRENT_TIME=`date +%H:%M:%S`

echo .....................................................................
echo Initiating release package build for $PROJ_NAME
echo .....................................................................
echo Build Mode             : $BUILD_MODE
echo Build Date             : $CURRENT_DATE
echo Build Time             : $CURRENT_TIME
echo Build By               : $USERNAME
echo .....................................................................


cd ..
export PROJ_DIR=`pwd`

echo Project Location : $PROJ_DIR
echo .....................................................................

#going back to current directory
cd $CURR_DIR

mkdir -p $DIST_DIR
cd $DIST_DIR

export DIST_DIR=`pwd`

echo Distribution Location : $DIST_DIR
echo .....................................................................
echo

#going back to current directory
cd $CURR_DIR

export FULL_DATE_TIME=`date +%Y-%m-%d_%H-%M-%S`
#echo fullstamp: "$FULL_DATE_TIME"

if [ $BUILD_MODE = "dev" ]
then
	export ACTUAL_DIST_DIR="$DIST_DIR/$BUILD_MODE"
elif [ $BUILD_MODE = "uat" ]
then
	export ACTUAL_DIST_DIR="$DIST_DIR/$BUILD_MODE/$FULL_DATE_TIME"
elif [ $BUILD_MODE = "prod" ]
then
	export ACTUAL_DIST_DIR="$DIST_DIR/$BUILD_MODE/$FULL_DATE_TIME"
else
	echo Set bulid mode properly
	return
fi

echo $1
if [ $BUILD_MODE = "dev" ]
then
	if [ ! -z "$1" ]
	then
		if [[ $1 = "-clean" ]]
		then
			echo .....................................................................
			echo "Deleting ACTUAL_DIST_DIR if present"
			rm -rf $ACTUAL_DIST_DIR
			echo .....................................................................
		fi
	fi
else
	echo .....................................................................
	echo "Deleting ACTUAL_DIST_DIR if present"
	rm -rf $ACTUAL_DIST_DIR
	echo .....................................................................
fi

#initiating logging
export LOG_DIR="$ACTUAL_DIST_DIR/log"
export LOG_FILE="$LOG_DIR/release.log"

mkdir -p $LOG_DIR


#####################################################################
make_script_executable()
{
#checking if the script is executable
if [ ! -x $1 ]
then
	echo "The script file is not executable...making the script executable"
	#making the script executable
	chmod a+x $1
fi
}
#####################################################################

echo Setting up the build...
SETUP_SCRIPT=./release-support/release_setup.sh
make_script_executable $SETUP_SCRIPT
$SETUP_SCRIPT >> $LOG_FILE

export SRC_DIR=$PROJ_DIR/src

echo Updating the version...
VERSION_SCRIPT=./release-support/version.sh
make_script_executable $VERSION_SCRIPT
$VERSION_SCRIPT >> $LOG_FILE

echo Preparing the release...
SRC_COPY_SCRIPT=./release-support/src_copy.sh
make_script_executable $SRC_COPY_SCRIPT
$SRC_COPY_SCRIPT >> $LOG_FILE

if [ $BUILD_MODE != "dev" ]
then
	echo Generating the documentation...
	GEN_DOC_SCRIPT=./release-support/gen-doc.sh
	make_script_executable $GEN_DOC_SCRIPT
	$GEN_DOC_SCRIPT >> $LOG_FILE
fi

echo "#####################################################################"
echo DONE
echo "#####################################################################"


